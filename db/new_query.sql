ALTER TABLE users ADD `user_create` int(10) unsigned NOT NULL DEFAULT 1;

/* Plugin pakage*/

CREATE TABLE IF NOT EXISTS `pakages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Tên gói',
  `max_viewer` int(11) DEFAULT '0' COMMENT 'số lượng viewer tối đa trên 1 đơn hàng',
  `num_hours` int(11) DEFAULT '0' COMMENT 'số giây của gói',
  `unit` int(11) DEFAULT '0' COMMENT '1 = gói tuần, 2 = gói tháng',
  `default_price` float DEFAULT '0' COMMENT 'số tiền mặc định của gói',
  `active` tinyint(2) DEFAULT '0' COMMENT '0: not active, 1 active',
  `created` datetime DEFAULT NULL COMMENT 'ngay tao',
  `modified` datetime DEFAULT NULL COMMENT 'ngay update',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `pakage_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'user id',
  `pakage_id` int(11) NOT NULL DEFAULT '0' COMMENT 'gói id',
  `price` float NOT NULL DEFAULT '0' COMMENT 'giá tiền của gói đối với từng KH',
  `active` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 : chưa active,1 active',
  `time_start` int(11) NOT NULL DEFAULT '0' COMMENT 'thời gian bắt đầu',
  `time_end` int(11) NOT NULL DEFAULT '0' COMMENT 'thời gian kết thúc',
  `time_exist` int(11) NOT NULL DEFAULT '0' COMMENT 'thời gian còn lại',
  `parrams` text,
  `is_payment` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: chưa thanh toán, 1 : thanh toán rồi',
  `created` datetime DEFAULT NULL COMMENT 'ngay tao',
  `modified` datetime DEFAULT NULL COMMENT 'ngay update',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) DEFAULT NULL COMMENT 'ten goi gia',
  `max_viewer` int(11) DEFAULT NULL COMMENT 'so luong toi thieu cua detail price',
  `max_time` int(11) DEFAULT NULL COMMENT 'so gio toi thieu cua detail price',
  `default_view` mediumtext COMMENT 'mac dinh gia cua view video',
  `default_live` mediumtext COMMENT 'mac dinh gia cua livestream',
  `default_like` mediumtext COMMENT 'mac dinh gia cua like',
  `default_share` mediumtext COMMENT 'mac dinh gia cua share',
  `default_comment` mediumtext COMMENT 'mac dinh gia cua comment',
  `active` tinyint(2) DEFAULT '0' COMMENT '0: deactive, 1 active',
  `created` datetime DEFAULT NULL COMMENT 'timestam add',
  `modified` datetime DEFAULT NULL COMMENT 'timestam update',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `price_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id detail price',
  `user_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `max_viewer` int(11) DEFAULT NULL,
  `max_time` int(11) DEFAULT NULL,
  `price_live` text,
  `price_view` text,
  `price_like` text,
  `price_share` text,
  `price_comment` text,
  `active` tinyint(4) DEFAULT '0' COMMENT '0: deactive, 1  : active',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='setting gia mac dinh cho tung user';