-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 14, 2017 at 12:28 AM
-- Server version: 5.1.73-community
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `moosocial`
--
CREATE DATABASE IF NOT EXISTS `moosocial` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `moosocial`;

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group` (`group`,`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `group`, `key`, `description`) VALUES
(1, 'activity', 'view', 'View feed from all members'),
(2, 'user', 'username', 'Select username'),
(17, 'attachment', 'upload', 'Upload attachment'),
(18, 'attachment', 'download', 'Download attachment');

-- --------------------------------------------------------

--
-- Table structure for table `admin_notifications`
--

CREATE TABLE IF NOT EXISTS `admin_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plugin_id` int(10) unsigned NOT NULL DEFAULT '0',
  `target_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `original_filename` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `downloads` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `plugin_id` (`plugin_id`,`target_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cake_sessions`
--

CREATE TABLE IF NOT EXISTS `cake_sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `expires` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cake_sessions`
--

INSERT INTO `cake_sessions` (`id`, `data`, `expires`, `user_id`) VALUES
('3248s9garcscj6a79gjrhrs6g7', 'Config|a:3:{s:9:"userAgent";s:32:"e299000d592d9c4462ef1602015d4bc1";s:4:"time";i:1505128415;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";', 1505128415, 'guest_3248s9garcscj6a79gjrhrs6g7'),
('51isvlg48kvmp598pivtul3fh7', 'Config|a:3:{s:9:"userAgent";s:32:"77ac761ca4a0b5c49c0d099582a1b5ea";s:4:"time";i:1504872481;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";Auth|a:1:{s:4:"User";a:50:{s:2:"id";s:1:"1";s:4:"name";s:5:"admin";s:5:"email";s:19:"admin@sonetrend.com";s:4:"salt";s:4:"7d54";s:7:"role_id";s:1:"1";s:6:"avatar";s:0:"";s:5:"photo";s:0:"";s:7:"created";s:19:"2017-08-21 20:20:05";s:10:"last_login";s:19:"2017-09-08 10:00:22";s:11:"photo_count";s:1:"0";s:12:"friend_count";s:1:"0";s:18:"notification_count";s:1:"0";s:20:"friend_request_count";s:1:"0";s:10:"blog_count";s:1:"0";s:11:"topic_count";s:1:"0";s:11:"group_count";s:1:"0";s:11:"event_count";s:1:"0";s:23:"conversation_user_count";s:1:"0";s:11:"video_count";s:1:"0";s:6:"gender";s:4:"Male";s:8:"birthday";s:10:"2017-08-21";s:6:"active";b:1;s:9:"confirmed";b:1;s:4:"code";s:32:"b0e8491e684bd1be18d9195d90c638bd";s:18:"notification_email";b:1;s:8:"timezone";s:12:"Asia/Jakarta";s:10:"ip_address";s:0:"";s:7:"privacy";s:1:"1";s:8:"username";s:0:"";s:5:"about";s:0:"";s:8:"featured";b:0;s:4:"lang";s:3:"eng";s:11:"hide_online";b:0;s:5:"cover";s:0:"";s:8:"approved";b:1;s:9:"is_social";b:0;s:23:"has_active_subscription";b:0;s:31:"receive_message_from_non_friend";b:1;s:28:"send_email_when_send_message";b:1;s:20:"request_friend_email";b:1;s:20:"notification_setting";N;s:15:"profile_type_id";s:1:"1";s:11:"user_create";s:1:"1";s:9:"moo_title";s:5:"admin";s:8:"moo_href";s:13:"/users/view/1";s:10:"moo_plugin";N;s:8:"moo_type";s:4:"User";s:7:"moo_url";s:13:"/users/view/1";s:9:"moo_thumb";s:6:"avatar";s:4:"Role";a:7:{s:2:"id";s:1:"1";s:4:"name";s:11:"Super Admin";s:6:"is_mod";b:1;s:8:"is_admin";b:1;s:8:"is_super";b:1;s:6:"params";s:65:"activity_view,user_username,attachment_upload,attachment_download";s:4:"core";b:1;}}}', 1504872481, '1'),
('6rnlev8i9anq1oeip8na0ra577', 'Config|a:3:{s:9:"userAgent";s:32:"6a7fef4750f8f70bc960bc25d7d26d57";s:4:"time";i:1505220935;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";', 1505220935, 'guest_6rnlev8i9anq1oeip8na0ra577'),
('7qqagqkf0v39cv0ik4mmc0srj1', 'Config|a:3:{s:9:"userAgent";s:32:"e299000d592d9c4462ef1602015d4bc1";s:4:"time";i:1505119745;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";', 1505119745, 'guest_7qqagqkf0v39cv0ik4mmc0srj1'),
('8c8p7javliktk162rtqav5bk65', 'Config|a:3:{s:9:"userAgent";s:32:"77ac761ca4a0b5c49c0d099582a1b5ea";s:4:"time";i:1504857600;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";Auth|a:1:{s:4:"User";a:50:{s:2:"id";s:1:"1";s:4:"name";s:5:"admin";s:5:"email";s:19:"admin@sonetrend.com";s:4:"salt";s:4:"7d54";s:7:"role_id";s:1:"1";s:6:"avatar";s:0:"";s:5:"photo";s:0:"";s:7:"created";s:19:"2017-08-21 20:20:05";s:10:"last_login";s:19:"2017-09-08 04:00:02";s:11:"photo_count";s:1:"0";s:12:"friend_count";s:1:"0";s:18:"notification_count";s:1:"0";s:20:"friend_request_count";s:1:"0";s:10:"blog_count";s:1:"0";s:11:"topic_count";s:1:"0";s:11:"group_count";s:1:"0";s:11:"event_count";s:1:"0";s:23:"conversation_user_count";s:1:"0";s:11:"video_count";s:1:"0";s:6:"gender";s:4:"Male";s:8:"birthday";s:10:"2017-08-21";s:6:"active";b:1;s:9:"confirmed";b:1;s:4:"code";s:32:"b0e8491e684bd1be18d9195d90c638bd";s:18:"notification_email";b:1;s:8:"timezone";s:12:"Asia/Jakarta";s:10:"ip_address";s:0:"";s:7:"privacy";s:1:"1";s:8:"username";s:0:"";s:5:"about";s:0:"";s:8:"featured";b:0;s:4:"lang";s:3:"eng";s:11:"hide_online";b:0;s:5:"cover";s:0:"";s:8:"approved";b:1;s:9:"is_social";b:0;s:23:"has_active_subscription";b:0;s:31:"receive_message_from_non_friend";b:1;s:28:"send_email_when_send_message";b:1;s:20:"request_friend_email";b:1;s:20:"notification_setting";N;s:15:"profile_type_id";s:1:"1";s:11:"user_create";s:1:"1";s:9:"moo_title";s:5:"admin";s:8:"moo_href";s:13:"/users/view/1";s:10:"moo_plugin";N;s:8:"moo_type";s:4:"User";s:7:"moo_url";s:13:"/users/view/1";s:9:"moo_thumb";s:6:"avatar";s:4:"Role";a:7:{s:2:"id";s:1:"1";s:4:"name";s:11:"Super Admin";s:6:"is_mod";b:1;s:8:"is_admin";b:1;s:8:"is_super";b:1;s:6:"params";s:65:"activity_view,user_username,attachment_upload,attachment_download";s:4:"core";b:1;}}}admin_login|i:1;Message|a:0:{}', 1504857600, '1'),
('g4q0kk7vj8f0973d7dpf0kbct1', 'Config|a:3:{s:9:"userAgent";s:32:"6a7fef4750f8f70bc960bc25d7d26d57";s:4:"time";i:1505119718;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";', 1505119718, 'guest_g4q0kk7vj8f0973d7dpf0kbct1'),
('i7k306mkfesuuuflip9s6bq8m3', 'Config|a:3:{s:9:"userAgent";s:32:"6a7fef4750f8f70bc960bc25d7d26d57";s:4:"time";i:1505242780;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";', 1505242780, 'guest_i7k306mkfesuuuflip9s6bq8m3'),
('iggio067o0211pmk63h5an0pb1', 'Config|a:3:{s:9:"userAgent";s:32:"77ac761ca4a0b5c49c0d099582a1b5ea";s:4:"time";i:1505123016;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";Auth|a:1:{s:4:"User";a:50:{s:2:"id";s:1:"1";s:4:"name";s:5:"admin";s:5:"email";s:19:"admin@sonetrend.com";s:4:"salt";s:4:"7d54";s:7:"role_id";s:1:"1";s:6:"avatar";s:0:"";s:5:"photo";s:0:"";s:7:"created";s:19:"2017-08-21 20:20:05";s:10:"last_login";s:19:"2017-09-11 07:39:47";s:11:"photo_count";s:1:"0";s:12:"friend_count";s:1:"0";s:18:"notification_count";s:1:"0";s:20:"friend_request_count";s:1:"0";s:10:"blog_count";s:1:"0";s:11:"topic_count";s:1:"0";s:11:"group_count";s:1:"0";s:11:"event_count";s:1:"0";s:23:"conversation_user_count";s:1:"0";s:11:"video_count";s:1:"0";s:6:"gender";s:4:"Male";s:8:"birthday";s:10:"2017-08-21";s:6:"active";b:1;s:9:"confirmed";b:1;s:4:"code";s:32:"b0e8491e684bd1be18d9195d90c638bd";s:18:"notification_email";b:1;s:8:"timezone";s:12:"Asia/Jakarta";s:10:"ip_address";s:0:"";s:7:"privacy";s:1:"1";s:8:"username";s:0:"";s:5:"about";s:0:"";s:8:"featured";b:0;s:4:"lang";s:3:"eng";s:11:"hide_online";b:0;s:5:"cover";s:0:"";s:8:"approved";b:1;s:9:"is_social";b:0;s:23:"has_active_subscription";b:0;s:31:"receive_message_from_non_friend";b:1;s:28:"send_email_when_send_message";b:1;s:20:"request_friend_email";b:1;s:20:"notification_setting";N;s:15:"profile_type_id";s:1:"1";s:11:"user_create";s:1:"1";s:9:"moo_title";s:5:"admin";s:8:"moo_href";s:13:"/users/view/1";s:10:"moo_plugin";N;s:8:"moo_type";s:4:"User";s:7:"moo_url";s:13:"/users/view/1";s:9:"moo_thumb";s:6:"avatar";s:4:"Role";a:7:{s:2:"id";s:1:"1";s:4:"name";s:11:"Super Admin";s:6:"is_mod";b:1;s:8:"is_admin";b:1;s:8:"is_super";b:1;s:6:"params";s:65:"activity_view,user_username,attachment_upload,attachment_download";s:4:"core";b:1;}}}admin_login|i:1;Message|a:0:{}', 1505123016, '1'),
('kkdhta8paiusfk15hecku63nv1', 'Config|a:3:{s:9:"userAgent";s:32:"6a7fef4750f8f70bc960bc25d7d26d57";s:4:"time";i:1505135045;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";Auth|a:1:{s:4:"User";a:51:{s:2:"id";s:1:"1";s:4:"name";s:5:"admin";s:5:"email";s:19:"admin@sonetrend.com";s:4:"salt";s:4:"7d54";s:7:"role_id";s:1:"1";s:6:"avatar";s:0:"";s:5:"photo";s:0:"";s:7:"created";s:19:"2017-08-21 20:20:05";s:10:"last_login";s:19:"2017-09-11 07:39:47";s:11:"photo_count";s:1:"0";s:12:"friend_count";s:1:"0";s:18:"notification_count";s:1:"0";s:20:"friend_request_count";s:1:"0";s:10:"blog_count";s:1:"0";s:11:"topic_count";s:1:"0";s:11:"group_count";s:1:"0";s:11:"event_count";s:1:"0";s:23:"conversation_user_count";s:1:"0";s:11:"video_count";s:1:"0";s:6:"gender";s:4:"Male";s:8:"birthday";s:10:"2017-08-21";s:6:"active";b:1;s:9:"confirmed";b:1;s:4:"code";s:32:"b0e8491e684bd1be18d9195d90c638bd";s:18:"notification_email";b:1;s:8:"timezone";s:12:"Asia/Jakarta";s:10:"ip_address";s:0:"";s:7:"privacy";s:1:"1";s:8:"username";s:0:"";s:5:"about";s:0:"";s:8:"featured";b:0;s:4:"lang";s:3:"eng";s:11:"hide_online";b:0;s:5:"cover";s:0:"";s:8:"approved";b:1;s:9:"is_social";b:0;s:23:"has_active_subscription";b:0;s:31:"receive_message_from_non_friend";b:1;s:28:"send_email_when_send_message";b:1;s:20:"request_friend_email";b:1;s:20:"notification_setting";N;s:15:"profile_type_id";s:1:"1";s:11:"user_create";s:1:"1";s:9:"moo_title";s:5:"admin";s:8:"moo_href";s:13:"/users/view/1";s:10:"moo_plugin";N;s:8:"moo_type";s:4:"User";s:7:"moo_url";s:13:"/users/view/1";s:9:"moo_thumb";s:6:"avatar";s:11:"ProfileType";a:5:{s:2:"id";s:1:"1";s:4:"name";s:7:"Default";s:7:"actived";b:1;s:7:"profile";b:0;s:5:"order";s:1:"0";}s:4:"Role";a:7:{s:2:"id";s:1:"1";s:4:"name";s:11:"Super Admin";s:6:"is_mod";b:1;s:8:"is_admin";b:1;s:8:"is_super";b:1;s:6:"params";s:65:"activity_view,user_username,attachment_upload,attachment_download";s:4:"core";b:1;}}}', 1505135045, '1'),
('lqsvlcj6id0a5vfo6mmi2f5qr7', 'Config|a:3:{s:9:"userAgent";s:32:"6a7fef4750f8f70bc960bc25d7d26d57";s:4:"time";i:1505128397;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";', 1505128397, 'guest_lqsvlcj6id0a5vfo6mmi2f5qr7'),
('m21a90rk4puu3vd70082ec2tc6', 'Config|a:3:{s:9:"userAgent";s:32:"77ac761ca4a0b5c49c0d099582a1b5ea";s:4:"time";i:1504864802;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";Auth|a:1:{s:4:"User";a:50:{s:2:"id";s:1:"1";s:4:"name";s:5:"admin";s:5:"email";s:19:"admin@sonetrend.com";s:4:"salt";s:4:"7d54";s:7:"role_id";s:1:"1";s:6:"avatar";s:0:"";s:5:"photo";s:0:"";s:7:"created";s:19:"2017-08-21 20:20:05";s:10:"last_login";s:19:"2017-09-08 06:00:10";s:11:"photo_count";s:1:"0";s:12:"friend_count";s:1:"0";s:18:"notification_count";s:1:"0";s:20:"friend_request_count";s:1:"0";s:10:"blog_count";s:1:"0";s:11:"topic_count";s:1:"0";s:11:"group_count";s:1:"0";s:11:"event_count";s:1:"0";s:23:"conversation_user_count";s:1:"0";s:11:"video_count";s:1:"0";s:6:"gender";s:4:"Male";s:8:"birthday";s:10:"2017-08-21";s:6:"active";b:1;s:9:"confirmed";b:1;s:4:"code";s:32:"b0e8491e684bd1be18d9195d90c638bd";s:18:"notification_email";b:1;s:8:"timezone";s:12:"Asia/Jakarta";s:10:"ip_address";s:0:"";s:7:"privacy";s:1:"1";s:8:"username";s:0:"";s:5:"about";s:0:"";s:8:"featured";b:0;s:4:"lang";s:3:"eng";s:11:"hide_online";b:0;s:5:"cover";s:0:"";s:8:"approved";b:1;s:9:"is_social";b:0;s:23:"has_active_subscription";b:0;s:31:"receive_message_from_non_friend";b:1;s:28:"send_email_when_send_message";b:1;s:20:"request_friend_email";b:1;s:20:"notification_setting";N;s:15:"profile_type_id";s:1:"1";s:11:"user_create";s:1:"1";s:9:"moo_title";s:5:"admin";s:8:"moo_href";s:13:"/users/view/1";s:10:"moo_plugin";N;s:8:"moo_type";s:4:"User";s:7:"moo_url";s:13:"/users/view/1";s:9:"moo_thumb";s:6:"avatar";s:4:"Role";a:7:{s:2:"id";s:1:"1";s:4:"name";s:11:"Super Admin";s:6:"is_mod";b:1;s:8:"is_admin";b:1;s:8:"is_super";b:1;s:6:"params";s:65:"activity_view,user_username,attachment_upload,attachment_download";s:4:"core";b:1;}}}', 1504864802, '1'),
('magg47bkmq41i6cl0au80u1ls3', 'Config|a:3:{s:9:"userAgent";s:32:"77ac761ca4a0b5c49c0d099582a1b5ea";s:4:"time";i:1505021524;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";Auth|a:1:{s:4:"User";a:50:{s:2:"id";s:1:"1";s:4:"name";s:5:"admin";s:5:"email";s:19:"admin@sonetrend.com";s:4:"salt";s:4:"7d54";s:7:"role_id";s:1:"1";s:6:"avatar";s:0:"";s:5:"photo";s:0:"";s:7:"created";s:19:"2017-08-21 20:20:05";s:10:"last_login";s:19:"2017-09-10 03:31:04";s:11:"photo_count";s:1:"0";s:12:"friend_count";s:1:"0";s:18:"notification_count";s:1:"0";s:20:"friend_request_count";s:1:"0";s:10:"blog_count";s:1:"0";s:11:"topic_count";s:1:"0";s:11:"group_count";s:1:"0";s:11:"event_count";s:1:"0";s:23:"conversation_user_count";s:1:"0";s:11:"video_count";s:1:"0";s:6:"gender";s:4:"Male";s:8:"birthday";s:10:"2017-08-21";s:6:"active";b:1;s:9:"confirmed";b:1;s:4:"code";s:32:"b0e8491e684bd1be18d9195d90c638bd";s:18:"notification_email";b:1;s:8:"timezone";s:12:"Asia/Jakarta";s:10:"ip_address";s:0:"";s:7:"privacy";s:1:"1";s:8:"username";s:0:"";s:5:"about";s:0:"";s:8:"featured";b:0;s:4:"lang";s:3:"eng";s:11:"hide_online";b:0;s:5:"cover";s:0:"";s:8:"approved";b:1;s:9:"is_social";b:0;s:23:"has_active_subscription";b:0;s:31:"receive_message_from_non_friend";b:1;s:28:"send_email_when_send_message";b:1;s:20:"request_friend_email";b:1;s:20:"notification_setting";N;s:15:"profile_type_id";s:1:"1";s:11:"user_create";s:1:"1";s:9:"moo_title";s:5:"admin";s:8:"moo_href";s:13:"/users/view/1";s:10:"moo_plugin";N;s:8:"moo_type";s:4:"User";s:7:"moo_url";s:13:"/users/view/1";s:9:"moo_thumb";s:6:"avatar";s:4:"Role";a:7:{s:2:"id";s:1:"1";s:4:"name";s:11:"Super Admin";s:6:"is_mod";b:1;s:8:"is_admin";b:1;s:8:"is_super";b:1;s:6:"params";s:65:"activity_view,user_username,attachment_upload,attachment_download";s:4:"core";b:1;}}}', 1505021524, '1'),
('mv1ap265s6p3anrn75qs9o5ah7', 'Config|a:3:{s:9:"userAgent";s:32:"e299000d592d9c4462ef1602015d4bc1";s:4:"time";i:1505030363;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";', 1505030363, 'guest_mv1ap265s6p3anrn75qs9o5ah7'),
('robqk63hi0fdafe3huaa47k642', 'Config|a:3:{s:9:"userAgent";s:32:"77ac761ca4a0b5c49c0d099582a1b5ea";s:4:"time";i:1504872012;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";Auth|a:1:{s:4:"User";a:50:{s:2:"id";s:1:"1";s:4:"name";s:5:"admin";s:5:"email";s:19:"admin@sonetrend.com";s:4:"salt";s:4:"7d54";s:7:"role_id";s:1:"1";s:6:"avatar";s:0:"";s:5:"photo";s:0:"";s:7:"created";s:19:"2017-08-21 20:20:05";s:10:"last_login";s:19:"2017-09-08 08:00:12";s:11:"photo_count";s:1:"0";s:12:"friend_count";s:1:"0";s:18:"notification_count";s:1:"0";s:20:"friend_request_count";s:1:"0";s:10:"blog_count";s:1:"0";s:11:"topic_count";s:1:"0";s:11:"group_count";s:1:"0";s:11:"event_count";s:1:"0";s:23:"conversation_user_count";s:1:"0";s:11:"video_count";s:1:"0";s:6:"gender";s:4:"Male";s:8:"birthday";s:10:"2017-08-21";s:6:"active";b:1;s:9:"confirmed";b:1;s:4:"code";s:32:"b0e8491e684bd1be18d9195d90c638bd";s:18:"notification_email";b:1;s:8:"timezone";s:12:"Asia/Jakarta";s:10:"ip_address";s:0:"";s:7:"privacy";s:1:"1";s:8:"username";s:0:"";s:5:"about";s:0:"";s:8:"featured";b:0;s:4:"lang";s:3:"eng";s:11:"hide_online";b:0;s:5:"cover";s:0:"";s:8:"approved";b:1;s:9:"is_social";b:0;s:23:"has_active_subscription";b:0;s:31:"receive_message_from_non_friend";b:1;s:28:"send_email_when_send_message";b:1;s:20:"request_friend_email";b:1;s:20:"notification_setting";N;s:15:"profile_type_id";s:1:"1";s:11:"user_create";s:1:"1";s:9:"moo_title";s:5:"admin";s:8:"moo_href";s:13:"/users/view/1";s:10:"moo_plugin";N;s:8:"moo_type";s:4:"User";s:7:"moo_url";s:13:"/users/view/1";s:9:"moo_thumb";s:6:"avatar";s:4:"Role";a:7:{s:2:"id";s:1:"1";s:4:"name";s:11:"Super Admin";s:6:"is_mod";b:1;s:8:"is_admin";b:1;s:8:"is_super";b:1;s:6:"params";s:65:"activity_view,user_username,attachment_upload,attachment_download";s:4:"core";b:1;}}}', 1504872012, '1'),
('t9bm7noidhkmoj3t6nslnnk0g2', 'Config|a:3:{s:9:"userAgent";s:32:"77ac761ca4a0b5c49c0d099582a1b5ea";s:4:"time";i:1505109555;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";Auth|a:1:{s:4:"User";a:50:{s:2:"id";s:1:"1";s:4:"name";s:5:"admin";s:5:"email";s:19:"admin@sonetrend.com";s:4:"salt";s:4:"7d54";s:7:"role_id";s:1:"1";s:6:"avatar";s:0:"";s:5:"photo";s:0:"";s:7:"created";s:19:"2017-08-21 20:20:05";s:10:"last_login";s:19:"2017-09-11 03:51:37";s:11:"photo_count";s:1:"0";s:12:"friend_count";s:1:"0";s:18:"notification_count";s:1:"0";s:20:"friend_request_count";s:1:"0";s:10:"blog_count";s:1:"0";s:11:"topic_count";s:1:"0";s:11:"group_count";s:1:"0";s:11:"event_count";s:1:"0";s:23:"conversation_user_count";s:1:"0";s:11:"video_count";s:1:"0";s:6:"gender";s:4:"Male";s:8:"birthday";s:10:"2017-08-21";s:6:"active";b:1;s:9:"confirmed";b:1;s:4:"code";s:32:"b0e8491e684bd1be18d9195d90c638bd";s:18:"notification_email";b:1;s:8:"timezone";s:12:"Asia/Jakarta";s:10:"ip_address";s:0:"";s:7:"privacy";s:1:"1";s:8:"username";s:0:"";s:5:"about";s:0:"";s:8:"featured";b:0;s:4:"lang";s:3:"eng";s:11:"hide_online";b:0;s:5:"cover";s:0:"";s:8:"approved";b:1;s:9:"is_social";b:0;s:23:"has_active_subscription";b:0;s:31:"receive_message_from_non_friend";b:1;s:28:"send_email_when_send_message";b:1;s:20:"request_friend_email";b:1;s:20:"notification_setting";N;s:15:"profile_type_id";s:1:"1";s:11:"user_create";s:1:"1";s:9:"moo_title";s:5:"admin";s:8:"moo_href";s:13:"/users/view/1";s:10:"moo_plugin";N;s:8:"moo_type";s:4:"User";s:7:"moo_url";s:13:"/users/view/1";s:9:"moo_thumb";s:6:"avatar";s:4:"Role";a:7:{s:2:"id";s:1:"1";s:4:"name";s:11:"Super Admin";s:6:"is_mod";b:1;s:8:"is_admin";b:1;s:8:"is_super";b:1;s:6:"params";s:65:"activity_view,user_username,attachment_upload,attachment_download";s:4:"core";b:1;}}}admin_login|i:1;', 1505109555, '1'),
('u6hts1ov1t9v6jcpmn9de1mr62', 'Config|a:3:{s:9:"userAgent";s:32:"6a7fef4750f8f70bc960bc25d7d26d57";s:4:"time";i:1505030344;s:9:"countdown";i:10;}facebook_sdk_version|s:5:"5.0.0";', 1505030344, 'guest_u6hts1ov1t9v6jcpmn9de1mr62');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `item_count` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `weight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `header` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `create_permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `type` (`type`,`active`,`weight`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `type`, `parent_id`, `name`, `description`, `item_count`, `active`, `weight`, `header`, `create_permission`) VALUES
(1, 'Photo', 0, 'Member Albums', '', 0, 1, 0, 0, ''),
(2, 'Group', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(3, 'Topic', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(4, 'Video', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(5, 'Event', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(6, 'Blog', 0, 'Default Category', '', 0, 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE IF NOT EXISTS `conversations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `message_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `lastposter_id` (`lastposter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_users`
--

CREATE TABLE IF NOT EXISTS `conversation_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `unread` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `conversation_id` (`conversation_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `core_blocks`
--

CREATE TABLE IF NOT EXISTS `core_blocks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path_view` varchar(75) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `plugin_id` int(11) NOT NULL DEFAULT '0',
  `group` varchar(75) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `restricted` text COLLATE utf8_unicode_ci NOT NULL,
  `plugin` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=67 ;

--
-- Dumping data for table `core_blocks`
--

INSERT INTO `core_blocks` (`id`, `name`, `path_view`, `params`, `is_active`, `plugin_id`, `group`, `restricted`, `plugin`) VALUES
(1, 'Who''s Online', 'user.onlineUsers', '{"0":{"label":"Title","input":"text","value":"Who''s Online","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Show member only","name":"member_only"},"3":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'user', '', ''),
(4, 'Recently Joined', 'user.recentlyJoined', '{"0":{"label":"Title","input":"text","value":"Recently Joined","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'user', '', ''),
(5, 'Tags', 'core.tags', '{"0":{"label":"Title","input":"text","value":"Tags","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Type","input":"select","value":{"all":"all","activities":"activity","comments":"comment","blogs":"blog","albums":"album","photos":"photo","topics":"topic","events":"event","videos":"video","groups":"group"},"name":"type"},"3":{"label":"Order by","input":"select","value":{"newest":"Newest","popular":"Popular","random":"Random"},"name":"order_by"},"4":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'core', '', ''),
(6, 'What''s New ', 'ajax.home_activity', '{"0":{"label":"Title","input":"text","value":"What''s new","name":"title"},"1":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, '', '', ''),
(7, 'Friends', 'user.friend', '{"0":{"label":"Title","input":"text","value":"Friends","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'user', '', ''),
(8, 'Upcoming Events', 'events.upcoming', '{"0":{"label":"Title","input":"text","value":"Upcoming Events","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"3":{"label":"plugin","input":"hidden","value":"Event","name":"plugin"}}', 1, 0, 'event', '', 'Event'),
(9, 'My Joined Groups', 'groups.myJoined', '{"0":{"label":"Title","input":"text","value":"My Joined Groups","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"3":{"label":"plugin","input":"hidden","value":"Group","name":"plugin"}}', 1, 0, 'group', '', 'Group'),
(10, 'Login', 'user.login', '{"0":{"label":"Title","input":"text","value":"Login","name":"title"},"1":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'landing', 'home.landing', ''),
(11, 'Sign up', 'user.signup', '{"0":{"label":"Title","input":"text","value":"Sign up","name":"title"},"1":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'landing', 'home.landing', ''),
(14, 'Popular Blogs', 'blogs.popular', '{"0":{"label":"Title","input":"text","value":"Popular Blogs","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"3":{"label":"plugin","input":"hidden","value":"Blog","name":"plugin"}}', 1, 0, 'blogs', '', 'Blog'),
(15, 'Popular Albums ', 'photos.popularAlbums', '{"0":{"label":"Title","input":"text","value":"Popular Albums","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"3":{"label":"plugin","input":"hidden","value":"Photo","name":"plugin"}}', 1, 0, 'photo', '', 'Photo'),
(16, 'Popular Videos', 'videos.popular', '{"0":{"label":"Title","input":"text","value":"Popular Videos","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"3":{"label":"plugin","input":"hidden","value":"Video","name":"plugin"}}', 1, 0, 'video', '', 'Video'),
(17, 'Popular Topics', 'topics.popular', '{"0":{"label":"Title","input":"text","value":"Popular Topics","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"3":{"label":"plugin","input":"hidden","value":"Topic","name":"plugin"}}', 1, 0, 'topic', '', 'Topic'),
(18, 'Popular Groups', 'groups.popular', '{"0":{"label":"Title","input":"text","value":"Popular Groups","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"3":{"label":"plugin","input":"hidden","value":"Group","name":"plugin"}}', 1, 0, 'group', '', 'Group'),
(19, 'Popular Events', 'events.popular', '{"0":{"label":"Title","input":"text","value":"Popular Events","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"3":{"label":"plugin","input":"hidden","value":"Event","name":"plugin"}}', 1, 0, 'event', '', 'Event'),
(21, 'Mutual Friends', 'user.mutualFriends', '{"0":{"label":"Title","input":"text","value":"Mutual Friends","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'user', 'users.view', ''),
(49, 'Featured Members', 'user.featured', '{"0":{"label":"Title","input":"text","value":"Featured Members","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'user', '', ''),
(51, 'Group Admin', 'groups.adminList', '{"0":{"label":"Title","input":"text","value":"Admin","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"3":{"label":"plugin","input":"hidden","value":"Group","name":"plugin"}}', 1, 0, 'group', 'groups.view', 'Group'),
(52, 'Group Members', 'groups.memberList', '{"0":{"label":"Title","input":"text","value":"Members","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"3":{"label":"plugin","input":"hidden","value":"Group","name":"plugin"}}', 1, 0, 'group', 'groups.view', 'Group'),
(53, 'People You Might Know', 'user.suggestions', '{"0":{"label":"Title","input":"text","value":"People You Might Know","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'user', '', ''),
(54, 'RSVP', 'events.rsvp', '{"0":{"label":"Title","input":"text","value":"RSVP","name":"title"},"1":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"2":{"label":"plugin","input":"hidden","value":"Event","name":"plugin"}}', 1, 0, 'events', 'events.view', 'Event'),
(55, 'Event Attending', 'events.attending', '{"0":{"label":"Title","input":"text","value":"Event Attending","name":"title"},"1":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"2":{"label":"plugin","input":"hidden","value":"Event","name":"plugin"}}', 1, 0, 'events', 'events.view', 'Event'),
(56, 'HTML Block', 'htmlBlock', '{"0":{"label":"Title","input":"text","value":"HTML Block","name":"title"},"1":{"label":"Html Content","input":"textarea","value":"","name":"html_block"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'core', '', ''),
(57, 'Today''s Birthday', 'birthdayBlock', '{"0":{"label":"Title","input":"text","value":"Today''s Birthday","name":"title"},"1":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'user', '', ''),
(58, 'Themes & Languages', 'theme_lang', '{"0":{"label":"Title","input":"text","value":"Themes & Languages","name":"title"},"1":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'core', '', ''),
(59, 'User Menu', 'welcomeBox', '{"0":{"label":"Title","input":"text","value":"User Menu","name":"title"},"1":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}}', 1, 0, 'core', 'home.index', ''),
(60, 'Main Menu', 'menu.widget', '[{"label":"Title","input":"text","value":"Main Menu","name":"title"},{"label":"Plugin","input":"hidden","value":"Menu","name":"plugin"},{"label":"Menu ID","input":"hidden","value":"1","name":"menu_id"}]', 1, 0, 'menu', '', 'Menu'),
(61, 'Footer Menu', 'menu.widget', '[{"label":"Title","input":"text","value":"Footer Menu","name":"title"},{"label":"Plugin","input":"hidden","value":"Menu","name":"plugin"},{"label":"Menu ID","input":"hidden","value":"2","name":"menu_id"}]', 1, 0, 'menu', '', 'Menu'),
(62, 'Featured Groups', 'groups.featured', '{"0":{"label":"Title","input":"text","value":"Featured Groups","name":"title"},"1":{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},"2":{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"},"3":{"label":"plugin","input":"hidden","value":"Group","name":"plugin"}}', 1, 0, 'group', '', 'Group'),
(63, 'Closed Network Signup', 'user.closeNetworkSignup', '{"0":{"label":"Title","input":"text","value":"Close Network Signup","name":"title"}}', 1, 0, 'user', '', ''),
(64, 'Menu Pakages', 'pakage.list', '[{"label":"Title","input":"text","value":"List pakages","name":"title"},{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},{"label":"plugin","input":"hidden","value":"Pakage","name":"plugin"},{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}]', 1, 0, 'pakage', '', 'Pakage'),
(65, 'Menu Detail Pakages', 'pakage.listdetail', '[{"label":"Title","input":"text","value":"List Detail Pakages","name":"title"},{"label":"Number of item to show","input":"text","value":"10","name":"num_item_show"},{"label":"plugin","input":"hidden","value":"Pakage","name":"plugin"},{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}]', 1, 0, 'pakage', '', 'Pakage'),
(66, 'Detail Pakage Search', 'pakage.search', '[{"label":"Title","input":"text","value":"Pakage Search","name":"title"},{"label":"plugin","input":"hidden","value":"Pakage","name":"plugin"},{"label":"Title","input":"checkbox","value":"Enable Title","name":"title_enable"}]', 1, 0, 'pakage', '', 'Pakage');

-- --------------------------------------------------------

--
-- Table structure for table `core_contents`
--

CREATE TABLE IF NOT EXISTS `core_contents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned NOT NULL DEFAULT '0',
  `type` enum('container','widget') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'widget',
  `name` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `component` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent_id` int(11) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `params` text COLLATE utf8_unicode_ci,
  `attribs` text COLLATE utf8_unicode_ci,
  `lft` int(11) unsigned DEFAULT NULL,
  `rght` int(10) unsigned DEFAULT NULL,
  `column` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `core_block_id` int(11) unsigned NOT NULL DEFAULT '0',
  `core_content_count` int(11) unsigned NOT NULL DEFAULT '0',
  `invisible` int(1) NOT NULL DEFAULT '0',
  `core_block_title` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `plugin` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `role_access` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_id` (`page_id`,`order`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=188 ;

--
-- Dumping data for table `core_contents`
--

INSERT INTO `core_contents` (`id`, `page_id`, `type`, `name`, `component`, `parent_id`, `order`, `params`, `attribs`, `lft`, `rght`, `column`, `core_block_id`, `core_content_count`, `invisible`, `core_block_title`, `plugin`, `role_access`) VALUES
(1, 1, 'container', 'west', '', NULL, 1, NULL, NULL, 1, 4, 0, 0, 1, 0, '', '', NULL),
(2, 1, 'widget', 'invisiblecontent', '', 1, 1, '{"title":"User Menu","maincontent":"1","role_access":"all"}', NULL, 2, 3, 0, 59, 0, 0, 'User Menu', '', 'all'),
(3, 1, 'container', 'east', '', NULL, 1, NULL, NULL, 5, 6, 0, 0, 0, 0, '', '', NULL),
(5, 1, 'container', 'center', '', NULL, 1, NULL, NULL, 7, 8, 0, 0, 0, 0, '', '', NULL),
(19, 2, 'container', 'east', '', NULL, 1, NULL, NULL, 9, 10, 0, 0, 0, 0, '', '', NULL),
(21, 2, 'container', 'center', '', NULL, 1, NULL, NULL, 11, 14, 0, 0, 1, 0, '', '', NULL),
(22, 2, 'widget', 'invisiblecontent', '', 21, 1, '{"title":"Contact","maincontent":"1","role_access":"all"}', NULL, 12, 13, 0, 1, 0, 0, 'Contact', '', 'all'),
(23, 3, 'container', 'west', '', NULL, 1, NULL, NULL, 15, 18, 0, 0, 1, 0, '', '', NULL),
(24, 3, 'widget', 'invisiblecontent', '', 23, 1, '{"title":"Menu friend &amp; Search","maincontent":"1","role_access":"all"}', NULL, 16, 17, 0, 1, 0, 0, 'Menu friend &amp; Search', '', 'all'),
(25, 3, 'container', 'east', '', NULL, 1, NULL, NULL, 19, 20, 0, 0, 0, 0, '', '', NULL),
(27, 3, 'container', 'center', '', NULL, 1, NULL, NULL, 21, 24, 0, 0, 1, 0, '', '', NULL),
(28, 3, 'widget', 'invisiblecontent', '', 27, 1, '{"title":"People","maincontent":"1","role_access":"all"}', NULL, 22, 23, 0, 4, 0, 0, 'People', '', 'all'),
(29, 4, 'container', 'west', '', NULL, 1, NULL, NULL, 25, 28, 0, 0, 1, 0, '', '', NULL),
(30, 4, 'widget', 'invisiblecontent', '', 29, 1, '{"title":"Profile menu &amp; Users","maincontent":"1","role_access":"all"}', NULL, 26, 27, 0, 1, 0, 0, 'Profile menu &amp; Users', '', 'all'),
(31, 4, 'container', 'center', '', NULL, 1, NULL, NULL, 29, 32, 0, 0, 1, 0, '', '', NULL),
(32, 4, 'widget', 'invisiblecontent', '', 31, 1, '{"title":"Recent Activities","maincontent":"1","role_access":"all"}', NULL, 30, 31, 0, 4, 0, 0, 'Recent Activities', '', 'all'),
(34, 6, 'container', 'west', '', NULL, 1, NULL, NULL, 33, 36, 0, 0, 1, 0, '', '', NULL),
(35, 6, 'widget', 'invisiblecontent', '', 34, 1, '{"title":"Blogs menu &amp; Search","maincontent":"1","role_access":"all"}', NULL, 34, 35, 0, 1, 0, 0, 'Blogs menu &amp; Search', '', 'all'),
(36, 6, 'container', 'east', '', NULL, 1, NULL, NULL, 37, 38, 0, 0, 0, 0, '', '', NULL),
(38, 6, 'container', 'center', '', NULL, 1, NULL, NULL, 39, 42, 0, 0, 1, 0, '', '', NULL),
(39, 6, 'widget', 'invisiblecontent', '', 38, 1, '{"title":"Blogs","maincontent":"1","role_access":"all"}', NULL, 40, 41, 0, 4, 0, 0, 'Blogs', '', 'all'),
(42, 8, 'container', 'west', '', NULL, 1, NULL, NULL, 43, 46, 0, 0, 1, 0, '', '', NULL),
(43, 8, 'widget', 'invisiblecontent', '', 42, 1, '{"title":"Blogs settings","maincontent":"1","role_access":"all"}', NULL, 44, 45, 0, 1, 0, 0, 'Blogs settings', '', 'all'),
(44, 8, 'container', 'east', '', NULL, 1, NULL, NULL, 47, 50, 0, 0, 1, 0, '', '', NULL),
(45, 8, 'widget', 'invisiblecontent', '', 44, 1, '{"title":"Other Entries","maincontent":"1","role_access":"all"}', NULL, 48, 49, 0, 5, 0, 0, 'Other Entries', '', 'all'),
(46, 8, 'container', 'center', '', NULL, 1, NULL, NULL, 51, 54, 0, 0, 1, 0, '', '', NULL),
(47, 8, 'widget', 'invisiblecontent', '', 46, 1, '{"title":"Blog Detail","maincontent":"1","role_access":"all"}', NULL, 52, 53, 0, 4, 0, 0, 'Blog Detail', '', 'all'),
(48, 9, 'container', 'west', '', NULL, 1, NULL, NULL, 55, 58, 0, 0, 1, 0, '', '', NULL),
(49, 9, 'widget', 'invisiblecontent', '', 48, 1, '{"title":"List photos &amp; Search","maincontent":"1","role_access":"all"}', NULL, 56, 57, 0, 1, 0, 0, 'List photos &amp; Search', '', 'all'),
(50, 9, 'container', 'center', '', NULL, 1, NULL, NULL, 59, 62, 0, 0, 1, 0, '', '', NULL),
(51, 9, 'widget', 'invisiblecontent', '', 50, 1, '{"title":"Photos","maincontent":"1","role_access":"all"}', NULL, 60, 61, 0, 4, 0, 0, 'Photos', '', 'all'),
(53, 10, 'container', 'east', '', NULL, 1, NULL, NULL, 63, 64, 0, 0, 0, 0, '', '', NULL),
(55, 10, 'container', 'center', '', NULL, 1, NULL, NULL, 65, 68, 0, 0, 1, 0, '', '', NULL),
(56, 10, 'widget', 'invisiblecontent', '', 55, 1, '{"title":"Album''s photos","maincontent":"1"}', NULL, 66, 67, 0, 1, 0, 0, '', '', NULL),
(57, 11, 'container', 'center', '', NULL, 1, NULL, NULL, 69, 72, 0, 0, 1, 0, '', '', NULL),
(58, 11, 'widget', 'invisiblecontent', '', 57, 1, '{"title":"Page Content","maincontent":"1"}', NULL, 70, 71, 0, 1, 0, 0, '', '', NULL),
(61, 12, 'container', 'west', '', NULL, 1, NULL, NULL, 73, 76, 0, 0, 1, 0, '', '', NULL),
(62, 12, 'widget', 'invisiblecontent', '', 61, 1, '{"title":"Menu video &amp; Search","maincontent":"1","role_access":"all"}', NULL, 74, 75, 0, 1, 0, 0, 'Menu video &amp; Search', '', 'all'),
(63, 12, 'container', 'east', '', NULL, 1, NULL, NULL, 77, 78, 0, 0, 0, 0, '', '', NULL),
(65, 12, 'container', 'center', '', NULL, 1, NULL, NULL, 79, 82, 0, 0, 1, 0, '', '', NULL),
(66, 12, 'widget', 'invisiblecontent', '', 65, 1, '{"title":"Videos","maincontent":"1","role_access":"all"}', NULL, 80, 81, 0, 4, 0, 0, 'Videos', '', 'all'),
(68, 13, 'container', 'east', '', NULL, 1, NULL, NULL, 83, 86, 0, 0, 1, 0, '', '', NULL),
(69, 13, 'widget', 'invisiblecontent', '', 68, 1, '{"title":"Tags &amp; Similar Videos","maincontent":"1","role_access":"all"}', NULL, 84, 85, 0, 4, 0, 0, 'Tags &amp; Similar Videos', '', 'all'),
(70, 13, 'container', 'center', '', NULL, 1, NULL, NULL, 87, 90, 0, 0, 1, 0, '', '', NULL),
(71, 13, 'widget', 'invisiblecontent', '', 70, 1, '{"title":"Video''s content","maincontent":"1","role_access":"all"}', NULL, 88, 89, 0, 1, 0, 0, 'Video''s content', '', 'all'),
(72, 15, 'container', 'west', '', NULL, 1, NULL, NULL, 91, 94, 0, 0, 1, 0, '', '', NULL),
(73, 15, 'widget', 'invisiblecontent', '', 72, 1, '{"title":"Topic''s setting","maincontent":"1","role_access":"all"}', NULL, 92, 93, 0, 1, 0, 0, 'Topic''s setting', '', 'all'),
(74, 15, 'container', 'east', '', NULL, 1, NULL, NULL, 95, 96, 0, 0, 0, 0, '', '', NULL),
(76, 15, 'container', 'center', '', NULL, 1, NULL, NULL, 97, 100, 0, 0, 1, 0, '', '', NULL),
(77, 15, 'widget', 'invisiblecontent', '', 76, 1, '{"title":"Topics contents","maincontent":"1","role_access":"all"}', NULL, 98, 99, 0, 4, 0, 0, 'Topics contents', '', 'all'),
(80, 16, 'container', 'west', '', NULL, 1, NULL, NULL, 101, 104, 0, 0, 1, 0, '', '', NULL),
(81, 16, 'widget', 'invisiblecontent', '', 80, 1, '{"title":"Groups menu &amp; Search","maincontent":"1","role_access":"all"}', NULL, 102, 103, 0, 1, 0, 0, 'Groups menu &amp; Search', '', 'all'),
(82, 16, 'container', 'east', '', NULL, 1, NULL, NULL, 105, 106, 0, 0, 0, 0, '', '', NULL),
(84, 16, 'container', 'center', '', NULL, 1, NULL, NULL, 107, 110, 0, 0, 1, 0, '', '', NULL),
(85, 16, 'widget', 'invisiblecontent', '', 84, 1, '{"title":"Groups","maincontent":"1","role_access":"all"}', NULL, 108, 109, 0, 4, 0, 0, 'Groups', '', 'all'),
(86, 18, 'container', 'west', '', NULL, 1, NULL, NULL, 111, 114, 0, 0, 1, 0, '', '', NULL),
(87, 18, 'widget', 'invisiblecontent', '', 86, 1, '{"title":"Group info &amp; setting","maincontent":"1","role_access":"all"}', NULL, 112, 113, 0, 1, 0, 0, 'Group info &amp; setting', '', 'all'),
(88, 18, 'container', 'east', '', NULL, 1, NULL, NULL, 115, 116, 0, 0, 0, 0, '', '', NULL),
(90, 18, 'container', 'center', '', NULL, 1, NULL, NULL, 117, 120, 0, 0, 1, 0, '', '', NULL),
(91, 18, 'widget', 'invisiblecontent', '', 90, 1, '{"title":"Info &amp; Photos &amp; Recent Activities","maincontent":"1","role_access":"all"}', NULL, 118, 119, 0, 4, 0, 0, 'Info &amp; Photos &amp; Recent Activities', '', 'all'),
(94, 19, 'container', 'west', '', NULL, 1, NULL, NULL, 121, 124, 0, 0, 1, 0, '', '', NULL),
(95, 19, 'widget', 'invisiblecontent', '', 94, 1, '{"title":"Events menu &amp; Search","maincontent":"1","role_access":"all"}', NULL, 122, 123, 0, 1, 0, 0, 'Events menu &amp; Search', '', 'all'),
(96, 19, 'container', 'east', '', NULL, 1, NULL, NULL, 125, 126, 0, 0, 0, 0, '', '', NULL),
(98, 19, 'container', 'center', '', NULL, 1, NULL, NULL, 127, 130, 0, 0, 1, 0, '', '', NULL),
(99, 19, 'widget', 'invisiblecontent', '', 98, 1, '{"title":"Events","maincontent":"1","role_access":"all"}', NULL, 128, 129, 0, 4, 0, 0, 'Events', '', 'all'),
(101, 21, 'container', 'west', '', NULL, 1, NULL, NULL, 131, 134, 0, 0, 1, 0, '', '', NULL),
(102, 21, 'widget', 'invisiblecontent', '', 101, 1, '{"title":"Event''s setting","maincontent":"1","role_access":"all"}', NULL, 132, 133, 0, 1, 0, 0, 'Event''s setting', '', 'all'),
(103, 21, 'container', 'east', '', NULL, 1, NULL, NULL, 135, 140, 0, 0, 2, 0, '', '', NULL),
(105, 21, 'container', 'center', '', NULL, 1, NULL, NULL, 141, 144, 0, 0, 1, 0, '', '', NULL),
(106, 21, 'widget', 'invisiblecontent', '', 105, 1, '{"title":"Event''s detail &amp; Message","maincontent":"1","role_access":"all"}', NULL, 142, 143, 0, 4, 0, 0, 'Event''s detail &amp; Message', '', 'all'),
(109, 22, 'container', 'east', '', NULL, 1, NULL, NULL, 145, 146, 0, 0, 0, 0, '', '', NULL),
(111, 22, 'container', 'center', '', NULL, 1, NULL, NULL, 147, 150, 0, 0, 1, 0, '', '', NULL),
(112, 22, 'widget', 'invisiblecontent', '', 111, 1, '{"title":"Forgot Password","maincontent":"1"}', NULL, 148, 149, 0, 1, 0, 0, '', '', NULL),
(113, 23, 'container', 'west', '', NULL, 1, NULL, NULL, 151, 154, 0, 0, 1, 0, '', '', NULL),
(114, 23, 'widget', 'invisiblecontent', '', 113, 1, '{"title":"Topics menu &amp; Search","maincontent":"1","role_access":"all"}', NULL, 152, 153, 0, 1, 0, 0, 'Topics menu &amp; Search', '', 'all'),
(115, 23, 'container', 'east', '', NULL, 1, NULL, NULL, 155, 156, 0, 0, 0, 0, '', '', NULL),
(117, 23, 'container', 'center', '', NULL, 1, NULL, NULL, 157, 160, 0, 0, 1, 0, '', '', NULL),
(118, 23, 'widget', 'invisiblecontent', '', 117, 1, '{"title":"Topics","maincontent":"1","role_access":"all"}', NULL, 158, 159, 0, 4, 0, 0, 'Topics', '', 'all'),
(119, 63, 'container', 'east', '', NULL, 1, NULL, NULL, 161, 172, 0, 0, 5, 0, '', '', NULL),
(120, 63, 'widget', 'theme_lang', '', 119, 5, '{"title":"Themes & Languages","title_enable":"1"}', NULL, 162, 163, 0, 58, 0, 0, 'Themes & Languages', '', NULL),
(121, 63, 'container', 'west', '', NULL, 1, NULL, NULL, 173, 180, 0, 0, 3, 0, '', '', NULL),
(122, 63, 'widget', 'events.upcoming', '', 121, 2, '{"title":"Upcoming Events","num_item_show":"5","title_enable":"1","plugin":"Event"}', NULL, 174, 175, 0, 8, 0, 0, 'Upcoming Events', 'Event', NULL),
(125, 63, 'widget', 'photos.popularAlbums', '', 121, 3, '{"title":"Popular Albums","num_item_show":"5","title_enable":"1","plugin":"Photo"}', NULL, 176, 177, 0, 15, 0, 0, 'Popular Albums', 'Photo', NULL),
(132, 63, 'widget', 'user.onlineUsers', '', 119, 1, '{"title":"Who''s Online","num_item_show":"10","member_only":"1","title_enable":"1"}', NULL, 164, 165, 0, 1, 0, 0, 'Who''s Online', '', NULL),
(133, 63, 'widget', 'user.recentlyJoined', '', 119, 2, '{"title":"Recently Joined","num_item_show":"10","title_enable":"1"}', NULL, 166, 167, 0, 4, 0, 0, 'Recently Joined', '', NULL),
(137, 63, 'container', 'center', '', NULL, 1, NULL, NULL, 181, 184, 0, 0, 1, 0, '', '', NULL),
(140, 90, 'container', 'center', '', NULL, 1, NULL, NULL, 185, 188, 0, 0, 1, 0, '', '', NULL),
(141, 90, 'widget', 'invisiblecontent', '', 140, 1, '{"title":"Login form","maincontent":"1"}', NULL, 186, 187, 0, 1, 0, 0, '', '', NULL),
(142, 96, 'container', 'center', '', NULL, 1, NULL, NULL, 189, 192, 0, 0, 1, 0, '', '', NULL),
(143, 96, 'widget', 'invisiblecontent', '', 142, 1, '{"title":"Register Form","maincontent":"1"}', NULL, 190, 191, 0, 1, 0, 0, '', '', NULL),
(146, 100, 'container', 'center', '', NULL, 1, NULL, NULL, 193, 196, 0, 0, 1, 0, '', NULL, NULL),
(147, 100, 'widget', 'invisiblecontent', '', 146, 1, '{"title":"Page Content","maincontent":"1"}', NULL, 194, 195, 0, 0, 0, 0, '', NULL, NULL),
(148, 101, 'container', 'center', '', NULL, 1, NULL, NULL, 197, 200, 0, 0, 1, 0, '', NULL, NULL),
(149, 101, 'widget', 'invisiblecontent', '', 148, 1, '{"title":"Page Content","maincontent":"1"}', NULL, 198, 199, 0, 0, 0, 0, '', NULL, NULL),
(150, 102, 'container', 'center', '', NULL, 1, NULL, NULL, 201, 204, 0, 0, 1, 0, '', NULL, NULL),
(151, 102, 'widget', 'invisiblecontent', '', 150, 1, '{"title":"Page Content","maincontent":"1"}', NULL, 202, 203, 0, 0, 0, 0, '', NULL, NULL),
(152, 69, 'container', 'footer', '', NULL, 1, NULL, NULL, 205, 208, 0, 0, 1, 0, '', NULL, NULL),
(153, 69, 'widget', 'menu.widget', '', 152, 1, '{"title":"Footer Menu","plugin":"Menu","menu_id":"2"}', NULL, 206, 207, 0, 61, 0, 0, 'Footer Menu', 'Menu', NULL),
(154, 63, 'widget', 'user.login', '', 121, 1, '{"title":"Login","title_enable":"1"}', NULL, 178, 179, 0, 10, 0, 0, 'Login', '', NULL),
(155, 63, 'widget', 'videos.popular', '', 119, 3, '{"title":"Popular Videos","num_item_show":"3","title_enable":"1","plugin":"Video"}', NULL, 168, 169, 0, 16, 0, 0, 'Popular Videos', 'Video', NULL),
(156, 63, 'widget', 'blogs.popular', '', 119, 4, '{"title":"Popular Blogs","num_item_show":"5","title_enable":"1","plugin":"Blog"}', NULL, 170, 171, 0, 14, 0, 0, 'Popular Blogs', 'Blog', NULL),
(178, 21, 'widget', 'events.rsvp', '', 103, 1, '{"title":"RSVP","title_enable":"Enable Title","plugin":"Event","role_access":"all"}', NULL, 136, 137, 0, 54, 0, 0, 'RSVP', 'Event', 'all'),
(179, 21, 'widget', 'events.attending', '', 103, 2, '{"title":"Event Attending","title_enable":"Enable Title","plugin":"Event","role_access":"all"}', NULL, 138, 139, 0, 55, 0, 0, 'Event Attending', 'Event', 'all'),
(181, 63, 'widget', 'user.closeNetworkSignup', '', 137, 1, '{"title":"Closed Network Signup"}', NULL, 182, 183, 0, 63, 0, 0, 'Closed Network Signup', '', NULL),
(183, 104, 'container', 'center', '', NULL, 1, NULL, NULL, 209, 212, 0, 0, 1, 0, '', NULL, NULL),
(184, 104, 'widget', 'invisiblecontent', '', 183, 1, '{"title":"Page Content","maincontent":"1","role_access":"all"}', NULL, 210, 211, 0, 0, 0, 0, 'Page Content', NULL, 'all'),
(185, 104, 'container', 'west', '', NULL, 1, NULL, NULL, 213, 214, 0, 0, 0, 0, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `core_menus`
--

CREATE TABLE IF NOT EXISTS `core_menus` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `style` enum('horizontal','vertical') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'horizontal',
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `menuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `core_menus`
--

INSERT INTO `core_menus` (`id`, `name`, `style`, `alias`, `menuid`) VALUES
(1, 'Main Menu', 'horizontal', 'main-menu', 'menu-1'),
(2, 'Footer Menu', 'horizontal', 'footer-menu', 'menu-2');

-- --------------------------------------------------------

--
-- Table structure for table `core_menu_items`
--

CREATE TABLE IF NOT EXISTS `core_menu_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL DEFAULT '0',
  `rght` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `original_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title_attribute` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `font_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `new_blank` tinyint(1) NOT NULL DEFAULT '0',
  `role_access` text COLLATE utf8_unicode_ci NOT NULL,
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `type` enum('page','link','header','plugin') COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `plugin` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `group` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `core_menu_items`
--

INSERT INTO `core_menu_items` (`id`, `parent_id`, `lft`, `rght`, `name`, `original_name`, `url`, `title_attribute`, `font_class`, `is_active`, `new_blank`, `role_access`, `menu_id`, `type`, `menu_order`, `plugin`, `group`) VALUES
(1, NULL, 1, 2, 'Home', 'Home', '/home', '', '', 1, 0, '["1","2","3","4","5","6"]', 1, 'page', 1, '', ''),
(2, NULL, 23, 24, 'People', 'People', '/users', '', '', 1, 0, '{"0":"1","3":"4"}', 1, 'page', 2, '', ''),
(9, NULL, 21, 22, 'Contact Us', 'Contact Page', '/home/contact', '', '', 1, 0, '["1","2","3","4","5","6"]', 2, 'page', 4, '', ''),
(10, NULL, 15, 16, 'About Us', 'About Us', '/pages/about-us', '', '', 1, 0, '["1","2","3","4","5","6"]', 2, 'page', 1, '', ''),
(11, NULL, 17, 18, 'Terms of Service', 'Terms of Service', '/pages/terms-of-service', '', '', 1, 0, '["1","2","3","4","5","6"]', 2, 'page', 2, '', ''),
(12, NULL, 19, 20, 'Privacy Policy', 'Privacy Policy', '/pages/privacy-policy', '', '', 1, 0, '["1","2","3","4","5","6"]', 2, 'page', 3, '', ''),
(14, NULL, 27, 28, 'Livestream Order', 'Livestream Order', '/livestreams', '', '', 1, 0, '{"0":"1","1":"2","3":"4","4":"5","5":"6"}', 1, 'plugin', 4, '', ''),
(15, NULL, 29, 30, 'Pakages', 'Pakage Detail Page', '/pakage/pakage_details', '', '', 1, 0, '{"0":"1","1":"2","3":"4","4":"5","5":"6"}', 1, 'page', 3, '', ''),
(16, NULL, 31, 32, 'Payment', 'Payment', '/payments', '', '', 1, 0, '["1","2","3","4","5","6"]', 1, 'plugin', 999, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country_iso` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `state_count` int(5) NOT NULL DEFAULT '0',
  `order` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=245 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `country_iso`, `state_count`, `order`) VALUES
(1, 'United States', 'US', 51, 231),
(2, 'United Arab Emirates', 'AE', 0, 229),
(3, 'Afghanistan', 'AF', 0, 1),
(4, 'Antigua and Barbuda', 'AG', 0, 10),
(5, 'Anguilla', 'AI', 0, 8),
(6, 'Albania', 'AL', 0, 3),
(7, 'Armenia', 'AM', 0, 12),
(8, 'Netherlands Antilles', 'AN', 0, 155),
(9, 'Angola', 'AO', 0, 7),
(10, 'Antarctica', 'AQ', 0, 9),
(11, 'Argentina', 'AR', 0, 11),
(12, 'American Samoa', 'AS', 0, 5),
(13, 'Austria', 'AT', 0, 15),
(14, 'Australia', 'AU', 0, 14),
(15, 'Aruba', 'AW', 0, 13),
(16, 'Aland Islands', 'AX', 0, 2),
(17, 'Azerbaijan', 'AZ', 0, 16),
(18, 'Bosnia and Herzegovina', 'BA', 0, 28),
(19, 'Barbados', 'BB', 0, 20),
(20, 'Bangladesh', 'BD', 0, 19),
(21, 'Belgium', 'BE', 0, 22),
(22, 'Burkina Faso', 'BF', 0, 35),
(23, 'Bulgaria', 'BG', 0, 34),
(24, 'Bahrain', 'BH', 0, 18),
(25, 'Burundi', 'BI', 0, 36),
(26, 'Benin', 'BJ', 0, 24),
(27, 'Saint Barthelemy', 'BL', 0, 183),
(28, 'Bermuda', 'BM', 0, 25),
(29, 'Brunei Darussalam', 'BN', 0, 33),
(30, 'Bolivia', 'BO', 0, 27),
(31, 'Brazil', 'BR', 0, 31),
(32, 'Bahamas', 'BS', 0, 17),
(33, 'Bhutan', 'BT', 0, 26),
(34, 'Bouvet Island', 'BV', 0, 30),
(35, 'Botswana', 'BW', 0, 29),
(36, 'Belarus', 'BY', 0, 21),
(37, 'Belize', 'BZ', 0, 23),
(38, 'Canada', 'CA', 0, 39),
(39, 'Cocos (Keeling) Islands', 'CC', 0, 47),
(40, 'Congo, the Democratic Republic of the', 'CD', 0, 51),
(41, 'Central African Republic', 'CF', 0, 42),
(42, 'Congo', 'CG', 0, 50),
(43, 'Switzerland', 'CH', 0, 211),
(44, 'Cote D''Ivoire', 'CI', 0, 54),
(45, 'Cook Islands', 'CK', 0, 52),
(46, 'Chile', 'CL', 0, 44),
(47, 'Cameroon', 'CM', 0, 38),
(48, 'China', 'CN', 0, 45),
(49, 'Colombia', 'CO', 0, 48),
(50, 'Costa Rica', 'CR', 0, 53),
(51, 'Cuba', 'CU', 0, 56),
(52, 'Cape Verde', 'CV', 0, 40),
(53, 'Christmas Island', 'CX', 0, 46),
(54, 'Cyprus', 'CY', 0, 57),
(55, 'Czech Republic', 'CZ', 0, 58),
(56, 'Germany', 'DE', 0, 81),
(57, 'Djibouti', 'DJ', 0, 60),
(58, 'Denmark', 'DK', 0, 59),
(59, 'Dominica', 'DM', 0, 61),
(60, 'Dominican Republic', 'DO', 0, 62),
(61, 'Algeria', 'DZ', 0, 4),
(62, 'Ecuador', 'EC', 0, 63),
(63, 'Estonia', 'EE', 0, 68),
(64, 'Egypt', 'EG', 0, 64),
(65, 'Western Sahara', 'EH', 0, 241),
(66, 'Eritrea', 'ER', 0, 67),
(67, 'Spain', 'ES', 0, 204),
(68, 'Ethiopia', 'ET', 0, 69),
(69, 'Finland', 'FI', 0, 73),
(70, 'Fiji', 'FJ', 0, 72),
(71, 'Falkland Islands (Malvinas)', 'FK', 0, 70),
(72, 'Micronesia, Federated States of', 'FM', 0, 142),
(73, 'Faroe Islands', 'FO', 0, 71),
(74, 'France', 'FR', 0, 74),
(75, 'Gabon', 'GA', 0, 78),
(76, 'United Kingdom', 'GB', 0, 230),
(77, 'Grenada', 'GD', 0, 86),
(78, 'Georgia', 'GE', 0, 80),
(79, 'French Guiana', 'GF', 0, 75),
(80, 'Guernsey', 'GG', 0, 90),
(81, 'Ghana', 'GH', 0, 82),
(82, 'Gibraltar', 'GI', 0, 83),
(83, 'Greenland', 'GL', 0, 85),
(84, 'Gambia', 'GM', 0, 79),
(85, 'Guinea', 'GN', 0, 91),
(86, 'Guadeloupe', 'GP', 0, 87),
(87, 'Equatorial Guinea', 'GQ', 0, 66),
(88, 'Greece', 'GR', 0, 84),
(89, 'South Georgia and the South Sandwich Islands', 'GS', 0, 203),
(90, 'Guatemala', 'GT', 0, 89),
(91, 'Guam', 'GU', 0, 88),
(92, 'Guinea-Bissau', 'GW', 0, 92),
(93, 'Guyana', 'GY', 0, 93),
(94, 'Hong Kong', 'HK', 0, 98),
(95, 'Heard Island and Mcdonald Islands', 'HM', 0, 95),
(96, 'Honduras', 'HN', 0, 97),
(97, 'Croatia', 'HR', 0, 55),
(98, 'Haiti', 'HT', 0, 94),
(99, 'Hungary', 'HU', 0, 99),
(100, 'Indonesia', 'ID', 0, 102),
(101, 'Ireland', 'IE', 0, 105),
(102, 'Israel', 'IL', 0, 107),
(103, 'Isle of Man', 'IM', 0, 106),
(104, 'India', 'IN', 0, 101),
(105, 'British Indian Ocean Territory', 'IO', 0, 32),
(106, 'Iraq', 'IQ', 0, 104),
(107, 'Iran, Islamic Republic of', 'IR', 0, 103),
(108, 'Iceland', 'IS', 0, 100),
(109, 'Italy', 'IT', 0, 108),
(110, 'Jamaica', 'JM', 0, 109),
(111, 'Jordan', 'JO', 0, 111),
(112, 'Japan', 'JP', 0, 110),
(113, 'Kenya', 'KE', 0, 113),
(114, 'Kyrgyzstan', 'KG', 0, 118),
(115, 'Cambodia', 'KH', 0, 37),
(116, 'Kiribati', 'KI', 0, 114),
(117, 'Comoros', 'KM', 0, 49),
(118, 'Saint Kitts and Nevis', 'KN', 0, 185),
(119, 'Korea, Democratic People''s Republic of', 'KP', 0, 115),
(120, 'Korea, Republic of', 'KR', 0, 116),
(121, 'Kuwait', 'KW', 0, 117),
(122, 'Cayman Islands', 'KY', 0, 41),
(123, 'Kazakhstan', 'KZ', 0, 112),
(124, 'Lao People''s Democratic Republic', 'LA', 0, 119),
(125, 'Lebanon', 'LB', 0, 121),
(126, 'Saint Lucia', 'LC', 0, 186),
(127, 'Liechtenstein', 'LI', 0, 125),
(128, 'Sri Lanka', 'LK', 0, 205),
(129, 'Liberia', 'LR', 0, 123),
(130, 'Lesotho', 'LS', 0, 122),
(131, 'Lithuania', 'LT', 0, 126),
(132, 'Luxembourg', 'LU', 0, 127),
(133, 'Latvia', 'LV', 0, 120),
(134, 'Libyan Arab Jamahiriya', 'LY', 0, 124),
(135, 'Morocco', 'MA', 0, 148),
(136, 'Monaco', 'MC', 0, 144),
(137, 'Moldova, Republic of', 'MD', 0, 143),
(138, 'Montenegro', 'ME', 0, 146),
(139, 'Madagascar', 'MG', 0, 130),
(140, 'Marshall Islands', 'MH', 0, 136),
(141, 'Macedonia, the Former Yugoslav Republic of', 'MK', 0, 129),
(142, 'Mali', 'ML', 0, 134),
(143, 'Myanmar', 'MM', 0, 150),
(144, 'Mongolia', 'MN', 0, 145),
(145, 'Macao', 'MO', 0, 128),
(146, 'Northern Mariana Islands', 'MP', 0, 163),
(147, 'Martinique', 'MQ', 0, 137),
(148, 'Mauritania', 'MR', 0, 138),
(149, 'Montserrat', 'MS', 0, 147),
(150, 'Malta', 'MT', 0, 135),
(151, 'Mauritius', 'MU', 0, 139),
(152, 'Maldives', 'MV', 0, 133),
(153, 'Malawi', 'MW', 0, 131),
(154, 'Mexico', 'MX', 0, 141),
(155, 'Malaysia', 'MY', 0, 132),
(156, 'Mozambique', 'MZ', 0, 149),
(157, 'Namibia', 'NA', 0, 151),
(158, 'New Caledonia', 'NC', 0, 156),
(159, 'Niger', 'NE', 0, 159),
(160, 'Norfolk Island', 'NF', 0, 162),
(161, 'Nigeria', 'NG', 0, 160),
(162, 'Nicaragua', 'NI', 0, 158),
(163, 'Netherlands', 'NL', 0, 154),
(164, 'Norway', 'NO', 0, 164),
(165, 'Nepal', 'NP', 0, 153),
(166, 'Nauru', 'NR', 0, 152),
(167, 'Niue', 'NU', 0, 161),
(168, 'New Zealand', 'NZ', 0, 157),
(169, 'Oman', 'OM', 0, 165),
(170, 'Panama', 'PA', 0, 169),
(171, 'Peru', 'PE', 0, 172),
(172, 'French Polynesia', 'PF', 0, 76),
(173, 'Papua New Guinea', 'PG', 0, 170),
(174, 'Philippines', 'PH', 0, 173),
(175, 'Pakistan', 'PK', 0, 166),
(176, 'Poland', 'PL', 0, 175),
(177, 'Saint Pierre and Miquelon', 'PM', 0, 187),
(178, 'Pitcairn', 'PN', 0, 174),
(179, 'Puerto Rico', 'PR', 0, 177),
(180, 'Palestinian Territory, Occupied', 'PS', 0, 168),
(181, 'Portugal', 'PT', 0, 176),
(182, 'Palau', 'PW', 0, 167),
(183, 'Paraguay', 'PY', 0, 171),
(184, 'Qatar', 'QA', 0, 178),
(185, 'Reunion', 'RE', 0, 179),
(186, 'Romania', 'RO', 0, 180),
(187, 'Serbia', 'RS', 0, 194),
(188, 'Russian Federation', 'RU', 0, 181),
(189, 'Rwanda', 'RW', 0, 182),
(190, 'Saudi Arabia', 'SA', 0, 192),
(191, 'Solomon Islands', 'SB', 0, 200),
(192, 'Seychelles', 'SC', 0, 195),
(193, 'Sudan', 'SD', 0, 206),
(194, 'Sweden', 'SE', 0, 210),
(195, 'Singapore', 'SG', 0, 197),
(196, 'Saint Helena', 'SH', 0, 184),
(197, 'Slovenia', 'SI', 0, 199),
(198, 'Svalbard and Jan Mayen', 'SJ', 0, 208),
(199, 'Slovakia', 'SK', 0, 198),
(200, 'Sierra Leone', 'SL', 0, 196),
(201, 'San Marino', 'SM', 0, 190),
(202, 'Senegal', 'SN', 0, 193),
(203, 'Somalia', 'SO', 0, 201),
(204, 'Suriname', 'SR', 0, 207),
(205, 'Sao Tome and Principe', 'ST', 0, 191),
(206, 'El Salvador', 'SV', 0, 65),
(207, 'Syrian Arab Republic', 'SY', 0, 212),
(208, 'Swaziland', 'SZ', 0, 209),
(209, 'Turks and Caicos Islands', 'TC', 0, 225),
(210, 'Chad', 'TD', 0, 43),
(211, 'French Southern Territories', 'TF', 0, 77),
(212, 'Togo', 'TG', 0, 218),
(213, 'Thailand', 'TH', 0, 216),
(214, 'Tajikistan', 'TJ', 0, 214),
(215, 'Tokelau', 'TK', 0, 219),
(216, 'Timor-Leste', 'TL', 0, 217),
(217, 'Turkmenistan', 'TM', 0, 224),
(218, 'Tunisia', 'TN', 0, 222),
(219, 'Tonga', 'TO', 0, 220),
(220, 'Turkey', 'TR', 0, 223),
(221, 'Trinidad and Tobago', 'TT', 0, 221),
(222, 'Tuvalu', 'TV', 0, 226),
(223, 'Taiwan, Province of China', 'TW', 0, 213),
(224, 'Tanzania, United Republic of', 'TZ', 0, 215),
(225, 'Ukraine', 'UA', 0, 228),
(226, 'Uganda', 'UG', 0, 227),
(227, 'United States Minor Outlying Islands', 'UM', 0, 232),
(228, 'Andorra', 'AD', 0, 6),
(229, 'Uruguay', 'UY', 0, 233),
(230, 'Uzbekistan', 'UZ', 0, 234),
(231, 'Holy See (Vatican City State)', 'VA', 0, 96),
(232, 'Saint Vincent and the Grenadines', 'VC', 0, 188),
(233, 'Venezuela', 'VE', 0, 236),
(234, 'Virgin Islands, British', 'VG', 0, 238),
(235, 'Virgin Islands, U.s.', 'VI', 0, 239),
(236, 'Viet Nam', 'VN', 0, 237),
(237, 'Vanuatu', 'VU', 0, 235),
(238, 'Wallis and Futuna', 'WF', 0, 240),
(239, 'Samoa', 'WS', 0, 189),
(240, 'Yemen', 'YE', 0, 242),
(241, 'Mayotte', 'YT', 0, 140),
(242, 'South Africa', 'ZA', 0, 202),
(243, 'Zambia', 'ZM', 0, 243),
(244, 'Zimbabwe', 'ZW', 0, 244);

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `symbol` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci,
  `ordering` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `currency_code`, `symbol`, `name`, `description`, `ordering`, `is_default`, `is_active`) VALUES
(1, 'HKD', 'HK$', 'Hong Kong Dollar', '', 0, 0, 1),
(2, 'SGD', 'S$', 'Singapore Dollar', '', 0, 0, 0),
(3, 'CHF', 'Fr.', 'Swiss Franc', '', 0, 0, 0),
(4, 'CNY', '¥', 'Chinese Yuan', '', 0, 0, 0),
(5, 'AUD', '$', 'Australian Dollar', NULL, 0, 0, 0),
(6, 'CAD', '$', 'Canadian Dollar', NULL, 0, 0, 0),
(7, 'JPY', '¥', 'Japanese Yen', 'This currency does not support decimals. Passing a decimal amount will throw an error.', 0, 0, 0),
(8, 'GBP', '£', 'Pound Sterling ', '', 0, 0, 0),
(9, 'EUR', '€', 'Euro', '', 0, 0, 0),
(10, 'USD', '$', 'U.S. Dollar ', '', 0, 1, 1),
(11, 'VND', 'đ', 'Vietnam Dong', 'tien te viet nam', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `gateways`
--

CREATE TABLE IF NOT EXISTS `gateways` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `plugin` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `test_mode` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ipn_log` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `config` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `gateways`
--

INSERT INTO `gateways` (`id`, `name`, `description`, `enabled`, `plugin`, `test_mode`, `ipn_log`, `config`) VALUES
(1, 'PayPal Adaptive', 'PayPal Adaptive', 0, 'PaypalAdaptive', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `i18n`
--

CREATE TABLE IF NOT EXISTS `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `foreign_key` int(10) NOT NULL DEFAULT '0',
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `locale` (`locale`),
  KEY `model` (`model`),
  KEY `row_id` (`foreign_key`),
  KEY `field` (`field`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1170 ;

--
-- Dumping data for table `i18n`
--

INSERT INTO `i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(1, 'eng', 'Category', 1, 'name', 'Member Albums'),
(2, 'eng', 'Category', 2, 'name', 'Default Category'),
(3, 'eng', 'Category', 3, 'name', 'Default Category'),
(4, 'eng', 'Category', 4, 'name', 'Default Category'),
(5, 'eng', 'Category', 5, 'name', 'Default Category'),
(6, 'eng', 'Mailtemplate', 1, 'content', 'Hello,'),
(7, 'eng', 'Mailtemplate', 1, 'subject', ''),
(8, 'eng', 'Mailtemplate', 2, 'content', '<p>Best Regards,<br />Social Network Administration</p>'),
(9, 'eng', 'Mailtemplate', 2, 'subject', ''),
(10, 'eng', 'Mailtemplate', 3, 'content', '<p>Hello [recipient_title],</p>'),
(11, 'eng', 'Mailtemplate', 3, 'subject', ''),
(12, 'eng', 'Mailtemplate', 4, 'subject', ''),
(13, 'eng', 'Mailtemplate', 4, 'content', '<p>Best Regards,<br />Social Network Administration</p>'),
(14, 'eng', 'Mailtemplate', 5, 'content', '<p>[header]</p><p>[name] has sent you a message using the contact page:</p><p>Email : [sender_email]</p><p>Subject: [subject]</p><p>Message:</p><p>[message]</p><p>[footer]</p>'),
(15, 'eng', 'Mailtemplate', 5, 'subject', 'A member has sent you a message using the contact page.'),
(16, 'eng', 'Mailtemplate', 6, 'content', '<p>[header]</p>\r\n<p>You have been invited to join the event "[event_title]". Please click the following link to view it:</p>\r\n<p><a href="[event_link]">[event_title]</a></p>\r\n<p>[footer]</p>'),
(17, 'eng', 'Mailtemplate', 6, 'subject', 'You have been invited to join the event [event_title]'),
(18, 'eng', 'Mailtemplate', 7, 'content', '<p>[header]</p>\r\n<p>[sender_title] wants to be friends with you.</p>\r\n<p>[message]</p>\r\n<p><a href="[request_link]">Click here</a> to respond this request</p>\r\n<p>[footer]</p>'),
(19, 'eng', 'Mailtemplate', 7, 'subject', '[sender_title] has sent you a friend request.'),
(20, 'eng', 'Mailtemplate', 8, 'content', '<p>[header]</p>\r\n<p>You have been invited by [sender_title] to join our social network. To join, please follow the link below:</p>\r\n<p><a href="[signup_link]">[signup_link]</a></p>\r\n<p>----------------------------------------<br />[message]<br />----------------------------------------</p>\r\n<p>[footer]</p>'),
(21, 'eng', 'Mailtemplate', 8, 'subject', 'You have received an invitation to join our social network.'),
(22, 'eng', 'Mailtemplate', 9, 'content', '<p>[header]</p>\r\n<p>You have been invited to join the group "[group_title]". Please click the following link to view it:</p>\r\n<p><a href="[group_link]">[group_title]</a></p>\r\n<p>[footer]</p>'),
(23, 'eng', 'Mailtemplate', 9, 'subject', 'You have been invited to join the group [group_title]'),
(24, 'eng', 'Mailtemplate', 10, 'content', ''),
(25, 'eng', 'Mailtemplate', 10, 'subject', ''),
(26, 'eng', 'Mailtemplate', 11, 'content', '<p>[header]</p>\r\n<p>Thank you for joining our social network. Click the following link and enter your information below to login:</p>\r\n<p><a href="[login_link]">[login_link]</a></p>\r\n<p>Email: [email]</p><p>Password: [password]</p>\r\n<p>[footer]</p>'),
(27, 'eng', 'Mailtemplate', 11, 'subject', 'Welcome to [site_name]'),
(28, 'eng', 'Mailtemplate', 12, 'content', '<p>[header]</p>\r\n<p>Thank you for joining our social network. Please click the link below to validate your email:</p>\r\n<p><a href="[confirm_link]">[confirm_link]</a></p>\r\n<p>Email: [email]</p><p>Password: [password]</p>\r\n<p>[footer]</p>'),
(29, 'eng', 'Mailtemplate', 12, 'subject', 'Welcome to [site_name]'),
(30, 'eng', 'Mailtemplate', 13, 'content', '<p>[header]</p>\r\n<p><a href="[new_user_link]">[new_user_title]</a> has just signed up on [site_name]</p>\r\n<p>[footer]</p>'),
(31, 'eng', 'Mailtemplate', 13, 'subject', 'New Registration'),
(32, 'eng', 'Mailtemplate', 14, 'content', '<p>[header]</p>\r\n<p>A request to reset password was submitted. If it''s not you, please ignore this email.</p>\r\n<p>To reset your password, please click <a href="[reset_link]">[reset_link]</a></p>\r\n<p>[footer]</p>'),
(33, 'eng', 'Mailtemplate', 14, 'subject', 'Password Change Request'),
(34, 'eng', 'Mailtemplate', 15, 'content', '<p>[header]</p>\r\n<p>The admin has changed your password to [password]</p>\r\n<p>[footer]</p>'),
(35, 'eng', 'Mailtemplate', 15, 'subject', 'Your password has been changed'),
(36, 'eng', 'Mailtemplate', 16, 'content', '<p>[header]</p>\r\n<p>[element]</p>\r\n<p>[footer]</p>'),
(37, 'eng', 'Mailtemplate', 16, 'subject', 'Your Notifications Summary'),
(38, 'eng', 'CoreContent', 2, 'core_block_title', 'User Menu'),
(47, 'eng', 'CoreContent', 22, 'core_block_title', 'Contact'),
(48, 'eng', 'CoreContent', 24, 'core_block_title', 'Menu friend &amp; Search'),
(49, 'eng', 'CoreContent', 28, 'core_block_title', 'People'),
(50, 'eng', 'CoreContent', 30, 'core_block_title', 'Profile menu &amp; Users'),
(51, 'eng', 'CoreContent', 32, 'core_block_title', 'Recent Activities'),
(52, 'eng', 'CoreContent', 35, 'core_block_title', 'Blogs menu &amp; Search'),
(54, 'eng', 'CoreContent', 39, 'core_block_title', 'Blogs'),
(55, 'eng', 'CoreContent', 43, 'core_block_title', 'Blogs settings'),
(56, 'eng', 'CoreContent', 45, 'core_block_title', 'Other Entries'),
(57, 'eng', 'CoreContent', 47, 'core_block_title', 'Blog Detail'),
(58, 'eng', 'CoreContent', 49, 'core_block_title', 'List photos &amp; Search'),
(59, 'eng', 'CoreContent', 51, 'core_block_title', 'Photos'),
(61, 'eng', 'CoreContent', 56, 'core_block_title', 'Album''s photos'),
(62, 'eng', 'CoreContent', 58, 'core_block_title', 'Page Content'),
(63, 'eng', 'CoreContent', 62, 'core_block_title', 'Menu video &amp; Search'),
(64, 'eng', 'CoreContent', 66, 'core_block_title', 'Videos'),
(66, 'eng', 'CoreContent', 69, 'core_block_title', 'Tags &amp; Similar Videos'),
(67, 'eng', 'CoreContent', 71, 'core_block_title', 'Video''s content'),
(68, 'eng', 'CoreContent', 73, 'core_block_title', 'Topic''s setting'),
(69, 'eng', 'CoreContent', 77, 'core_block_title', 'Topics contents'),
(71, 'eng', 'CoreContent', 81, 'core_block_title', 'Groups menu &amp; Search'),
(73, 'eng', 'CoreContent', 85, 'core_block_title', 'Groups'),
(74, 'eng', 'CoreContent', 87, 'core_block_title', 'Group info &amp; setting'),
(76, 'eng', 'CoreContent', 91, 'core_block_title', 'Info &amp; Photos &amp; Recent Activities'),
(77, 'eng', 'CoreContent', 95, 'core_block_title', 'Events menu &amp; Search'),
(79, 'eng', 'CoreContent', 99, 'core_block_title', 'Events'),
(80, 'eng', 'CoreContent', 102, 'core_block_title', 'Event''s setting'),
(82, 'eng', 'CoreContent', 106, 'core_block_title', 'Event''s detail &amp; Message'),
(83, 'eng', 'CoreContent', 112, 'core_block_title', 'Forgot Password'),
(84, 'eng', 'CoreContent', 114, 'core_block_title', 'Topics menu &amp; Search'),
(86, 'eng', 'CoreContent', 118, 'core_block_title', 'Topics'),
(87, 'eng', 'CoreContent', 120, 'core_block_title', 'Themes & Languages'),
(88, 'eng', 'CoreContent', 122, 'core_block_title', 'Upcoming Events'),
(89, 'eng', 'CoreContent', 125, 'core_block_title', 'Popular Albums'),
(90, 'eng', 'CoreContent', 132, 'core_block_title', 'Who''s Online'),
(91, 'eng', 'CoreContent', 133, 'core_block_title', 'Recently Joined'),
(92, 'eng', 'CoreContent', 141, 'core_block_title', 'Login form'),
(93, 'eng', 'CoreContent', 143, 'core_block_title', 'Register Form'),
(95, 'eng', 'CoreContent', 147, 'core_block_title', ''),
(96, 'eng', 'CoreContent', 156, 'core_block_title', 'Popular Blogs'),
(97, 'eng', 'CoreContent', 119, 'core_block_title', ''),
(98, 'eng', 'CoreContent', 121, 'core_block_title', ''),
(99, 'eng', 'CoreContent', 137, 'core_block_title', ''),
(100, 'eng', 'CoreContent', 148, 'core_block_title', ''),
(101, 'eng', 'CoreContent', 152, 'core_block_title', ''),
(102, 'eng', 'CoreContent', 53, 'core_block_title', ''),
(103, 'eng', 'CoreContent', 55, 'core_block_title', ''),
(104, 'eng', 'CoreContent', 42, 'core_block_title', ''),
(105, 'eng', 'CoreContent', 44, 'core_block_title', ''),
(106, 'eng', 'CoreContent', 46, 'core_block_title', ''),
(107, 'eng', 'CoreContent', 34, 'core_block_title', ''),
(108, 'eng', 'CoreContent', 36, 'core_block_title', ''),
(109, 'eng', 'CoreContent', 38, 'core_block_title', ''),
(110, 'eng', 'CoreContent', 19, 'core_block_title', ''),
(111, 'eng', 'CoreContent', 21, 'core_block_title', ''),
(112, 'eng', 'CoreContent', 101, 'core_block_title', ''),
(113, 'eng', 'CoreContent', 103, 'core_block_title', ''),
(114, 'eng', 'CoreContent', 105, 'core_block_title', ''),
(115, 'eng', 'CoreContent', 94, 'core_block_title', ''),
(116, 'eng', 'CoreContent', 96, 'core_block_title', ''),
(117, 'eng', 'CoreContent', 98, 'core_block_title', ''),
(118, 'eng', 'CoreContent', 109, 'core_block_title', ''),
(119, 'eng', 'CoreContent', 111, 'core_block_title', ''),
(120, 'eng', 'CoreContent', 86, 'core_block_title', ''),
(121, 'eng', 'CoreContent', 88, 'core_block_title', ''),
(122, 'eng', 'CoreContent', 90, 'core_block_title', ''),
(123, 'eng', 'CoreContent', 80, 'core_block_title', ''),
(124, 'eng', 'CoreContent', 82, 'core_block_title', ''),
(125, 'eng', 'CoreContent', 84, 'core_block_title', ''),
(126, 'eng', 'CoreContent', 1, 'core_block_title', ''),
(127, 'eng', 'CoreContent', 3, 'core_block_title', ''),
(128, 'eng', 'CoreContent', 5, 'core_block_title', ''),
(129, 'eng', 'CoreContent', 23, 'core_block_title', ''),
(130, 'eng', 'CoreContent', 25, 'core_block_title', ''),
(131, 'eng', 'CoreContent', 27, 'core_block_title', ''),
(132, 'eng', 'CoreContent', 57, 'core_block_title', ''),
(133, 'eng', 'CoreContent', 48, 'core_block_title', ''),
(134, 'eng', 'CoreContent', 50, 'core_block_title', ''),
(135, 'eng', 'CoreContent', 29, 'core_block_title', ''),
(136, 'eng', 'CoreContent', 31, 'core_block_title', ''),
(137, 'eng', 'CoreContent', 140, 'core_block_title', ''),
(138, 'eng', 'CoreContent', 142, 'core_block_title', ''),
(139, 'eng', 'CoreContent', 72, 'core_block_title', ''),
(140, 'eng', 'CoreContent', 74, 'core_block_title', ''),
(141, 'eng', 'CoreContent', 76, 'core_block_title', ''),
(142, 'eng', 'CoreContent', 113, 'core_block_title', ''),
(143, 'eng', 'CoreContent', 115, 'core_block_title', ''),
(144, 'eng', 'CoreContent', 117, 'core_block_title', ''),
(145, 'eng', 'CoreContent', 68, 'core_block_title', ''),
(146, 'eng', 'CoreContent', 70, 'core_block_title', ''),
(147, 'eng', 'CoreContent', 61, 'core_block_title', ''),
(148, 'eng', 'CoreContent', 63, 'core_block_title', ''),
(149, 'eng', 'CoreContent', 65, 'core_block_title', ''),
(150, 'eng', 'CoreMenuItem', 1, 'name', 'Home'),
(151, 'eng', 'CoreMenuItem', 2, 'name', 'People'),
(152, 'eng', 'CoreMenuItem', 3, 'name', 'Blogs'),
(153, 'eng', 'CoreMenuItem', 4, 'name', 'Photos'),
(154, 'eng', 'CoreMenuItem', 5, 'name', 'Groups'),
(155, 'eng', 'CoreMenuItem', 6, 'name', 'Topics'),
(156, 'eng', 'CoreMenuItem', 7, 'name', 'Videos'),
(157, 'eng', 'CoreMenuItem', 8, 'name', 'Events'),
(158, 'eng', 'CoreContent', 146, 'core_block_title', ''),
(159, 'eng', 'CoreContent', 149, 'core_block_title', ''),
(160, 'eng', 'CoreContent', 150, 'core_block_title', ''),
(161, 'eng', 'CoreContent', 151, 'core_block_title', ''),
(162, 'eng', 'CoreMenuItem', 9, 'name', 'Contact Us'),
(163, 'eng', 'CoreMenuItem', 10, 'name', 'About Us'),
(164, 'eng', 'CoreMenuItem', 11, 'name', 'Terms of Service'),
(165, 'eng', 'CoreMenuItem', 12, 'name', 'Privacy Policy'),
(166, 'eng', 'CoreContent', 153, 'core_block_title', 'Footer Menu'),
(167, 'eng', 'CoreContent', 154, 'core_block_title', 'Login'),
(168, 'eng', 'CoreContent', 155, 'core_block_title', 'Popular Videos'),
(190, 'eng', 'CoreContent', 178, 'core_block_title', 'RSVP'),
(191, 'eng', 'CoreContent', 179, 'core_block_title', 'Event Attending'),
(192, 'eng', 'Mailtemplate', 17, 'content', '<p>[header]</p>\r\n<p>Thank you for subscribing to our social network! Your subscription is now active.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Please follow the link below to sign in.</p>\r\n<p><a href="[login_link]">[login_link]</a></p>\r\n<p>[footer]</p>'),
(193, 'eng', 'Mailtemplate', 17, 'subject', 'Your subscription is now active.'),
(194, 'eng', 'Mailtemplate', 18, 'content', '<p>[header]</p>\r\n<p>Your subscription cannot be completed because of a failed or missed payment.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Please follow the link below to try again.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p>[footer]</p>'),
(195, 'eng', 'Mailtemplate', 18, 'subject', 'Your subscription is Pending.'),
(196, 'eng', 'Mailtemplate', 19, 'content', '<p>[header]</p>\r\n<p>Your subscription has expired.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Please follow the link below to renew your subscription.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p></p>\r\n<p>[footer]</p>'),
(197, 'eng', 'Mailtemplate', 19, 'subject', 'Your subscription has expired.'),
(198, 'eng', 'Mailtemplate', 20, 'content', '<p>[header]</p>\r\n<p>Your [subscription_title] will be expired on [expire_time].</p>\r\n<p>Please follow the link below to renew your membership.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p>[footer]</p>'),
(199, 'eng', 'Mailtemplate', 20, 'subject', 'Subscription Reminder'),
(200, 'eng', 'Mailtemplate', 21, 'content', '<p>[header]</p>\r\n<p>Your subscription has been billed. You should receive an email from the</p>\r\n<p>payment gateway regarding this charge. Thank you for subscribing to our social</p>\r\n<p>network.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>You may follow the link below to sign in.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p>[footer]</p>'),
(201, 'eng', 'Mailtemplate', 21, 'subject', 'Your subscription has been billed'),
(202, 'eng', 'Mailtemplate', 22, 'content', '<p>[header]</p>\r\n<p>[sender_title] cancels his/her subscription.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Reason: [reason]</p>\r\n<p>[footer]</p>'),
(203, 'eng', 'Mailtemplate', 22, 'subject', '[sender_title] cancelled his subscription'),
(204, 'eng', 'Mailtemplate', 23, 'content', '<p>[header]</p>\r\n<p>[sender_title] sent a refund request.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Reason: [reason]</p>\r\n<p>[footer]</p>'),
(205, 'eng', 'Mailtemplate', 23, 'subject', 'Request Refund'),
(206, 'eng', 'Mailtemplate', 24, 'content', '<p>[header]</p>\r\n<p>Admin denies your refund request.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Reason: [reason]</p>\r\n<p>Please follow the link below to sign in.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p>[footer]</p>'),
(207, 'eng', 'Mailtemplate', 24, 'subject', 'Refund Deny'),
(208, 'eng', 'Mailtemplate', 25, 'content', '<p>[header]</p>\r\n<p></p>\r\n<p>Your subscription has been refunded and is no longer active.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Please follow the link below to choose a different package.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p></p>\r\n<p>[footer]</p>'),
(209, 'eng', 'Mailtemplate', 25, 'subject', 'Accept Refund'),
(210, 'eng', 'Mailtemplate', 26, 'content', '<p>[header]</p>\r\n<p>Your subscription has been cancelled.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Please follow the link below to re-subscribe.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p>[footer]</p>'),
(211, 'eng', 'Mailtemplate', 26, 'subject', 'Your subscription has been cancelled.'),
(212, 'eng', 'Mailtemplate', 27, 'content', '<p>Hi [shared_user]</p>\r\n<p>[user_shared] shared for you a link: <a href="[shared_link]">[shared_link]</a></p>\r\n<p>[shared_content]</p>\r\n<p>Please see my link guy</p>\r\n<p>[footer]</p>'),
(213, 'eng', 'Mailtemplate', 27, 'subject', '[user_shared] shared for you a link'),
(214, 'eng', 'CoreContent', 181, 'core_block_title', 'Closed Network Signup'),
(215, 'eng', 'Page', 1, 'title', 'Home Page'),
(216, 'eng', 'Page', 1, 'content', ''),
(217, 'eng', 'Page', 2, 'title', 'Contact Page'),
(218, 'eng', 'Page', 2, 'content', ''),
(219, 'eng', 'Page', 3, 'title', 'People Page'),
(220, 'eng', 'Page', 3, 'content', ''),
(221, 'eng', 'Page', 4, 'title', 'Profile Page'),
(222, 'eng', 'Page', 4, 'content', ''),
(223, 'eng', 'Page', 6, 'title', 'Blogs Browse Page'),
(224, 'eng', 'Page', 6, 'content', ''),
(225, 'eng', 'Page', 8, 'title', 'Blog Detail Page'),
(226, 'eng', 'Page', 8, 'content', ''),
(227, 'eng', 'Page', 9, 'title', 'Photos Browse Page'),
(228, 'eng', 'Page', 9, 'content', ''),
(229, 'eng', 'Page', 10, 'title', 'Album Detail Page'),
(230, 'eng', 'Page', 10, 'content', ''),
(231, 'eng', 'Page', 11, 'title', 'Photo Detail Page'),
(232, 'eng', 'Page', 11, 'content', ''),
(233, 'eng', 'Page', 12, 'title', 'Videos Browse Page'),
(234, 'eng', 'Page', 12, 'content', ''),
(235, 'eng', 'Page', 13, 'title', 'Video Detail Page'),
(236, 'eng', 'Page', 13, 'content', ''),
(237, 'eng', 'Page', 15, 'title', 'Topic Detail Page'),
(238, 'eng', 'Page', 15, 'content', ''),
(239, 'eng', 'Page', 16, 'title', 'Groups Browse Page'),
(240, 'eng', 'Page', 16, 'content', ''),
(241, 'eng', 'Page', 18, 'title', 'Group Detail Page'),
(242, 'eng', 'Page', 18, 'content', ''),
(243, 'eng', 'Page', 19, 'title', 'Events Browse Page'),
(244, 'eng', 'Page', 19, 'content', ''),
(245, 'eng', 'Page', 21, 'title', 'Event Detail Page'),
(246, 'eng', 'Page', 21, 'content', ''),
(247, 'eng', 'Page', 22, 'title', 'Forgot Password Page'),
(248, 'eng', 'Page', 22, 'content', ''),
(249, 'eng', 'Page', 23, 'title', 'Topics Browse Page'),
(250, 'eng', 'Page', 23, 'content', ''),
(251, 'eng', 'Page', 63, 'title', 'Landing Page'),
(252, 'eng', 'Page', 63, 'content', ''),
(253, 'eng', 'Page', 68, 'title', 'Site Header'),
(254, 'eng', 'Page', 68, 'content', ''),
(255, 'eng', 'Page', 69, 'title', 'Site Footer'),
(256, 'eng', 'Page', 69, 'content', ''),
(257, 'eng', 'Page', 90, 'title', 'Sign in'),
(258, 'eng', 'Page', 90, 'content', ''),
(259, 'eng', 'Page', 96, 'title', 'Sign up'),
(260, 'eng', 'Page', 96, 'content', ''),
(261, 'eng', 'Page', 100, 'title', 'About Us'),
(262, 'eng', 'Page', 100, 'content', '<p><span>This is the About Us page</span></p>'),
(263, 'eng', 'Page', 101, 'title', 'Terms of Service'),
(264, 'eng', 'Page', 101, 'content', '<p><span>This is the Terms of Service page</span></p>'),
(265, 'eng', 'Page', 102, 'title', 'Privacy Policy'),
(266, 'eng', 'Page', 102, 'content', '<p><span>This is the Privacy Policy page</span></p>'),
(267, 'eng', 'Category', 6, 'name', 'Default Category'),
(268, 'eng', 'Mailtemplate', 28, 'content', '<p>[header]</p>\r\n<p><a href="[sender_link]">[sender_title]</a> sent you a private message <small>[time]</small></p>\n<p><a href="[message_link]">Click here</a> to view your message</p>\r\n<p>[footer]</p>'),
(269, 'eng', 'Mailtemplate', 28, 'subject', '[sender_title] sent your a message on [site_name]'),
(270, 'eng', 'Mailtemplate', 29, 'content', '<p>[header]</p><p>Your account has been approved by site admin. Enjoy our social network!</p><p><a href="[link]">[link]</a></p><p>[footer]</p>'),
(271, 'eng', 'Mailtemplate', 29, 'subject', 'Your account has been approved'),
(272, 'eng', 'Mailtemplate', 30, 'content', '<p>[header]</p><p>Your account has been Unapproved by site admin. Contact site admin at the below link for more details.</p><p><a href="[link]">[link]</a></p><p>[footer]</p>'),
(273, 'eng', 'Mailtemplate', 30, 'subject', 'Your account has been Unapproved'),
(274, 'eng', 'Mailtemplate', 31, 'content', '<p>[header]</p><p>Your account has been enabled by site admin. Enjoy our social network!</p><p><a href="[link]">[link]</a></p><p>[footer]</p>'),
(275, 'eng', 'Mailtemplate', 31, 'subject', 'Your account has been enabled'),
(276, 'eng', 'Mailtemplate', 32, 'content', '<p>[header]</p><p>Your account has been disabled by site admin. Contact site admin at the below link for more details.</p><p><a href="[link]">[link]</a></p><p>[footer]</p>'),
(277, 'eng', 'Mailtemplate', 32, 'subject', 'Your account has been disabled'),
(278, 'eng', 'Country', 1, 'name', 'United States'),
(279, 'eng', 'Country', 2, 'name', 'United Arab Emirates'),
(280, 'eng', 'Country', 3, 'name', 'Afghanistan'),
(281, 'eng', 'Country', 4, 'name', 'Antigua and Barbuda'),
(282, 'eng', 'Country', 5, 'name', 'Anguilla'),
(283, 'eng', 'Country', 6, 'name', 'Albania'),
(284, 'eng', 'Country', 7, 'name', 'Armenia'),
(285, 'eng', 'Country', 8, 'name', 'Netherlands Antilles'),
(286, 'eng', 'Country', 9, 'name', 'Angola'),
(287, 'eng', 'Country', 10, 'name', 'Antarctica'),
(288, 'eng', 'Country', 11, 'name', 'Argentina'),
(289, 'eng', 'Country', 12, 'name', 'American Samoa'),
(290, 'eng', 'Country', 13, 'name', 'Austria'),
(291, 'eng', 'Country', 14, 'name', 'Australia'),
(292, 'eng', 'Country', 15, 'name', 'Aruba'),
(293, 'eng', 'Country', 16, 'name', 'Aland Islands'),
(294, 'eng', 'Country', 17, 'name', 'Azerbaijan'),
(295, 'eng', 'Country', 18, 'name', 'Bosnia and Herzegovina'),
(296, 'eng', 'Country', 19, 'name', 'Barbados'),
(297, 'eng', 'Country', 20, 'name', 'Bangladesh'),
(298, 'eng', 'Country', 21, 'name', 'Belgium'),
(299, 'eng', 'Country', 22, 'name', 'Burkina Faso'),
(300, 'eng', 'Country', 23, 'name', 'Bulgaria'),
(301, 'eng', 'Country', 24, 'name', 'Bahrain'),
(302, 'eng', 'Country', 25, 'name', 'Burundi'),
(303, 'eng', 'Country', 26, 'name', 'Benin'),
(304, 'eng', 'Country', 27, 'name', 'Saint Barthelemy'),
(305, 'eng', 'Country', 28, 'name', 'Bermuda'),
(306, 'eng', 'Country', 29, 'name', 'Brunei Darussalam'),
(307, 'eng', 'Country', 30, 'name', 'Bolivia'),
(308, 'eng', 'Country', 31, 'name', 'Brazil'),
(309, 'eng', 'Country', 32, 'name', 'Bahamas'),
(310, 'eng', 'Country', 33, 'name', 'Bhutan'),
(311, 'eng', 'Country', 34, 'name', 'Bouvet Island'),
(312, 'eng', 'Country', 35, 'name', 'Botswana'),
(313, 'eng', 'Country', 36, 'name', 'Belarus'),
(314, 'eng', 'Country', 37, 'name', 'Belize'),
(315, 'eng', 'Country', 38, 'name', 'Canada'),
(316, 'eng', 'Country', 39, 'name', 'Cocos (Keeling) Islands'),
(317, 'eng', 'Country', 40, 'name', 'Congo, the Democratic Republic of the'),
(318, 'eng', 'Country', 41, 'name', 'Central African Republic'),
(319, 'eng', 'Country', 42, 'name', 'Congo'),
(320, 'eng', 'Country', 43, 'name', 'Switzerland'),
(321, 'eng', 'Country', 44, 'name', 'Cote D''Ivoire'),
(322, 'eng', 'Country', 45, 'name', 'Cook Islands'),
(323, 'eng', 'Country', 46, 'name', 'Chile'),
(324, 'eng', 'Country', 47, 'name', 'Cameroon'),
(325, 'eng', 'Country', 48, 'name', 'China'),
(326, 'eng', 'Country', 49, 'name', 'Colombia'),
(327, 'eng', 'Country', 50, 'name', 'Costa Rica'),
(328, 'eng', 'Country', 51, 'name', 'Cuba'),
(329, 'eng', 'Country', 52, 'name', 'Cape Verde'),
(330, 'eng', 'Country', 53, 'name', 'Christmas Island'),
(331, 'eng', 'Country', 54, 'name', 'Cyprus'),
(332, 'eng', 'Country', 55, 'name', 'Czech Republic'),
(333, 'eng', 'Country', 56, 'name', 'Germany'),
(334, 'eng', 'Country', 57, 'name', 'Djibouti'),
(335, 'eng', 'Country', 58, 'name', 'Denmark'),
(336, 'eng', 'Country', 59, 'name', 'Dominica'),
(337, 'eng', 'Country', 60, 'name', 'Dominican Republic'),
(338, 'eng', 'Country', 61, 'name', 'Algeria'),
(339, 'eng', 'Country', 62, 'name', 'Ecuador'),
(340, 'eng', 'Country', 63, 'name', 'Estonia'),
(341, 'eng', 'Country', 64, 'name', 'Egypt'),
(342, 'eng', 'Country', 65, 'name', 'Western Sahara'),
(343, 'eng', 'Country', 66, 'name', 'Eritrea'),
(344, 'eng', 'Country', 67, 'name', 'Spain'),
(345, 'eng', 'Country', 68, 'name', 'Ethiopia'),
(346, 'eng', 'Country', 69, 'name', 'Finland'),
(347, 'eng', 'Country', 70, 'name', 'Fiji'),
(348, 'eng', 'Country', 71, 'name', 'Falkland Islands (Malvinas)'),
(349, 'eng', 'Country', 72, 'name', 'Micronesia, Federated States of'),
(350, 'eng', 'Country', 73, 'name', 'Faroe Islands'),
(351, 'eng', 'Country', 74, 'name', 'France'),
(352, 'eng', 'Country', 75, 'name', 'Gabon'),
(353, 'eng', 'Country', 76, 'name', 'United Kingdom'),
(354, 'eng', 'Country', 77, 'name', 'Grenada'),
(355, 'eng', 'Country', 78, 'name', 'Georgia'),
(356, 'eng', 'Country', 79, 'name', 'French Guiana'),
(357, 'eng', 'Country', 80, 'name', 'Guernsey'),
(358, 'eng', 'Country', 81, 'name', 'Ghana'),
(359, 'eng', 'Country', 82, 'name', 'Gibraltar'),
(360, 'eng', 'Country', 83, 'name', 'Greenland'),
(361, 'eng', 'Country', 84, 'name', 'Gambia'),
(362, 'eng', 'Country', 85, 'name', 'Guinea'),
(363, 'eng', 'Country', 86, 'name', 'Guadeloupe'),
(364, 'eng', 'Country', 87, 'name', 'Equatorial Guinea'),
(365, 'eng', 'Country', 88, 'name', 'Greece'),
(366, 'eng', 'Country', 89, 'name', 'South Georgia and the South Sandwich Islands'),
(367, 'eng', 'Country', 90, 'name', 'Guatemala'),
(368, 'eng', 'Country', 91, 'name', 'Guam'),
(369, 'eng', 'Country', 92, 'name', 'Guinea-Bissau'),
(370, 'eng', 'Country', 93, 'name', 'Guyana'),
(371, 'eng', 'Country', 94, 'name', 'Hong Kong'),
(372, 'eng', 'Country', 95, 'name', 'Heard Island and Mcdonald Islands'),
(373, 'eng', 'Country', 96, 'name', 'Honduras'),
(374, 'eng', 'Country', 97, 'name', 'Croatia'),
(375, 'eng', 'Country', 98, 'name', 'Haiti'),
(376, 'eng', 'Country', 99, 'name', 'Hungary'),
(377, 'eng', 'Country', 100, 'name', 'Indonesia'),
(378, 'eng', 'Country', 0, 'name', 'name'),
(379, 'eng', 'Country', 101, 'name', 'Ireland'),
(380, 'eng', 'Country', 102, 'name', 'Israel'),
(381, 'eng', 'Country', 103, 'name', 'Isle of Man'),
(382, 'eng', 'Country', 104, 'name', 'India'),
(383, 'eng', 'Country', 105, 'name', 'British Indian Ocean Territory'),
(384, 'eng', 'Country', 106, 'name', 'Iraq'),
(385, 'eng', 'Country', 107, 'name', 'Iran, Islamic Republic of'),
(386, 'eng', 'Country', 108, 'name', 'Iceland'),
(387, 'eng', 'Country', 109, 'name', 'Italy'),
(388, 'eng', 'Country', 110, 'name', 'Jamaica'),
(389, 'eng', 'Country', 111, 'name', 'Jordan'),
(390, 'eng', 'Country', 112, 'name', 'Japan'),
(391, 'eng', 'Country', 113, 'name', 'Kenya'),
(392, 'eng', 'Country', 114, 'name', 'Kyrgyzstan'),
(393, 'eng', 'Country', 115, 'name', 'Cambodia'),
(394, 'eng', 'Country', 116, 'name', 'Kiribati'),
(395, 'eng', 'Country', 117, 'name', 'Comoros'),
(396, 'eng', 'Country', 118, 'name', 'Saint Kitts and Nevis'),
(397, 'eng', 'Country', 119, 'name', 'Korea, Democratic People''s Republic of'),
(398, 'eng', 'Country', 120, 'name', 'Korea, Republic of'),
(399, 'eng', 'Country', 121, 'name', 'Kuwait'),
(400, 'eng', 'Country', 122, 'name', 'Cayman Islands'),
(401, 'eng', 'Country', 123, 'name', 'Kazakhstan'),
(402, 'eng', 'Country', 124, 'name', 'Lao People''s Democratic Republic'),
(403, 'eng', 'Country', 125, 'name', 'Lebanon'),
(404, 'eng', 'Country', 126, 'name', 'Saint Lucia'),
(405, 'eng', 'Country', 127, 'name', 'Liechtenstein'),
(406, 'eng', 'Country', 128, 'name', 'Sri Lanka'),
(407, 'eng', 'Country', 129, 'name', 'Liberia'),
(408, 'eng', 'Country', 130, 'name', 'Lesotho'),
(409, 'eng', 'Country', 131, 'name', 'Lithuania'),
(410, 'eng', 'Country', 132, 'name', 'Luxembourg'),
(411, 'eng', 'Country', 133, 'name', 'Latvia'),
(412, 'eng', 'Country', 134, 'name', 'Libyan Arab Jamahiriya'),
(413, 'eng', 'Country', 135, 'name', 'Morocco'),
(414, 'eng', 'Country', 136, 'name', 'Monaco'),
(415, 'eng', 'Country', 137, 'name', 'Moldova, Republic of'),
(416, 'eng', 'Country', 138, 'name', 'Montenegro'),
(417, 'eng', 'Country', 139, 'name', 'Madagascar'),
(418, 'eng', 'Country', 140, 'name', 'Marshall Islands'),
(419, 'eng', 'Country', 141, 'name', 'Macedonia, the Former Yugoslav Republic of'),
(420, 'eng', 'Country', 142, 'name', 'Mali'),
(421, 'eng', 'Country', 143, 'name', 'Myanmar'),
(422, 'eng', 'Country', 144, 'name', 'Mongolia'),
(423, 'eng', 'Country', 145, 'name', 'Macao'),
(424, 'eng', 'Country', 146, 'name', 'Northern Mariana Islands'),
(425, 'eng', 'Country', 147, 'name', 'Martinique'),
(426, 'eng', 'Country', 148, 'name', 'Mauritania'),
(427, 'eng', 'Country', 149, 'name', 'Montserrat'),
(428, 'eng', 'Country', 150, 'name', 'Malta'),
(429, 'eng', 'Country', 151, 'name', 'Mauritius'),
(430, 'eng', 'Country', 152, 'name', 'Maldives'),
(431, 'eng', 'Country', 153, 'name', 'Malawi'),
(432, 'eng', 'Country', 154, 'name', 'Mexico'),
(433, 'eng', 'Country', 155, 'name', 'Malaysia'),
(434, 'eng', 'Country', 156, 'name', 'Mozambique'),
(435, 'eng', 'Country', 157, 'name', 'Namibia'),
(436, 'eng', 'Country', 158, 'name', 'New Caledonia'),
(437, 'eng', 'Country', 159, 'name', 'Niger'),
(438, 'eng', 'Country', 160, 'name', 'Norfolk Island'),
(439, 'eng', 'Country', 161, 'name', 'Nigeria'),
(440, 'eng', 'Country', 162, 'name', 'Nicaragua'),
(441, 'eng', 'Country', 163, 'name', 'Netherlands'),
(442, 'eng', 'Country', 164, 'name', 'Norway'),
(443, 'eng', 'Country', 165, 'name', 'Nepal'),
(444, 'eng', 'Country', 166, 'name', 'Nauru'),
(445, 'eng', 'Country', 167, 'name', 'Niue'),
(446, 'eng', 'Country', 168, 'name', 'New Zealand'),
(447, 'eng', 'Country', 169, 'name', 'Oman'),
(448, 'eng', 'Country', 170, 'name', 'Panama'),
(449, 'eng', 'Country', 171, 'name', 'Peru'),
(450, 'eng', 'Country', 172, 'name', 'French Polynesia'),
(451, 'eng', 'Country', 173, 'name', 'Papua New Guinea'),
(452, 'eng', 'Country', 174, 'name', 'Philippines'),
(453, 'eng', 'Country', 175, 'name', 'Pakistan'),
(454, 'eng', 'Country', 176, 'name', 'Poland'),
(455, 'eng', 'Country', 177, 'name', 'Saint Pierre and Miquelon'),
(456, 'eng', 'Country', 178, 'name', 'Pitcairn'),
(457, 'eng', 'Country', 179, 'name', 'Puerto Rico'),
(458, 'eng', 'Country', 180, 'name', 'Palestinian Territory, Occupied'),
(459, 'eng', 'Country', 181, 'name', 'Portugal'),
(460, 'eng', 'Country', 182, 'name', 'Palau'),
(461, 'eng', 'Country', 183, 'name', 'Paraguay'),
(462, 'eng', 'Country', 184, 'name', 'Qatar'),
(463, 'eng', 'Country', 185, 'name', 'Reunion'),
(464, 'eng', 'Country', 186, 'name', 'Romania'),
(465, 'eng', 'Country', 187, 'name', 'Serbia'),
(466, 'eng', 'Country', 188, 'name', 'Russian Federation'),
(467, 'eng', 'Country', 189, 'name', 'Rwanda'),
(468, 'eng', 'Country', 190, 'name', 'Saudi Arabia'),
(469, 'eng', 'Country', 191, 'name', 'Solomon Islands'),
(470, 'eng', 'Country', 192, 'name', 'Seychelles'),
(471, 'eng', 'Country', 193, 'name', 'Sudan'),
(472, 'eng', 'Country', 194, 'name', 'Sweden'),
(473, 'eng', 'Country', 195, 'name', 'Singapore'),
(474, 'eng', 'Country', 196, 'name', 'Saint Helena'),
(475, 'eng', 'Country', 197, 'name', 'Slovenia'),
(476, 'eng', 'Country', 198, 'name', 'Svalbard and Jan Mayen'),
(477, 'eng', 'Country', 199, 'name', 'Slovakia'),
(478, 'eng', 'Country', 200, 'name', 'Sierra Leone'),
(479, 'eng', 'Country', 0, 'name', 'name'),
(480, 'eng', 'Country', 201, 'name', 'San Marino'),
(481, 'eng', 'Country', 202, 'name', 'Senegal'),
(482, 'eng', 'Country', 203, 'name', 'Somalia'),
(483, 'eng', 'Country', 204, 'name', 'Suriname'),
(484, 'eng', 'Country', 205, 'name', 'Sao Tome and Principe'),
(485, 'eng', 'Country', 206, 'name', 'El Salvador'),
(486, 'eng', 'Country', 207, 'name', 'Syrian Arab Republic'),
(487, 'eng', 'Country', 208, 'name', 'Swaziland'),
(488, 'eng', 'Country', 209, 'name', 'Turks and Caicos Islands'),
(489, 'eng', 'Country', 210, 'name', 'Chad'),
(490, 'eng', 'Country', 211, 'name', 'French Southern Territories'),
(491, 'eng', 'Country', 212, 'name', 'Togo'),
(492, 'eng', 'Country', 213, 'name', 'Thailand'),
(493, 'eng', 'Country', 214, 'name', 'Tajikistan'),
(494, 'eng', 'Country', 215, 'name', 'Tokelau'),
(495, 'eng', 'Country', 216, 'name', 'Timor-Leste'),
(496, 'eng', 'Country', 217, 'name', 'Turkmenistan'),
(497, 'eng', 'Country', 218, 'name', 'Tunisia'),
(498, 'eng', 'Country', 219, 'name', 'Tonga'),
(499, 'eng', 'Country', 220, 'name', 'Turkey'),
(500, 'eng', 'Country', 221, 'name', 'Trinidad and Tobago'),
(501, 'eng', 'Country', 222, 'name', 'Tuvalu'),
(502, 'eng', 'Country', 223, 'name', 'Taiwan, Province of China'),
(503, 'eng', 'Country', 224, 'name', 'Tanzania, United Republic of'),
(504, 'eng', 'Country', 225, 'name', 'Ukraine'),
(505, 'eng', 'Country', 226, 'name', 'Uganda'),
(506, 'eng', 'Country', 227, 'name', 'United States Minor Outlying Islands'),
(507, 'eng', 'Country', 228, 'name', 'Andorra'),
(508, 'eng', 'Country', 229, 'name', 'Uruguay'),
(509, 'eng', 'Country', 230, 'name', 'Uzbekistan'),
(510, 'eng', 'Country', 231, 'name', 'Holy See (Vatican City State)'),
(511, 'eng', 'Country', 232, 'name', 'Saint Vincent and the Grenadines'),
(512, 'eng', 'Country', 233, 'name', 'Venezuela'),
(513, 'eng', 'Country', 234, 'name', 'Virgin Islands, British'),
(514, 'eng', 'Country', 235, 'name', 'Virgin Islands, U.s.'),
(515, 'eng', 'Country', 236, 'name', 'Viet Nam'),
(516, 'eng', 'Country', 237, 'name', 'Vanuatu'),
(517, 'eng', 'Country', 238, 'name', 'Wallis and Futuna'),
(518, 'eng', 'Country', 239, 'name', 'Samoa'),
(519, 'eng', 'Country', 240, 'name', 'Yemen'),
(520, 'eng', 'Country', 241, 'name', 'Mayotte'),
(521, 'eng', 'Country', 242, 'name', 'South Africa'),
(522, 'eng', 'Country', 243, 'name', 'Zambia'),
(523, 'eng', 'Country', 244, 'name', 'Zimbabwe'),
(524, 'eng', 'State', 1, 'name', 'Alaska'),
(525, 'eng', 'State', 2, 'name', 'Alabama'),
(526, 'eng', 'State', 3, 'name', 'Arkansas'),
(527, 'eng', 'State', 4, 'name', 'Arizona'),
(528, 'eng', 'State', 5, 'name', 'California'),
(529, 'eng', 'State', 6, 'name', 'Colorado'),
(530, 'eng', 'State', 7, 'name', 'Connecticut'),
(531, 'eng', 'State', 8, 'name', 'District of Columbia'),
(532, 'eng', 'State', 9, 'name', 'Delaware'),
(533, 'eng', 'State', 10, 'name', 'Florida'),
(534, 'eng', 'State', 11, 'name', 'Georgia'),
(535, 'eng', 'State', 12, 'name', 'Hawaii'),
(536, 'eng', 'State', 13, 'name', 'Iowa'),
(537, 'eng', 'State', 14, 'name', 'Idaho'),
(538, 'eng', 'State', 15, 'name', 'Illinois'),
(539, 'eng', 'State', 16, 'name', 'Indiana'),
(540, 'eng', 'State', 17, 'name', 'Kansas'),
(541, 'eng', 'State', 18, 'name', 'Kentucky'),
(542, 'eng', 'State', 19, 'name', 'Louisiana'),
(543, 'eng', 'State', 20, 'name', 'Massachusetts'),
(544, 'eng', 'State', 21, 'name', 'Maryland'),
(545, 'eng', 'State', 22, 'name', 'Maine'),
(546, 'eng', 'State', 23, 'name', 'Michigan'),
(547, 'eng', 'State', 24, 'name', 'Minnesota'),
(548, 'eng', 'State', 25, 'name', 'Missouri'),
(549, 'eng', 'State', 26, 'name', 'Mississippi'),
(550, 'eng', 'State', 27, 'name', 'Montana'),
(551, 'eng', 'State', 28, 'name', 'North Carolina'),
(552, 'eng', 'State', 29, 'name', 'North Dakota'),
(553, 'eng', 'State', 30, 'name', 'Nebraska'),
(554, 'eng', 'State', 31, 'name', 'New Hampshire'),
(555, 'eng', 'State', 32, 'name', 'New Jersey'),
(556, 'eng', 'State', 33, 'name', 'New Mexico'),
(557, 'eng', 'State', 34, 'name', 'Nevada'),
(558, 'eng', 'State', 35, 'name', 'New York'),
(559, 'eng', 'State', 36, 'name', 'Ohio'),
(560, 'eng', 'State', 37, 'name', 'Oklahoma'),
(561, 'eng', 'State', 38, 'name', 'Oregon'),
(562, 'eng', 'State', 39, 'name', 'Pennsylvania'),
(563, 'eng', 'State', 40, 'name', 'Rhode Island'),
(564, 'eng', 'State', 41, 'name', 'South Carolina'),
(565, 'eng', 'State', 42, 'name', 'South Dakota'),
(566, 'eng', 'State', 43, 'name', 'Tennessee'),
(567, 'eng', 'State', 44, 'name', 'Texas'),
(568, 'eng', 'State', 45, 'name', 'Utah'),
(569, 'eng', 'State', 46, 'name', 'Virginia'),
(570, 'eng', 'State', 47, 'name', 'Vermont'),
(571, 'eng', 'State', 48, 'name', 'Washington'),
(572, 'eng', 'State', 49, 'name', 'Wisconsin'),
(573, 'eng', 'State', 50, 'name', 'West Virginia'),
(574, 'eng', 'State', 51, 'name', 'Wyoming'),
(575, 'vie', 'Category', 1, 'name', 'Member Albums'),
(576, 'vie', 'Category', 2, 'name', 'Default Category'),
(577, 'vie', 'Category', 3, 'name', 'Default Category'),
(578, 'vie', 'Category', 4, 'name', 'Default Category'),
(579, 'vie', 'Category', 5, 'name', 'Default Category'),
(580, 'vie', 'Mailtemplate', 1, 'content', 'Hello,'),
(581, 'vie', 'Mailtemplate', 1, 'subject', ''),
(582, 'vie', 'Mailtemplate', 2, 'content', '<p>Best Regards,<br />Social Network Administration</p>'),
(583, 'vie', 'Mailtemplate', 2, 'subject', ''),
(584, 'vie', 'Mailtemplate', 3, 'content', '<p>Hello [recipient_title],</p>'),
(585, 'vie', 'Mailtemplate', 3, 'subject', ''),
(586, 'vie', 'Mailtemplate', 4, 'subject', ''),
(587, 'vie', 'Mailtemplate', 4, 'content', '<p>Best Regards,<br />Social Network Administration</p>'),
(588, 'vie', 'Mailtemplate', 5, 'content', '<p>[header]</p><p>[name] has sent you a message using the contact page:</p><p>Email : [sender_email]</p><p>Subject: [subject]</p><p>Message:</p><p>[message]</p><p>[footer]</p>'),
(589, 'vie', 'Mailtemplate', 5, 'subject', 'A member has sent you a message using the contact page.'),
(590, 'vie', 'Mailtemplate', 6, 'content', '<p>[header]</p>\r\n<p>You have been invited to join the event "[event_title]". Please click the following link to view it:</p>\r\n<p><a href="[event_link]">[event_title]</a></p>\r\n<p>[footer]</p>'),
(591, 'vie', 'Mailtemplate', 6, 'subject', 'You have been invited to join the event [event_title]'),
(592, 'vie', 'Mailtemplate', 7, 'content', '<p>[header]</p>\r\n<p>[sender_title] wants to be friends with you.</p>\r\n<p>[message]</p>\r\n<p><a href="[request_link]">Click here</a> to respond this request</p>\r\n<p>[footer]</p>'),
(593, 'vie', 'Mailtemplate', 7, 'subject', '[sender_title] has sent you a friend request.'),
(594, 'vie', 'Mailtemplate', 8, 'content', '<p>[header]</p>\r\n<p>You have been invited by [sender_title] to join our social network. To join, please follow the link below:</p>\r\n<p><a href="[signup_link]">[signup_link]</a></p>\r\n<p>----------------------------------------<br />[message]<br />----------------------------------------</p>\r\n<p>[footer]</p>'),
(595, 'vie', 'Mailtemplate', 8, 'subject', 'You have received an invitation to join our social network.'),
(596, 'vie', 'Mailtemplate', 9, 'content', '<p>[header]</p>\r\n<p>You have been invited to join the group "[group_title]". Please click the following link to view it:</p>\r\n<p><a href="[group_link]">[group_title]</a></p>\r\n<p>[footer]</p>'),
(597, 'vie', 'Mailtemplate', 9, 'subject', 'You have been invited to join the group [group_title]'),
(598, 'vie', 'Mailtemplate', 10, 'content', ''),
(599, 'vie', 'Mailtemplate', 10, 'subject', ''),
(600, 'vie', 'Mailtemplate', 11, 'content', '<p>[header]</p>\r\n<p>Thank you for joining our social network. Click the following link and enter your information below to login:</p>\r\n<p><a href="[login_link]">[login_link]</a></p>\r\n<p>Email: [email]</p><p>Password: [password]</p>\r\n<p>[footer]</p>'),
(601, 'vie', 'Mailtemplate', 11, 'subject', 'Welcome to [site_name]'),
(602, 'vie', 'Mailtemplate', 12, 'content', '<p>[header]</p>\r\n<p>Thank you for joining our social network. Please click the link below to validate your email:</p>\r\n<p><a href="[confirm_link]">[confirm_link]</a></p>\r\n<p>Email: [email]</p><p>Password: [password]</p>\r\n<p>[footer]</p>'),
(603, 'vie', 'Mailtemplate', 12, 'subject', 'Welcome to [site_name]'),
(604, 'vie', 'Mailtemplate', 13, 'content', '<p>[header]</p>\r\n<p><a href="[new_user_link]">[new_user_title]</a> has just signed up on [site_name]</p>\r\n<p>[footer]</p>'),
(605, 'vie', 'Mailtemplate', 13, 'subject', 'New Registration'),
(606, 'vie', 'Mailtemplate', 14, 'content', '<p>[header]</p>\r\n<p>A request to reset password was submitted. If it''s not you, please ignore this email.</p>\r\n<p>To reset your password, please click <a href="[reset_link]">[reset_link]</a></p>\r\n<p>[footer]</p>'),
(607, 'vie', 'Mailtemplate', 14, 'subject', 'Password Change Request'),
(608, 'vie', 'Mailtemplate', 15, 'content', '<p>[header]</p>\r\n<p>The admin has changed your password to [password]</p>\r\n<p>[footer]</p>'),
(609, 'vie', 'Mailtemplate', 15, 'subject', 'Your password has been changed'),
(610, 'vie', 'Mailtemplate', 16, 'content', '<p>[header]</p>\r\n<p>[element]</p>\r\n<p>[footer]</p>'),
(611, 'vie', 'Mailtemplate', 16, 'subject', 'Your Notifications Summary'),
(612, 'vie', 'CoreContent', 2, 'core_block_title', 'User Menu'),
(621, 'vie', 'CoreContent', 22, 'core_block_title', 'Contact'),
(622, 'vie', 'CoreContent', 24, 'core_block_title', 'Menu friend & Search'),
(623, 'vie', 'CoreContent', 28, 'core_block_title', 'People'),
(624, 'vie', 'CoreContent', 30, 'core_block_title', 'Profile menu & Users'),
(625, 'vie', 'CoreContent', 32, 'core_block_title', 'Recent Activities'),
(626, 'vie', 'CoreContent', 35, 'core_block_title', 'Blogs menu & Search'),
(628, 'vie', 'CoreContent', 39, 'core_block_title', 'Blogs'),
(629, 'vie', 'CoreContent', 43, 'core_block_title', 'Blogs settings'),
(630, 'vie', 'CoreContent', 45, 'core_block_title', 'Other Entries'),
(631, 'vie', 'CoreContent', 47, 'core_block_title', 'Blog Detail'),
(632, 'vie', 'CoreContent', 49, 'core_block_title', 'List photos & Search'),
(633, 'vie', 'CoreContent', 51, 'core_block_title', 'Photos'),
(635, 'vie', 'CoreContent', 56, 'core_block_title', 'Album''s photos'),
(636, 'vie', 'CoreContent', 58, 'core_block_title', 'Page Content'),
(637, 'vie', 'CoreContent', 62, 'core_block_title', 'Menu video & Search'),
(638, 'vie', 'CoreContent', 66, 'core_block_title', 'Videos'),
(640, 'vie', 'CoreContent', 69, 'core_block_title', 'Tags & Similar Videos'),
(641, 'vie', 'CoreContent', 71, 'core_block_title', 'Video''s content'),
(642, 'vie', 'CoreContent', 73, 'core_block_title', 'Topic''s setting'),
(643, 'vie', 'CoreContent', 77, 'core_block_title', 'Topics contents'),
(645, 'vie', 'CoreContent', 81, 'core_block_title', 'Groups menu & Search'),
(647, 'vie', 'CoreContent', 85, 'core_block_title', 'Groups'),
(648, 'vie', 'CoreContent', 87, 'core_block_title', 'Group info & setting'),
(650, 'vie', 'CoreContent', 91, 'core_block_title', 'Info & Photos & Recent Activities'),
(651, 'vie', 'CoreContent', 95, 'core_block_title', 'Events menu & Search'),
(653, 'vie', 'CoreContent', 99, 'core_block_title', 'Events'),
(654, 'vie', 'CoreContent', 102, 'core_block_title', 'Event''s setting'),
(656, 'vie', 'CoreContent', 106, 'core_block_title', 'Event''s detail & Message'),
(657, 'vie', 'CoreContent', 112, 'core_block_title', 'Forgot Password'),
(658, 'vie', 'CoreContent', 114, 'core_block_title', 'Topics menu & Search'),
(660, 'vie', 'CoreContent', 118, 'core_block_title', 'Topics'),
(661, 'vie', 'CoreContent', 120, 'core_block_title', 'Themes & Languages'),
(662, 'vie', 'CoreContent', 122, 'core_block_title', 'Upcoming Events'),
(663, 'vie', 'CoreContent', 125, 'core_block_title', 'Popular Albums'),
(664, 'vie', 'CoreContent', 132, 'core_block_title', 'Who''s Online'),
(665, 'vie', 'CoreContent', 133, 'core_block_title', 'Recently Joined'),
(666, 'vie', 'CoreContent', 141, 'core_block_title', 'Login form'),
(667, 'vie', 'CoreContent', 143, 'core_block_title', 'Register Form'),
(669, 'vie', 'CoreContent', 147, 'core_block_title', ''),
(670, 'vie', 'CoreContent', 156, 'core_block_title', 'Popular Blogs'),
(671, 'vie', 'CoreContent', 119, 'core_block_title', ''),
(672, 'vie', 'CoreContent', 121, 'core_block_title', ''),
(673, 'vie', 'CoreContent', 137, 'core_block_title', ''),
(674, 'vie', 'CoreContent', 148, 'core_block_title', ''),
(675, 'vie', 'CoreContent', 152, 'core_block_title', ''),
(676, 'vie', 'CoreContent', 53, 'core_block_title', ''),
(677, 'vie', 'CoreContent', 55, 'core_block_title', ''),
(678, 'vie', 'CoreContent', 42, 'core_block_title', ''),
(679, 'vie', 'CoreContent', 44, 'core_block_title', ''),
(680, 'vie', 'CoreContent', 46, 'core_block_title', ''),
(681, 'vie', 'CoreContent', 34, 'core_block_title', ''),
(682, 'vie', 'CoreContent', 36, 'core_block_title', ''),
(683, 'vie', 'CoreContent', 38, 'core_block_title', ''),
(684, 'vie', 'CoreContent', 19, 'core_block_title', ''),
(685, 'vie', 'CoreContent', 21, 'core_block_title', ''),
(686, 'vie', 'CoreContent', 101, 'core_block_title', ''),
(687, 'vie', 'CoreContent', 103, 'core_block_title', ''),
(688, 'vie', 'CoreContent', 105, 'core_block_title', ''),
(689, 'vie', 'CoreContent', 94, 'core_block_title', ''),
(690, 'vie', 'CoreContent', 96, 'core_block_title', ''),
(691, 'vie', 'CoreContent', 98, 'core_block_title', ''),
(692, 'vie', 'CoreContent', 109, 'core_block_title', ''),
(693, 'vie', 'CoreContent', 111, 'core_block_title', ''),
(694, 'vie', 'CoreContent', 86, 'core_block_title', ''),
(695, 'vie', 'CoreContent', 88, 'core_block_title', ''),
(696, 'vie', 'CoreContent', 90, 'core_block_title', ''),
(697, 'vie', 'CoreContent', 80, 'core_block_title', ''),
(698, 'vie', 'CoreContent', 82, 'core_block_title', ''),
(699, 'vie', 'CoreContent', 84, 'core_block_title', ''),
(700, 'vie', 'CoreContent', 1, 'core_block_title', ''),
(701, 'vie', 'CoreContent', 3, 'core_block_title', ''),
(702, 'vie', 'CoreContent', 5, 'core_block_title', ''),
(703, 'vie', 'CoreContent', 23, 'core_block_title', ''),
(704, 'vie', 'CoreContent', 25, 'core_block_title', ''),
(705, 'vie', 'CoreContent', 27, 'core_block_title', ''),
(706, 'vie', 'CoreContent', 57, 'core_block_title', ''),
(707, 'vie', 'CoreContent', 48, 'core_block_title', ''),
(708, 'vie', 'CoreContent', 50, 'core_block_title', ''),
(709, 'vie', 'CoreContent', 29, 'core_block_title', ''),
(710, 'vie', 'CoreContent', 31, 'core_block_title', ''),
(711, 'vie', 'CoreContent', 140, 'core_block_title', ''),
(712, 'vie', 'CoreContent', 142, 'core_block_title', ''),
(713, 'vie', 'CoreContent', 72, 'core_block_title', ''),
(714, 'vie', 'CoreContent', 74, 'core_block_title', ''),
(715, 'vie', 'CoreContent', 76, 'core_block_title', ''),
(716, 'vie', 'CoreContent', 113, 'core_block_title', ''),
(717, 'vie', 'CoreContent', 115, 'core_block_title', ''),
(718, 'vie', 'CoreContent', 117, 'core_block_title', ''),
(719, 'vie', 'CoreContent', 68, 'core_block_title', ''),
(720, 'vie', 'CoreContent', 70, 'core_block_title', ''),
(721, 'vie', 'CoreContent', 61, 'core_block_title', ''),
(722, 'vie', 'CoreContent', 63, 'core_block_title', ''),
(723, 'vie', 'CoreContent', 65, 'core_block_title', ''),
(724, 'vie', 'CoreMenuItem', 1, 'name', 'Home'),
(725, 'vie', 'CoreMenuItem', 2, 'name', 'People'),
(726, 'vie', 'CoreMenuItem', 3, 'name', 'Blogs'),
(727, 'vie', 'CoreMenuItem', 4, 'name', 'Photos'),
(728, 'vie', 'CoreMenuItem', 5, 'name', 'Groups'),
(729, 'vie', 'CoreMenuItem', 6, 'name', 'Topics'),
(730, 'vie', 'CoreMenuItem', 7, 'name', 'Videos'),
(731, 'vie', 'CoreMenuItem', 8, 'name', 'Events'),
(732, 'vie', 'CoreContent', 146, 'core_block_title', ''),
(733, 'vie', 'CoreContent', 149, 'core_block_title', ''),
(734, 'vie', 'CoreContent', 150, 'core_block_title', ''),
(735, 'vie', 'CoreContent', 151, 'core_block_title', ''),
(736, 'vie', 'CoreMenuItem', 9, 'name', 'Contact Us'),
(737, 'vie', 'CoreMenuItem', 10, 'name', 'About Us'),
(738, 'vie', 'CoreMenuItem', 11, 'name', 'Terms of Service'),
(739, 'vie', 'CoreMenuItem', 12, 'name', 'Privacy Policy'),
(740, 'vie', 'CoreContent', 153, 'core_block_title', 'Footer Menu'),
(741, 'vie', 'CoreContent', 154, 'core_block_title', 'Login'),
(742, 'vie', 'CoreContent', 155, 'core_block_title', 'Popular Videos'),
(764, 'vie', 'CoreContent', 178, 'core_block_title', 'RSVP'),
(765, 'vie', 'CoreContent', 179, 'core_block_title', 'Event Attending'),
(766, 'vie', 'Mailtemplate', 17, 'content', '<p>[header]</p>\r\n<p>Thank you for subscribing to our social network! Your subscription is now active.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Please follow the link below to sign in.</p>\r\n<p><a href="[login_link]">[login_link]</a></p>\r\n<p>[footer]</p>'),
(767, 'vie', 'Mailtemplate', 17, 'subject', 'Your subscription is now active.'),
(768, 'vie', 'Mailtemplate', 18, 'content', '<p>[header]</p>\r\n<p>Your subscription cannot be completed because of a failed or missed payment.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Please follow the link below to try again.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p>[footer]</p>'),
(769, 'vie', 'Mailtemplate', 18, 'subject', 'Your subscription is Pending.'),
(770, 'vie', 'Mailtemplate', 19, 'content', '<p>[header]</p>\r\n<p>Your subscription has expired.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Please follow the link below to renew your subscription.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p></p>\r\n<p>[footer]</p>'),
(771, 'vie', 'Mailtemplate', 19, 'subject', 'Your subscription has expired.'),
(772, 'vie', 'Mailtemplate', 20, 'content', '<p>[header]</p>\r\n<p>Your [subscription_title] will be expired on [expire_time].</p>\r\n<p>Please follow the link below to renew your membership.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p>[footer]</p>'),
(773, 'vie', 'Mailtemplate', 20, 'subject', 'Subscription Reminder'),
(774, 'vie', 'Mailtemplate', 21, 'content', '<p>[header]</p>\r\n<p>Your subscription has been billed. You should receive an email from the</p>\r\n<p>payment gateway regarding this charge. Thank you for subscribing to our social</p>\r\n<p>network.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>You may follow the link below to sign in.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p>[footer]</p>'),
(775, 'vie', 'Mailtemplate', 21, 'subject', 'Your subscription has been billed'),
(776, 'vie', 'Mailtemplate', 22, 'content', '<p>[header]</p>\r\n<p>[sender_title] cancels his/her subscription.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Reason: [reason]</p>\r\n<p>[footer]</p>'),
(777, 'vie', 'Mailtemplate', 22, 'subject', '[sender_title] cancelled his subscription'),
(778, 'vie', 'Mailtemplate', 23, 'content', '<p>[header]</p>\r\n<p>[sender_title] sent a refund request.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Reason: [reason]</p>\r\n<p>[footer]</p>'),
(779, 'vie', 'Mailtemplate', 23, 'subject', 'Request Refund'),
(780, 'vie', 'Mailtemplate', 24, 'content', '<p>[header]</p>\r\n<p>Admin denies your refund request.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Reason: [reason]</p>\r\n<p>Please follow the link below to sign in.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p>[footer]</p>'),
(781, 'vie', 'Mailtemplate', 24, 'subject', 'Refund Deny'),
(782, 'vie', 'Mailtemplate', 25, 'content', '<p>[header]</p>\r\n<p></p>\r\n<p>Your subscription has been refunded and is no longer active.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Please follow the link below to choose a different package.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p></p>\r\n<p>[footer]</p>'),
(783, 'vie', 'Mailtemplate', 25, 'subject', 'Accept Refund');
INSERT INTO `i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(784, 'vie', 'Mailtemplate', 26, 'content', '<p>[header]</p>\r\n<p>Your subscription has been cancelled.</p>\r\n<p>[subscription_title]</p>\r\n<p>[subscription_description]</p>\r\n<p>[plan_title]</p>\r\n<p>[plan_description]</p>\r\n<p>Please follow the link below to re-subscribe.</p>\r\n<p><a href="[link]">[link]</a></p>\r\n<p>[footer]</p>'),
(785, 'vie', 'Mailtemplate', 26, 'subject', 'Your subscription has been cancelled.'),
(786, 'vie', 'Mailtemplate', 27, 'content', '<p>Hi [shared_user]</p>\r\n<p>[user_shared] shared for you a link: <a href="[shared_link]">[shared_link]</a></p>\r\n<p>[shared_content]</p>\r\n<p>Please see my link guy</p>\r\n<p>[footer]</p>'),
(787, 'vie', 'Mailtemplate', 27, 'subject', '[user_shared] shared for you a link'),
(788, 'vie', 'CoreContent', 181, 'core_block_title', 'Closed Network Signup'),
(789, 'vie', 'Page', 1, 'title', 'Home Page'),
(790, 'vie', 'Page', 1, 'content', ''),
(791, 'vie', 'Page', 2, 'title', 'Contact Page'),
(792, 'vie', 'Page', 2, 'content', ''),
(793, 'vie', 'Page', 3, 'title', 'People Page'),
(794, 'vie', 'Page', 3, 'content', ''),
(795, 'vie', 'Page', 4, 'title', 'Profile Page'),
(796, 'vie', 'Page', 4, 'content', ''),
(797, 'vie', 'Page', 6, 'title', 'Blogs Browse Page'),
(798, 'vie', 'Page', 6, 'content', ''),
(799, 'vie', 'Page', 8, 'title', 'Blog Detail Page'),
(800, 'vie', 'Page', 8, 'content', ''),
(801, 'vie', 'Page', 9, 'title', 'Photos Browse Page'),
(802, 'vie', 'Page', 9, 'content', ''),
(803, 'vie', 'Page', 10, 'title', 'Album Detail Page'),
(804, 'vie', 'Page', 10, 'content', ''),
(805, 'vie', 'Page', 11, 'title', 'Photo Detail Page'),
(806, 'vie', 'Page', 11, 'content', ''),
(807, 'vie', 'Page', 12, 'title', 'Videos Browse Page'),
(808, 'vie', 'Page', 12, 'content', ''),
(809, 'vie', 'Page', 13, 'title', 'Video Detail Page'),
(810, 'vie', 'Page', 13, 'content', ''),
(811, 'vie', 'Page', 15, 'title', 'Topic Detail Page'),
(812, 'vie', 'Page', 15, 'content', ''),
(813, 'vie', 'Page', 16, 'title', 'Groups Browse Page'),
(814, 'vie', 'Page', 16, 'content', ''),
(815, 'vie', 'Page', 18, 'title', 'Group Detail Page'),
(816, 'vie', 'Page', 18, 'content', ''),
(817, 'vie', 'Page', 19, 'title', 'Events Browse Page'),
(818, 'vie', 'Page', 19, 'content', ''),
(819, 'vie', 'Page', 21, 'title', 'Event Detail Page'),
(820, 'vie', 'Page', 21, 'content', ''),
(821, 'vie', 'Page', 22, 'title', 'Forgot Password Page'),
(822, 'vie', 'Page', 22, 'content', ''),
(823, 'vie', 'Page', 23, 'title', 'Topics Browse Page'),
(824, 'vie', 'Page', 23, 'content', ''),
(825, 'vie', 'Page', 63, 'title', 'Landing Page'),
(826, 'vie', 'Page', 63, 'content', ''),
(827, 'vie', 'Page', 68, 'title', 'Site Header'),
(828, 'vie', 'Page', 68, 'content', ''),
(829, 'vie', 'Page', 69, 'title', 'Site Footer'),
(830, 'vie', 'Page', 69, 'content', ''),
(831, 'vie', 'Page', 90, 'title', 'Sign in'),
(832, 'vie', 'Page', 90, 'content', ''),
(833, 'vie', 'Page', 96, 'title', 'Sign up'),
(834, 'vie', 'Page', 96, 'content', ''),
(835, 'vie', 'Page', 100, 'title', 'About Us'),
(836, 'vie', 'Page', 100, 'content', '<p><span>This is the About Us page</span></p>'),
(837, 'vie', 'Page', 101, 'title', 'Terms of Service'),
(838, 'vie', 'Page', 101, 'content', '<p><span>This is the Terms of Service page</span></p>'),
(839, 'vie', 'Page', 102, 'title', 'Privacy Policy'),
(840, 'vie', 'Page', 102, 'content', '<p><span>This is the Privacy Policy page</span></p>'),
(841, 'vie', 'Category', 6, 'name', 'Default Category'),
(842, 'vie', 'Mailtemplate', 28, 'content', '<p>[header]</p>\r\n<p><a href="[sender_link]">[sender_title]</a> sent you a private message <small>[time]</small></p>\n<p><a href="[message_link]">Click here</a> to view your message</p>\r\n<p>[footer]</p>'),
(843, 'vie', 'Mailtemplate', 28, 'subject', '[sender_title] sent your a message on [site_name]'),
(844, 'vie', 'Mailtemplate', 29, 'content', '<p>[header]</p><p>Your account has been approved by site admin. Enjoy our social network!</p><p><a href="[link]">[link]</a></p><p>[footer]</p>'),
(845, 'vie', 'Mailtemplate', 29, 'subject', 'Your account has been approved'),
(846, 'vie', 'Mailtemplate', 30, 'content', '<p>[header]</p><p>Your account has been Unapproved by site admin. Contact site admin at the below link for more details.</p><p><a href="[link]">[link]</a></p><p>[footer]</p>'),
(847, 'vie', 'Mailtemplate', 30, 'subject', 'Your account has been Unapproved'),
(848, 'vie', 'Mailtemplate', 31, 'content', '<p>[header]</p><p>Your account has been enabled by site admin. Enjoy our social network!</p><p><a href="[link]">[link]</a></p><p>[footer]</p>'),
(849, 'vie', 'Mailtemplate', 31, 'subject', 'Your account has been enabled'),
(850, 'vie', 'Mailtemplate', 32, 'content', '<p>[header]</p><p>Your account has been disabled by site admin. Contact site admin at the below link for more details.</p><p><a href="[link]">[link]</a></p><p>[footer]</p>'),
(851, 'vie', 'Mailtemplate', 32, 'subject', 'Your account has been disabled'),
(852, 'vie', 'Country', 1, 'name', 'United States'),
(853, 'vie', 'Country', 2, 'name', 'United Arab Emirates'),
(854, 'vie', 'Country', 3, 'name', 'Afghanistan'),
(855, 'vie', 'Country', 4, 'name', 'Antigua and Barbuda'),
(856, 'vie', 'Country', 5, 'name', 'Anguilla'),
(857, 'vie', 'Country', 6, 'name', 'Albania'),
(858, 'vie', 'Country', 7, 'name', 'Armenia'),
(859, 'vie', 'Country', 8, 'name', 'Netherlands Antilles'),
(860, 'vie', 'Country', 9, 'name', 'Angola'),
(861, 'vie', 'Country', 10, 'name', 'Antarctica'),
(862, 'vie', 'Country', 11, 'name', 'Argentina'),
(863, 'vie', 'Country', 12, 'name', 'American Samoa'),
(864, 'vie', 'Country', 13, 'name', 'Austria'),
(865, 'vie', 'Country', 14, 'name', 'Australia'),
(866, 'vie', 'Country', 15, 'name', 'Aruba'),
(867, 'vie', 'Country', 16, 'name', 'Aland Islands'),
(868, 'vie', 'Country', 17, 'name', 'Azerbaijan'),
(869, 'vie', 'Country', 18, 'name', 'Bosnia and Herzegovina'),
(870, 'vie', 'Country', 19, 'name', 'Barbados'),
(871, 'vie', 'Country', 20, 'name', 'Bangladesh'),
(872, 'vie', 'Country', 21, 'name', 'Belgium'),
(873, 'vie', 'Country', 22, 'name', 'Burkina Faso'),
(874, 'vie', 'Country', 23, 'name', 'Bulgaria'),
(875, 'vie', 'Country', 24, 'name', 'Bahrain'),
(876, 'vie', 'Country', 25, 'name', 'Burundi'),
(877, 'vie', 'Country', 26, 'name', 'Benin'),
(878, 'vie', 'Country', 27, 'name', 'Saint Barthelemy'),
(879, 'vie', 'Country', 28, 'name', 'Bermuda'),
(880, 'vie', 'Country', 29, 'name', 'Brunei Darussalam'),
(881, 'vie', 'Country', 30, 'name', 'Bolivia'),
(882, 'vie', 'Country', 31, 'name', 'Brazil'),
(883, 'vie', 'Country', 32, 'name', 'Bahamas'),
(884, 'vie', 'Country', 33, 'name', 'Bhutan'),
(885, 'vie', 'Country', 34, 'name', 'Bouvet Island'),
(886, 'vie', 'Country', 35, 'name', 'Botswana'),
(887, 'vie', 'Country', 36, 'name', 'Belarus'),
(888, 'vie', 'Country', 37, 'name', 'Belize'),
(889, 'vie', 'Country', 38, 'name', 'Canada'),
(890, 'vie', 'Country', 39, 'name', 'Cocos (Keeling) Islands'),
(891, 'vie', 'Country', 40, 'name', 'Congo, the Democratic Republic of the'),
(892, 'vie', 'Country', 41, 'name', 'Central African Republic'),
(893, 'vie', 'Country', 42, 'name', 'Congo'),
(894, 'vie', 'Country', 43, 'name', 'Switzerland'),
(895, 'vie', 'Country', 44, 'name', 'Cote D''Ivoire'),
(896, 'vie', 'Country', 45, 'name', 'Cook Islands'),
(897, 'vie', 'Country', 46, 'name', 'Chile'),
(898, 'vie', 'Country', 47, 'name', 'Cameroon'),
(899, 'vie', 'Country', 48, 'name', 'China'),
(900, 'vie', 'Country', 49, 'name', 'Colombia'),
(901, 'vie', 'Country', 50, 'name', 'Costa Rica'),
(902, 'vie', 'Country', 51, 'name', 'Cuba'),
(903, 'vie', 'Country', 52, 'name', 'Cape Verde'),
(904, 'vie', 'Country', 53, 'name', 'Christmas Island'),
(905, 'vie', 'Country', 54, 'name', 'Cyprus'),
(906, 'vie', 'Country', 55, 'name', 'Czech Republic'),
(907, 'vie', 'Country', 56, 'name', 'Germany'),
(908, 'vie', 'Country', 57, 'name', 'Djibouti'),
(909, 'vie', 'Country', 58, 'name', 'Denmark'),
(910, 'vie', 'Country', 59, 'name', 'Dominica'),
(911, 'vie', 'Country', 60, 'name', 'Dominican Republic'),
(912, 'vie', 'Country', 61, 'name', 'Algeria'),
(913, 'vie', 'Country', 62, 'name', 'Ecuador'),
(914, 'vie', 'Country', 63, 'name', 'Estonia'),
(915, 'vie', 'Country', 64, 'name', 'Egypt'),
(916, 'vie', 'Country', 65, 'name', 'Western Sahara'),
(917, 'vie', 'Country', 66, 'name', 'Eritrea'),
(918, 'vie', 'Country', 67, 'name', 'Spain'),
(919, 'vie', 'Country', 68, 'name', 'Ethiopia'),
(920, 'vie', 'Country', 69, 'name', 'Finland'),
(921, 'vie', 'Country', 70, 'name', 'Fiji'),
(922, 'vie', 'Country', 71, 'name', 'Falkland Islands (Malvinas)'),
(923, 'vie', 'Country', 72, 'name', 'Micronesia, Federated States of'),
(924, 'vie', 'Country', 73, 'name', 'Faroe Islands'),
(925, 'vie', 'Country', 74, 'name', 'France'),
(926, 'vie', 'Country', 75, 'name', 'Gabon'),
(927, 'vie', 'Country', 76, 'name', 'United Kingdom'),
(928, 'vie', 'Country', 77, 'name', 'Grenada'),
(929, 'vie', 'Country', 78, 'name', 'Georgia'),
(930, 'vie', 'Country', 79, 'name', 'French Guiana'),
(931, 'vie', 'Country', 80, 'name', 'Guernsey'),
(932, 'vie', 'Country', 81, 'name', 'Ghana'),
(933, 'vie', 'Country', 82, 'name', 'Gibraltar'),
(934, 'vie', 'Country', 83, 'name', 'Greenland'),
(935, 'vie', 'Country', 84, 'name', 'Gambia'),
(936, 'vie', 'Country', 85, 'name', 'Guinea'),
(937, 'vie', 'Country', 86, 'name', 'Guadeloupe'),
(938, 'vie', 'Country', 87, 'name', 'Equatorial Guinea'),
(939, 'vie', 'Country', 88, 'name', 'Greece'),
(940, 'vie', 'Country', 89, 'name', 'South Georgia and the South Sandwich Islands'),
(941, 'vie', 'Country', 90, 'name', 'Guatemala'),
(942, 'vie', 'Country', 91, 'name', 'Guam'),
(943, 'vie', 'Country', 92, 'name', 'Guinea-Bissau'),
(944, 'vie', 'Country', 93, 'name', 'Guyana'),
(945, 'vie', 'Country', 94, 'name', 'Hong Kong'),
(946, 'vie', 'Country', 95, 'name', 'Heard Island and Mcdonald Islands'),
(947, 'vie', 'Country', 96, 'name', 'Honduras'),
(948, 'vie', 'Country', 97, 'name', 'Croatia'),
(949, 'vie', 'Country', 98, 'name', 'Haiti'),
(950, 'vie', 'Country', 99, 'name', 'Hungary'),
(951, 'vie', 'Country', 100, 'name', 'Indonesia'),
(952, 'vie', 'Country', 0, 'name', 'name'),
(953, 'vie', 'Country', 101, 'name', 'Ireland'),
(954, 'vie', 'Country', 102, 'name', 'Israel'),
(955, 'vie', 'Country', 103, 'name', 'Isle of Man'),
(956, 'vie', 'Country', 104, 'name', 'India'),
(957, 'vie', 'Country', 105, 'name', 'British Indian Ocean Territory'),
(958, 'vie', 'Country', 106, 'name', 'Iraq'),
(959, 'vie', 'Country', 107, 'name', 'Iran, Islamic Republic of'),
(960, 'vie', 'Country', 108, 'name', 'Iceland'),
(961, 'vie', 'Country', 109, 'name', 'Italy'),
(962, 'vie', 'Country', 110, 'name', 'Jamaica'),
(963, 'vie', 'Country', 111, 'name', 'Jordan'),
(964, 'vie', 'Country', 112, 'name', 'Japan'),
(965, 'vie', 'Country', 113, 'name', 'Kenya'),
(966, 'vie', 'Country', 114, 'name', 'Kyrgyzstan'),
(967, 'vie', 'Country', 115, 'name', 'Cambodia'),
(968, 'vie', 'Country', 116, 'name', 'Kiribati'),
(969, 'vie', 'Country', 117, 'name', 'Comoros'),
(970, 'vie', 'Country', 118, 'name', 'Saint Kitts and Nevis'),
(971, 'vie', 'Country', 119, 'name', 'Korea, Democratic People''s Republic of'),
(972, 'vie', 'Country', 120, 'name', 'Korea, Republic of'),
(973, 'vie', 'Country', 121, 'name', 'Kuwait'),
(974, 'vie', 'Country', 122, 'name', 'Cayman Islands'),
(975, 'vie', 'Country', 123, 'name', 'Kazakhstan'),
(976, 'vie', 'Country', 124, 'name', 'Lao People''s Democratic Republic'),
(977, 'vie', 'Country', 125, 'name', 'Lebanon'),
(978, 'vie', 'Country', 126, 'name', 'Saint Lucia'),
(979, 'vie', 'Country', 127, 'name', 'Liechtenstein'),
(980, 'vie', 'Country', 128, 'name', 'Sri Lanka'),
(981, 'vie', 'Country', 129, 'name', 'Liberia'),
(982, 'vie', 'Country', 130, 'name', 'Lesotho'),
(983, 'vie', 'Country', 131, 'name', 'Lithuania'),
(984, 'vie', 'Country', 132, 'name', 'Luxembourg'),
(985, 'vie', 'Country', 133, 'name', 'Latvia'),
(986, 'vie', 'Country', 134, 'name', 'Libyan Arab Jamahiriya'),
(987, 'vie', 'Country', 135, 'name', 'Morocco'),
(988, 'vie', 'Country', 136, 'name', 'Monaco'),
(989, 'vie', 'Country', 137, 'name', 'Moldova, Republic of'),
(990, 'vie', 'Country', 138, 'name', 'Montenegro'),
(991, 'vie', 'Country', 139, 'name', 'Madagascar'),
(992, 'vie', 'Country', 140, 'name', 'Marshall Islands'),
(993, 'vie', 'Country', 141, 'name', 'Macedonia, the Former Yugoslav Republic of'),
(994, 'vie', 'Country', 142, 'name', 'Mali'),
(995, 'vie', 'Country', 143, 'name', 'Myanmar'),
(996, 'vie', 'Country', 144, 'name', 'Mongolia'),
(997, 'vie', 'Country', 145, 'name', 'Macao'),
(998, 'vie', 'Country', 146, 'name', 'Northern Mariana Islands'),
(999, 'vie', 'Country', 147, 'name', 'Martinique'),
(1000, 'vie', 'Country', 148, 'name', 'Mauritania'),
(1001, 'vie', 'Country', 149, 'name', 'Montserrat'),
(1002, 'vie', 'Country', 150, 'name', 'Malta'),
(1003, 'vie', 'Country', 151, 'name', 'Mauritius'),
(1004, 'vie', 'Country', 152, 'name', 'Maldives'),
(1005, 'vie', 'Country', 153, 'name', 'Malawi'),
(1006, 'vie', 'Country', 154, 'name', 'Mexico'),
(1007, 'vie', 'Country', 155, 'name', 'Malaysia'),
(1008, 'vie', 'Country', 156, 'name', 'Mozambique'),
(1009, 'vie', 'Country', 157, 'name', 'Namibia'),
(1010, 'vie', 'Country', 158, 'name', 'New Caledonia'),
(1011, 'vie', 'Country', 159, 'name', 'Niger'),
(1012, 'vie', 'Country', 160, 'name', 'Norfolk Island'),
(1013, 'vie', 'Country', 161, 'name', 'Nigeria'),
(1014, 'vie', 'Country', 162, 'name', 'Nicaragua'),
(1015, 'vie', 'Country', 163, 'name', 'Netherlands'),
(1016, 'vie', 'Country', 164, 'name', 'Norway'),
(1017, 'vie', 'Country', 165, 'name', 'Nepal'),
(1018, 'vie', 'Country', 166, 'name', 'Nauru'),
(1019, 'vie', 'Country', 167, 'name', 'Niue'),
(1020, 'vie', 'Country', 168, 'name', 'New Zealand'),
(1021, 'vie', 'Country', 169, 'name', 'Oman'),
(1022, 'vie', 'Country', 170, 'name', 'Panama'),
(1023, 'vie', 'Country', 171, 'name', 'Peru'),
(1024, 'vie', 'Country', 172, 'name', 'French Polynesia'),
(1025, 'vie', 'Country', 173, 'name', 'Papua New Guinea'),
(1026, 'vie', 'Country', 174, 'name', 'Philippines'),
(1027, 'vie', 'Country', 175, 'name', 'Pakistan'),
(1028, 'vie', 'Country', 176, 'name', 'Poland'),
(1029, 'vie', 'Country', 177, 'name', 'Saint Pierre and Miquelon'),
(1030, 'vie', 'Country', 178, 'name', 'Pitcairn'),
(1031, 'vie', 'Country', 179, 'name', 'Puerto Rico'),
(1032, 'vie', 'Country', 180, 'name', 'Palestinian Territory, Occupied'),
(1033, 'vie', 'Country', 181, 'name', 'Portugal'),
(1034, 'vie', 'Country', 182, 'name', 'Palau'),
(1035, 'vie', 'Country', 183, 'name', 'Paraguay'),
(1036, 'vie', 'Country', 184, 'name', 'Qatar'),
(1037, 'vie', 'Country', 185, 'name', 'Reunion'),
(1038, 'vie', 'Country', 186, 'name', 'Romania'),
(1039, 'vie', 'Country', 187, 'name', 'Serbia'),
(1040, 'vie', 'Country', 188, 'name', 'Russian Federation'),
(1041, 'vie', 'Country', 189, 'name', 'Rwanda'),
(1042, 'vie', 'Country', 190, 'name', 'Saudi Arabia'),
(1043, 'vie', 'Country', 191, 'name', 'Solomon Islands'),
(1044, 'vie', 'Country', 192, 'name', 'Seychelles'),
(1045, 'vie', 'Country', 193, 'name', 'Sudan'),
(1046, 'vie', 'Country', 194, 'name', 'Sweden'),
(1047, 'vie', 'Country', 195, 'name', 'Singapore'),
(1048, 'vie', 'Country', 196, 'name', 'Saint Helena'),
(1049, 'vie', 'Country', 197, 'name', 'Slovenia'),
(1050, 'vie', 'Country', 198, 'name', 'Svalbard and Jan Mayen'),
(1051, 'vie', 'Country', 199, 'name', 'Slovakia'),
(1052, 'vie', 'Country', 200, 'name', 'Sierra Leone'),
(1053, 'vie', 'Country', 0, 'name', 'name'),
(1054, 'vie', 'Country', 201, 'name', 'San Marino'),
(1055, 'vie', 'Country', 202, 'name', 'Senegal'),
(1056, 'vie', 'Country', 203, 'name', 'Somalia'),
(1057, 'vie', 'Country', 204, 'name', 'Suriname'),
(1058, 'vie', 'Country', 205, 'name', 'Sao Tome and Principe'),
(1059, 'vie', 'Country', 206, 'name', 'El Salvador'),
(1060, 'vie', 'Country', 207, 'name', 'Syrian Arab Republic'),
(1061, 'vie', 'Country', 208, 'name', 'Swaziland'),
(1062, 'vie', 'Country', 209, 'name', 'Turks and Caicos Islands'),
(1063, 'vie', 'Country', 210, 'name', 'Chad'),
(1064, 'vie', 'Country', 211, 'name', 'French Southern Territories'),
(1065, 'vie', 'Country', 212, 'name', 'Togo'),
(1066, 'vie', 'Country', 213, 'name', 'Thailand'),
(1067, 'vie', 'Country', 214, 'name', 'Tajikistan'),
(1068, 'vie', 'Country', 215, 'name', 'Tokelau'),
(1069, 'vie', 'Country', 216, 'name', 'Timor-Leste'),
(1070, 'vie', 'Country', 217, 'name', 'Turkmenistan'),
(1071, 'vie', 'Country', 218, 'name', 'Tunisia'),
(1072, 'vie', 'Country', 219, 'name', 'Tonga'),
(1073, 'vie', 'Country', 220, 'name', 'Turkey'),
(1074, 'vie', 'Country', 221, 'name', 'Trinidad and Tobago'),
(1075, 'vie', 'Country', 222, 'name', 'Tuvalu'),
(1076, 'vie', 'Country', 223, 'name', 'Taiwan, Province of China'),
(1077, 'vie', 'Country', 224, 'name', 'Tanzania, United Republic of'),
(1078, 'vie', 'Country', 225, 'name', 'Ukraine'),
(1079, 'vie', 'Country', 226, 'name', 'Uganda'),
(1080, 'vie', 'Country', 227, 'name', 'United States Minor Outlying Islands'),
(1081, 'vie', 'Country', 228, 'name', 'Andorra'),
(1082, 'vie', 'Country', 229, 'name', 'Uruguay'),
(1083, 'vie', 'Country', 230, 'name', 'Uzbekistan'),
(1084, 'vie', 'Country', 231, 'name', 'Holy See (Vatican City State)'),
(1085, 'vie', 'Country', 232, 'name', 'Saint Vincent and the Grenadines'),
(1086, 'vie', 'Country', 233, 'name', 'Venezuela'),
(1087, 'vie', 'Country', 234, 'name', 'Virgin Islands, British'),
(1088, 'vie', 'Country', 235, 'name', 'Virgin Islands, U.s.'),
(1089, 'vie', 'Country', 236, 'name', 'Viet Nam'),
(1090, 'vie', 'Country', 237, 'name', 'Vanuatu'),
(1091, 'vie', 'Country', 238, 'name', 'Wallis and Futuna'),
(1092, 'vie', 'Country', 239, 'name', 'Samoa'),
(1093, 'vie', 'Country', 240, 'name', 'Yemen'),
(1094, 'vie', 'Country', 241, 'name', 'Mayotte'),
(1095, 'vie', 'Country', 242, 'name', 'South Africa'),
(1096, 'vie', 'Country', 243, 'name', 'Zambia'),
(1097, 'vie', 'Country', 244, 'name', 'Zimbabwe'),
(1098, 'vie', 'State', 1, 'name', 'Alaska'),
(1099, 'vie', 'State', 2, 'name', 'Alabama'),
(1100, 'vie', 'State', 3, 'name', 'Arkansas'),
(1101, 'vie', 'State', 4, 'name', 'Arizona'),
(1102, 'vie', 'State', 5, 'name', 'California'),
(1103, 'vie', 'State', 6, 'name', 'Colorado'),
(1104, 'vie', 'State', 7, 'name', 'Connecticut'),
(1105, 'vie', 'State', 8, 'name', 'District of Columbia'),
(1106, 'vie', 'State', 9, 'name', 'Delaware'),
(1107, 'vie', 'State', 10, 'name', 'Florida'),
(1108, 'vie', 'State', 11, 'name', 'Georgia'),
(1109, 'vie', 'State', 12, 'name', 'Hawaii'),
(1110, 'vie', 'State', 13, 'name', 'Iowa'),
(1111, 'vie', 'State', 14, 'name', 'Idaho'),
(1112, 'vie', 'State', 15, 'name', 'Illinois'),
(1113, 'vie', 'State', 16, 'name', 'Indiana'),
(1114, 'vie', 'State', 17, 'name', 'Kansas'),
(1115, 'vie', 'State', 18, 'name', 'Kentucky'),
(1116, 'vie', 'State', 19, 'name', 'Louisiana'),
(1117, 'vie', 'State', 20, 'name', 'Massachusetts'),
(1118, 'vie', 'State', 21, 'name', 'Maryland'),
(1119, 'vie', 'State', 22, 'name', 'Maine'),
(1120, 'vie', 'State', 23, 'name', 'Michigan'),
(1121, 'vie', 'State', 24, 'name', 'Minnesota'),
(1122, 'vie', 'State', 25, 'name', 'Missouri'),
(1123, 'vie', 'State', 26, 'name', 'Mississippi'),
(1124, 'vie', 'State', 27, 'name', 'Montana'),
(1125, 'vie', 'State', 28, 'name', 'North Carolina'),
(1126, 'vie', 'State', 29, 'name', 'North Dakota'),
(1127, 'vie', 'State', 30, 'name', 'Nebraska'),
(1128, 'vie', 'State', 31, 'name', 'New Hampshire'),
(1129, 'vie', 'State', 32, 'name', 'New Jersey'),
(1130, 'vie', 'State', 33, 'name', 'New Mexico'),
(1131, 'vie', 'State', 34, 'name', 'Nevada'),
(1132, 'vie', 'State', 35, 'name', 'New York'),
(1133, 'vie', 'State', 36, 'name', 'Ohio'),
(1134, 'vie', 'State', 37, 'name', 'Oklahoma'),
(1135, 'vie', 'State', 38, 'name', 'Oregon'),
(1136, 'vie', 'State', 39, 'name', 'Pennsylvania'),
(1137, 'vie', 'State', 40, 'name', 'Rhode Island'),
(1138, 'vie', 'State', 41, 'name', 'South Carolina'),
(1139, 'vie', 'State', 42, 'name', 'South Dakota'),
(1140, 'vie', 'State', 43, 'name', 'Tennessee'),
(1141, 'vie', 'State', 44, 'name', 'Texas'),
(1142, 'vie', 'State', 45, 'name', 'Utah'),
(1143, 'vie', 'State', 46, 'name', 'Virginia'),
(1144, 'vie', 'State', 47, 'name', 'Vermont'),
(1145, 'vie', 'State', 48, 'name', 'Washington'),
(1146, 'vie', 'State', 49, 'name', 'Wisconsin'),
(1147, 'vie', 'State', 50, 'name', 'West Virginia'),
(1148, 'vie', 'State', 51, 'name', 'Wyoming'),
(1151, 'eng', 'CoreMenuItem', 13, 'name', 'Pakage'),
(1152, 'eng', 'Page', 104, 'title', 'Pakage Detail Page'),
(1153, 'eng', 'Page', 104, 'content', ''),
(1154, 'eng', 'CoreContent', 183, 'core_block_title', ''),
(1155, 'eng', 'CoreContent', 184, 'core_block_title', 'Page Content'),
(1156, 'vie', 'CoreContent', 183, 'core_block_title', ''),
(1157, 'vie', 'CoreContent', 184, 'core_block_title', ''),
(1158, 'vie', 'Page', 104, 'title', 'Pakage Detail Page'),
(1159, 'vie', 'Page', 104, 'content', ''),
(1160, 'eng', 'CoreContent', 185, 'core_block_title', ''),
(1161, 'vie', 'CoreContent', 185, 'core_block_title', ''),
(1166, 'eng', 'CoreMenuItem', 14, 'name', 'Livestream Order'),
(1167, 'eng', 'CoreMenuItem', 15, 'name', 'Pakages'),
(1168, 'vie', 'CoreMenuItem', 15, 'name', 'Pakage Detail Page'),
(1169, 'eng', 'CoreMenuItem', 16, 'name', 'Payment');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jobtype_id` int(10) unsigned NOT NULL DEFAULT '0',
  `state` enum('pending','active','sleeping','failed','cancelled','completed','timeout') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `is_complete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `progress` decimal(5,4) unsigned NOT NULL DEFAULT '0.0000',
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `started_date` datetime DEFAULT NULL,
  `completion_date` datetime DEFAULT NULL,
  `priority` mediumint(9) NOT NULL DEFAULT '100',
  `data` text COLLATE utf8_unicode_ci,
  `messages` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `jobtype_id` (`jobtype_id`),
  KEY `state` (`state`),
  KEY `is_complete` (`is_complete`,`priority`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobtypes`
--

CREATE TABLE IF NOT EXISTS `jobtypes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `class` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `plugin` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `priority` mediumint(9) NOT NULL DEFAULT '100',
  `multi` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jobtypes`
--

INSERT INTO `jobtypes` (`id`, `title`, `class`, `plugin`, `enabled`, `priority`, `multi`) VALUES
(1, 'Test', 'Cron_Task_Job_Test', 'cron', 1, 100, 1);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rtl` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `key`, `name`, `rtl`) VALUES
(1, 'eng', 'English', 0),
(2, 'vie', 'Tiếng Việt', 0);

-- --------------------------------------------------------

--
-- Table structure for table `livestream_discountcodes`
--

CREATE TABLE IF NOT EXISTS `livestream_discountcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_viewer` int(11) DEFAULT NULL COMMENT 'max viewer for discount code',
  `max_time` float DEFAULT NULL COMMENT 'max timefor discount code',
  `num_used` int(11) DEFAULT '0',
  `count_used` int(11) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0' COMMENT '0: not active , 1 active',
  `user_id` int(11) DEFAULT '0' COMMENT 'user_create',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `livestream_logs`
--

CREATE TABLE IF NOT EXISTS `livestream_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT '0',
  `param` mediumtext COMMENT '{''time_write_log'' =1,''view_count'' = 12}',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `livestream_orders`
--

CREATE TABLE IF NOT EXISTS `livestream_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT '1' COMMENT '1 now 2 later',
  `video_id` varchar(255) DEFAULT NULL,
  `other_param` mediumtext,
  `amount_live` mediumint(9) DEFAULT '0',
  `amount_view` mediumint(9) DEFAULT '0',
  `amount_like` mediumint(9) DEFAULT '0',
  `amount_share` mediumint(9) DEFAULT '0',
  `amount_comment` mediumint(9) DEFAULT '0',
  `viewer_location` varchar(255) DEFAULT NULL,
  `time_need_viewer` int(11) DEFAULT NULL,
  `time_start_schedule` int(11) DEFAULT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `start_time_real` int(11) DEFAULT '0',
  `add_time` int(11) DEFAULT NULL,
  `start_time` int(11) DEFAULT NULL,
  `complete_time` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1: added, 2: active, 3: running, 4 : complete; 5: cancaled',
  `price` mediumtext,
  `note` mediumtext,
  `run_number` int(11) DEFAULT NULL,
  `run_total` mediumint(9) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  `reaction` varchar(50) DEFAULT '0' COMMENT '1,2,3,4',
  `time_per` int(11) DEFAULT '0',
  `view_per` int(11) DEFAULT '0',
  `run_amount_live` mediumint(9) DEFAULT '0',
  `start_last_time` int(11) DEFAULT '0',
  `started_viewer_live` int(11) DEFAULT '0',
  `is_auto_create` tinyint(1) DEFAULT '0' COMMENT '0: khong auto, 1 auto',
  `parent_order_id` int(22) DEFAULT '0' COMMENT 'id order auto create this order',
  `pakage_id` int(11) DEFAULT '0' COMMENT 'id cua goi',
  `distcount_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mailrecipients`
--

CREATE TABLE IF NOT EXISTS `mailrecipients` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `params` longtext COLLATE utf8_unicode_ci NOT NULL,
  `priority` smallint(3) DEFAULT '100',
  `creation_time` datetime DEFAULT NULL,
  `recipient` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mailtemplates`
--

CREATE TABLE IF NOT EXISTS `mailtemplates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `content` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `plugin` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vars` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subject` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `mailtemplates`
--

INSERT INTO `mailtemplates` (`id`, `type`, `content`, `plugin`, `vars`, `subject`) VALUES
(1, 'header', '', 'Mail', '', ''),
(2, 'footer', '', 'Mail', '', ''),
(3, 'header_member', '', 'Mail', '', ''),
(4, 'footer_member', '', 'Mail', '', ''),
(5, 'mail_contact', '', 'Mail', '[message],[name],[subject],[sender_email]', ''),
(6, 'event_invite_none_member', '<p>[header]</p>\r\n<p>You have been invited to join the event "[event_title]". Please click the following link to view it:</p>\r\n<p><a href="[event_link]">[event_title]</a></p>\r\n<p>[footer]</p>', 'Event', '[email],[sender_title],[sender_link],[event_link],[event_title]', 'You have been invited to join the event [event_title]'),
(7, 'friend_request', '<p>[header]</p>\r\n<p>[sender_title] wants to be friends with you.</p>\r\n<p>[message]</p>\r\n<p>[footer]</p>', 'Mail', '[recipient_title],[recipient_link],[sender_title],[sender_link],[message],[request_link]', '[sender_title] has sent you a friend request.'),
(8, 'site_invite', '<p>[header]</p>\r\n<p>You have been invited by [sender_title] to join our social network. To join, please follow the link below:</p>\r\n<p><a href="[signup_link]">[signup_link]</a></p>\r\n<p>----------------------------------------<br />[message]<br />----------------------------------------</p>\r\n<p>[footer]</p>', 'Mail', '[email],[sender_title],[sender_link],[message],[sigup_link]', 'You have received an invitation to join our social network.'),
(9, 'group_invite_none_member', '<p>[header]</p>\r\n<p>You have been invited to join the group "[group_title]". Please click the following link to view it:</p>\r\n<p><a href="[group_link]">[group_title]</a></p>\r\n<p>[footer]</p>', 'Group', '[email],[sender_title],[sender_link],[group_link],[group_title]', 'You have been invited to join the group [group_title]'),
(10, 'bulkmail', '', 'Mail', '[recipient_title],[recipient_link],[sender_title],[sender_link]', ''),
(11, 'welcome_user', '<p>[header]</p>\r\n<p>Thank you for joining our social network. Click the following link and enter your information below to login:</p>\r\n<p><a href="[login_link]">[login_link]</a></p>\r\n<p>Email: [email]</p><p>Password: [password]</p>\r\n<p>[footer]</p>', 'Mail', '[recipient_title],[recipient_link],[site_name],[login_link],[email],[password]', 'Welcome to [site_name]'),
(12, 'welcome_user_confirm', '<p>[header]</p>\r\n<p>Thank you for joining our social network. Please click the link below to validate your email:</p>\r\n<p><a href="[confirm_link]">[confirm_link]</a></p>\r\n<p>Email: [email]</p><p>Password: [password]</p>\r\n<p>[footer]</p>', 'Mail', '[recipient_title],[recipient_link],[site_name],[email],[confirm_link],[password]', 'Welcome to [site_name]'),
(13, 'new_registration', '<p>[header]</p>\r\n<p><a href="[new_user_link]">[new_user_title]</a> has just signed up on [site_name]</p>\r\n<p>[footer]</p>', 'Mail', '[new_user_title],[new_user_link],[site_name]', 'New Registration'),
(14, 'reset_password', '<p>[header]</p>\r\n<p>A request to reset password was submitted. If it''s not you, please ignore this email.</p>\r\n<p>To reset your password, please click <a href="[reset_link]">[reset_link]</a></p>\r\n<p>[footer]</p>', 'Mail', '[recipient_title],[recipient_link],[reset_link]', 'Password Change Request'),
(15, 'admin_change_password', '<p>[header]</p>\r\n<p>The admin has changed your password to [password]</p>\r\n<p>[footer]</p>', 'Mail', '[recipient_title],[recipient_link],[password]', 'Your password has been changed'),
(16, 'notifications_summary', '<p>[header]</p>\r\n<p>[element]</p>\r\n<p>[footer]</p>', 'Mail', '[recipient_title],[recipient_link],[element]', 'Your Notifications Summary'),
(17, 'subscription_activated', '', 'Subscription', '[subscription_title],[subscription_description],[login_link],[plan_title],[plan_description]', ''),
(18, 'subscription_pending', '', 'Subscription', '[subscription_title],[subscription_description],[link],[plan_title],[plan_description]', ''),
(19, 'subscription_expire', '', 'Subscription', '[subscription_title],[subscription_description],[link],[plan_title],[plan_description]', ''),
(20, 'subscription_reminder', '', 'Subscription', '[subscription_title],[subscription_description],[link],[expire_time],[plan_title],[plan_description]', ''),
(21, 'subscription_recurrence', '', 'Subscription', '[subscription_title],[subscription_description],[link],[plan_title],[plan_description]', ''),
(22, 'subscription_cancel_admin', '', 'Subscription', '[subscription_title],[subscription_description],[sender_title],[sender_link],[reason],[plan_title],[plan_description]', ''),
(23, 'subscription_refund_admin', '', 'Subscription', '[subscription_title],[subscription_description],[sender_title],[sender_link],[reason],[plan_title],[plan_description]', ''),
(24, 'subscription_refund_deny', '', 'Subscription', '[subscription_title],[subscription_description],[reason],[link],[plan_title],[plan_description]', ''),
(25, 'subscription_refund_accept', '', 'Subscription', '[subscription_title],[subscription_description],[link],[plan_title],[plan_description]', ''),
(26, 'subscription_cancel', '', 'Subscription', '[subscription_title],[subscription_description],[link],[plan_title],[plan_description]', ''),
(27, 'shared_item', '<p>[header]</p>\r\n<p>', 'Mail', '[email],[shared_user],[user_shared],[shared_link][shared_content]', '<p>Hi [shared_user]</p>\r\n<p>[user_shared] shared for you a link: <a href="[shared_link]">[shared_link]</a></p>\r\n<p>[shared_content]</p>\r\n<p>Please see my link guy</p>'),
(28, 'private_message', '', 'Mail', '[sender_link],[sender_title],[time],[message_link],[site_name]', ''),
(29, 'approve_user', '', 'Mail', '[link]', ''),
(30, 'unapprove_user', '', 'Mail', '[link]', ''),
(31, 'active_user', '', 'Mail', '[link]', ''),
(32, 'inactive_user', '', 'Mail', '[link]', '');

-- --------------------------------------------------------

--
-- Table structure for table `minify_urls`
--

CREATE TABLE IF NOT EXISTS `minify_urls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `minify_urls`
--

INSERT INTO `minify_urls` (`id`, `name`, `url`) VALUES
(12, 'c14cb2850f36f11e9661d272d0ab7c80', '["pakage\\/css\\/main.css","livestream\\/css\\/main.css","css\\/common.css","css\\/feed.css","css\\/video.css","css\\/blog.css","css\\/event.css","css\\/group.css","css\\/photo.css","css\\/topic.css","css\\/button.css","css\\/subscription.css","css\\/main.css","theme\\/default\\/css\\/custom.css","css\\/elastislide.css","css\\/fineuploader.css","css\\/pickadate.css","css\\/jquery.Jcrop.css","css\\/jquery.mp.css","css\\/token-input.css","css\\/echarts.css","css\\/qtip.css"]'),
(13, '2e3954403187975db117af2465ba8620', '["pakage\\/css\\/main.css","livestream\\/css\\/main.css","payment\\/css\\/main.css","css\\/common.css","css\\/feed.css","css\\/video.css","css\\/blog.css","css\\/event.css","css\\/group.css","css\\/photo.css","css\\/topic.css","css\\/button.css","css\\/subscription.css","css\\/main.css","theme\\/default\\/css\\/custom.css","css\\/elastislide.css","css\\/fineuploader.css","css\\/pickadate.css","css\\/jquery.Jcrop.css","css\\/jquery.mp.css","css\\/token-input.css","css\\/echarts.css","css\\/qtip.css"]');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `action` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `plugin` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification_stops`
--

CREATE TABLE IF NOT EXISTS `notification_stops` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `item_type` varchar(255) NOT NULL DEFAULT '',
  `item_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `access_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `client_id` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `refresh_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `client_id` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `params` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `icon_class` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `weight` int(10) unsigned NOT NULL DEFAULT '0',
  `url` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `custom` tinyint(1) NOT NULL DEFAULT '1',
  `fragment` tinyint(1) NOT NULL DEFAULT '0',
  `layout` int(11) NOT NULL DEFAULT '0',
  `levels` text COLLATE utf8_unicode_ci,
  `provides` text COLLATE utf8_unicode_ci,
  `view_count` int(11) unsigned NOT NULL DEFAULT '0',
  `type` enum('core','plugin','page') COLLATE utf8_unicode_ci DEFAULT NULL,
  `search` tinyint(1) NOT NULL DEFAULT '0',
  `theme_id` int(11) unsigned NOT NULL DEFAULT '0',
  `core_content_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=105 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `alias`, `content`, `permission`, `params`, `created`, `modified`, `menu`, `icon_class`, `weight`, `url`, `uri`, `description`, `keywords`, `custom`, `fragment`, `layout`, `levels`, `provides`, `view_count`, `type`, `search`, `theme_id`, `core_content_count`) VALUES
(1, 'Home Page', 'home_index', '', '', '', NULL, '2014-09-10 02:06:12', 0, '', 0, '/home', 'home.index', '', '', 1, 0, 2, NULL, NULL, 0, 'core', 0, 0, 4),
(2, 'Contact Page', 'home_contact', '', '', '', NULL, NULL, 0, '', 0, '/home/contact', 'home.contact', '', '', 1, 0, 4, NULL, NULL, 0, 'core', 0, 0, 3),
(3, 'People Page', 'users_index', '', '', '', NULL, NULL, 0, '', 0, '/users', 'users.index', '', '', 1, 0, 2, NULL, NULL, 0, 'core', 0, 0, 5),
(4, 'Profile Page', 'users_view', '', '', '', NULL, NULL, 0, '', 0, '/users/view/$id', 'users.view', '', '', 1, 0, 2, NULL, NULL, 0, 'core', 0, 0, 4),
(6, 'Blogs Browse Page', 'blogs_index', '', '', '', NULL, NULL, 0, '', 0, '/blogs', 'blogs.index', '', '', 1, 0, 1, NULL, NULL, 0, 'core', 0, 0, 5),
(8, 'Blog Detail Page', 'blogs_view', '', '', '', NULL, NULL, 0, '', 0, '/blogs/view/$id', 'blogs.view', '', '', 1, 0, 2, NULL, NULL, 0, 'core', 0, 0, 6),
(9, 'Photos Browse Page', 'photos_index', '', '', '', NULL, NULL, 0, '', 0, '/photos', 'photos.index', '', '', 1, 0, 2, NULL, NULL, 0, 'core', 0, 0, 4),
(10, 'Album Detail Page', 'albums_view', '', '', '', NULL, NULL, 0, '', 0, '/albums/view/$id/{album name}', 'albums.view', '', '', 1, 0, 4, NULL, NULL, 0, 'core', 0, 0, 3),
(11, 'Photo Detail Page', 'photos_view', '', '', '', NULL, NULL, 0, '', 0, '/photos/view/$id', 'photos.view', '', '', 1, 0, 4, NULL, NULL, 0, 'core', 0, 0, 2),
(12, 'Videos Browse Page', 'videos', '', '', '', NULL, NULL, 0, '', 0, '/videos', 'videos.index', '', '', 1, 0, 2, NULL, NULL, 0, 'core', 0, 0, 5),
(13, 'Video Detail Page', 'videos_view', '', '', '', NULL, NULL, 0, '', 0, '/videos/view/$id/{video''s name}', 'videos.view', '', '', 1, 0, 3, NULL, NULL, 0, 'core', 0, 0, 4),
(15, 'Topic Detail Page', 'topics_view', '', '', '', NULL, NULL, 0, '', 0, '/topics/view/$id/{topic''s name}', 'topics.view', '', '', 1, 0, 2, NULL, NULL, 0, 'core', 0, 0, 5),
(16, 'Groups Browse Page', 'groups', '', '', '', NULL, NULL, 0, '', 0, '/groups', 'groups.index', '', '', 1, 0, 2, NULL, NULL, 0, 'core', 0, 0, 5),
(18, 'Group Detail Page', 'groups_view', '', '', '', NULL, NULL, 0, '', 0, '/groups/view/$id/{group''s name}', 'groups.view', '', '', 1, 0, 2, NULL, NULL, 0, 'core', 0, 0, 5),
(19, 'Events Browse Page', 'events', '', '', '', NULL, NULL, 0, '', 0, '/events', 'events.index', '', '', 1, 0, 2, NULL, NULL, 0, 'core', 0, 0, 5),
(21, 'Event Detail Page', 'events_view', '', '', '', NULL, NULL, 0, '', 0, '/events/view/$id/{event''s name}', 'events.view', '', '', 1, 0, 1, NULL, NULL, 0, 'core', 0, 0, 7),
(22, 'Forgot Password Page', 'user_recover', '', '', '', NULL, NULL, 0, '', 0, '/users/recover', 'users.recover', '', '', 1, 0, 4, NULL, NULL, 0, 'core', 0, 0, 3),
(23, 'Topics Browse Page', 'topics', '', '', '', NULL, NULL, 0, '', 0, '/topics', 'topics.index', '', '', 1, 0, 2, NULL, NULL, 0, 'core', 0, 0, 5),
(63, 'Landing Page', 'landing_index', '', '', '', '2014-08-28 04:20:32', '2014-08-28 04:20:32', 0, '', 0, '/home/landing', 'home.landing', '', '', 1, 0, 4, NULL, NULL, 0, 'core', 0, 0, 12),
(68, 'Site Header', 'site_header', '', '', '', NULL, '2014-09-08 01:58:40', 0, '', 0, 'site/header', 'site.header', '', '', 1, 0, 4, NULL, NULL, 0, 'core', 0, 0, 1),
(69, 'Site Footer', 'site_footer', '', '', '', NULL, NULL, 0, '', 0, 'site/footer', 'site.footer', '', '', 1, 0, 4, NULL, NULL, 0, 'core', 0, 0, 2),
(90, 'Sign in', 'signin', '', '', '', '2014-09-11 09:14:58', '2014-09-11 09:14:58', 0, '', 0, '/users/member_login', 'users.member_login', '', '', 1, 0, 4, NULL, NULL, 0, 'core', 0, 0, 2),
(96, 'Sign up', 'signup', '', '', '', '2014-09-15 03:34:56', '2014-09-15 03:34:56', 0, '', 0, '/users/register', 'users.register', '', '', 1, 0, 4, NULL, NULL, 0, 'core', 0, 0, 2),
(100, 'About Us', 'about-us', '<p><span>This is the About Us page</span></p>', '', '', '2015-06-11 09:41:53', '2015-06-11 09:41:53', 0, '', 0, '/pages/about-us', 'pages.about-us', '', '', 1, 0, 0, NULL, NULL, 0, 'page', 0, 0, 2),
(101, 'Terms of Service', 'terms-of-service', '<p><span>This is the Terms of Service page</span></p>', '', '', '2015-06-11 09:43:12', '2015-06-11 09:43:12', 0, '', 0, '/pages/terms-of-service', 'pages.terms-of-service', '', '', 1, 0, 0, NULL, NULL, 0, 'page', 0, 0, 2),
(102, 'Privacy Policy', 'privacy-policy', '<p><span>This is the Privacy Policy page</span></p>', '', '', '2015-06-11 09:44:06', '2015-06-11 09:44:06', 0, '', 0, '/pages/privacy-policy', 'pages.privacy-policy', '', '', 1, 0, 0, NULL, NULL, 0, 'page', 0, 0, 2),
(104, 'Pakage Detail Page', 'pakage_details_index', '', '1,2,3,4,5,6', 'a:1:{s:8:"comments";s:1:"0";}', '2017-08-27 15:43:29', '2017-08-27 15:43:30', 0, '', 0, '/pakage/pakage_details', 'pakages.index', '', '', 1, 0, 4, NULL, NULL, 0, 'page', 0, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pakages`
--

CREATE TABLE IF NOT EXISTS `pakages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Tên gói',
  `max_viewer` int(11) DEFAULT '0' COMMENT 'số lượng viewer tối đa trên 1 đơn hàng',
  `num_hours` int(11) DEFAULT '0' COMMENT 'số giây của gói',
  `unit` int(11) DEFAULT '0' COMMENT '1 = gói tuần, 2 = gói tháng',
  `default_price` float DEFAULT '0' COMMENT 'số tiền mặc định của gói',
  `active` tinyint(2) DEFAULT '0' COMMENT '0: not active, 1 active',
  `user_create` int(11) DEFAULT '0' COMMENT 'user create',
  `created` datetime DEFAULT NULL COMMENT 'ngay tao',
  `modified` datetime DEFAULT NULL COMMENT 'ngay update',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pakage_details`
--

CREATE TABLE IF NOT EXISTS `pakage_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT 'user id',
  `pakage_id` int(11) DEFAULT NULL COMMENT 'gói id',
  `name` varchar(255) DEFAULT NULL COMMENT 'ten goi',
  `price` float NOT NULL DEFAULT '0' COMMENT 'giá tiền của gói đối với từng KH',
  `active` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 : chưa active,1 active',
  `time_start` int(11) NOT NULL DEFAULT '0' COMMENT 'thời gian bắt đầu',
  `time_end` int(11) NOT NULL DEFAULT '0' COMMENT 'thời gian kết thúc',
  `num_time` int(11) NOT NULL DEFAULT '0' COMMENT 'tong thoi gian cua goi (so gio)',
  `hour_exist` float NOT NULL DEFAULT '0' COMMENT 'so gio con lai',
  `minute_exist` int(11) NOT NULL DEFAULT '0' COMMENT 'so phut con lai',
  `second_exist` int(11) NOT NULL DEFAULT '0' COMMENT 'so giay con lai',
  `parrams` text,
  `is_payment` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: chưa thanh toán, 1 : thanh toán rồi',
  `created` datetime DEFAULT NULL COMMENT 'ngay tao',
  `modified` datetime DEFAULT NULL COMMENT 'ngay update',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_requests`
--

CREATE TABLE IF NOT EXISTS `password_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_infos`
--

CREATE TABLE IF NOT EXISTS `payment_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` mediumtext COLLATE utf8_unicode_ci,
  `currency` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `method` int(11) DEFAULT '0' COMMENT '1: paypal, 2: ATM',
  `user_id` int(11) DEFAULT '0',
  `user_create` int(11) DEFAULT '0',
  `transaction_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '1; pending, 2: approve, 3: cancel',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='info payment' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE IF NOT EXISTS `plugins` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `core` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `version` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `weight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `icon_class` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bootstrap` tinyint(1) NOT NULL DEFAULT '0',
  `routes` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `name`, `key`, `permission`, `enabled`, `core`, `version`, `weight`, `menu`, `url`, `icon_class`, `bootstrap`, `routes`) VALUES
(1, 'Billing', 'Billing', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(2, 'Menu', 'Menu', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(3, 'Payment Gateway', 'PaymentGateway', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(4, 'Cron', 'Cron', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(5, 'Mail', 'Mail', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(8, 'Page', 'Page', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(13, 'Social Integration', 'SocialIntegration', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(14, 'MooUpload', 'MooUpload', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(15, 'Subscription', 'Subscription', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(16, 'Paypal Adaptive', 'PaypalAdaptive', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(17, 'Minify', 'Minify', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(18, 'Storage', 'Storage', '', 1, 1, '2.6.1', 0, 1, '', '', 1, 1),
(19, 'Pakage', 'Pakage', '', 1, 0, '1.0', 0, 1, '', '', 1, 1),
(20, 'Livestream Order', 'Livestream', '', 1, 0, '1.0', 0, 1, '', '', 1, 1),
(21, 'Payment', 'Payment', '', 1, 0, '1.0', 0, 1, '', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE IF NOT EXISTS `prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) DEFAULT NULL COMMENT 'ten goi gia',
  `max_viewer` int(11) DEFAULT NULL COMMENT 'so luong toi thieu cua detail price',
  `max_time` int(11) DEFAULT NULL COMMENT 'so gio toi thieu cua detail price',
  `default_view` float DEFAULT NULL COMMENT 'mac dinh gia cua view video',
  `default_live` text COMMENT 'mac dinh gia cua livestream',
  `default_like` float DEFAULT NULL COMMENT 'mac dinh gia cua like',
  `default_share` float DEFAULT NULL COMMENT 'mac dinh gia cua share',
  `default_comment` float DEFAULT NULL COMMENT 'mac dinh gia cua comment',
  `active` tinyint(2) DEFAULT '0' COMMENT '0: deactive, 1 active',
  `created` datetime DEFAULT NULL COMMENT 'timestam add',
  `modified` datetime DEFAULT NULL COMMENT 'timestam update',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`id`, `name`, `max_viewer`, `max_time`, `default_view`, `default_live`, `default_like`, `default_share`, `default_comment`, `active`, `created`, `modified`) VALUES
(1, 'System price', 6000, 6, 10, '[{"view":1000,"price":0.0125},{"view":2000,"price":0.0160},{"view":3000,"price":0.0195},{"view":4000,"price":0.0230},\r\n{"view":5000,"price":0.0265},{"view":6000,"price":0.03}]', 1, 1, 1, 1, '2017-08-28 13:40:20', '2017-08-28 13:40:20');

-- --------------------------------------------------------

--
-- Table structure for table `price_details`
--

CREATE TABLE IF NOT EXISTS `price_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id detail price',
  `user_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `max_viewer` int(11) DEFAULT NULL,
  `max_time` int(11) DEFAULT NULL,
  `price_live` text,
  `price_view` float DEFAULT NULL,
  `price_like` float DEFAULT NULL,
  `price_share` float DEFAULT NULL,
  `price_comment` float DEFAULT NULL,
  `active` tinyint(4) DEFAULT '0' COMMENT '0: deactive, 1  : active',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='setting gia mac dinh cho tung user' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `price_details`
--

INSERT INTO `price_details` (`id`, `user_id`, `price_id`, `max_viewer`, `max_time`, `price_live`, `price_view`, `price_like`, `price_share`, `price_comment`, `active`, `created`, `modified`) VALUES
(1, 7, 1, 6000, 6, '[{"view":1000,"price":0.0125},{"view":2000,"price":0.0160},{"view":3000,"price":0.0195},{"view":4000,"price":0.0230}, {"view":5000,"price":0.0265},{"view":6000,"price":0.03}]', 6, 1, 1, 1, 1, '2017-08-28 13:45:59', '2017-08-28 13:45:59');

-- --------------------------------------------------------

--
-- Table structure for table `processes`
--

CREATE TABLE IF NOT EXISTS `processes` (
  `pid` int(10) unsigned NOT NULL,
  `parent_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `system_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `started` int(10) unsigned NOT NULL DEFAULT '0',
  `timeout` mediumint(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile_fields`
--

CREATE TABLE IF NOT EXISTS `profile_fields` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `values` text COLLATE utf8_unicode_ci NOT NULL,
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `registration` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `searchable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `profile` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `plugin` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_type_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_field_values`
--

CREATE TABLE IF NOT EXISTS `profile_field_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `profile_field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `profile_field_id` (`profile_field_id`),
  FULLTEXT KEY `value` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_types`
--

CREATE TABLE IF NOT EXISTS `profile_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `actived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `profile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `profile_types`
--

INSERT INTO `profile_types` (`id`, `name`, `actived`, `profile`, `order`) VALUES
(1, 'Default', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE IF NOT EXISTS `ratings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `plugin` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `type_id` int(10) DEFAULT NULL,
  `score` tinyint(2) NOT NULL DEFAULT '0',
  `rating_user_count` int(10) unsigned DEFAULT NULL,
  `total_score` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rating_settings`
--

CREATE TABLE IF NOT EXISTS `rating_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `rating_settings`
--

INSERT INTO `rating_settings` (`id`, `label`, `name`, `value`, `type`) VALUES
(1, 'Enable Rating', 'enable_rating', '{"users":"1","blogs":"1","albums":"1","photos":"1","topics":"1","events":"1","videos":"1","groups":"1"}', 'checkbox'),
(2, 'Allow re-rating', 're_rating', '1', 'checkbox'),
(3, 'Skin', 'skin', 'skin.png', 'text'),
(4, 'Rating system', 'rating_system', '5', 'number');

-- --------------------------------------------------------

--
-- Table structure for table `rating_users`
--

CREATE TABLE IF NOT EXISTS `rating_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_id` int(11) unsigned NOT NULL DEFAULT '0',
  `score` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `target_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_mod` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_super` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `core` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `is_mod`, `is_admin`, `is_super`, `params`, `core`) VALUES
(1, 'Super Admin', 1, 1, 1, 'activity_view,user_username,attachment_upload,attachment_download', 1),
(2, 'Member', 0, 0, 0, '', 1),
(3, 'Guest', 0, 0, 0, '', 1),
(4, 'Administrator', 1, 1, 0, 'activity_view,user_username,attachment_upload,attachment_download', 0),
(5, 'Sale', 1, 0, 0, 'activity_view,user_username', 0),
(6, 'Seller', 0, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `version_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value_actual` mediumtext COLLATE utf8_unicode_ci,
  `value_default` mediumtext COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `ordering` tinyint(3) NOT NULL DEFAULT '0',
  `is_boot` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=161 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `group_id`, `label`, `name`, `field`, `value`, `is_hidden`, `version_id`, `type_id`, `value_actual`, `value_default`, `description`, `ordering`, `is_boot`) VALUES
(1, 1, 'Site Name', 'site_name', 'site_name', 'test', 0, NULL, 'text', 'Sonetrend', 'mooSocial', NULL, 1, 0),
(2, 1, 'Site Email', 'site_email', 'site_email', 'info@socialloft.com', 0, NULL, 'text', 'info@sonetrend.com', 'info@socialloft.com', NULL, 2, 0),
(3, 1, 'Site Keywords', 'site_keywords', 'site_keywords', '', 0, NULL, 'textarea', '', '', '', 3, 0),
(4, 1, 'Site Description', 'site_description', 'site_description', '', 0, NULL, 'textarea', '', '', '', 4, 0),
(5, 1, 'Default Home Feed', 'default_feed', 'default_feed', 'everyone', 0, NULL, 'select', '[{"name":"Everyone","value":"everyone","select":1},{"name":"Friends & Me","value":"friends","select":0}]', '[{"name":"Everyone","value":"everyone","select":0},{"name":"Friends & Me","value":"friends","select":0}]', NULL, 10, 0),
(6, 1, 'Note', 'admin_notes', 'admin_notes', '', 0, NULL, 'textarea', '', '', '', 4, 0),
(7, 3, 'Enable reCaptcha', 'recaptcha', 'recaptcha', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"1"}]', 'You must enter recaptcha public and private key if you<br>enable this. Click <a href="http://www.google.com/recaptcha" target="_blank">this</a> for more details', 2, 0),
(8, 3, 'reCaptcha Public Key', 'recaptcha_publickey', 'recaptcha_publickey', '', 0, NULL, 'text', '', '', NULL, 3, 0),
(9, 3, 'reCaptcha Private Key', 'recaptcha_privatekey', 'recaptcha_privatekey', '', 0, NULL, 'text', '', '', NULL, 4, 0),
(10, 3, 'Require Email Validation', 'email_validation', 'email_validation', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '0', NULL, 1, 0),
(11, 4, 'Homepage Guest Message', 'guest_message', 'guest_message', '', 0, NULL, 'textarea', '', '', NULL, 2, 0),
(12, 4, 'Header Block Code', 'header_code', 'header_code', '', 0, NULL, 'textarea', '', '', NULL, 4, 0),
(13, 4, 'Footer Block Code', 'footer_code', 'footer_code', '', 0, NULL, 'textarea', '', '', NULL, 7, 0),
(14, 4, 'Homepage Member Message', 'member_message', 'member_message', '', 0, NULL, 'textarea', '', '', NULL, 3, 0),
(15, 2, 'Allow Users To Select Theme', 'select_theme', 'select_theme', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 8, 0),
(16, 2, 'Allow Guests To Search', 'guest_search', 'guest_search', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 1, 0),
(17, 2, 'Photo theater mode', 'photo_theater_mode', 'photo_theater_mode', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 1, 0),
(18, 2, 'Enable follow function', 'enable_follow', 'enable_follow', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 1, 0),
(19, 1, 'Default Theme', 'default_theme', 'default_theme', 'default', 0, NULL, 'select', '[{"name":"Default Theme","value":"default","select":1}]', 'default', NULL, 9, 0),
(20, 2, 'Force Login', 'force_login', 'force_login', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"0"}]', 'Check this to force users to login to view the site', 5, 0),
(21, 4, 'Homepage Block Code', 'homepage_code', 'homepage_code', '', 0, NULL, 'textarea', '', '', NULL, 6, 0),
(22, 3, 'Ban IP Addresses', 'ban_ips', 'ban_ips', '', 0, NULL, 'textarea', '', '', 'Enter xyz.xyz.xyz.xyz to ban the exact ip address<br>Or xyz.xyz.xyz to ban ip addresses that start with xyz.xyz.xyz<br>One ip address per line', 5, 0),
(23, 3, 'Ban Email Addresses', 'ban_emails', 'ban_emails', '', 0, NULL, 'textarea', '', '', 'Enter xyz@abc.com to ban the exact email address<br>Or abc@* to ban email addresses that start with abc@<br>Or *@abc.com to ban email addresses that end with @abc.com<br>One email address per line', 5, 0),
(24, 2, 'Enable Activities Feed Selection', 'feed_selection', 'feed_selection', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 2, 0),
(25, 9, 'Take Site Offline', 'site_offline', 'site_offline', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '0', NULL, 5, 0),
(26, 9, 'Offline Message', 'offline_message', 'offline_message', '', 0, NULL, 'textarea', '', '', '', 6, 0),
(27, 1, 'Default Profile Privacy', 'profile_privacy', 'profile_privacy', '[{"name":"Everyone","value":"1","select":1},{"name":"Friends & Me","value":"2","select":0},{"name":"Only Me","value":"3","select":0}]', 0, NULL, 'select', '[{"name":"Everyone","value":"1","select":0},{"name":"Friends & Me","value":"2","select":0},{"name":"Only Me","value":"3","select":1}]', '1', NULL, 8, 0),
(28, 1, 'Popular Items Interval', 'popular_interval', 'popular_interval', '30', 0, NULL, 'text', '30', '30', 'Display popular items within X days ', 7, 0),
(29, 1, 'Default Language', 'default_language', 'default_language', 'eng', 0, NULL, 'language', 'eng', 'eng', '', 15, 0),
(30, 2, 'Disable Registration', 'disable_registration', 'disable_registration', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"1"}]', '0', NULL, 6, 0),
(31, 2, 'Allow invite friend to join site', 'allow_invite_friend', 'allow_invite_friend', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 7, 0),
(32, 1, 'Time Format', 'time_format', 'time_format', '12h', 0, NULL, 'select', '[{"name":"12-hour","value":"12","select":1},{"name":"24-hour","value":"24","select":0}]', '[{"name":"12-hour","value":"12","select":"1"},{"name":"24-hour","value":"24","select":0}]', NULL, 11, 0),
(33, 1, 'Date Format', 'date_format', 'date_format', '%B %d at %I:%M %p', 0, NULL, 'text', '%B %d at %I:%M %p', '%B %d at %I:%M %p', '<a target="_blank" href="http://php.net/manual/en/function.strftime.php">Refer to PHP date function for more information about<br>date format. Click this for more details</a>', 12, 0),
(34, 6, 'Upload New Logo', 'logo', 'logo', 'img/logo.png', 0, NULL, 'text', 'img/logo.png', '', NULL, 1, 0),
(35, 6, 'Upload New Og Image', 'og_image', 'og_image', 'img/og-image.png', 0, NULL, 'text', 'img/og-image.png', '', NULL, 2, 0),
(36, 3, 'Remove Admin Link', 'hide_admin_link', 'hide_admin_link', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"0"}]', 'Check this box to remove Admin link from user menu', 6, 0),
(37, 2, 'Allow Users To Select Language', 'select_language', 'select_language', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"1"}]', '1', NULL, 9, 0),
(38, 5, 'Analytics Code', 'analytics_code', 'analytics_code', '', 0, NULL, 'textarea', '', '', 'Enter your analytics code here<br>Click <a href="http://www.google.com/analytics/" target="_blank">this</a> to visit Google Analytics', 4, 0),
(39, 2, 'Registration Notification', 'registration_notify', 'registration_notify', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"0"}]', 'Check this to enable the system to send a notification email to the site email whenever a new user signs up', 7, 0),
(40, 2, 'Hide Activities Feed From Guests', 'hide_activites', 'hide_activites', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '0', NULL, 3, 0),
(41, 2, 'Allow Username Change', 'username_change', 'username_change', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 11, 0),
(42, 3, 'Restricted Usernames', 'restricted_usernames', 'restricted_usernames', '', 0, NULL, 'textarea', '', '', 'Enter usernames that you do not want users to register with. One username per line', 7, 0),
(43, 3, 'Enable Registration Code', 'enable_registration_code', 'enable_registration_code', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"0"}]', 'Check this box to force users to enter the correct registration code defined below in order to register', 8, 0),
(44, 3, 'Registration Code', 'registration_code', 'registration_code', '', 0, NULL, 'text', '', '', NULL, 9, 0),
(45, 5, 'Google Developer Key', 'google_dev_key', 'google_dev_key', '    ', 0, NULL, 'text', '', '', 'Use for youtube API and Google Map API. For more info please go to <a href="https://community.moosocial.com/topics/view/1337">link</a>', 5, 0),
(46, 1, 'Default Timezone', 'timezone', 'timezone', 'Africa/Accra', 0, NULL, 'timezone', 'Asia/Jakarta', 'Asia/Ho_Chi_Minh', '', 13, 0),
(47, 1, 'Enable Timezone Selection', 'enable_timezone_selection', 'enable_timezone_selection', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"1"}]', '[{"name":"","value":"1","select":"0"}]', 'If your users live in different timezones then enable this ', 14, 0),
(48, 2, 'Require Birthday', 'require_birthday', 'require_birthday', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 12, 0),
(49, 2, 'Show birthday field at sign up form', 'show_birthday_signup', 'show_birthday_signup', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 12, 0),
(50, 3, 'Enable Spam Challenge', 'enable_spam_challenge', 'enable_spam_challenge', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"0"}]', 'Check this box to force users to answer a<br>challenge question in order to register<br>Click <a href="/admin/spam_challenges">here</a> to manage challenge questions/answers', 10, 0),
(51, 1, 'Show "Powered by" link', 'show_credit', 'show_credit', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"1"}]', '[{"name":"","value":"1","select":"1"}]', NULL, 16, 0),
(52, 2, 'Allow Name Change', 'name_change', 'name_change', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"1"}]', 'Check this to allow users to change name ', 10, 0),
(53, 4, 'Registration Message', 'registration_message', 'registration_message', '', 0, NULL, 'textarea', '', '', NULL, 1, 0),
(54, 3, 'Enable SSL Mode', 'ssl_mode', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":0}]', '[{"name":"","value":"1","select":0}]', '', 1, 1),
(55, 9, 'System Mode', 'production_mode', NULL, '', 0, NULL, 'radio', '[{"name":"Production","value":"0","select":1},{"name":"Development 1","value":"1","select":0},{"name":"Development 2","value":"2","select":0}]', '[{"name":"Production","value":"0","select":1},{"name":"Development 1","value":"1","select":0},{"name":"Development 2","value":"2","select":0}]', 'Production Mode: No error messages, errors, or warnings shown. Flash messages redirect.Development Mode: 1: Errors and warnings shown, model caches refreshed, flash messages halted. 2: As in 1, but also with full debug messages and SQL output.', 1, 1),
(56, 1, 'Notification Interval', 'notification_interval', 'notification_interval', '10', 0, NULL, 'text', '10', '10', 'Refresh notification within X seconds', 7, 0),
(57, 1, 'Version', 'version', 'version', '2.6.1', 0, NULL, 'text', '2.6.1', '2.6.1', '', 17, 0),
(58, 3, 'Approve Users', 'approve_users', 'approve_users', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"0"}]', 'Enable this if you want to approve users before they can do activities on the site.', 1, 0),
(59, 13, 'Enable Blog Plugin', 'blog_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '', 1, 1),
(60, 12, 'Trigger Access Key', 'cron_key', NULL, '', 0, NULL, 'text', '3FSE@', '3FSE@', 'Used to prevent unauthorized running of the task scheduler.', 1, 0),
(61, 12, 'Concurrent Processes', 'cron_processes', NULL, '', 0, NULL, 'text', '2', '2', 'The maximum number of concurrent processes running tasks that are allowed.', 1, 0),
(62, 12, 'Tasks Run per Request', 'cron_count', NULL, '', 0, NULL, 'text', '1', '1', 'The maximum number of tasks that are run during each request. If a task is determined to have done nothing, it may not count towards this number.', 1, 0),
(63, 12, 'Time per Request', 'cron_time', NULL, '', 0, NULL, 'text', '120', '120', 'The maximum time allowed per request. This number will be automatically scaled if ini_get() can read max_execution_time from php.ini.', 1, 0),
(64, 12, 'Process Timeout', 'cron_timeout', NULL, '', 0, NULL, 'text', '900', '900', 'The maximum time before a task is considered to have died. The task will then be reset and freed to execute again.', 1, 0),
(65, 12, 'Concurrent Jobs', 'cron_jobs', NULL, '', 0, NULL, 'text', '3', '3', 'The maximum number of concurrently running jobs. This setting is limited by "Concurrent Processes"', 1, 0),
(66, 12, 'Javascript Enable', 'cron_javascript', NULL, '', 0, NULL, 'checkbox', '[{"name":"Enable","value":"1","select":"1"}]', '[{"name":"Enable","value":"1","select":"1"}]', 'Cron job auto call when user go to site', 2, 0),
(67, 14, 'Item per pages', 'topic_item_per_pages', NULL, '', 0, NULL, 'text', '10', '10', '', 1, 0),
(68, 14, 'Enable Topic Plugin', 'topic_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '', 1, 1),
(69, 13, 'Item per pages', 'blog_item_per_pages', NULL, '', 0, NULL, 'text', '10', '10', 'The number of blogs to display per page', 1, 0),
(70, 15, 'Photo item per pages', 'photo_item_per_pages', NULL, '', 0, NULL, 'text', '10', '10', '', 1, 0),
(71, 15, 'Album item per pages', 'album_item_per_pages', NULL, '', 0, NULL, 'text', '10', '10', '', 1, 0),
(72, 1, 'Max upload size (Mb)', 'photo_max_upload_size', NULL, '', 0, NULL, 'text', '10', '10', '', 1, 0),
(73, 15, 'Enable Photo Plugin', 'photo_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '', 1, 1),
(74, 16, 'Enable Video Plugin', 'video_enabled', '', '', 0, '2.2.0', 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', NULL, 1, 1),
(75, 16, 'Item per pages', 'video_item_per_pages', NULL, '', 0, '2.2.0', 'text', '10', '10', NULL, 1, 0),
(76, 17, 'Enable Page Plugin', 'page_enabled', '', '', 0, '2.2.0', 'radio', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', NULL, 1, 1),
(77, 18, 'Enable Event Plugin', 'event_enabled', '', '', 0, '2.2.0', 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', NULL, 1, 1),
(78, 18, 'Item per pages', 'event_item_per_pages', NULL, '', 0, '2.2.0', 'text', '10', '10', NULL, 1, 0),
(79, 19, 'Item per pages', 'group_item_per_pages', NULL, '', 0, NULL, 'text', '10', '10', '', 1, 0),
(80, 19, 'Enable Group Plugin', 'group_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '', 1, 1),
(81, 7, 'From Name', 'mail_name', NULL, '', 0, NULL, 'text', '', '', 'Enter the name you want the emails from the system to come from in the field below.', 1, 0),
(82, 7, 'From Address', 'mail_from', NULL, '', 0, NULL, 'text', '', '', 'Enter the email address you want the emails from the system to come from in the field below.', 2, 0),
(83, 7, 'Send through SMTP', 'mail_smtp', 'mail_smtp', 'Mail', 0, NULL, 'radio', '[{"name":"Use the built-in mail() function","value":"0","select":1},{"name":"Send emails through an SMTP server","value":"1","select":0}]', '[{"name":"Use the built-in mail() function","value":"0","select":1},{"name":"Send emails through an SMTP server","value":"1","select":0}]', 'Emails typically get sent through the web server using the PHP mail() function. Alternatively you can have emails sent out using SMTP, usually requiring a username and password, and optionally using an external mail server.', 5, 0),
(84, 7, 'SMTP Host', 'mail_smtp_host', 'mail_smtp_host', 'localhost', 0, NULL, 'text', 'localhost', 'localhost', 'In some cases, you might need to include ssl:// in the hostname ', 6, 0),
(85, 7, 'SMTP Username', 'mail_smtp_username', 'mail_smtp_username', '', 0, NULL, 'text', '', '', NULL, 8, 0),
(86, 7, 'SMTP Password', 'mail_smtp_password', 'mail_smtp_password', '', 0, NULL, 'text', '', '', NULL, 9, 0),
(87, 7, 'SMTP Port', 'mail_smtp_port', 'mail_smtp_port', '', 0, NULL, 'text', '', '', 'Default: 25. Also commonly on port 465 (SMTP over SSL) or port 587.', 7, 0),
(88, 7, 'Email Queue', 'mail_queueing', 'mail_queueing', '', 0, NULL, 'radio', '[{"name":"Yes, enable email queue","value":"1","select":0},{"name":"No, always send emails immediately","value":"0","select":1}]', '[{"name":"Yes, enable email queue","value":"1","select":0},{"name":"No, always send emails immediately","value":"0","select":1}]', 'Utilizing an email queue, you can allow your website to throttle the emails being sent out to prevent overloading the mail server.', 4, 0),
(89, 7, 'Use SSL or TLS?', 'mail_smtp_ssl', NULL, '', 0, NULL, 'radio', '[{"name":"None","value":"","select":1},{"name":"TLS","value":"tls","select":0},{"name":"SSL","value":"ssl","select":0}]', '[{"name":"None","value":"","select":1},{"name":"TLS","value":"tls","select":0},{"name":"SSL","value":"ssl","select":0}]', '', 10, 0),
(90, 7, 'Mail Count', 'mail_count', NULL, '', 0, NULL, 'text', '25', '25', 'The number of emails to send out each time the Background Mailer task is run.', 3, 0),
(91, 21, 'Facebook App Api', 'facebook_app_id', NULL, '1413388488948132', 0, NULL, 'text', '', '', '', 2, 0),
(92, 21, 'Facebook App Secret', 'facebook_app_secret', NULL, '', 0, NULL, 'text', '', '', '', 3, 0),
(93, 21, 'Scope', 'facebook_app_scope', NULL, '', 0, NULL, 'textarea', 'public_profile, email, user_friends', '', '', 4, 0),
(94, 21, 'Return Url', 'facebook_app_return_url', NULL, '', 0, NULL, 'text', 'http://localhost/social/auths/endpoint/facebook/', '', '', 5, 0),
(95, 21, 'SDK version', 'facebook_sdk_version', 'facebook_sdk_version', '[{"name":"5.0.0","value":"5.0.0","select":0},{"name":"3.2.3","value":"3.2.3","select":1}]', 0, NULL, 'select', '[{"name":"5.0.0","value":"5.0.0","select":1},{"name":"3.2.3","value":"3.2.3","select":0}]', '5.0.0', NULL, 6, 0),
(96, 21, 'Enable', 'facebook_integration', 'facebook_integration', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"0","select":"1"}]', 'You must enter FB application ID and secret if you enable this', 7, 0),
(97, 22, 'Google App Api', 'google_app_id', 'google_app_id', '', 0, NULL, 'text', '', '', '', 2, 0),
(98, 22, 'Google Client secret', 'google_app_secret', NULL, '', 0, NULL, 'text', '', '', '', 6, 0),
(99, 22, 'Scope', 'google_app_scope', NULL, '', 0, NULL, 'textarea', 'profile email', '', '', 7, 0),
(100, 22, 'Return Url', 'google_app_return_url', NULL, '', 0, NULL, 'text', 'http://localhost/social/auths/endpoint/google?hauth.done=Google', '', '', 8, 0),
(101, 22, 'Enable', 'google_integration', 'google_integration', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"0","select":"1"}]', 'You must enter Google application ID and secret if you enable this', 9, 0),
(102, 2, 'Allow Profile Picture upload during SignUp', 'allow_upload_avatar_signup', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"1"}]', '', 13, 0),
(103, 2, 'Allow and require Profile Picture upload during SignUp', 'require_upload_avatar', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"1"}]', '', 14, 0),
(104, 2, 'Auto add friend with', 'auto_add_friend', NULL, '', 0, NULL, 'text', '', '', '', 15, 0),
(105, 2, 'Auto Load More', 'auto_load_more', 'auto_load_more', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"1"}]', '1', NULL, 16, 0),
(106, 2, 'Enable hashtag on activity feed', 'enable_hashtag_activity', NULL, '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', '', 17, 0),
(107, 13, 'Enable Blog Hashtag', 'blog_hashtag_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '', 2, 0),
(108, 14, 'Enable Topic Hashtag', 'topic_hashtag_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '', 2, 0),
(109, 15, 'Enable Photo Hashtag', 'photo_hashtag_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '', 2, 0),
(110, 16, 'Enable Video Hashtag', 'video_hashtag_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '', 2, 0),
(111, 18, 'Enable Event Hashtag', 'event_hashtag_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '', 2, 0),
(112, 19, 'Enable Group Hashtag', 'group_hashtag_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '', 2, 0),
(113, 19, 'Notification for group''s activities', 'group_enable_send_notification', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":1},{"name":"Enable","value":"1","select":0}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '', 3, 0),
(114, 11, 'Enable Subscription Packages', 'enable_subscription_packages', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":0}]', 'Enable Subscription Packages ', 1, 0),
(115, 11, 'Select theme', 'select_theme_subscription_packages', NULL, '', 0, NULL, 'radio', '[{"name":"Select plan basic theme","value":"0","select":1},{"name":"Select plan compare table theme","value":"1","select":0}]', '[{"name":"Select plan basic theme","value":"0","select":1},{"name":"Select plan compare table theme","value":"1","select":0}]', 'Select Theme Subscription Packages ', 1, 0),
(116, 2, 'Enable Cookies', 'enable_cookies', 'enable_cookies', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"1"}]', '1', 'You must enter deny url if you<br>enable this.', 17, 0),
(117, 2, 'Deny Url', 'deny_url', 'deny_url', '', 0, NULL, 'text', '', '', 'Please include protocol http:// or https://', 17, 0),
(118, 2, 'Hide dislike', 'hide_dislike', 'hide_dislike', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"0"}]', '', 19, 0),
(119, 2, 'Allow sending message to non-friend', 'send_message_to_non_friend', 'send_message_to_non_friend', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', 'Check if users are allowed to send message to non-friend. Individual users can set option whether or not to only receive message from friend in profile setting. Otherwise, If unchecked, users can send message to friends only.', 18, 0),
(120, 1, 'Comment sort style', 'comment_sort_style', NULL, '', 0, NULL, 'radio', '[{"name":"Recent comment","value":"0","select":1},{"name":"Chronological","value":"1","select":0}]', '[{"name":"Recent comment","value":"0","select":1},{"name":"Chronological","value":"1","select":0}]', 'Recent comment : New comments go to the top. Chronological : All comments with newest comments at the bottom.', 100, 1),
(121, 2, 'Require gender', 'require_gender', 'require_gender', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 20, 0),
(122, 2, 'Show gender field at sign up form', 'show_gender_signup', 'show_gender_signup', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 20, 0),
(123, 2, 'Enable "Unspecified" Gender', 'enable_unspecified_gender', 'enable_unspecified_gender', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"0"}]', 'Members will be able to select "Unspecified" as their gender during signup or edit their profile', 20, 0),
(124, 2, 'Enable Profile pop-up', 'profile_popup', 'profile_popup', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '1', NULL, 1, 0),
(125, 2, 'Min age Restriction', 'min_age_restriction', 'min_age_restriction', '', 0, NULL, 'text', '0', '0', 'Min age to be able to create an account. Enter "0" to disable this Restriction.', 21, 0),
(126, 2, 'Max age Restriction', 'max_age_restriction', 'max_age_restriction', '', 0, NULL, 'text', '0', '0', 'Max age to be able to create an account. Enter "0" to disable this Restriction.', 22, 0),
(127, 2, 'Auto disable accounts that do not match above min age restriction or has reached max age restriction', 'auto_disable_reach_max_age', 'auto_disable_reach_max_age', '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '0', 'If this option is checked, accounts that do not match with new min age restriction or has reached new max age restriction changed by you above will be auto disabled once they sign in again.', 23, 0),
(128, 2, 'Enable category toggle', 'enable_category_toggle', 'enable_category_toggle', '1', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"1"}]', '1', NULL, 24, 0),
(129, 2, 'Site domain', 'site_domain', 'site_domain', '', 0, NULL, 'text', 'http://live.sonetech.info/', '0', 'This is your current registered domain. If you changed to new domain you need to update this information', 25, 0),
(130, 2, 'Display profile type on the main user profile tab', 'enable_show_profile_type', 'enable_show_profile_type', '0', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '0', NULL, 26, 0),
(131, 2, 'By pass force login for user', 'user_consider_force', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":0}]', 'By pass force login for user', 27, 1),
(132, 2, 'Allow Users To Change profile type', 'allow_edit_profile_type', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":0}]', 'Allow Users To Change profile type', 28, 1),
(133, 26, 'Access Key ID', 'storage_amazon_access_key', NULL, '', 0, NULL, 'text', '', '', 'If you don''t have access keys, you can create them by using the AWS Management Console.', 1, 0),
(134, 26, 'Secret Access Key', 'storage_amazon_secret_key', NULL, '', 0, NULL, 'text', '', 'vhD+zhRtLgnw95bqEzt6IoGnfjS/U/tiUWxRCiVC', '', 2, 0),
(135, 26, 'Bucket Name', 'storage_amazon_bucket_name', NULL, '', 0, NULL, 'text', 'moosocial', '', 'If the bucket does not exist, we will attempt to create it. Please note the following restrictions on bucket names: must start and end with a number or letter,must only contain lowercase letters, numbers, and dashes [a-z0-9-],must be between 3 and 255 characters long', 3, 0),
(136, 26, 'Region', 'storage_amazon_s3_region', NULL, '', 0, NULL, 'select', '[{"name":"US East (N. Virginia)","value":"us-east-1","select":0},{"name":"US East (Ohio)","value":"us-east-2","select":0},{"name":"US West (N. California)","value":"us-west-1","select":0},{"name":"US West (Oregon)","value":"us-west-2","select":1},{"name":"Asia Pacific (Mumbai)","value":"ap-south-1","select":0},{"name":"Asia Pacific (Seoul)","value":"ap-northeast-2","select":0},{"name":"Asia Pacific (Singapore)","value":"ap-southeast-1","select":0},{"name":"Asia Pacific (Sydney)","value":"ap-southeast-2","select":0},{"name":"Asia Pacific (Tokyo)","value":"ap-northeast-1","select":0},{"name":"EU (Frankfurt)","value":"eu-central-1","select":0},{"name":"EU (Ireland)","value":"eu-west-1","select":0},{"name":"South America (S\\u00e3o Paulo)","value":"sa-east-1","select":0}]', '[{"name":"US East (N. Virginia)","value":"us-east-1","select":0},{"name":"US East (Ohio)","value":"us-east-2","select":0},{"name":"US West (N. California)","value":"us-west-1","select":0},{"name":"US West (Oregon)","value":"us-west-2","select":0},{"name":"Asia Pacific (Mumbai)","value":"ap-south-1","select":0},{"name":"Asia Pacific (Seoul)","value":"ap-northeast-2","select":0},{"name":"Asia Pacific (Singapore)","value":"ap-southeast-1","select":0},{"name":"Asia Pacific (Sydney)","value":"ap-southeast-2","select":0},{"name":"Asia Pacific (Tokyo)","value":"ap-northeast-1","select":0},{"name":"EU (Frankfurt)","value":"eu-central-1","select":0},{"name":"EU (Ireland)","value":"eu-west-1","select":0},{"name":"South America (S\\u00e3o Paulo)","value":"sa-east-1","select":0}]', 'The region in which your bucket resides . Be cafeful to specify this accurtely,as you are likely to see strange or broken behavior if the region is set wrong.', 4, 0),
(137, 26, 'Use a CNAME', 'storage_amazon_use_cname', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":0}]', 'Serve files from a cusom domain by using an appropriately named bucket, e.g. mybucket.mydomain.com', 101, 0),
(138, 26, 'Always serve files from Amazon S3 via HTTPS', 'storage_amazon_server_file_vi_https', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"1"}]', '[{"name":"","value":"1","select":"1"}]', 'Forces S3 File System to always generate HTTPS URLs for files in your bucket, e.g. ''https://mybucket.s3.amazonaws.com/file.jpg''. Without this setting enabled,URLs for your files will use the same scheme as the page they are served from.', 103, 0),
(139, 26, 'Current Storage', 'storage_current_type', NULL, '', 0, NULL, 'text', 'local', '', '', 104, 1),
(140, 26, 'Customizing Amazon S3 URLs with CNAMEs', 'storage_amazon_url_cname', NULL, '', 0, NULL, 'text', '', '', 'Depending on your needs, you might not want s3.amazonaws.com to appear on your website or service. For example, if you host your website images on Amazon S3, you might prefer http://images.moosite.net/ instead of http://s3.amazonaws.com/imgages.For more information , see http://docs.aws.amazon.com/AmazonS3/latest/dev/VirtualHosting.html', 105, 0),
(141, 26, 'Do not rewrite CSS/JS file paths', 'storage_amazon_use_css_js_path', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":0}]', 'If this box is checked, the CSS/JS file paths will not be replaced on the page with their CDN path. Notice that the CDN path is only to be used in Production mode.', 106, 0),
(142, 26, 'Enable CloudFront', 'storage_cloudfront_enable', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":0}]', '[{"name":"","value":"1","select":0}]', '', 108, 0),
(143, 26, 'CDN Mapping', 'storage_cloudfront_cdn_mapping', NULL, '', 0, NULL, 'text', '', '', 'Be sure to add the protocol in front of the domain name. For example, http://my.cloudfrontcdndomain.net will work but my.cloudfrontcdndomain.net may cause problems. Be sure to use HTTPS.', 107, 0),
(144, 26, 'Enable CDN', 'storage_localcdn_enable', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":0}]', '[{"name":"","value":"1","select":0}]', '', 110, 1),
(145, 26, 'CDN Mapping', 'storage_local_cdn_mapping', NULL, '', 0, NULL, 'text', '', '', 'Be sure to add the protocol in front of the domain name. For example, http://my.cloudflare.com will work but my.cloudflare.com may cause problems. Be sure to use HTTPS.', 109, 1),
(146, 26, 'Delete image when it is added to Amazon S3', 'storage_amazon_delete_image_after_adding', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":"0"}]', 'Delete image when it is added to Amazon S3', 111, 0),
(147, 13, 'By pass force login', 'blog_consider_force', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":0}]', 'By pass force login', 3, 1),
(148, 16, 'By pass force login', 'video_consider_force', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":0}]', 'By pass force login', 3, 1),
(149, 14, 'By pass force login', 'topic_consider_force', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":0}]', 'By pass force login', 3, 1),
(150, 15, 'By pass force login', 'photo_consider_force', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":0}]', 'By pass force login', 3, 1),
(151, 19, 'By pass force login', 'group_consider_force', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"0"}]', '[{"name":"","value":"1","select":0}]', 'By pass force login', 4, 1),
(152, 18, 'By pass force login', 'event_consider_force', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":0}]', '[{"name":"","value":"1","select":0}]', 'By pass force login', 3, 1),
(153, 27, 'Pakage', 'pakage_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', NULL, 0, 1),
(154, 27, 'Pakage', 'pakage_item_per_pages', NULL, '', 0, NULL, 'text', '10', '', NULL, 1, 1),
(155, 27, 'Pakage', 'pakage_allow_sale_create', NULL, '', 0, NULL, 'checkbox', '[{"name":"","value":"1","select":"1"}]', '[{"name":"","value":"1","select":0}]', NULL, 2, 1),
(156, 28, 'Livestream', 'livestream_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', NULL, 0, 1),
(157, 28, 'Livestream', 'livestream_item_per_pages', NULL, '', 0, NULL, 'text', '10', NULL, NULL, 1, 1),
(158, 29, 'Payment', 'payment_enabled', NULL, '', 0, NULL, 'radio', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', '[{"name":"Disable","value":"0","select":0},{"name":"Enable","value":"1","select":1}]', NULL, 0, 1),
(159, 29, 'Payment', 'payment_item_per_pages', NULL, '', 0, NULL, 'text', '10', '10', NULL, 1, 1),
(160, 29, 'Payment', 'payment_price_owe_show', NULL, '', 0, NULL, 'text', '10', '10', NULL, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `setting_groups`
--

CREATE TABLE IF NOT EXISTS `setting_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `group_type` varchar(255) NOT NULL DEFAULT '',
  `module_id` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `group_id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `setting_groups`
--

INSERT INTO `setting_groups` (`id`, `parent_id`, `lft`, `rght`, `group_type`, `module_id`, `name`) VALUES
(1, 0, NULL, NULL, 'core', 'core', 'General'),
(2, 0, NULL, NULL, 'core', 'core', 'Features'),
(3, 0, NULL, NULL, 'core', 'core', 'Security'),
(4, 0, NULL, NULL, 'core', 'core', 'Custom Blocks'),
(5, 0, NULL, NULL, 'core', 'core', 'Intergration'),
(6, 0, NULL, NULL, 'core', 'core', 'Logo & OG Image'),
(7, 0, NULL, NULL, 'core', 'Mail', 'Mail'),
(9, 0, NULL, NULL, 'core', 'core', 'System Mode'),
(10, 0, NULL, NULL, 'core', 'Billing', 'Billing'),
(11, 0, NULL, NULL, 'core', 'Subscription', 'Subscription'),
(12, 0, NULL, NULL, 'core', 'Cron', 'Cron'),
(13, 0, NULL, NULL, 'Blog', 'Blog', 'Blog'),
(14, 0, NULL, NULL, 'Topic', 'Topic', 'Topic'),
(15, 0, NULL, NULL, 'Photo', 'Photo', 'Photo'),
(16, 0, NULL, NULL, 'Video', 'Video', 'Video'),
(17, 0, NULL, NULL, 'Page', 'Page', 'Page'),
(18, 0, NULL, NULL, 'Event', 'Event', 'Event'),
(19, 0, NULL, NULL, 'Group', 'Group', 'Group'),
(20, 0, NULL, NULL, 'SocialIntegration', 'SocialIntegration', 'Social Integration'),
(21, 20, NULL, NULL, 'FacebookIntegration', 'FacebookIntegration', 'Facebook Integration'),
(22, 20, NULL, NULL, 'GoogleIntegration', 'GoogleIntegration', 'Google Integration'),
(23, 0, NULL, NULL, 'core', 'core', 'Site Map'),
(24, 0, NULL, NULL, 'core', 'core', 'SEO Settings & Rules'),
(25, 0, NULL, NULL, 'core', 'core', 'Activity Feed Settings'),
(26, 0, NULL, NULL, 'Storage', 'Storage', 'Storage System'),
(27, 0, NULL, NULL, 'Pakage', 'Pakage', 'Pakage'),
(28, 0, NULL, NULL, 'Livestream', 'Livestream', 'Livestream Order'),
(29, 0, NULL, NULL, 'Payment', 'Payment', 'Payment');

-- --------------------------------------------------------

--
-- Table structure for table `sitemaps`
--

CREATE TABLE IF NOT EXISTS `sitemaps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `type` varchar(16) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `sitemaps`
--

INSERT INTO `sitemaps` (`id`, `url`, `type`) VALUES
(1, 'http://live.sonetech.info/users/view/1', 'Core'),
(2, 'http://live.sonetech.info/users/view/2', 'Core'),
(3, 'http://live.sonetech.info/users/view/3', 'Core'),
(4, 'http://live.sonetech.info/users/view/4', 'Core'),
(5, 'http://live.sonetech.info/users/view/5', 'Core'),
(6, 'http://live.sonetech.info/users/view/6', 'Core'),
(7, 'http://live.sonetech.info/users/view/7', 'Core'),
(8, 'http://live.sonetech.info/users/view/8', 'Core'),
(9, 'http://live.sonetech.info/users/view/9', 'Core'),
(10, 'http://live.sonetech.info/users/view/10', 'Core'),
(11, 'http://live.sonetech.info/users/view/11', 'Core'),
(12, 'http://live.sonetech.info/users/view/12', 'Core'),
(13, 'http://live.sonetech.info/users/view/13', 'Core'),
(14, 'http://live.sonetech.info/users/view/14', 'Core'),
(15, 'http://live.sonetech.info/users/view/15', 'Core'),
(16, 'http://live.sonetech.info/users/view/16', 'Core'),
(17, 'http://live.sonetech.info/users/view/17', 'Core'),
(18, 'http://live.sonetech.info/users/view/18', 'Core'),
(19, 'http://live.sonetech.info/users/view/19', 'Core'),
(20, 'http://live.sonetech.info/users/view/20', 'Core'),
(21, 'http://live.sonetech.info/users/view/21', 'Core'),
(22, 'http://live.sonetech.info/users/view/22', 'Core'),
(23, 'http://live.sonetech.info/users/view/23', 'Core'),
(24, 'http://live.sonetech.info/users/view/24', 'Core'),
(25, 'http://live.sonetech.info/users/view/25', 'Core'),
(26, 'http://live.sonetech.info/users/view/26', 'Core'),
(27, 'http://live.sonetech.info/users/view/27', 'Core'),
(28, 'http://live.sonetech.info/users/view/28', 'Core'),
(29, 'http://live.sonetech.info/users/view/29', 'Core'),
(30, 'http://live.sonetech.info/users/view/30', 'Core'),
(31, 'http://live.sonetech.info/users/view/31', 'Core'),
(32, 'http://live.sonetech.info/users/view/32', 'Core'),
(33, 'http://live.sonetech.info/users/view/33', 'Core'),
(34, 'http://live.sonetech.info/users/view/34', 'Core'),
(35, 'http://live.sonetech.info/users/view/35', 'Core'),
(36, 'http://live.sonetech.info/users/view/36', 'Core'),
(37, 'http://live.sonetech.info/users/view/37', 'Core'),
(38, 'http://live.sonetech.info/users/view/38', 'Core'),
(39, 'http://live.sonetech.info/users/view/39', 'Core'),
(40, 'http://live.sonetech.info/users/view/40', 'Core'),
(41, 'http://live.sonetech.info/users/view/41', 'Core'),
(42, 'http://live.sonetech.info/users/view/42', 'Core'),
(43, 'http://live.sonetech.info/users/view/43', 'Core'),
(44, 'http://live.sonetech.info/users/view/44', 'Core'),
(45, 'http://live.sonetech.info/users/view/45', 'Core'),
(46, 'http://live.sonetech.info/users/view/46', 'Core'),
(47, 'http://live.sonetech.info/users/view/47', 'Core'),
(48, 'http://live.sonetech.info/users/view/48', 'Core'),
(49, 'http://live.sonetech.info/users/view/49', 'Core'),
(50, 'http://live.sonetech.info/users/view/50', 'Core'),
(51, 'http://live.sonetech.info/users/view/51', 'Core'),
(52, 'http://live.sonetech.info/users/view/52', 'Core'),
(53, 'http://live.sonetech.info/users/view/53', 'Core'),
(54, 'http://live.sonetech.info/users/view/54', 'Core'),
(55, 'http://live.sonetech.info/users/view/55', 'Core'),
(56, 'http://live.sonetech.info/users/view/56', 'Core'),
(57, 'http://live.sonetech.info/users/view/57', 'Core'),
(58, 'http://live.sonetech.info/users/view/58', 'Core'),
(59, 'http://live.sonetech.info/users/view/59', 'Core'),
(60, 'http://live.sonetech.info/users/view/60', 'Core'),
(61, 'http://live.sonetech.info/users/view/61', 'Core'),
(62, 'http://live.sonetech.info/users/view/62', 'Core'),
(63, 'http://live.sonetech.info/users/view/63', 'Core'),
(64, 'http://live.sonetech.info/users/view/64', 'Core'),
(65, 'http://live.sonetech.info/users/view/65', 'Core'),
(66, 'http://live.sonetech.info/users/view/66', 'Core'),
(67, 'http://live.sonetech.info/users/view/67', 'Core'),
(68, 'http://live.sonetech.info/users/view/68', 'Core'),
(69, 'http://live.sonetech.info/users/view/69', 'Core'),
(70, 'http://live.sonetech.info/users/view/70', 'Core'),
(71, 'http://live.sonetech.info/users/view/71', 'Core'),
(72, 'http://live.sonetech.info/users/view/72', 'Core'),
(73, 'http://live.sonetech.info/users/view/73', 'Core'),
(74, 'http://live.sonetech.info/users/view/74', 'Core'),
(75, 'http://live.sonetech.info/users/view/75', 'Core'),
(76, 'http://live.sonetech.info/users/view/76', 'Core'),
(77, 'http://live.sonetech.info/users/view/77', 'Core'),
(78, 'http://live.sonetech.info/users/view/78', 'Core'),
(79, 'http://live.sonetech.info/users/view/79', 'Core'),
(80, 'http://live.sonetech.info/users/view/80', 'Core');

-- --------------------------------------------------------

--
-- Table structure for table `sitemap_settings`
--

CREATE TABLE IF NOT EXISTS `sitemap_settings` (
  `name` varchar(128) NOT NULL,
  `value` text NOT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sitemap_settings`
--

INSERT INTO `sitemap_settings` (`name`, `value`) VALUES
('sitemap_build_in_progress', '0'),
('sitemap_enable', '1'),
('sitemap_entities', '{"Core":{"items":[{"name":"user","data_fetched":true,"urls_count":80}],"enabled":true,"priority":0.5,"changefreq":"weekly"},"Page":{"items":[{"name":"page","data_fetched":true,"urls_count":0}],"enabled":true,"priority":0.5,"changefreq":"weekly"},"Pakage":{"items":[{"name":"pakage","data_fetched":false,"urls_count":0}],"enabled":true,"priority":0.5,"changefreq":"weekly"},"LivestreamListener":{"items":[{"name":"livestream","data_fetched":false,"urls_count":0}],"enabled":true,"priority":0.5,"changefreq":"weekly"}}'),
('sitemap_entitites_limit', '5000'),
('sitemap_entitites_max_count', '50000'),
('sitemap_ignore_key', ''),
('sitemap_index', '0'),
('sitemap_in_progress', '1'),
('sitemap_in_progress_time', '1505121248'),
('sitemap_last_build', '2'),
('sitemap_last_start', '1504077433'),
('sitemap_max_urls_in_file', '50000'),
('sitemap_schedule_update', 'weekly');

-- --------------------------------------------------------

--
-- Table structure for table `social_users`
--

CREATE TABLE IF NOT EXISTS `social_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `provider` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `provider_uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `access_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `code_secret` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `spam_challenges`
--

CREATE TABLE IF NOT EXISTS `spam_challenges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `answers` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `state_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=52 ;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `state_code`, `country_id`, `order`) VALUES
(1, 'Alaska', 'AK', 1, 2),
(2, 'Alabama', 'AL', 1, 1),
(3, 'Arkansas', 'AR', 1, 4),
(4, 'Arizona', 'AZ', 1, 3),
(5, 'California', 'CA', 1, 5),
(6, 'Colorado', 'CO', 1, 6),
(7, 'Connecticut', 'CT', 1, 7),
(8, 'District of Columbia', 'DC', 1, 9),
(9, 'Delaware', 'DE', 1, 8),
(10, 'Florida', 'FL', 1, 10),
(11, 'Georgia', 'GA', 1, 11),
(12, 'Hawaii', 'HI', 1, 12),
(13, 'Iowa', 'IA', 1, 16),
(14, 'Idaho', 'ID', 1, 13),
(15, 'Illinois', 'IL', 1, 14),
(16, 'Indiana', 'IN', 1, 15),
(17, 'Kansas', 'KS', 1, 17),
(18, 'Kentucky', 'KY', 1, 18),
(19, 'Louisiana', 'LA', 1, 19),
(20, 'Massachusetts', 'MA', 1, 22),
(21, 'Maryland', 'MD', 1, 21),
(22, 'Maine', 'ME', 1, 20),
(23, 'Michigan', 'MI', 1, 23),
(24, 'Minnesota', 'MN', 1, 24),
(25, 'Missouri', 'MO', 1, 26),
(26, 'Mississippi', 'MS', 1, 25),
(27, 'Montana', 'MT', 1, 27),
(28, 'North Carolina', 'NC', 1, 34),
(29, 'North Dakota', 'ND', 1, 35),
(30, 'Nebraska', 'NE', 1, 28),
(31, 'New Hampshire', 'NH', 1, 30),
(32, 'New Jersey', 'NJ', 1, 31),
(33, 'New Mexico', 'NM', 1, 32),
(34, 'Nevada', 'NV', 1, 29),
(35, 'New York', 'NY', 1, 33),
(36, 'Ohio', 'OH', 1, 36),
(37, 'Oklahoma', 'OK', 1, 37),
(38, 'Oregon', 'OR', 1, 38),
(39, 'Pennsylvania', 'PA', 1, 39),
(40, 'Rhode Island', 'RI', 1, 40),
(41, 'South Carolina', 'SC', 1, 41),
(42, 'South Dakota', 'SD', 1, 42),
(43, 'Tennessee', 'TN', 1, 43),
(44, 'Texas', 'TX', 1, 44),
(45, 'Utah', 'UT', 1, 45),
(46, 'Virginia', 'VA', 1, 47),
(47, 'Vermont', 'VT', 1, 46),
(48, 'Washington', 'WA', 1, 48),
(49, 'Wisconsin', 'WI', 1, 50),
(50, 'West Virginia', 'WV', 1, 49),
(51, 'Wyoming', 'WY', 1, 51);

-- --------------------------------------------------------

--
-- Table structure for table `storage_aws_object_maps`
--

CREATE TABLE IF NOT EXISTS `storage_aws_object_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `bucket` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `storage_aws_object_transfers`
--

CREATE TABLE IF NOT EXISTS `storage_aws_object_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `storage_aws_tasks`
--

CREATE TABLE IF NOT EXISTS `storage_aws_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `oid` int(11) NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(2048) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(2048) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bucket` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `key` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subscribes`
--

CREATE TABLE IF NOT EXISTS `subscribes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `plan_id` int(11) unsigned NOT NULL DEFAULT '0',
  `status` enum('initial','active','pending','expired','refunded','failed','cancel','process','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'initial',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `reminder_date` datetime DEFAULT NULL,
  `pay_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `gateway_id` int(10) DEFAULT NULL,
  `is_warning_email_sent` tinyint(1) DEFAULT '0',
  `currency_code` text COLLATE utf8_unicode_ci NOT NULL,
  `package_id` int(11) NOT NULL DEFAULT '0',
  `is_trial` tinyint(1) NOT NULL DEFAULT '1',
  `is_request_refund` tinyint(1) NOT NULL DEFAULT '0',
  `transaction_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_compares`
--

CREATE TABLE IF NOT EXISTS `subscription_compares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `compare_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `compare_value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_packages`
--

CREATE TABLE IF NOT EXISTS `subscription_packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(10) DEFAULT '0',
  `recommended` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) DEFAULT '1',
  `deleted` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_package_plans`
--

CREATE TABLE IF NOT EXISTS `subscription_package_plans` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subscription_package_id` int(11) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(2) NOT NULL DEFAULT '0',
  `price` decimal(16,2) unsigned NOT NULL DEFAULT '0.00',
  `plan_duration` int(11) unsigned NOT NULL DEFAULT '0',
  `plan_type` enum('day','week','month','year','forever') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'day',
  `expiration_reminder` int(11) unsigned NOT NULL DEFAULT '0',
  `expiration_reminder_type` enum('day','week','month','year') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'day',
  `show_at` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `enable_plan` tinyint(1) NOT NULL DEFAULT '0',
  `billing_cycle` int(11) unsigned NOT NULL DEFAULT '0',
  `billing_cycle_type` enum('day','week','month','year') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'day',
  `trial_price` decimal(16,2) unsigned NOT NULL DEFAULT '0.00',
  `trial_duration` int(11) unsigned NOT NULL DEFAULT '0',
  `trial_duration_type` enum('day','week','month','year') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'day',
  `deleted` tinyint(2) NOT NULL DEFAULT '0',
  `order` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_refunds`
--

CREATE TABLE IF NOT EXISTS `subscription_refunds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscribe_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('initial','denied','process','completed','failed') NOT NULL DEFAULT 'initial',
  `account` varchar(128) NOT NULL DEFAULT '',
  `reason` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `transaction_id` int(11) NOT NULL DEFAULT '0',
  `plan_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_transactions`
--

CREATE TABLE IF NOT EXISTS `subscription_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `subscribes_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `gateway_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status` enum('initial','completed','pending','expired','refunded','failed','cancel','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'initial',
  `amount` decimal(16,2) NOT NULL DEFAULT '0.00',
  `currency` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `callback_params` text COLLATE utf8_unicode_ci,
  `plan_id` int(11) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `type` enum('receive','pay') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'receive',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `plugin` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `timeout` int(11) unsigned NOT NULL DEFAULT '60',
  `processes` smallint(3) unsigned NOT NULL DEFAULT '1',
  `semaphore` smallint(3) NOT NULL DEFAULT '0',
  `started_last` int(11) NOT NULL DEFAULT '0',
  `started_count` int(11) unsigned NOT NULL DEFAULT '0',
  `completed_last` int(11) NOT NULL DEFAULT '0',
  `completed_count` int(11) unsigned NOT NULL DEFAULT '0',
  `failure_last` int(11) NOT NULL DEFAULT '0',
  `failure_count` int(11) unsigned NOT NULL DEFAULT '0',
  `success_last` int(11) NOT NULL DEFAULT '0',
  `success_count` int(11) unsigned NOT NULL DEFAULT '0',
  `enable` tinyint(1) NOT NULL DEFAULT '0',
  `class` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `started_last` (`started_last`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `title`, `plugin`, `timeout`, `processes`, `semaphore`, `started_last`, `started_count`, `completed_last`, `completed_count`, `failure_last`, `failure_count`, `success_last`, `success_count`, `enable`, `class`) VALUES
(1, 'Job Queue', 'Cron', 5, 1, 0, 1505235580, 424, 1505235580, 424, 0, 0, 1505235580, 424, 1, 'Cron_Task_Jobs'),
(2, 'Subscription', 'Subscription', 600, 1, 0, 1505235580, 94, 1505235580, 94, 0, 0, 1505235580, 94, 1, 'Subscription_Task_Subscription'),
(3, 'Background Mailer', 'Mail', 60, 1, 0, 1505121321, 181, 1505121321, 181, 0, 0, 1505121321, 181, 1, 'Mail_Task_Cron'),
(4, 'Notifications Summary', 'Core', 86400, 1, 0, 1505102323, 13, 1505102324, 13, 0, 0, 1505102324, 13, 1, 'Task_Reminder_Notifications'),
(5, 'Sitemap generate', 'Core', 5, 1, 0, 1505121328, 185, 1505121328, 183, 1505121248, 5, 1505121328, 178, 1, 'Task_Sitemap');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `core` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `custom_css_enable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `custom_css` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `key`, `name`, `core`, `custom_css_enable`, `custom_css`) VALUES
(1, 'default', 'Default Theme', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salt` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `friend_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `notification_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `friend_request_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `blog_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `topic_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `group_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `event_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `conversation_user_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `video_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `birthday` date DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `confirmed` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `notification_email` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `timezone` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ip_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lang` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `hide_online` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cover` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `approved` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'admin approved user feature',
  `is_social` tinyint(1) NOT NULL DEFAULT '0',
  `has_active_subscription` tinyint(1) NOT NULL DEFAULT '0',
  `receive_message_from_non_friend` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `send_email_when_send_message` tinyint(1) NOT NULL DEFAULT '1',
  `request_friend_email` tinyint(1) NOT NULL DEFAULT '1',
  `notification_setting` text COLLATE utf8_unicode_ci,
  `profile_type_id` int(11) NOT NULL DEFAULT '1',
  `user_create` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `gender` (`gender`),
  KEY `active` (`active`,`id`),
  KEY `username` (`username`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=81 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `salt`, `role_id`, `avatar`, `photo`, `created`, `last_login`, `photo_count`, `friend_count`, `notification_count`, `friend_request_count`, `blog_count`, `topic_count`, `group_count`, `event_count`, `conversation_user_count`, `video_count`, `gender`, `birthday`, `active`, `confirmed`, `code`, `notification_email`, `timezone`, `ip_address`, `privacy`, `username`, `about`, `featured`, `lang`, `hide_online`, `cover`, `approved`, `is_social`, `has_active_subscription`, `receive_message_from_non_friend`, `send_email_when_send_message`, `request_friend_email`, `notification_setting`, `profile_type_id`, `user_create`) VALUES
(1, 'admin', 'admin@sonetrend.com', '7eae6c369a25bd2e858a6abdb632bbbb', '7d54', 1, '', '', '2017-08-21 20:20:05', '2017-09-11 09:13:59', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Male', '2017-08-21', 1, 1, 'b0e8491e684bd1be18d9195d90c638bd', 1, 'Asia/Jakarta', '', 1, '', '', 0, 'eng', 0, '', 1, 0, 0, 1, 1, 1, NULL, 1, 1),
(2, 'Lê Nhâm Thân', 'than.le@sonetrend.com', '1bd956bbb21307e396c85a9302b81180', 'b719', 4, '', '', '2017-08-22 08:24:09', '2017-09-05 15:31:40', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Male', '1992-07-03', 1, 1, '16f11dee00b0e66c4115e99771e80b93', 1, '', '192.168.1.51', 1, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 1, 1),
(3, 'Nguyễn Văn Thịnh', 'thinh.nguyen@sonetrend.com', '16da28278247bd6bf6d2e9cc85402e06', '9450', 4, '', '', '2017-08-23 14:23:07', '2017-09-05 05:27:25', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'f7bbef12011d382d2383583cd9c0efee', 1, '', '115.72.62.28', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 1, 1),
(4, 'Nguyễn Lê Trung', 'trung.nguyen@sonetrend.com', 'afc3f3c86020ac2db132a6b5ec78c552', 'bc81', 4, '', '', '2017-08-24 13:28:04', '2017-08-24 13:28:04', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'd60d04534512fa5544171928beff3458', 1, '', '115.72.62.28', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 1, 1),
(5, 'Nguyễn Hoàng Vương', 'vuong.nguyen@sonetrend.com', '10c043f691d15b12d2e6c9fcb3dc901f', 'd663', 4, '', '', '2017-08-25 03:23:40', '2017-08-25 03:23:40', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'f5af581e0c22fe010e73aae45556b296', 1, '', '1.52.157.5', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 1, 1),
(6, 'Nguyễn Đỗ Tố Uyên', 'uyen.nguyen@sonetrend.com', '4606a1bc4f7df1194c732607f0f65c6f', '2d36', 5, '', '', '2017-09-05 07:56:45', '2017-09-05 07:56:45', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'a33dad962e3355d6a665345053d981ed', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 1, 1),
(7, 'styloxkids', 'styloxkids@gmail.com', 'f4b9a967c45ea298494e1de0dc748193', '9eb4', 2, '', '', '2017-09-05 07:59:37', '2017-09-05 07:59:37', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '62e326d46248e37375ccdf27bbd2a311', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(8, 'digitview', 'digitview@gmail.com', '1cd91b598053e5da18ac16810d3436e9', '18e1', 2, '', '', '2017-09-05 08:00:05', '2017-09-05 08:00:05', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '3628b96e83aa58565029775a0ec4918a', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(9, 'naeemak01', 'naeemak01@gmail.com', '7ff60719bd6a06bd5c01c4f285578627', '32e1', 2, '', '', '2017-09-05 08:00:32', '2017-09-05 08:00:32', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '3e51c854a94505f1aaa8e506217862e3', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(10, 'highfollows', 'highfollows@gmail.com', 'd39895a50887b43db3b170b02e8cb15f', '9f09', 2, '', '', '2017-09-05 08:01:09', '2017-09-05 08:01:09', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '2a28ff272f4e8814dab02db2e7f77a6d', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(11, 'styloxkid', 'styloxkid@gmail.com', '78b68515707e4d3cb021513315b06bb6', 'c1e9', 2, '', '', '2017-09-05 08:01:30', '2017-09-05 08:01:30', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '7149823fd7a3c20a6187a7ae74deb89a', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(12, 'arshadali', 'arshadali@gmail.com', 'aa93e175f413761da83ebaf26ee2b645', '9f13', 2, '', '', '2017-09-05 08:02:05', '2017-09-05 08:02:05', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'be516959b32b6c75926a43a1659df36b', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(13, 'view', 'view@gmail.com', '9bef3310f27dd97b27eff55e53d6d6a1', '7529', 2, '', '', '2017-09-05 08:02:27', '2017-09-05 08:02:27', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'd7277d8d64bead4241415a13ceed3691', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(14, 'digitviews', 'digitviews@gmail.com', '23dadca8c1b28413b79379affdf8002c', 'e328', 2, '', '', '2017-09-05 08:02:52', '2017-09-05 08:02:52', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '003719838ee3aeb5144c0065af5f9a56', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(15, 'Nova', 'nova@gmail.com', '7fd7c4614c404965b27c513603f48b8f', '8c61', 2, '', '', '2017-09-05 08:03:23', '2017-09-05 08:03:23', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'f2a1ad7be0260f71bc10efa98b985017', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(16, 'smmraja', 'smmraja@gmail.com', '2657f21dddbea101d18e6cc9b4d65870', 'f299', 2, '', '', '2017-09-05 08:03:40', '2017-09-05 08:03:40', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '40e4c11b15cb3f5a077501a3575a295c', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(17, 'Kwests', 'kwests@gmail.com', '9a374a11e8ebe6f09d85c830046ca8a3', '1592', 2, '', '', '2017-09-05 08:04:07', '2017-09-05 08:04:07', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '3e580f82165449a11cc8dd82a259022d', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(18, 'hanhtrinh24h', 'hanhtrinh24h@gmail.com', '918477200db5057251fa44d34b2a6e6c', 'ae83', 2, '', '', '2017-09-05 08:04:27', '2017-09-05 08:04:27', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'd200306ed17f3c9868773505403af9f7', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(19, 'smfpromotions', 'smfpromotions@gmail.com', 'c07f5b4b22f011fe1be7ae42433c9bb4', 'f065', 2, '', '', '2017-09-05 08:04:45', '2017-09-05 08:04:45', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '1065f63f7b63b11c8ca217bd72e74334', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(20, 'quoclich', 'quoclich@gmail.com', '22ac03ea5eef38c7f5dd3bcd8f8c28cb', '94e1', 2, '', '', '2017-09-05 08:05:04', '2017-09-05 08:05:04', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '8c96411b615baff35abcb244c3a9c8ec', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(21, 'clashshot', 'clashshot@gmail.com', 'a1768c88e4d8595978428986e8b8fcf4', '8e3e', 2, '', '', '2017-09-05 08:05:46', '2017-09-05 08:05:46', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '232847b799158ac798975067d0c25ff4', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(22, 'vib', 'vib@gmail.com', '70a8c2ec53a856e0b6ad711060641140', 'f167', 2, '', '', '2017-09-05 08:06:06', '2017-09-05 08:06:06', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'b6d5612bd1ee44560ed5b86f1d4d6b4c', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(23, 'bulkfollows', 'bulkfollows@gmail.com', '073d95dea3c800ceb0330aa35b4e2284', 'c90f', 2, '', '', '2017-09-05 08:06:30', '2017-09-05 08:06:30', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'c33907fa06347390b2adb15d5887fc1e', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(24, 'khaitoan', 'khaitoan@gmail.com', '1ce64715d28e784ed801991a473b314d', '65ad', 2, '', '', '2017-09-05 08:06:49', '2017-09-05 08:06:49', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'fa9d4cf26214ec59f5c5ce17a835e38f', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(25, 'mrtuyen861991', 'mrtuyen861991@gmail.com', '95add684abf308322bbb141035d036c9', '1579', 2, '', '', '2017-09-05 08:07:14', '2017-09-05 08:07:14', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'eb69aad1d7d52d1125e922cafe5fdeb5', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(26, 'avivemedia', 'avivemedia@gmail.com', 'a1c20294646edcb80935b27558288a01', '9ad4', 2, '', '', '2017-09-05 08:16:26', '2017-09-05 08:16:26', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'a41e2547e8b86c6f8560a4ce6efc1fb4', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(27, 'shadhen13', 'shadhen13@gmail.com', '98f4e8b1720c5830c9147ee41bfcf482', 'bf08', 2, '', '', '2017-09-05 08:16:47', '2017-09-05 08:16:47', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '3c2c31633b067ca01f92b3eff3725ba8', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(28, 'moyejuddin', 'moyejuddin@gmail.com', '2c3b5f640e51832095760d6b5b111307', 'f6a7', 2, '', '', '2017-09-05 08:17:35', '2017-09-05 08:17:35', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '6dade94bc33a3afd1fe21c064a21603e', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(29, 'Thelabel1', 'thelabel1@gmail.com', '33547d48e89970b75cb5e3ae3a00d931', 'e18e', 2, '', '', '2017-09-05 08:18:02', '2017-09-05 08:18:02', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '7a9a90cd36a302377deca56c26ea5fb5', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(30, 'workwithhym', 'workwithhym@gmail.com', '81107d02973dc0a9e4cf3c92dad3af88', 'f959', 2, '', '', '2017-09-05 08:18:24', '2017-09-05 08:18:24', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '695561e740e24623b903ed9e712f22de', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(31, 'Kwest', 'kwest@gmail.com', '0ae732d5dc79c3fd8a850db28cfa9a7f', '721e', 2, '', '', '2017-09-05 08:18:53', '2017-09-05 08:18:53', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '3676f833565fdc86d60642058bfd324e', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(32, 'tonhumai', 'tonhumai@gmail.com', '74d3779e3593498fb34c315007a8ae37', 'dcf6', 2, '', '', '2017-09-05 08:19:20', '2017-09-05 08:19:20', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '39277ea2854c3d3ce05735ebf0a5acf2', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(33, 'edwinsoong', 'edwinsoong@gmail.com', 'f8b98cd7dbfc945d4f493ca193ed52d8', 'ef3b', 2, '', '', '2017-09-05 08:19:42', '2017-09-05 08:19:42', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'cd9aa515f9291d82aebb4eace395f6ca', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(34, 'newjhelum', 'newjhelum@gmail.com', '5605e15d3f4367e8a1a60c4fb2963e89', '699b', 2, '', '', '2017-09-05 08:19:59', '2017-09-05 08:19:59', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '3f0abc90163861745f56df794bd41457', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(35, 'Cnccadillac', 'cnccadillac@gmail.com', 'c7e47ccd1be394fad23b8d3a6c52b4bc', 'e668', 2, '', '', '2017-09-05 08:20:28', '2017-09-05 08:20:28', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'b62928e8953db8f0b55db55baae00601', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(36, 'facebookguy', 'facebookguy@gmail.com', 'a84e3f620d7a8c26e15e9b5ad4413888', '8922', 2, '', '', '2017-09-05 08:20:52', '2017-09-05 08:20:52', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'eff3428351d88d472c07eac9ba99ffe7', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(37, 'abulhossain55', 'abulhossain55@gmail.com', '4cc44e3343aa56ffbb7644d17e746696', '8f4b', 2, '', '', '2017-09-05 08:21:29', '2017-09-05 08:21:29', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '3368559355c0dff33142713cf2fe2505', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(38, 'AlAmeer Solutions', 'aiameersolutions@gmail.com', '8f596d3f4766c56cbb4376f6607d7047', '459d', 2, '', '', '2017-09-05 08:22:01', '2017-09-05 08:22:01', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '8ec47ab73742fd7228c9abd3f40095e1', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(39, 'jaderson', 'jaderson@gmail.com', '213e6b702ab1671d394eeef984b0902a', '8103', 2, '', '', '2017-09-05 08:22:30', '2017-09-05 08:22:30', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '57c6da8c5b1068f4cf97cf1a03481cc6', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(40, 'Parvez6367', 'parvez6367@gmail.com', '4d9238cc5d478417072d6d6c57d20734', 'e3cc', 2, '', '', '2017-09-05 08:22:56', '2017-09-05 08:22:56', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'b8dcf5bfbc5fd17e15d1b6f4c180aaf4', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(41, 'rabiya', 'rabiya@gmail.com', '927060177700b3cb3b15e947914a0b86', 'f90e', 2, '', '', '2017-09-05 08:23:15', '2017-09-05 08:23:15', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'ba83cc7865dcf9c1101996a923918e6e', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(42, 'nthb.loan', 'nthb.loan@gmail.com', '4cdd39d4f305db5c481ab3d97d2f5fda', 'c725', 2, '', '', '2017-09-05 08:23:35', '2017-09-05 08:23:35', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'cff1ea2ed084126a8ea740cfee117247', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(43, 'cute.sweat.aleem', 'cute.sweat.aleem@gmail.com', 'a39425b2a325ae2cc5a05b95749323e1', 'd4f1', 2, '', '', '2017-09-05 08:23:54', '2017-09-05 08:23:54', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '8e2f21a1520b5cbbc75316b6699d6887', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(44, 'thaonguyen', 'thaonguyen@gmail.com', '90938c4430895b570649a6171007e12d', '6a24', 2, '', '', '2017-09-05 08:24:13', '2017-09-05 08:24:13', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '5b69c414b3c67fc9e38dc4e00076cefb', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(45, 'thuy.nguyen', 'thuy.nguyen@gmail.com', 'a143eee6e24220b3897cde012765b458', '4f01', 2, '', '', '2017-09-05 08:24:38', '2017-09-05 08:24:38', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'd54ce594e65e40d1d78df5a4c5c962ad', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(46, 'haidang', 'haidang@gmail.com', '544b7e7402b5ace829839c456bd5ce69', '688a', 2, '', '', '2017-09-05 08:24:58', '2017-09-05 08:24:58', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '0d0d9cd32a182c70e6e282a93a9f9c3a', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(47, 'dammy.paul', 'dammy.paul@gmail.com', '3d752fb4f7bb7ba9c26083209c2c33a8', 'c279', 2, '', '', '2017-09-05 08:25:16', '2017-09-05 08:25:16', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '8b106c34569b80f459efab667f37c53d', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(48, 'musictycoon', 'musictycoon@gmail.com', 'f7d45cab6beafc7ca3a8babc3fcf1132', '5ac0', 2, '', '', '2017-09-05 08:25:35', '2017-09-05 08:25:35', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '1a3fccb0adf7739fbec54a129b5d9c82', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(49, 'kenhlike', 'kenhlike@gmail.com', '50eb2d3783f9955bc452eed8c1d6fed6', '1776', 2, '', '', '2017-09-05 08:25:52', '2017-09-05 08:25:52', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '2940c37fe6adff070d40c9240df93d0b', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(50, 'j seven', 'jseven@gmail.com', 'f424a29cdf25cbbe88050019494d7562', '22b4', 2, '', '', '2017-09-05 08:26:27', '2017-09-05 08:26:27', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '9a43e378b7a8033d156e44dc430cfdd9', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(51, 'rwillmg', 'rwillmg@gmail.com', '4be9f274af8999954a29b0a6f40dbc73', '6dca', 2, '', '', '2017-09-05 08:26:44', '2017-09-05 08:26:44', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '756e8a6fe82b56e85ea51fc035351b30', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(52, 'J2TeaM', 'j2team@gmail.com', 'b5183546287f183d892911366c18c753', '1c23', 2, '', '', '2017-09-05 08:27:12', '2017-09-05 08:27:12', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '875634e38f8d699a93a0c15f40931b99', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(53, 'Phi123', 'phi123@gmail.com', 'f62008289332205f9fcf224a513725b1', 'eb98', 2, '', '', '2017-09-05 08:27:46', '2017-09-05 08:27:46', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '7115646e1706f3b7a080aad2c747b369', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(54, 'marcow', 'marcow@gmail.com', 'ff9f75f5bcb45daae1fcd326fc744e7b', '360d', 2, '', '', '2017-09-05 08:28:13', '2017-09-05 08:28:13', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '76d87202dbeaaa8879e7987fac6ca680', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(55, 'tswan', 'tswan@gmail.com', 'b248a81015c1c26d4682013dc21621e5', '3257', 2, '', '', '2017-09-05 08:28:30', '2017-09-05 08:28:30', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'd30213cc62c64081e537f243dd88f917', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(56, 'La musa radio', 'lamusaradio@gmail.com', '75f3adeb59dde5f86a77621022a340ae', '195b', 2, '', '', '2017-09-05 08:28:58', '2017-09-05 08:28:58', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '4fe509f889da024f600afc75abc25ed3', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(57, 'nguyenngocthien', 'nguyenngocthien@gmail.com', 'fefc520da77947dd31362f975ff98e32', '6719', 2, '', '', '2017-09-05 08:29:17', '2017-09-05 08:29:17', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'e3d65cdffbc7e7adfd88d02b67c3927a', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(58, 'dothanhphu', 'dothanhphu@gmail.com', '1126d665a77476ea79c1c053741bdaa2', '9b1a', 2, '', '', '2017-09-05 08:29:40', '2017-09-05 08:29:40', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '9dacf7d8900431bdc4bc182864498d56', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(59, 'nguyendung', 'nguyendung@gmail.com', 'd21ba14a8f8246de4399e4715b8b092b', '1a3b', 2, '', '', '2017-09-05 08:30:06', '2017-09-05 08:30:06', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'be51133e160108d0933b658922570caf', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(60, 'kieumy', 'kieumy@gmail.com', '874d2b5e8b32e24803ee95fd842addc2', '80ad', 2, '', '', '2017-09-05 08:30:33', '2017-09-05 08:30:33', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '47ec019a0738bd6b5f9d10801c8b45f2', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(61, 'huynhnhu', 'huynhnhu@gmail.com', 'b5979ba3b89b7d498e23575a033639a6', '816d', 2, '', '', '2017-09-05 08:30:51', '2017-09-05 08:30:51', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '3697c1970f2a449cf636d6d49761cdb8', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(62, 'dungduong', 'dungduong@gmail.com', 'c2fc5123e87c2ab1f467e1d01d36c947', 'acd6', 2, '', '', '2017-09-05 08:31:20', '2017-09-05 08:31:20', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '1d65efc5001033cb54158c168800d838', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(63, 'bishopzjackson', 'bishopzjackson@gmail.com', '7f655cf52d398d5a31cd455920a68f19', '91fc', 2, '', '', '2017-09-05 08:31:36', '2017-09-05 08:31:36', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '8ce84bdd98ae4c23050b7b019365b063', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(64, 'duychinh', 'duychinh@gmail.com', '2fb38830dc78a6cea5325888844b5e0b', '769c', 2, '', '', '2017-09-05 08:31:56', '2017-09-05 08:31:56', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '7f1a0787e6adfc9e7de4d21d2b344dd5', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(65, 'm93ilyas', 'm93ilyas@gmail.com', 'c915e0bcfaf3f143e67cc037893e6cc2', '341e', 2, '', '', '2017-09-05 08:32:14', '2017-09-05 08:32:14', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'e48fb28f45baead48f298bedcdf0a322', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(66, 'thuyan', 'thuyan@gmail.com', '49b0c0852172492c3f22230f34e0258f', '0bb4', 2, '', '', '2017-09-05 08:32:31', '2017-09-05 08:32:31', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '5d51a7674a2cfc2c8bed6e5422ba8e62', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(67, 'nguyenthuyvan', 'nguyenthuyvan@gmail.com', '81f50596f06a709fa502311c35ff35e8', '6816', 2, '', '', '2017-09-05 08:32:50', '2017-09-05 08:32:50', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'db3cb3cda1d506909ccecc1652d12e1a', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(68, 'tramanhmodel', 'tramanhmodel@gmail.com', 'efcb077c4085f847d5c3c511bc771a9d', '760c', 2, '', '', '2017-09-05 08:33:30', '2017-09-05 08:33:30', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'e2f0de09781eb56c44dbc4c3f4509bf2', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(69, 'ChuyenSiQuyenNhi', 'chuyensiquyennhi@gmail.com', 'f51ee3364fc0db01ebd9345c9f80993e', 'babb', 2, '', '', '2017-09-05 08:34:10', '2017-09-05 08:34:10', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'c91e37d660db208b2ed76770e2008454', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(70, 'madieulinh', 'madieulinh@gmail.com', 'd95dd3c328f4c53e884c8a4794fff853', '6e36', 2, '', '', '2017-09-05 08:34:35', '2017-09-05 08:34:35', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'ed5aad26ca5d08eeb4a3a683e2e83adf', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(71, 'quoclong', 'quoclong@gmail.com', '8e014a0acd879d3fe0e50a3e40e77267', 'f46b', 2, '', '', '2017-09-05 08:34:57', '2017-09-05 08:34:57', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '18432b28251df526170a121fe319adf4', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(72, 'taducphuong', 'taducphuong@gmail.com', 'a121020286db513387a995e98733f1d3', '08c1', 2, '', '', '2017-09-05 08:35:15', '2017-09-05 08:35:15', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '81618707408e3c7e614e642d0219713c', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(73, 'moni88lala', 'moni88lala@gmail.com', '3834625c37130eec61948d4e1384d93a', 'e418', 2, '', '', '2017-09-05 08:35:41', '2017-09-05 08:35:41', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '77ee470e4362e175b8218c55b2af4017', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(74, 'munich', 'munich@gmail.com', '172a76872bc839320032f77a6f327054', '7e58', 2, '', '', '2017-09-05 08:35:57', '2017-09-05 08:35:57', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'fdb5878bb7e94a29c7788e46833ee0bd', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(75, 'nhuy.kim', 'nhuy.kim@gmail.com', '8d8bd310f71db0260b672c762bfe7516', '91f6', 2, '', '', '2017-09-05 08:36:14', '2017-09-05 08:36:14', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '97481d173a04323598d0df7b48f0a2b7', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(76, 'tientran', 'tientran@gmail.com', '31c3c049f3644f2c2705cf9eede3edc0', '906e', 2, '', '', '2017-09-05 08:36:32', '2017-09-05 08:36:32', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, '2d6d53dbd6efad4356c63a7a6117079f', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(77, 'chipduongpho', 'chipduongpho@gmail.com', 'e3b702c91d7fb84c18c8bd2f3f1e47a8', '2529', 2, '', '', '2017-09-05 08:36:49', '2017-09-05 08:36:49', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'e70f9c77a830cc1e806ce97ff804b7b4', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(78, 'shamil', 'shamil@gmail.com', '7ce6fccca954255df0258304e7afa386', '5c21', 2, '', '', '2017-09-05 08:37:09', '2017-09-05 08:37:09', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'd2ae024809ac4093820d9d676e53a685', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(79, 'hoangquynhnhu', 'hoangquynhnhu@gmail.com', '6b264a8a2c19a39757a7a9b5038cb6ac', '6c65', 2, '', '', '2017-09-05 08:37:25', '2017-09-05 08:37:25', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'a5fddd106e5d5e139920e88bbdd409a6', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0),
(80, 'nguyenhuongthao', 'nguyenhuongthao@gmail.com', 'c26bd3493f3fa969b4287cf97cde0f39', '9656', 2, '', '', '2017-09-05 08:37:41', '2017-09-05 08:37:41', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, 1, 1, 'f61470cf03dafc019167bc84a5e24e29', 1, '', '210.245.35.205', 3, '', '', 0, '', 0, '', 1, 0, 0, 1, 1, 1, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_blocks`
--

CREATE TABLE IF NOT EXISTS `user_blocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_countries`
--

CREATE TABLE IF NOT EXISTS `user_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `address` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_follows`
--

CREATE TABLE IF NOT EXISTS `user_follows` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_follow_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_mentions`
--

CREATE TABLE IF NOT EXISTS `user_mentions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `item_table` varchar(80) NOT NULL DEFAULT '',
  `users_mentions` varchar(800) NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_setting_feeds`
--

CREATE TABLE IF NOT EXISTS `user_setting_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(128) CHARACTER SET latin1 NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `plugin` varchar(16) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_setting_feeds`
--

INSERT INTO `user_setting_feeds` (`id`, `type`, `active`, `text`, `plugin`) VALUES
(1, 'user_create', 0, 'When a new user sign up', ''),
(2, 'group_join', 0, 'When a user join a Group (apply for both home feed and on group feed)', ''),
(3, 'friend_add', 0, 'When a user becomes friends with another user', ''),
(4, 'user_avatar', 0, 'When a user updates profile photo', ''),
(5, 'event_attend', 0, 'When a user attents an event', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_taggings`
--

CREATE TABLE IF NOT EXISTS `user_taggings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `item_table` varchar(80) NOT NULL DEFAULT '',
  `users_taggings` varchar(800) NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`,`item_table`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
