<div id="footer">
    <?php echo html_entity_decode( Configure::read('core.footer_code') )?>
    <?php if (Configure::read('core.show_credit')): ?>
    <span class="date"><?php echo __('Powered by')?> <a href="http://www.sonetrend.com" target="_blank">SoneTrend © 2017</a></span>
    <?php endif; ?>
</div>