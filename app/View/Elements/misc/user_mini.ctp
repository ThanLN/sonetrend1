<?php 
	$block_users = array();
	if (!empty($uid))
	{
		$model = MooCore::getInstance()->getModel('UserBlock');
		$block_users = $model->getBlockedUsers($uid);
	}
?>
<div class="user_mini">
	<?php echo $this->Moo->getItemPhoto(array('User' => $user),array( 'prefix' => '100_square'), array('class' => 'user_avatar_large img_wrapper2'))?>
	<div class="user-info">
		<?php echo $this->Moo->getName($user)?>
		<div>
			<?php if ( !empty($uid) && $uid != $user['id'] && !$areFriends && !in_array($user['id'], $block_users)): ?>
                        <?php
      $this->MooPopup->tag(array(
             'href'=>$this->Html->url(array("controller" => "friends",
                                            "action" => "ajax_add",
                                            "plugin" => false,
                                            $user['id']
                                            
                                        )),
             'title' => sprintf( __('Send %s a friend request'), h($user['name']) ),
             'innerHtml'=> __('Add as Friend'),
          'id' => 'addFriend_' . $user['id']
     ));
 ?>
			<br />
			<?php endif; ?>
		</div>
	</div>
</div>