<?php
if (count($users) > 0)
{
	foreach ($users as $user):
?>
	<li <?php if ( isset($type) && $type == 'home' ): ?>id="friend_<?php echo $user['Friend']['friend_id']?>"<?php endif; ?>
		<?php if ( isset($group) ): ?>id="member_<?php echo $user['GroupUser']['id']?>"<?php endif; ?>
        class="user-list-index">
            <div class="list-content">
                <div class="user-idx-item">
                   <a href="<?php echo $this->request->base?>/<?php echo (!empty( $user['User']['username'] )) ? '-' . $user['User']['username'] : 'users/view/'.$user['User']['id']?>"><?php echo $this->Moo->getItemPhoto(array('User' => $user['User']), array('prefix' => '200_square'))?></a>
                   <?php
                   if(isset($user_block)){
                        $this->MooPopup->tag(array(
                               'href'=>$this->Html->url(array("controller" => "user_blocks",
                                                              "action" => "ajax_remove",
                                                              "plugin" => false,
                                                              $user['User']['id']
                                                          )),
                               'title' => __('Unblock'),
                               'innerHtml'=> '<i class="material-icons icon-large delete-icon">clear</i> ' . __('Unblock'),
                            'id' => 'unblock_'.$user['User']['id'],
                            'class' => 'add_people unblock'
                       ));
                   }
                ?>

                </div>

		<?php if ( isset($type) && $type == 'home' ): ?>
                <?php
      $this->MooPopup->tag(array(
             'href'=>$this->Html->url(array("controller" => "friends",
                                            "action" => "ajax_remove",
                                            "plugin" => false,
                                            $user['User']['id']
                                        )),
             'title' => '',
             'innerHtml'=> '<i class="material-icons icon-large delete-icon">clear</i>',
          'id' => 'removeFriend_'. $user['User']['id']
     ));
 ?>
                    
		<?php endif; ?>

		<div class="user-list-info">
                        <div class="user-name-info">
			<?php echo $this->Moo->getName($user['User'])?>
                        </div>
			<div class="">
				<span class="date">

					<?php if ( isset($group) && isset($admins) && $user['User']['id'] != $uid && $group['User']['id'] != $user['User']['id'] &&
							   ( !empty($cuser['Role']['is_admin']) || in_array($uid, $admins) ) ):
					?>
					<a href="javascript:void(0)" class="removeMember" data-id="<?php echo $user['GroupUser']['id']?>"><?php echo __('Remove Member')?></a> .
					<?php endif; ?>

					<?php if ( isset($group) && isset($admins) && !in_array($user['User']['id'], $admins) &&
							   ( !empty($cuser['Role']['is_admin']) || $uid == $group['User']['id'] ) ):
					?>
					<a href="javascript:void(0)" class="changeAdmin" data-id="<?php echo $user['GroupUser']['id']?>" data-type="make"><?php echo __('Make Admin')?></a>
					<?php endif; ?>

					<?php if ( isset($group) && isset($admins) && in_array($user['User']['id'], $admins) && $user['User']['id'] != $group['User']['id'] &&
							   ( !empty($cuser['Role']['is_admin']) || $uid == $group['User']['id'] ) ):
					?>
					<a href="javascript:void(0)" class="changeAdmin" data-id="<?php echo $user['GroupUser']['id']?>" data-type="remove"><?php echo __('Remove Admin')?></a>
					<?php endif; ?>


				</span>
			</div>
		</div>
            </div>
        <?php $this->Html->rating($user['User']['id'],'users'); ?>
	</li>
<?php
	endforeach;
}
else
	echo '<div class="clear">' . __('No more results found') . '</div>';
?>
    