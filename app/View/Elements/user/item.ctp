<?php if($this->request->is('ajax')): ?>
<script>
    require(["jquery","mooUser"], function($,mooUser) {
        mooUser.initRespondRequest();
    });
</script>
<?php else: ?>
    <?php $this->Html->scriptStart(array('inline' => false,'requires'=>array('jquery','mooUser'),'object'=>array('$','mooUser'))); ?>
    mooUser.initRespondRequest();
    <?php $this->Html->scriptEnd(); ?>
<?php endif; ?>

<?php if (!empty($user)): ?>
    <div class="list-content">
        <div class="user-idx-item">
                   <?php echo $this->Moo->getItemPhoto(array('User' => $user['User']), array('prefix' => '200_square'))?>
                   
        </div>
        <div class="user-list-info">
            <div class="user-name-info">
                <?php echo $this->Moo->getName($user['User']) ?>
            </div>
            <div class="">
                
            </div>
        </div>
    </div>
<?php endif; ?>