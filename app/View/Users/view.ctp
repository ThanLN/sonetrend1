
<style>
    #themeModal .modal-body{
        padding:15px;
    }
</style>

<?php if($this->request->is('ajax')): ?>
<script type="text/javascript">
    require(["jquery", "mooUser"], function ($, mooUser) {
        mooUser.initOnUserView();
    });
</script>
<?php else: ?>
<?php $this->Html->scriptStart(array('inline' => false, 'domReady' => true, 'requires'=>array('jquery','mooUser'), 'object' => array('$', 'mooUser'))); ?>
mooUser.initOnUserView();
<?php $this->Html->scriptEnd(); ?> 
<?php endif; ?>


<div class="box2">
<!--    <div class="box_content">
        <ul class="list6 list6sm">
			<?php if ( !empty($cuser['role_id']) && $cuser['Role']['is_admin'] && !$user['User']['featured'] ): ?>
            <li><a href="<?php echo $this->request->base?>/admin/users/feature/<?php echo $user['User']['id']?>"><?php echo __('Feature User')?></a></li>
			<?php endif; ?>
			<?php if ( !empty($cuser['role_id']) && $cuser['Role']['is_admin'] && $user['User']['featured'] ): ?>
            <li><a href="<?php echo $this->request->base?>/admin/users/unfeature/<?php echo $user['User']['id']?>"><?php echo __('Unfeature User')?></a></li>
			<?php endif; ?>
			<?php if ( !empty($cuser['role_id']) && $cuser['Role']['is_admin'] && !$user['Role']['is_admin'] ): ?>
            <li><a href="<?php echo $this->request->base?>/admin/users/edit/<?php echo $user['User']['id']?>"><?php echo __('Edit User')?></a></li>
			<?php endif; ?>
            <li>
                            <?php
      $this->MooPopup->tag(array(
             'href'=>$this->Html->url(array("controller" => "reports",
                                            "action" => "ajax_create",
                                            "plugin" => false,
                                            'user',
                                            $user['User']['id']
                                        )),
             'title' => __('Report User'),
             'innerHtml'=> __('Report User'),
     ));
 ?>
            </li>
				
            <?php if ( !empty($uid) && ($uid != $user['User']['id'] ) && !$user['Role']['is_admin'] && !$user['Role']['is_super']): ?>
            <li><?php
                if(!$is_viewer_block){
                    $this->MooPopup->tag(array(
                        'href'=>$this->Html->url(array("controller" => "user_blocks",
                                            "action" => "ajax_add",
                                            "plugin" => false,
                                             $user['User']['id']
                                            
                                        )),
                            'title' => __('Block'),
                            'innerHtml'=> __('Block'),
                         ));
                }else{
                    $this->MooPopup->tag(array(
                        'href'=>$this->Html->url(array("controller" => "user_blocks",
                                            "action" => "ajax_remove",
                                            "plugin" => false,
                                            $user['User']['id']
                                            
                                        )),
                            'title' => __('Unblock'),
                            'innerHtml'=> __('Unblock'),
                         ));
                }
 ?></li>
            <?php endif; ?>	
        </ul>	
    </div>-->
</div>	
<?php $this->end(); ?>

<div class="profilePage ">
    <div id="profile-content"></div>
</div> 