<?php if(!empty($tab)): ?>

<?php if($this->request->is('ajax')): ?>
<script type="text/javascript">
    require(["jquery","mooTab"], function($, mooTab) {
        mooTab.initHomeTabs("<?php echo $tab; ?>");
    });
</script>
<?php else: ?>
<?php $this->Html->scriptStart(array('inline' => false, 'domReady' => true, 'requires'=>array('jquery', 'mooTab'), 'object' => array('$', 'mooTab'))); ?>
mooTab.initHomeTabs("<?php echo $tab; ?>");
<?php $this->Html->scriptEnd(); ?>
<?php endif; ?>

<?php endif; ?>



<?php
if ( empty($uid) && Configure::read('core.force_login') ):
    $guest_message = Configure::read('core.guest_message');
    if ( !empty($guest_message) ): ?>
    <div class="box1 guest_msg"><?php echo nl2br(Configure::read('core.guest_message'))?></div>
<?php
    endif;
else:
?>
<?php $this->setNotEmpty('west');?>
<?php $this->start('west'); ?>

    <?php if ($uid): ?>
    <div id="browse">
        <div class="summary-info p_15">
            <a href="<?php echo $this->Moo->getProfileUrl( $cuser )?>" class="no-ajax">
            <?php echo $this->Moo->getImage($user, array('prefix' => '50_square', 'class' => 'ava_home', 'width' => '50'));?>
            </a>
            <div class="user-info-home">
                <h3 class="info-home-name" style="padding: 0px;">
                    <a class="no-ajax" href="<?php echo $this->Moo->getProfileUrl( $cuser )?>">
                    <?php echo h($cuser['name'])?>
                    </a>
                </h3>
                <a class="no-ajax" href="<?php echo  $this->request->base; ?>/users/profile"><?php echo  __('Edit Profile') ?></a>
            </div>
        </div>
        <div class="clear"></div>
    </div>
<?php endif; ?>

<?php echo html_entity_decode( Configure::read('core.homepage_code') )?>

<?php $this->end(); ?>


    
<?php endif; ?>