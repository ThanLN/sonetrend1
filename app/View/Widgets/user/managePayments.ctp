<?php

if ( !( empty($uid) && Configure::read('core.force_login') ) ):
    if(empty($num_item_show)): $num_item_show=10; endif;
    if(isset($title_enable)&&($title_enable)=== ""): $title_enable = false; else: $title_enable = true;endif;
    ?>
<div class="bar-content">
    <div id="browse">
        <?php if(!$viewer['Role']['is_mod'] && $show): ?>
        <div class="summary-info p_15">
            <div class="container">
                <h2><?php echo __d('payment','Hello %s,', $viewer['User']['name']) ?></h2>    
                <h2><?php echo __d('payment','May we remind you that your payment for orders is overdue.') ?></h2>
                <h2><?php echo __d('payment','Total amount orders: ') ?>
                    <button type="button" class="btn btn-action">$<?php echo number_format($price_order,2); ?></button>
                </h2>
                <h2><?php echo __d('payment','Total amount pakages: ') ?>
                    <button type="button" class="btn btn-info">$<?php echo number_format($price_pakage,2); ?></button>
                </h2>
                <h2><?php echo __d('payment','Total amount you was payed: ') ?>
                    <button type="button" class="btn btn-success">$<?php echo number_format($payed,2); ?></button>
                </h2>
                <h2><?php echo __d('payment','The total amount payable is : ') ?>
                    <button type="button" class="btn btn-danger">$<?php echo number_format($price_order +  $price_pakage - $payed, 2)  ?></button>
                </h2>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>

<?php
endif;
?>
