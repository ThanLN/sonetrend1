<?php
if (!( empty($uid) && Configure::read('core.force_login') )):
    if (empty($num_item_show)){
        $num_item_show = 10;
    }
    if(isset($title_enable)&&($title_enable)=== ""){
        $title_enable = false; 
    }else {
        $title_enable = true;
    }

    if (!empty($friend_suggestions)) :
    ?>
    <div class="box2 suggestion_block">
        <?php if($title_enable): ?>
        <h3><?php echo  $title ?></h3>
        <?php endif; ?>
        <div class="box_content">
            <ul class="list6">
            <?php foreach ($friend_suggestions as $friend): ?>
                <li><?php echo  $this->Moo->getItemPhoto(array('User' => $friend['User']), array( 'prefix' => '100_square')) ?>
                    
                </li>
            <?php endforeach; ?>
            </ul>
            
        </div>
    </div>
    <?php
endif;
endif;
?>