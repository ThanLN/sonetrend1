<?php if ($uid): ?>
    <div id="browse">
        <div class="summary-info p_15">
            <a href="<?php echo $this->Moo->getProfileUrl( $cuser )?>" class="no-ajax">
            <?php echo $this->Moo->getImage($user, array('prefix' => '50_square', 'class' => 'ava_home', 'width' => '50'));?>
            </a>
            <div class="user-info-home">
                <h3 class="info-home-name" style="padding: 0px;">
                    <a class="no-ajax" href="<?php echo $this->Moo->getProfileUrl( $cuser )?>">
                    <?php echo h($cuser['name'])?>
                    </a>
                </h3>
                <a class="no-ajax" href="<?php echo  $this->request->base; ?>/users/profile"><?php echo  __('Edit Profile') ?></a>
            </div>
        </div>
        <div class="clear"></div>
    </div>
<?php endif; ?>

<?php echo html_entity_decode( Configure::read('core.homepage_code') )?>