<?php

App::uses('LivestreamAppModel', 'Livestream.Model');

class LivestreamLog extends LivestreamAppModel {

    public $actsAs = array();
//public $belongsTo = array('User' => array('className' => 'User','foreignKey' => 'user_id'),'PakageDetail' => array('className' => 'PakageDetail','foreignKey' => 'pakage_id'));
//    public $mooFields = array('title','href','plugin','type','url','thumb','privacy');

    public $hasMany = array();
    public $order = 'LivestreamLog.id desc';
    public $validate = array(
        'order_id' => array(
            'rule' => 'notBlank',
            'message' => 'order_id is required',
        ),
        'param' => array(
            'rule' => 'notBlank',
            'message' => 'param is required',
        ),
    );

   
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }
    public function getLivetreamLog($status,$cond){
        return $this->find($status, array('conditions' => $cond));
    }
}
