<?php

App::uses('LivestreamAppModel', 'Livestream.Model');

class LivestreamOrder extends LivestreamAppModel {

    public $actsAs = array();
    public $belongsTo = array('User' => array('className' => 'User', 'foreignKey' => 'user_id'), 'PakageDetail' => array('className' => 'PakageDetail', 'foreignKey' => 'pakage_id'));
//    public $mooFields = array('title','href','plugin','type','url','thumb','privacy');

    public $hasMany = array();
    public $order = 'LivestreamOrder.id desc';
    public $validate = array(
        'url' => array(
            'rule' => 'notBlank',
            'message' => 'url is required',
        ),
        'type' => array(
            'rule' => 'notBlank',
            'message' => 'type is required',
        ),
        'amount_live' => array(
            'rule' => 'notBlank',
            'message' => 'amount_live is required',
        ),
        'time_need_viewer' => array(
            'rule' => 'notBlank',
            'message' => 'time_need_viewer is required',
        ),
        'add_time' => array(
            'rule' => 'notBlank',
            'message' => 'add_time is required',
        ),
        'status' => array(
            'rule' => 'notBlank',
            'message' => 'status is required',
        ),
        'price' => array(
            'rule' => 'notBlank',
            'message' => 'price is required',
        ),
        'user_id' => array(
            'rule' => 'notBlank',
            'message' => 'user_id is required',
        ),
        'user_create' => array(
            'rule' => 'notBlank',
            'message' => 'user_create is required',
        ),
    );

    public function getActive($row) {
        if (isset($row['active'])) {
            return $row['active'];
        }
        return false;
    }

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    public function getLivetreamOrder($status, $cond) {
        return $this->find($status, array('conditions' => $cond));
    }

    public function countPrice($amount_live, $time, $amount_view, $user_id, $pakage_id, $discount_code = '') {
        $price = '0';
        if ($discount_code == '' && $pakage_id == '0' && $user_id != '0') {
            $priceDetailModel = MooCore::getInstance()->getModel("Pakage.PriceDetail");
            $priceModel = MooCore::getInstance()->getModel("Pakage.Price");
            $priceDetail = $priceDetailModel->getPriceDetailByUserId($user_id);
            if (!empty($priceDetail)) {
                $price = floatval($price) + floatval($priceDetail['PriceDetail']['price_view']) * intval($amount_view);
                $json_price = json_decode($priceDetail['PriceDetail']['price_live']);
                foreach ($json_price as $json) {
                    if (intval($amount_live) <= intval($json->view)) {
                        $price = floatval($price) + floatval($json->price) * intval($amount_live) * (intval($time) / 30);
                        return (string) $price;
                    }
                }
            } else {
                $priceDefault = $priceModel->getSystemPrice();
                $price = floatval($price) + floatval($priceDefault['Price']['default_view']) * intval($amount_view);
                $json_price = json_decode($priceDefault['Price']['default_live']);
                foreach ($json_price as $json) {
                    if (intval($amount_live) <= intval($json->view)) {
                        $price = floatval($price) + floatval($json->price) * intval($amount_live) * (intval($time) / 30);
                        return (string) $price;
                    }
                }
            }
        }
        return $price;
    }

    public function validateLivestreamOrder($data) {

        $pass = true;
        $message = NULL;
        if (empty($data['url'])) {
            $pass = false;
            $message = __d('livestream', 'Url can not empty');
        }

        if (empty($data['type'])) {
            $pass = false;
            $message = __d('livestream', 'Type can not empty');
        }
        if ($data['type'] != ORDER_TYPE_NOW && $data['type'] != ORDER_TYPE_LATER) {
            $pass = false;
            $message = __d('livestream', 'Type range is 1 or 2');
        }
        if (empty($data['amount_live'])) {
            $pass = false;
            $message = __d('livestream', 'Amount livestream is not empty');
        }
        if (empty($data['time_need_viewer'])) {
            $pass = false;
            $message = __d('livestream', 'time_need_viewer livestream is not empty');
        }
        if ($data['type'] == ORDER_TYPE_LATER && empty($data['time_start_schedule'])) {
            $pass = false;
            $message = __d('livestream', 'time_start_schedule livestream is not empty');
        }
        if ($data['type'] == ORDER_TYPE_LATER && empty($data['timezone'])) {
            $pass = false;
            $message = __d('livestream', 'time_start_schedule livestream is not empty');
        }
        if (empty($data['add_time'])) {
            $pass = false;
            $message = __d('livestream', 'add_time livestream is not empty');
        }
        if (empty($data['status'])) {
            $pass = false;
            $message = __d('livestream', 'add_time livestream is not empty');
        }
        if (!is_numeric($data['price'])) {
            $pass = false;
            $message = __d('livestream', 'price livestream is not numberic');
        }
        if (empty($data['user_id']) && empty($data['distcount_code'])) {
            $pass = false;
            $message = __d('livestream', 'user_id livestream is not empty');
        }
        if (empty($data['user_create']) && empty($data['distcount_code'])) {
            $pass = false;
            $message = __d('livestream', 'user_create livestream is not empty');
        }
        $result['pass'] = $pass;
        $result['message'] = $message;
        return $result;
    }

    public function createLivestreamOrder($data) {
        if (isset($data['id']))
            $data_save['id'] = $data['id'];
        $data_save['url'] = $data['url'];
        $data_save['type'] = $data['type'];
        $data_save['amount_live'] = $data['amount_live'];
        $data_save['time_need_viewer'] = $data['time_need_viewer'];
        $data_save['time_start_schedule'] = $data['time_start_schedule'];
        $data_save['timezone'] = $data['timezone'];
        $data_save['add_time'] = time();
        $data_save['status'] = ORDER_STATUS_ADD;
        $data_save['price'] = $data['price'];
        $data_save['note'] = $data['note'];
        $data_save['user_id'] = $data['user_id'];
        if (!isset($data['user_id']))
            $data_save['user_id'] = 0;
        $data_save['user_create'] = $data['user_create'];
        if (!isset($data['user_create']))
            $data_save['user_create'] = 0;
        $data_save['reaction'] = $data['reaction'];
        if (isset($data['time_per']))
            $data_save['time_per'] = $data['time_per'];
        if (isset($data['view_per']))
            $data_save['view_per'] = $data['view_per'];
        $data_save['run_amount_live'] = $data['amount_live'];
        $data_save['is_auto_create'] = $data['is_auto_create'];
        $data_save['pakage_id'] = $data['pakage_id'];
        if (isset($data['distcount_code']))
            $data_save['distcount_code'] = $data['distcount_code'];
        $flag = $this->save($data_save);
        if (!empty($flag)) {
            //tru so phu trong pakage
            if ($data_save['pakage_id'] != '0') {
                $pakageDetailModel = MooCore::getInstance()->getModel("Pakage.PakageDetail");
                $pakageDetailModel->writeLogPakageDetail($flag['LivestreamOrder']);
            }
            //them count demo code
            if(isset($data['distcount_code']) && !empty($data['distcount_code'])){
                $discountModel = MooCore::getInstance()->getModel("Livestream.LivestreamDiscountcode");
                $discountModel->updateDiscountcode($data['distcount_code']);
            }
        }
        return true;
    }
    public function updateLivestreamOrder($id, $data){
        $data_save = array();
        if (isset($data['id']))
            $data_save['id'] = $data['id'];
        if (isset($data['url']))
            $data_save['url'] = $data['url'];
        if (isset($data['amount_live'])){
            $data_save['amount_live'] = $data['amount_live'];
            $data_save['run_amount_live'] = $data['amount_live'];
        }
        if (isset($data['amount_view']))
            $data_save['amount_view'] = $data['amount_view'];
        if (isset($data['time_need_viewer']))
            $data_save['time_need_viewer'] = $data['time_need_viewer'];
        $flag = $this->save($data_save);
        if (!empty($flag)) {
            $flag = $this->findById($id);
            //tru so phu trong pakage
            if ($flag['LivestreamOrder']['pakage_id'] != '0') {
                $pakageDetailModel = MooCore::getInstance()->getModel("Pakage.PakageDetail");
                $pakageDetailModel->writeLogPakageDetail($flag['LivestreamOrder']);
            }
            
        }
        return true;
    }

    public function convertTime($time, $timezone, $rowback = true) {
        if (strpos($timezone, '-') !== false) {
            $value = FALSE; //-
        } else {
            $value = TRUE; //+
        }
        $tmp = str_replace('-', '', $timezone);
        if ($rowback) {
            if ($value) {
                $time = intval($time) + 7 * 3600 - (intval($tmp) * 3600);
            } else {
                $time = intval($time) + 7 * 3600 + (intval($tmp) * 3600);
            }
        }else{
            if ($value) {
                $time = intval($time) - 7 * 3600 + (intval($tmp) * 3600);
            } else {
                $time = intval($time) - 7 * 3600 - (intval($tmp) * 3600);
            }
        }
        return $time;
    }
    public function getListStatus(){
        $result = array(); 
        $result[ORDER_STATUS_ADD] = __d('livestream','ADDED');
        $result[ORDER_STATUS_ACTIVE] = __d('livestream','ACTIVED');
        $result[ORDER_STATUS_RUN] = __d('livestream','RUNING');
        $result[ORDER_STATUS_COMPLETE] = __d('livestream','COMPLETE');
        $result[ORDER_STATUS_CANCEL] = __d('livestream','CANCELLED');
        return $result;
    }
    public function getArrayTimePer($number = 12){
        $result = array();
        for($i = 1; $i<= $number; $i++){
            $result[$i*5] = __d('livestream','%s minutes', $i*5);
        }
        return $result;
    }
    public function getArrayViewPer(){
        $result = array();
        $tmp = [25,50,100,200,500,1000,2000];
        foreach($tmp as $value){
            $result[$value] = __d('livestream','%s viewer', $value);
        }
        return $result;
    }
    public function getArrayViewer($amount){
        $result = array();
        $num = intval($amount) / 100;
        for ($i = 1; $i <= $num; $i++) {
            $result[$i * 100] = $i * 100;
        }
        return $result;
    }
    public function getArrTime($hour){
        $num = intval(floatval($hour) * 2);
        for ($i = 1; $i <= $num; $i++) {
            $result[$i * 30] = $this->convertToHoursMins($i * 30);
        }
        return $result;
    }
    public function convertToHoursMins($time, $format = '%02d hours %02d minutes') {
        if ($time <= 60) {
            return __d('livestream','%s minutes', $time);
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }
    public function countPricePayment($user_id, $time_start = false, $time_end = false){
        $cond = array();
        $cond['LivestreamOrder.user_id'] = $user_id;
        $cond['LivestreamOrder.status'] = ORDER_STATUS_COMPLETE;
        if($time_start){
            $cond['LivestreamOrder.add_time >'] = $time_start;
        }
        if($time_end){
            $cond['LivestreamOrder.add_time <'] = $time_end;
        }
        $orders = $this->getLivetreamOrder('all', $cond);
        $price = 0;
        foreach ($orders as $order){
            $price += floatval($order['LivestreamOrder']['price']);
        }
        return $price;
    }
    public function checkOwner($uid, $order_id){
        $order = $this->findById($order_id);
        $userModel = MooCore::getInstance()->getModel("User");
        $is_owner = false;
        $user = $userModel->findById($uid);
        //get role admin
        if($user['Role']['id'] == ROLE_ADMIN || $user['Role']['id'] == ROLE_ADMIN_2 || $user['Role']['id'])
            $is_owner = TRUE;
        if($order['LivestreamOrder']['user_id'] == $uid || $order['LivestreamOrder']['user_create'] == $uid){
            $is_owner = true;
        }
        return $is_owner;
    }
    public function checkEdit($uid, $order_id){
        $canEdit = array();
        $canEdit['url'] = false;
        $canEdit['amount_live'] = false;
        $canEdit['amount_view'] = false;
        $canEdit['time_need_viewer'] = false;
        $canEdit['type'] = false;
        $canEdit['reaction'] = false;
        $canEdit['slowly'] = false;
        $canEdit['note'] = false;
        $checkOwn = $this->checkOwner($uid, $order_id);
        $order = $this->findById($order_id);
        if($checkOwn){
            switch ($order['LivestreamOrder']['status']){
                case('1'):
                    $canEdit['url'] = true;
                    $canEdit['amount_live'] = true;
                    $canEdit['amount_view'] = true;
                    $canEdit['time_need_viewer'] = true;
                    $canEdit['type'] = true;
                    $canEdit['reaction'] = true;
                    $canEdit['slowly'] = true;
                    $canEdit['note'] = true;
                break;
                case('2'):
                    $canEdit['url'] = false;
                    $canEdit['amount_live'] = true;
                    $canEdit['amount_view'] = true;
                    $canEdit['time_need_viewer'] = true;
                    $canEdit['type'] = false;
                    $canEdit['reaction'] = false;
                    $canEdit['slowly'] = false;
                    $canEdit['note'] = false;
                break;
                case('3'):
                    $canEdit['url'] = false;
                    $canEdit['amount_live'] = false;
                    $canEdit['amount_view'] = false;
                    $canEdit['time_need_viewer'] = false;
                    $canEdit['type'] = false;
                    $canEdit['reaction'] = false;
                    $canEdit['slowly'] = false;
                    $canEdit['note'] = false;
                break;
                case('4'):
                    $canEdit['url'] = false;
                    $canEdit['amount_live'] = false;
                    $canEdit['amount_view'] = false;
                    $canEdit['time_need_viewer'] = false;
                    $canEdit['type'] = false;
                    $canEdit['reaction'] = false;
                    $canEdit['slowly'] = false;
                    $canEdit['note'] = false;
                break;
                case('5'):
                    $canEdit['url'] = false;
                    $canEdit['amount_live'] = false;
                    $canEdit['amount_view'] = false;
                    $canEdit['time_need_viewer'] = false;
                    $canEdit['type'] = false;
                    $canEdit['reaction'] = false;
                    $canEdit['slowly'] = false;
                    $canEdit['note'] = false;
                break;
            }
        }
        return $canEdit;
    }
}
