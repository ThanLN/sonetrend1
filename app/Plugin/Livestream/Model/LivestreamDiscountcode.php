<?php

App::uses('LivestreamAppModel', 'Livestream.Model');

class LivestreamDiscountcode extends LivestreamAppModel {

    public $actsAs = array();
    public $belongsTo = array('User' => array('className' => 'User', 'foreignKey' => 'user_id'));
    public $hasMany = array();
    public $order = 'LivestreamDiscountcode.id desc';
    public $validate = array(
        'code' => array(
            'rule' => 'notBlank',
            'message' => 'code is required',
        ),
        'num_used' => array(
            'rule' => 'notBlank',
            'message' => 'num_used is required',
        ),
        'active' => array(
            'rule' => 'notBlank',
            'message' => 'active is required',
        ),
        'user_id' => array(
            'rule' => 'notBlank',
            'message' => 'user_id is required',
        )
    );

    public function getActive($row) {
        if (isset($row['active'])) {
            return $row['active'];
        }
        return false;
    }

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    public function getLivestreamDiscountcode($status, $cond) {
        return $this->find($status, array('conditions' => $cond));
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function prepairData($data) {
        $result = array();
        $result['code'] = $this->generateRandomString();
        $result['user_id'] = $data['user_id'];
        $result['num_used'] = $data['num_used'];
        $result['count_used'] = $data['count_used'];
        $result['active'] = $data['active'];
        $result['max_viewer'] = $data['max_viewer'];
        $result['max_time'] = intval($data['max_time'])/60;
        return $result;
    }
    public function updateDiscountcode($code){
        $data = $this->find('first', array('conditions' => array('code'=> $code)));
        if($data){
            $data_save['id'] = $data['LivestreamDiscountcode']['id'];
            $data_save['count_used'] = intval($data['LivestreamDiscountcode']['count_used']) +1;
            if($data_save['count_used'] >= intval($data['LivestreamDiscountcode']['num_used'])){
                $data_save['active'] = 0;
            }
            $this->save($data_save);
        }
    }
}
