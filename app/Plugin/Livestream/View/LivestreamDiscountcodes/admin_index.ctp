
<?php

echo $this->Html->script(array('admin/layout/scripts/compare.js?'.Configure::read('core.version')), array('inline' => false));
echo $this->Html->css(array('jquery-ui','Pakage.admin', 'footable.core.min'), null, array('inline' => false));
echo $this->Html->script(array('jquery-ui', 'footable'), array('inline' => false));

$this->Html->addCrumb(__d('livestream','Plugins Manager'), '/admin/plugins');
$this->Html->addCrumb(__d('livestream', 'Livestream Order'), '/admin/livestream/livestreams');
$this->Html->addCrumb(__d('livestream', 'Demo Codes'), '/admin/livestream/livestream_discountcodes');

$this->startIfEmpty('sidebar-menu');
echo $this->element('admin/adminnav', array("cmenu" => "Livestream Order"));
$this->end();

$this->Paginator->options(array('url' => $passedArgs));
?>
<style>
    <!--
    .dataTables_filter span
    {
        padding: 5px;
    }
    .dataTables_filter .form-control
    {
        margin-top: 7px !important;
    }
    -->
</style>
<?php echo$this->Moo->renderMenu('Livestream', 'Demo Codes');?>
<div class="portlet-body">
    <div class="table-toolbar">

<!--	<form style="padding: 14px;"  method="post" action="<?php echo $this->base.'/admin/pakage/prices';?>" class="form-inline">
<div class="form-group">
<label><?php echo __d('livestream','name');?></label>
<input class="form-control input-medium input-inline" value="<?php if (isset($name)) echo $name;?>" type="text" name="title">
</div>
<button class="btn btn-gray" id="sample_editable_1_new" type="submit">
			<?php echo __d('livestream','Search');?>
</button>
</form>-->
        <?php if($cuser['Role']['is_admin']): ?>
        <div class="row">
            <div class="col-md-6">
                <div class="btn-group">
                    <button class="btn btn-gray" data-toggle="modal" data-target="#ajax" href="<?php echo $this->request->base ?>/admin/livestream/livestream_discountcodes/create">
                        <?php echo __d('livestream','Add New Code'); ?>
                    </button>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <table class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo __d('livestream','ID'); ?></th>
                <th><?php echo __d('livestream','Code'); ?></th>
                <th><?php echo __d('livestream','Max view'); ?></th>
                <th><?php echo __d('livestream','Max time'); ?></th>
                <th><?php echo __d('livestream','Number used'); ?></th>
                <th><?php echo __d('livestream','Count used'); ?></th>
                <th><?php echo __d('livestream','User Created'); ?></th>
                <th><?php echo __d('livestream','Active'); ?></th>
                <th><?php echo __d('livestream','Created'); ?></th>
                <th><?php echo __d('livestream','Updated'); ?></th>
                <th><?php echo __d('livestream','Action'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($codes)):?>
            <?php foreach ($codes as $code): ?>
            <tr>
                <td>
                    <?php echo $code['LivestreamDiscountcode']['id'];?>
                </td>
                <td>
                    <?php echo $code['LivestreamDiscountcode']['code'];?>
                </td>
                 <td>
                    <?php echo $code['LivestreamDiscountcode']['max_viewer'];?>
                </td>
                 <td>
                    <?php echo $code['LivestreamDiscountcode']['max_time'];?>
                </td>
                <td>
                    <?php echo $code['LivestreamDiscountcode']['num_used'];?>
                </td>
                <td>
                    <?php echo $code['LivestreamDiscountcode']['count_used'];?>
                </td>

                <td>
                    <a href="<?php echo $this->request->base ?>/users/view/<?php echo $code['LivestreamDiscountcode']['user_id'] ?>"><?php echo $code['User']['name'] ?></a>
                </td>
                <td>
                    <?php echo $code['LivestreamDiscountcode']['active'] ? __d('livestream','Yes') : __d('livestream','No');?>
                </td>
                <td><?php echo $this->Moo->getTime($code['LivestreamDiscountcode']['created']);?></td>
                <td><?php echo $this->Moo->getTime($code['LivestreamDiscountcode']['modified']);?></td>
                <td>
                </td>
            </tr>
            <?php endforeach ?>
            <?php else:?>
            <tr>
                <td colspan="9">
                    <?php echo __d('livestream','No code found');?>
                </td>
            </tr>
            <?php endif;?>
        </tbody>
    </table>

    <div class="pagination">
        <?php echo $this->Paginator->first(__d('livestream','First'));?>&nbsp;
        <?php echo $this->Paginator->hasPage(2) ? $this->Paginator->prev(__d('livestream','Prev')) : '';?>&nbsp;
		<?php echo $this->Paginator->numbers();?>&nbsp;
		<?php echo $this->Paginator->hasPage(2) ?  $this->Paginator->next(__d('livestream','Next')) : '';?>&nbsp;
		<?php echo $this->Paginator->last(__d('livestream','Last'));?>
    </div>

</div>
<script>
<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    function changeVisiable(id, e)
    {
        var value = 0;
        if ($(e).hasClass('poll_no'))
        {
            value = 1;
        }
        $(e).attr('class', '');
        if (value)
        {
            $(e).addClass('poll_yes');
            $(e).attr('title', '<?php echo __d('poll','yes');?>');
        } else
        {
            $(e).addClass('poll_no');
            $(e).attr('title', '<?php echo __d('poll','no');?>');
        }

        $.post("<?php echo $this->request->base?>/admin/poll/polls/visiable", {'id': id, 'value': value}, function (data) {

        });
    }
    $(document).on('hidden.bs.modal', function (e) {
        $(e.target).removeData('bs.modal');
    });
<?php echo $this->Html->scriptEnd(); ?>
</script>