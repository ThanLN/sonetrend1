<script>
    $(document).ready(function () {
        $('#createButton').click(function () {
            disableButton('createButton');
            $.post("<?php echo  $this->request->base ?>/admin/livestream/livestream_discountcodes/save_create", $("#createForm").serialize(), function (data) {
                enableButton('createButton');
                var json = $.parseJSON(data);

                if (json.result == 1)
                    location.reload();
                else
                {
                    $(".error-message").show();
                    $(".error-message").html('<strong>Error!</strong>' + json.message);
                }
            });

            return false;
        });

    });

    function toggleField()
    {
        $('.opt_field').toggle();
    }
</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo __d('livestream','Add New Code');?></h4>
</div>
<div class="modal-body">
    <form id="createForm" class="form-horizontal" role="form">
        <?php echo $this->Form->hidden('user_id', array('value' =>  $uid)); ?>
        <div class="form-body">
             <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('livestream','Loop');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('loop', array('placeholder' => __d('pakage','Enter text'), 'class' => 'form-control','value'=> 10)); ?>
                </div>            
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('livestream','Max view');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->select('max_viewer', $arr_viewer, array('class'=>'form-control','value'=> '','empty'=>false)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('livestream','Max time');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->select('max_time', $arr_timer, array('class'=>'form-control','value'=> '','empty'=>false)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('livestream','Number use');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('num_used', array('placeholder' => __d('pakage','Enter number'), 'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('livestream','Counter userd');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('count_used', array('placeholder' => __d('pakage','Enter number'), 'class' => 'form-control', 'value' => 0,'readOnly'=> true)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('livestream','Active');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->select('active', array( 1 => __d('pakage','Yes'), 2 => __d('pakage','No')),array('empty' => false,'class' => 'form-control')); ?>
                </div>
            </div>
        </div>
    </form>
    <div class="alert alert-danger error-message" style="display:none;margin-top:10px;">
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal"><?php echo  __d('livestream','Close') ?></button>
    <a href="#" id="createButton" class="btn btn-action"><?php echo  __d('livestream','Save') ?></a>

</div>