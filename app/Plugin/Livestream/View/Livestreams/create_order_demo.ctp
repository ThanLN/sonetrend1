
<?php if($is_ajax): ?>
<div class="modal-dialog">
    <div class="modal-content">
<?php endif; ?>

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><?php echo __d('livestream','Create Order Demo');?></h4>
        </div>
        <div class="modal-body">
            <?php if($success): ?>
            <form id="createFormLivestreamOrder" class="form-horizontal" role="form">
                <?php echo $this->Form->hidden('user_id', array('value' => 0)); ?>
                <?php echo $this->Form->hidden('pakage_id', array('value' => 0)); ?>
                <?php echo $this->Form->hidden('type', array('value' => ORDER_TYPE_NOW)); ?>
                <?php echo $this->Form->hidden('is_auto_create', array('value' => 0)); ?>
                <?php echo $this->Form->hidden('reaction', array('value' => 1)); ?>
                <?php echo $this->Form->hidden('distcount_code', array('value' => $distcount_code)); ?>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','URL');?></label>
                            <?php echo $this->Form->text('url', array('placeholder' => __d('livestream','https://www.facebook.com/your_facebook_id/posts/post_id'), 'class'=>'form-control')); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Amount livestream');?></label>
                            <?php echo $this->Form->select('amount_live', $arr_amount, array('class'=>'form-control','value'=> '','empty'=>false)); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Time need to view livestream');?></label>
                            <?php echo $this->Form->select('time_need_viewer', $arr_time, array('class'=>'form-control','value'=> '','empty'=>false)); ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Note');?></label>
                            <?php echo $this->Form->textarea('note', array('placeholder' => __d('livestream','note'), 'class'=>'form-control')); ?>
                    </div>

                </div>
            </form>
            <div class="alert alert-danger error-message" style="display:none;margin-top:10px;">
            </div>
            <?php else: ?>
            <div class="alert alert-danger error-message" style="margin-top:10px;">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn default btn-close-model" data-dismiss="modal"><?php echo  __d('livestream','Close') ?></button>
            <?php if($success): ?>
            <a href="#" id="createButtonOrderCreate" class="btn btn-action"><?php echo  __d('livestream','Save') ?></a>
            <?php endif; ?>
        </div>


        <?php if($is_ajax): ?>
    </div>
</div>
<?php endif; ?>
<script>
    $(document).ready(function () {
        $('#createButtonOrderCreate').unbind('click').click(function () {
            $.ajax({
                type: "POST",
                cache: false,
                url: '<?php echo  $this->request->base ?>/livestreams/save',
                dataType: "json",
                data: $("#createFormLivestreamOrder").serialize(),
                success: function (data)
                {
                    if (data.result) {
                        window.location.reload(true);
                    } else {
                        $('.error-message').show();
                        $('.error-message').html(data.message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    console.log(thrownError);
                }
            });
        });
        $('.btn-close-model').click(function () {
            window.location.reload(true);
        });
    });

    function toggleField()
    {
        $('.opt_field').toggle();
    }
</script>