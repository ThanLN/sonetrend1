<?php if($is_ajax): ?>
<div class="modal-dialog">
    <div class="modal-content">
<?php endif; ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><?php echo __d('livestream','Select Pakage');?></h4>
        </div>
        <div class="modal-body">
            <form id="createForm" class="form-horizontal" role="form">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Select Pakage');?></label>
                            <?php echo $this->Form->select('pakage_id', $pakages, array('class'=>'form-control','value'=> '','empty'=>false)); ?>
                    </div>
                </div>
            </form>
            <div class="alert alert-danger error-message" style="display:none;margin-top:10px;">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn default btn-close-model" data-dismiss="modal"><?php echo  __d('livestream','Close') ?></button>
            <a href="#" id="createButtonOrderPakage" class="btn btn-action"><?php echo  __d('livestream','Next') ?></a>
        </div>
<?php if($is_ajax): ?>
    </div>
</div>
<?php endif; ?>
<script>
    $(document).ready(function () {
        $('#createButtonOrderPakage').unbind('click').click(function () {
            var pakage_id = $("#pakage_id option:selected").val();
              $.ajax({
                type: "GET",
                cache: false,
                url: '<?php echo  $this->request->base ?>/livestreams/create/<?php echo $user_id ?>/' + pakage_id,
                dataType: "html",
                success: function (res)
                {
                    $('#createOrderStep3Modal').html(res);
                    $('#createOrderStep2Modal').modal('hide');
                    $('#createOrderStep3Modal').modal({
                        show: true,
                        backdrop: 'static'
                    });
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    console.log(thrownError);
                }
            });
        });
        $('.btn-close-model').click(function () {
           window.location.reload(true);
        });
    });

    function toggleField()
    {
        $('.opt_field').toggle();
    }
</script>