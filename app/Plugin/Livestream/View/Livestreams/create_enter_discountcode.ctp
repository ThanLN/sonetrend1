<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title"><?php echo __d('livestream','Enter Discount code');?></h4>
</div>
<div class="modal-body">
    <form id="createForm" class="form-horizontal" role="form">
        <div class="form-body">
            <div class="form-group">
                <label class="control-label"><?php echo __d('livestream','Discount code');?></label>
                    <?php echo $this->Form->text('discount_code', array('placeholder' => __d('livestream','Enter discount code'), 'class'=>'form-control')); ?>
            </div>
        </div>
    </form>
    <div class="alert alert-danger error-message" style="display:none;margin-top:10px;">
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default btn-close-model-select-user" data-dismiss="modal" data-target="#modalRegistration"><?php echo  __d('livestream','Close') ?></button>
    <a href="#" id="createButtonDemo" class="btn btn-action"><?php echo  __d('livestream','Next') ?></a>

</div>
<script>
    $(document).ready(function () {
        $('#createButtonDemo').unbind('click').click(function () {
            var discount_code = $("#discount_code").val();
              $.ajax({
                type: "GET",
                cache: false,
                url: '<?php echo  $this->request->base ?>/livestreams/create_demo/' + discount_code,
                dataType: "html",
                success: function (res)
                {
                    $('#createOrderStep3Modal').html(res);
                    $('#langModal').modal('hide');
                    $('#createOrderStep3Modal').modal({
                        show: true,
                        backdrop: 'static'
                    });
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    console.log(thrownError);
                }
            });
        });
        $('.btn-close-model').click(function () {
           window.location.reload(true);
        });
    });

    function toggleField()
    {
        $('.opt_field').toggle();
    }
</script>