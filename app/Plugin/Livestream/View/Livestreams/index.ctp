<?php
$helper = MooCore::getInstance()->getHelper('Livestream_Livestream');?>
<?php $have_pakage = $helper->checkUserHavePakage($cuser['id']); ?>
<div class="bar-content">
    <div class="content_center">
        <div class="mo_breadcrumb">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" <?php if ($type == 'not_auto'): ?>class="active" <?php endif; ?>>
                    <a href="<?php echo $this->request->base ?>/livestreams" role="tab"><?php echo __d('livestream', 'Orders') ?></a>
                </li>

                <li role="presentation"<?php if ($type == 'auto'): ?>class="active" <?php endif; ?>>
                    <a href="<?php echo $this->request->base ?>/livestreams/index/type=auto" role="tab"><?php echo __d('livestream', 'Auto Orders') ?></a>
                </li>
                <li role="presentation"<?php if ($type == 'demo'): ?>class="active" <?php endif; ?>>
                    <a href="<?php echo $this->request->base ?>/livestreams/index/type=demo" role="tab"><?php echo __d('livestream', 'Demo Orders') ?></a>
                </li>
            </ul>
            <!-- END Nav tabs -->
        </div>
        <?php if ($type == 'not_auto'): ?>
        <div class="mo_breadcrumb">
            <h1><?php echo __d('livestream', 'Livestream Orders')?></h1>
            <?php if($cuser['Role']['is_mod']): ?>
            <a class="button button-action topButton button-mobi-top" href="<?php echo $this->request->base?>/livestreams/create_select_user" data-target="#createOrderStep1Modal" data-toggle="modal" data-backdrop="static" title="<?php echo __d('livestream', 'Create New Order')?>">
                <?php echo __d('livestream', 'Create New Order')?>
            </a>
            <?php else: ?>
            <?php if($have_pakage): ?>
            <a class="button button-action topButton button-mobi-top" href="<?php echo $this->request->base?>/livestreams/create_select_pakage/<?php echo $cuser['id'] ?>" data-target="#createOrderStep2Modal" data-toggle="modal" data-backdrop="static" title="<?php echo __d('livestream', 'Create New Order')?>">
                <?php echo __d('livestream', 'Create New Order')?>
            </a>
            <?php else: ?>
            <a class="button button-action topButton button-mobi-top" href="<?php echo $this->request->base ?>/livestreams/create/<?php echo $cuser['id'] ?>" data-target="#createOrderStep3Modal" data-toggle="modal" data-backdrop="static" title="<?php echo __d('livestream', 'Create New Order')?>">
                <?php echo __d('livestream', 'Create New Order')?>
            </a>
            <?php endif; ?>
            <?php endif; ?>
        </div>
        <?php endif; ?>
         <?php if ($type == 'auto'): ?>
        <div class="mo_breadcrumb">
            <h1><?php echo __d('livestream', 'Livestream Auto Orders ')?></h1>
            <?php if($cuser['Role']['is_mod']): ?>
            <a class="button button-action topButton button-mobi-top" href="<?php echo $this->request->base?>/livestreams/create_select_user_auto" data-target="#createOrderStep1Modal" data-toggle="modal" data-backdrop="static" title="<?php echo __d('livestream', 'Create New Order')?>">
                <?php echo __d('livestream', 'Create New Order')?>
            </a>
            <?php else: ?>
            <?php if($have_pakage): ?>
            <a class="button button-action topButton button-mobi-top" href="<?php echo $this->request->base?>/livestreams/create_select_pakage/<?php echo $cuser['id'] ?>" data-target="#createOrderStep2Modal" data-toggle="modal" data-backdrop="static" title="<?php echo __d('livestream', 'Create New Order')?>">
                <?php echo __d('livestream', 'Create New Order')?>
            </a>
            <?php else: ?>
            <a class="button button-action topButton button-mobi-top" href="<?php echo $this->request->base ?>/livestreams/create_order_auto/<?php echo $cuser['id'] ?>" data-target="#createOrderStep3Modal" data-toggle="modal" data-backdrop="static" title="<?php echo __d('livestream', 'Create New Order')?>">
                <?php echo __d('livestream', 'Create New Order')?>
            </a>
            <?php endif; ?>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <div class="mo_breadcrumb">
            <?php echo $this->Form->hidden('type_order', array('value' => $type)); ?>
            <?php echo $this->Form->hidden('from_date', array('value' => $from_date)); ?>
            <?php echo $this->Form->hidden('to_date', array('value' => $to_date)); ?>
            
            <div class="form-group">
                <div class="col-md-3">
                    <p><?php echo __d('livestream','Sort by date');?></p>
                    <?php echo $this->Form->text('date_range_picker', array('class'=>'form-control')); ?>
                </div>
                <div class="col-md-3">
                    <p><?php echo __d('livestream','Sort by status');?></p>
                    <?php echo $this->Form->select('status', $list_status, array('class'=>'form-control','value'=> $status,'empty'=>true)); ?>
                </div>
            </div>
        </div>
        <div class="table-responsive">          
            <table class="table">
                <thead>
                    <tr>
                        <th><span>ID</span></th>
                        <th>Url</th>
                        <th>Type</th>
                        <th>Video ID</th>
                        <th>Amount</th>
                        <th>Run Amount</th>
                        <th>Time</th>
                        <th>Start time</th>
                        <th>End time</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>User</th>
                        <th>Pakage</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($orders)): ?>
                    <?php foreach ($orders as $order): ?>
                    <?php 
                    $class_btn = 'btn-default' ;
                    if($order['LivestreamOrder']['status'] == ORDER_STATUS_ACTIVE)
                        $class_btn = 'btn-success' ;
                    if($order['LivestreamOrder']['status'] == ORDER_STATUS_RUN)
                        $class_btn = 'btn-info' ;
                    if($order['LivestreamOrder']['status'] == ORDER_STATUS_COMPLETE)
                        $class_btn = 'btn-danger' ;
                    if($order['LivestreamOrder']['status'] == ORDER_STATUS_CANCEL)
                        $class_btn = 'btn-warning' ;
                            ?>
                    <tr>
                        <td><?php echo $order['LivestreamOrder']['id'] ?></td>
                        <td><?php echo $order['LivestreamOrder']['url'] ?></td>
                        <td><?php echo $order['LivestreamOrder']['type'] == ORDER_TYPE_NOW? __d('livestream','Now') :__d('livestream','Later') ?></td>
                        <td><?php echo $order['LivestreamOrder']['video_id'] ?></td>
                        <td><?php echo $order['LivestreamOrder']['amount_live'] ?></td>
                        <td><?php echo $order['LivestreamOrder']['run_amount_live'] ?></td>
                        <td><?php echo $order['LivestreamOrder']['time_need_viewer'] ?></td>
                        <td><?php echo $order['LivestreamOrder']['start_time']? date('Y-m-d H:i:s', $order['LivestreamOrder']['start_time']) : __d('livestream','Not exits') ?></td>
                        <td><?php echo $order['LivestreamOrder']['complete_time']? date('Y-m-d H:i:s', $order['LivestreamOrder']['complete_time']) : __d('livestream','Not exits') ?></td>
                        <td><?php echo $order['LivestreamOrder']['price'] ?></td>
                        <td> <button type="button" class="btn <?php echo $class_btn ?>"><?php echo $list_status[$order['LivestreamOrder']['status']] ?></button></td>
                        <td><a href="<?php $this->request->base?>/user/view/<?php echo $order['User']['id'] ?>"><?php echo $order['User']['name'] ?></a> </td>
                        <td><?php echo $order['LivestreamOrder']['pakage_id']? $order['LivestreamOrder']['pakage_id'] : __d('livestream','Dont use pakage') ?></td>
                        <td>
                            <a class="button button-action topButton button-mobi-top" href="<?php echo $this->request->base?>/livestream/livestream_logs/view/<?php echo $order['LivestreamOrder']['id'] ?>"><span class="glyphicon glyphicon-align-left" aria-hidden="true"></span></a>
                            <!--<a href="<?php echo $this->request->base?>/livestream/livestreams/edit/<?php echo $order['LivestreamOrder']['id'] ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>-->
                            <a class="button button-action topButton button-mobi-top" href="<?php echo $this->request->base?>/livestream/livestreams/edit/<?php echo $order['LivestreamOrder']['id'] ?>" data-target="#createOrderStep3Modal" data-toggle="modal" data-backdrop="static" title="<?php echo __d('livestream', 'Edit Order')?>">                
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                    <?php else: ?>
                    <tr>
                        <td colspan="14"><?php echo __d('livestream','No order found') ?></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <div class="pagination">
                <?php echo $this->Paginator->first(__d('livestream','First'));?>&nbsp;
                <?php echo $this->Paginator->hasPage(2) ? $this->Paginator->prev(__d('livestream','Prev')) : '';?>&nbsp;
		<?php echo $this->Paginator->numbers();?>&nbsp;
		<?php echo $this->Paginator->hasPage(2) ?  $this->Paginator->next(__d('livestream','Next')) : '';?>&nbsp;
		<?php echo $this->Paginator->last(__d('livestream','Last'));?>
            </div>
        </div>

    </div>
</div>
<?php $this->Html->scriptStart(array('inline' => false, 'domReady' => true, 'requires'=>array('jquery','mooOrder'), 'object' => array('$', 'mooOrder'))); ?>
mooOrder.initViewOrder();
<?php $this->Html->scriptEnd(); ?> 
<style>
    .table-responsive table thead tr th{
        background-color: #43abde !important;
    }
</style>