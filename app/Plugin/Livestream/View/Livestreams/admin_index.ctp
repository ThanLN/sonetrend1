
<?php

echo $this->Html->script(array('admin/layout/scripts/compare.js?'.Configure::read('core.version')), array('inline' => false));
echo $this->Html->css(array('jquery-ui','Pakage.admin', 'footable.core.min'), null, array('inline' => false));
echo $this->Html->script(array('jquery-ui', 'footable'), array('inline' => false));

$this->Html->addCrumb(__d('livestream','Plugins Manager'), '/admin/plugins');
$this->Html->addCrumb(__d('livestream', 'Livestream Order'), '/admin/livestream/livestreams');
$this->Html->addCrumb(__d('livestream', 'Orders'), '/admin/livestream/livestreams');

$this->startIfEmpty('sidebar-menu');
echo $this->element('admin/adminnav', array("cmenu" => "Livestream Order"));
$this->end();

$this->Paginator->options(array('url' => $passedArgs));
?>
<style>
    <!--
    .dataTables_filter span
    {
        padding: 5px;
    }
    .dataTables_filter .form-control
    {
        margin-top: 7px !important;
    }
    -->
</style>
<?php echo$this->Moo->renderMenu('Livestream', 'Orders');?>
<div class="portlet-body">
    <div class="table-toolbar">

<!--	<form style="padding: 14px;"  method="post" action="<?php echo $this->base.'/admin/pakage/prices';?>" class="form-inline">
      <div class="form-group">
        <label><?php echo __d('livestream','name');?></label>
        <input class="form-control input-medium input-inline" value="<?php if (isset($name)) echo $name;?>" type="text" name="title">
      </div>
      <button class="btn btn-gray" id="sample_editable_1_new" type="submit">
			<?php echo __d('livestream','Search');?>
  </button>
    </form>-->
    </div>
    <table class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo __d('livestream','ID'); ?></th>
                <th><?php echo __d('livestream','Url'); ?></th>
                <th><?php echo __d('livestream','Type'); ?></th>
                <th><?php echo __d('livestream','Amount'); ?></th>
                <th><?php echo __d('livestream','Run Amount'); ?></th>
                <th><?php echo __d('livestream','Time view'); ?></th>
                <th><?php echo __d('livestream','Add time'); ?></th>
                <th><?php echo __d('livestream','Start time'); ?></th>
                <th><?php echo __d('livestream','Complete time'); ?></th>
                <th><?php echo __d('livestream','Status'); ?></th>
                <th><?php echo __d('livestream','Price'); ?></th>
                <th><?php echo __d('livestream','User'); ?></th>
                <th><?php echo __d('livestream','Reaction'); ?></th>
                <th><?php echo __d('livestream','Time per'); ?></th>
                <th><?php echo __d('livestream','View per'); ?></th>
                <th><?php echo __d('livestream','User create'); ?></th>
                <th><?php echo __d('livestream','Is auto'); ?></th>
                <th><?php echo __d('livestream','Pakage'); ?></th>
                <th><?php echo __d('livestream','Note'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($orders)):?>
            <?php foreach ($orders as $order): ?>
            <tr>
                <td>
                    <?php echo $order['LivestreamOrder']['id'];?>
                </td>
                <td>
                    <a href="<?php echo $order['LivestreamOrder']['url'] ?>"><?php echo $order['LivestreamOrder']['url'] ?></a>
                </td>
                <td>
                    <?php echo $order['LivestreamOrder']['type'] == ORDER_TYPE_NOW ? __d('livestream','Now'): __d('livestream','Later'); ?>
                </td>

                <td>
                    <?php echo $order['LivestreamOrder']['amount_live'];?>
                </td>
                <td>
                    <?php echo $order['LivestreamOrder']['run_amount_live'];?>
                </td>
                <td>
                    <?php echo __d('livestream','%s minutes',$order['LivestreamOrder']['time_need_viewer']) ;?>
                </td>
                <td>
                    <?php echo date('d-m-Y',$order['LivestreamOrder']['add_time']);?>
                </td>
                <td>
                    <?php echo date('d-m-Y',$order['LivestreamOrder']['start_time']);?>
                </td>
                <td>
                    <?php echo date('d-m-Y',$order['LivestreamOrder']['complete_time']);?>
                </td>
                <td>
                    <?php if($order['LivestreamOrder']['status'] == ORDER_STATUS_ADD) echo __d('livestream','ADDED');?>
                    <?php if($order['LivestreamOrder']['status'] == ORDER_STATUS_ACTIVE) echo __d('livestream','ACTIVED');?>
                    <?php if($order['LivestreamOrder']['status'] == ORDER_STATUS_RUN) echo __d('livestream','RUNNING');?>
                    <?php if($order['LivestreamOrder']['status'] == ORDER_STATUS_COMPLETE) echo __d('livestream','COMPLETED');?>
                    <?php if($order['LivestreamOrder']['status'] == ORDER_STATUS_CANCEL) echo __d('livestream','CANCELLED');?>
                </td>
                <td>
                    <?php echo $order['LivestreamOrder']['price'];?>
                </td>
                <td>
                    <?php echo $order['User']['name'];?>
                </td>
                <td>
                    <?php echo $order['LivestreamOrder']['reaction'];?>
                </td>
                <td>
                    <?php echo $order['LivestreamOrder']['time_per'];?>
                </td>
                <td>
                    <?php echo $order['LivestreamOrder']['view_per'];?>
                </td>
                <td>
                    <?php echo $order['LivestreamOrder']['user_create'];?>
                </td>
                <td>
                    <?php echo $order['LivestreamOrder']['is_auto_create'];?>
                </td>
                <td>
                    <?php echo $order['LivestreamOrder']['pakage_id'];?>
                </td>
                <td>
                    <?php echo $order['LivestreamOrder']['note'];?>
                </td>
            </tr>
            <?php endforeach ?>
            <?php else:?>
            <tr>
                <td colspan="16">
                    <?php echo __d('livestream','No order found');?>
                </td>
            </tr>
            <?php endif;?>
        </tbody>
    </table>

    <div class="pagination">
        <?php echo $this->Paginator->first(__d('livestream','First'));?>&nbsp;
        <?php echo $this->Paginator->hasPage(2) ? $this->Paginator->prev(__d('livestream','Prev')) : '';?>&nbsp;
		<?php echo $this->Paginator->numbers();?>&nbsp;
		<?php echo $this->Paginator->hasPage(2) ?  $this->Paginator->next(__d('livestream','Next')) : '';?>&nbsp;
		<?php echo $this->Paginator->last(__d('livestream','Last'));?>
    </div>

</div>
<script>
<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    function changeVisiable(id, e)
    {
        var value = 0;
        if ($(e).hasClass('poll_no'))
        {
            value = 1;
        }
        $(e).attr('class', '');
        if (value)
        {
            $(e).addClass('poll_yes');
            $(e).attr('title', '<?php echo __d('poll','yes');?>');
        } else
        {
            $(e).addClass('poll_no');
            $(e).attr('title', '<?php echo __d('poll','no');?>');
        }

        $.post("<?php echo $this->request->base?>/admin/poll/polls/visiable", {'id': id, 'value': value}, function (data) {

        });
    }
    $(document).on('hidden.bs.modal', function (e) {
        $(e.target).removeData('bs.modal');
    });
<?php echo $this->Html->scriptEnd(); ?>
</script>