<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title"><?php echo __d('livestream','Select User');?></h4>
</div>
<div class="modal-body">
    <form id="createForm" class="form-horizontal" role="form">
        <div class="form-body">
            <div class="form-group">
                <label class="control-label"><?php echo __d('livestream','Select User');?></label>
                    <?php echo $this->Form->select('user_id', $members, array('class'=>'form-control','value'=> '','empty'=>false)); ?>
            </div>
        </div>
    </form>
    <div class="alert alert-danger error-message" style="display:none;margin-top:10px;">
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default btn-close-model-select-user" data-dismiss="modal" data-target="#modalRegistration"><?php echo  __d('livestream','Close') ?></button>
    <a href="#" id="createButtonUser" class="btn btn-action"><?php echo  __d('livestream','Next') ?></a>

</div>
<script>
    $(document).ready(function () {
        $('#createButtonUser').unbind('click').click(function () {
            var user_id = $("#user_id option:selected").val();
            $.ajax({
                type: "GET",
                cache: false,
                url: '<?php echo  $this->request->base ?>/livestreams/create_select_pakage/'+ user_id+'',
                dataType: "html",
                success: function (res)
                {
                    $('#createOrderStep2Modal').html(res);
                    $('#createOrderStep1Modal').modal('hide');
                    $('#createOrderStep2Modal').modal({
                        show: true,
                        backdrop: 'static'
                    });
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    console.log(thrownError);
                }
            });
        });

    });

    function toggleField()
    {
        $('.opt_field').toggle();
    }
</script>