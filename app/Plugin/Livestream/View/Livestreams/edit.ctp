
<?php if($is_ajax): ?>
<div class="modal-dialog">
    <div class="modal-content">
<?php endif; ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><?php echo __d('livestream','Edit Order');?></h4>
        </div>
        <div class="modal-body">
            <form id="createFormLivestreamOrder" class="form-horizontal" role="form">
                <div class="form-body">
                    <?php echo $this->Form->hidden('id', array('value' => $order['LivestreamOrder']['id'])); ?>
                    <?php echo $this->Form->hidden('user_id', array('value' => $order['LivestreamOrder']['user_id'])); ?>
                    <?php echo $this->Form->hidden('pakage_id', array('value' => $order['LivestreamOrder']['pakage_id'])); ?>
                    <?php echo $this->Form->hidden('is_auto_create', array('value' => 0)); ?>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Url');?></label>
                        <?php echo $this->Form->text('url', array('placeholder' => __d('livestream','https://www.facebook.com/your_facebook_id/posts/post_id'), 'class'=>'form-control','value' => $order['LivestreamOrder']['url'], 'readonly' => !$can_edit['url'])); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Amount view livestream');?></label>
                            <?php echo $this->Form->select('amount_live', $arr_amount, array('class'=>'form-control','value'=> $order['LivestreamOrder']['amount_live'],'empty'=>false, 'disabled' => !$can_edit['amount_view'])); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Amount view video after livestream');?></label>
                            <?php echo $this->Form->select('amount_view', $arr_amount, array('class'=>'form-control','value'=> $order['LivestreamOrder']['amount_view'],'empty'=>false, 'disabled' => !$can_edit['amount_view'])); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Time need to view livestream');?></label>
                            <?php echo $this->Form->select('time_need_viewer', $arr_time, array('class'=>'form-control','value'=> $order['LivestreamOrder']['time_need_viewer'],'empty'=>false, 'disabled' => !$can_edit['amount_view'])); ?>
                    </div>
                    <a href="#" id="advance-link"><?php echo __d('livestream','Advance') ?></a>
                    <div id="more-information" style="display: none">
                        <div class="form-group">
                            <label class="control-label"><?php echo __d('livestream','You create order for livestream now or later?');?></label>
                            <?php echo $this->Form->select('type', $type, array('class'=>'form-control','value'=> $order['LivestreamOrder']['type'],'empty'=>false)); ?>
                        </div>
                        <div id="set-time-start-livestream" style="display: none">
                            <div class="form-group">
                                <label class="control-label"><?php echo __d('livestream','When will you livestream?');?></label>
                            <?php echo $this->Form->text('time_start_schedule', array('placeholder' => __d('livestream', 'Select time'),'value' => $order['LivestreamOrder']['time_start_schedule'], 'class' => 'form-control','readOnly'=>true)); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo __d('livestream','Select Timezone');?></label>
                            <?php echo $this->Form->select('timezone', $this->Moo->getTimeZoneNumbers(), array('class'=>'form-control','value'=> $order['LivestreamOrder']['timezone'],'empty'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo __d('livestream','Do you want reaction');?></label>
                            <?php echo $this->Form->select('reaction', $arr_reaction, array('class'=>'form-control','value'=> $order['LivestreamOrder']['reaction'],'empty'=>false)); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo __d('livestream','Do you want the number of viewers to increase slowly?Such as: 30 minutes increase 100 viewer');?></label>
                            <?php echo $this->Form->select('slow', $arr_slow, array('class'=>'form-control','value'=> $order['LivestreamOrder']['time_per']>'0'?1:0,'empty'=>false)); ?>
                        </div>
                        <div id="is_slow" <?php if($order['LivestreamOrder']['time_per']<= '0'): ?>style="display: none"<?php endif; ?> >
                            <div class="form-group">
                                <label class="control-label"><?php echo __d('livestream','How many minutes?');?></label>
                            <?php echo $this->Form->select('time_per', $arr_time_per, array('class'=>'form-control','value'=> $order['LivestreamOrder']['time_per'],'empty'=>TRUE)); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo __d('livestream','How many viewer?');?></label>
                            <?php echo $this->Form->select('view_per', $arr_view_per, array('class'=>'form-control','value'=> $order['LivestreamOrder']['view_per'],'empty'=>TRUE)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo __d('livestream','Note');?></label>
                            <?php echo $this->Form->textarea('note', array('placeholder' => __d('livestream','note'), 'class'=>'form-control','value'=> $order['LivestreamOrder']['note'])); ?>
                        </div>
                    </div>
                </div>
            </form>
            <div class="alert alert-danger error-message" style="display:none;margin-top:10px;">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn default btn-close-model" data-dismiss="modal"><?php echo  __d('livestream','Close') ?></button>
            <a href="#" id="createButtonOrderCreate" class="btn btn-action"><?php echo  __d('livestream','Save') ?></a>
        </div>
        <?php if($is_ajax): ?>
    </div>
</div>
<?php endif; ?>
<script>
    $(document).ready(function () {
        $('#createButtonOrderCreate').unbind('click').click(function () {
            $.ajax({
                type: "POST",
                cache: false,
                url: '<?php echo  $this->request->base ?>/livestreams/save',
                dataType: "json",
                data: $("#createFormLivestreamOrder").serialize(),
                success: function (data)
                {
                    if (data.result) {
                        window.location.reload(true);
                    } else {
                        $('.error-message').show();
                        $('.error-message').html(data.message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    console.log(thrownError);
                }
            });
        });
        $('.btn-close-model').click(function () {
            window.location.reload(true);
        });
        $("#type").change(function () {
            var value = $(this).val();
            if (value == '<?php echo ORDER_TYPE_LATER ?>') {
                $('#set-time-start-livestream').show();
                $('#url').val('');
                $('#timezone').val(getClientTimezone());
                $("#url").attr("placeholder", "https://www.facebook.com/your_facebook_id").blur();
            } else
            {
                $('#date_start_schedule').val('');
                $('#time_start_schedule').val('');
                $('#url').val('');
                $("#url").attr("placeholder", "https://www.facebook.com/your_facebook_id/posts/post_id").blur();
                $('#set-time-start-livestream').hide();
            }
        });
        $("#slow").change(function () {
            var value = $(this).val();
            if (value == '1') {
                $('#is_slow').show();
            } else
            {
                $('#is_slow').hide();
                $('#time_per').val('');
                $('#view_per').val('');
            }
        });

        $('#time_start_schedule').datetimepicker({
            language: 'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0
        });
    });
    function getClientTimezone() {
        var offset = new Date().getTimezoneOffset();
        var result = '';
        if (offset.toString().indexOf('-') >= 0) {
            result += '+';
        }
        result += Math.abs(offset) / 60;
        return result;
    }
    function toggleField()
    {
        $('.opt_field').toggle();
    }
    $('#advance-link').click(function () {
        $('#more-information').slideToggle();
    });

</script>