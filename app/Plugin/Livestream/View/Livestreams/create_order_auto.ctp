
<?php if($is_ajax): ?>
<div class="modal-dialog">
    <div class="modal-content">
<?php endif; ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><?php echo __d('livestream','Create Order Auto');?></h4>
        </div>
        <div class="modal-body">
            <form id="createFormLivestreamOrder" class="form-horizontal" role="form">
                <?php echo $this->Form->hidden('user_id', array('value' => $user_id)); ?>
                <?php echo $this->Form->hidden('pakage_id', array('value' => $pakage_id)); ?>
                <?php echo $this->Form->hidden('type', array('value' => ORDER_TYPE_NOW)); ?>
                <?php echo $this->Form->hidden('is_auto_create', array('value' => 1)); ?>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Profile URL');?></label>
                            <?php echo $this->Form->text('url', array('placeholder' => __d('livestream','https://www.facebook.com/your_facebook_id/posts/post_id'), 'class'=>'form-control')); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Amount livestream');?></label>
                            <?php echo $this->Form->select('amount_live', $arr_amount, array('class'=>'form-control','value'=> '','empty'=>false)); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Amount view video after livestream');?></label>
                            <?php echo $this->Form->select('amount_view', $arr_amount, array('class'=>'form-control','value'=> '','empty'=>false)); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Time need to view livestream');?></label>
                            <?php echo $this->Form->select('time_need_viewer', $arr_time, array('class'=>'form-control','value'=> '','empty'=>false)); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Do you want reaction');?></label>
                            <?php echo $this->Form->select('reaction', $arr_reaction, array('class'=>'form-control','value'=> '','empty'=>false)); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Do you want the number of viewers to increase slowly?Such as: 30 minutes increase 100 viewer');?></label>
                            <?php echo $this->Form->select('slow', $arr_slow, array('class'=>'form-control','value'=> '','empty'=>false)); ?>
                    </div>
                    <div id="is_slow" style="display: none">
                        <div class="form-group">
                            <label class="control-label"><?php echo __d('livestream','How many minutes?');?></label>
                            <?php echo $this->Form->select('time_per', $arr_time_per, array('class'=>'form-control','value'=> '','empty'=>TRUE)); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo __d('livestream','How many viewer?');?></label>
                            <?php echo $this->Form->select('view_per', $arr_view_per, array('class'=>'form-control','value'=> '','empty'=>TRUE)); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __d('livestream','Note');?></label>
                            <?php echo $this->Form->textarea('note', array('placeholder' => __d('livestream','note'), 'class'=>'form-control')); ?>
                    </div>
                    
                </div>
            </form>
            <div class="alert alert-danger error-message" style="display:none;margin-top:10px;">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn default btn-close-model" data-dismiss="modal"><?php echo  __d('livestream','Close') ?></button>
            <a href="#" id="createButtonOrderCreate" class="btn btn-action"><?php echo  __d('livestream','Save') ?></a>
        </div>
        <?php if($is_ajax): ?>
    </div>
</div>
<?php endif; ?>
<script>
    $(document).ready(function () {
        $('#createButtonOrderCreate').unbind('click').click(function () {
            $.ajax({
                type: "POST",
                cache: false,
                url: '<?php echo  $this->request->base ?>/livestreams/save',
                dataType: "json",
                data: $("#createFormLivestreamOrder").serialize(),
                success: function (data)
                {
                    if (data.result) {
                        window.location.reload(true);
                    } else {
                        $('.error-message').show();
                        $('.error-message').html(data.message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    console.log(thrownError);
                }
            });
        });
        $('.btn-close-model').click(function () {
            window.location.reload(true);
        });
        $("#type").change(function () {
            var value = $(this).val();
            if (value == '<?php echo ORDER_TYPE_LATER ?>') {
                $('#set-time-start-livestream').show();
                $('#id-is-auto-create').show();
                $('#url').val('');
                $('#is_auto_create').val('0');
                $("#url").attr("placeholder", "https://www.facebook.com/your_facebook_id").blur();
            } else
            {
                $('#date_start_schedule').val('');
                $('#time_start_schedule').val('');
                $('#url').val('');
                $('#is_auto_create').val('0');
                $("#url").attr("placeholder", "https://www.facebook.com/your_facebook_id/posts/post_id").blur();
                $('#set-time-start-livestream').hide();
                $('#id-is-auto-create').hide();
            }
        });
        $("#slow").change(function () {
            var value = $(this).val();
            if (value == '1') {
                $('#is_slow').show();
            } else
            {
                $('#is_slow').hide();
                $('#time_per').val('');
                $('#view_per').val('');
            }
        });
        $(".datepicker").pickadate({
            format: 'yyyy-mm-dd',
            close: false,
            onClose: function () {
                if ($('#date_start_schedule').val() != '')
                {
                    var s_time = $('#date_start_schedule').val() + ' 23:59';
                    var ss_time = s_time.replace("-", "/").replace("-", "/");
                    var current_time = new Date().getTime();
                    var select_time = new Date(ss_time).getTime();
                    if (current_time > select_time) {
                        $('#date_start_schedule').val('');
                    }
                }
            }
        });
        $(".timepicker").pickatime({
            format: 'H:i',
            interval: 5,
            onClose: function (time) {
                if ($('#date_start_schedule').val() != '' && $('#time_start_schedule').val() != '')
                {
                    var s_time = $('#date_start_schedule').val() + ' ' + $('#time_start_schedule').val();
                    var ss_time = s_time.replace("-", "/").replace("-", "/");
                    var current_time = new Date().getTime();
                    var select_time = new Date(ss_time).getTime();
                    if (current_time > select_time) {
                        $('#time_start_schedule').val('');
                    }
                }
                $('#timezone').val(getClientTimezone());
            }
        });
    });
    function getClientTimezone() {
        var offset = new Date().getTimezoneOffset();
        var result = '';
        if (offset.toString().indexOf('-') >= 0) {
            result += '+';
        }
        result += Math.abs(offset) / 60;
        return result;
    }
    function toggleField()
    {
        $('.opt_field').toggle();
    }
</script>