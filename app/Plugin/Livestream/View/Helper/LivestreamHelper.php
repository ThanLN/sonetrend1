<?php

App::uses('AppHelper', 'View/Helper');

class LivestreamHelper extends AppHelper {

    public function checkUserHavePakage($user_id) {
        $pakageDetailModel = MooCore::getInstance()->getModel('Pakage.PakageDetail');
        $cond['PakageDetail.user_id'] = $user_id;
        $cond['PakageDetail.active'] = 1;
        $count = $pakageDetailModel->find('count', array('conditions' => $cond));
        
        if ($count > 0)
            return true;
        return FALSE;
    }

}
