<?php

class LivestreamsController extends LivestreamAppController {

    public $components = array('Paginator');

    public function admin_index() {
        $this->set('title_for_layout', __d('livestream', 'Livestream Order'));
        $this->loadModel('Livestream.LivestreamOrder');
        $this->Paginator->settings = array(
            'limit' => Configure::read('Livestream.livestream_item_per_pages'),
            'order' => array(
                'LivestreamOrder.id' => 'DESC'
            )
        );

        $cond = array();
        $passedArgs = array();


        $orders = $this->Paginator->paginate('LivestreamOrder', $cond);
        $this->set('orders', $orders);
        $this->set('list_status', $this->LivestreamOrder->getListStatus());
        $this->set('passedArgs', $passedArgs);
    }

    public function index() {
        $this->set('title_for_layout', __d('livestream', 'Livestream Orders'));
        $this->loadModel('Livestream.LivestreamOrder');
        $this->loadModel('User');
        if (empty($this->request->pass)) {
            $type = 'not_auto';
            $status = 0;
            $from_date = date("Y-m-d", strtotime("first day of this month"));
            $to_date = date("Y-m-d", strtotime("last day of this month"));
        } else {
            $url = $this->request->pass[0];
            $type = $this->getRequestGet('type', $url);
            $from_date = $this->getRequestGet('from_date', $url);
            if (!$from_date) {
                $from_date = date("Y-m-d", strtotime("first day of this month"));
            }
            $to_date = $this->getRequestGet('to_date', $url);
            if (!$to_date) {
                $to_date = date("Y-m-d", strtotime("last day of this month"));
            }
            $status = $this->getRequestGet('status', $url);
            if (!$status) {
                $status = 0;
            }
        }

        $this->Paginator->settings = array(
            'limit' => Configure::read('Livestream.livestream_item_per_pages'),
            'order' => array(
                'LivestreamOrder.id' => 'DESC'
            )
        );
        $viewer = MooCore::getInstance()->getViewer();
        $cond = array();
        $passedArgs = array();
        if ($viewer['Role']['is_admin']) {
            
        } else if ($viewer['Role']['is_mod']) {
            $list_ids = array();
            $ids = $this->User->getUsersByCondition(array('User.user_create' => $viewer['User']['id']));
            foreach ($ids as $item) {
                array_push($list_ids, $item['User']['id']);
            }
            $cond['LivestreamOrder.user_id IN'] = $list_ids;
            $passedArgs['user_id'] = $list_ids;
        } else {
            $cond['LivestreamOrder.user_id'] = $viewer['User']['id'];
            $passedArgs['user_id'] = $viewer['User']['id'];
        }
        if ($type == 'auto') {
            $cond['LivestreamOrder.is_auto_create'] = 1;
            $passedArgs['is_auto_create'] = 1;
        } else if ($type == 'not_auto') {
            $cond['LivestreamOrder.is_auto_create'] = 0;
            $passedArgs['is_auto_create'] = 0;
        } else {
            $cond['LivestreamOrder.user_id'] = '0';
            $passedArgs['user_id'] = '0';
        }
        if ($status) {
            $cond['LivestreamOrder.status'] = $status;
            $passedArgs['status'] = $status;
        }
        $cond['LivestreamOrder.add_time >'] = strtotime($from_date . ' 00:00:00');
        $cond['LivestreamOrder.add_time <'] = strtotime($to_date . ' 23:59:59');
        $passedArgs['add_time >'] = strtotime($from_date . ' 00:00:00');
        $passedArgs['add_time <'] = strtotime($to_date . ' 23:59:59');

        $orders = $this->Paginator->paginate('LivestreamOrder', $cond);

        $this->set('orders', $orders);
        $this->set('type', $type);
        $this->set('from_date', date('d-m-Y', strtotime($from_date)));
        $this->set('to_date', date('d-m-Y', strtotime($to_date)));
        $this->set('list_status', $this->LivestreamOrder->getListStatus());
        $this->set('status', $status);
        $this->set('passedArgs', $passedArgs);
    }

    public function create($user_id = null, $pakage_id = 0) {
        $this->loadModel('Livestream.LivestreamOrder');
        $this->loadModel('Pakage.Price');

        $arr_amount = $this->Price->getArrayAmountByUser($user_id, $pakage_id);
        $arr_time = $this->Price->getArrayTimeneedviewerByUser($user_id, $pakage_id);
        $arr_reaction = array(0 => __d('livestream', 'I dont want reaction'), 1 => __d('livestream', 'Yes, I want reaction in my video'));
        $arr_slow = array(0 => __d('livestream', 'I dont want slowlly'), 1 => __d('livestream', 'Yes, I want slowlly'));
        $arr_time_per = $this->LivestreamOrder->getArrayTimePer();
        $arr_view_per = $this->LivestreamOrder->getArrayViewPer();
        $arr_auto_create = array(0 => __d('livestream', 'No, I dont want order auto create next time livestream'), ORDER_TYPE_LATER => __d('livestream', 'Yes, I want to create order in the next time livestream'));
        $type = array(ORDER_TYPE_NOW => __d('livestream', 'Now'), ORDER_TYPE_LATER => __d('livestream', 'Later'));
        $is_ajax = $this->request->is('ajax');

        $this->set('user_id', $user_id);
        $this->set('pakage_id', $pakage_id);
        $this->set('type', $type);
        $this->set('arr_amount', $arr_amount);
        $this->set('arr_time', $arr_time);
        $this->set('arr_reaction', $arr_reaction);
        $this->set('arr_slow', $arr_slow);
        $this->set('arr_time_per', $arr_time_per);
        $this->set('arr_view_per', $arr_view_per);
        $this->set('arr_auto_create', $arr_auto_create);
        $this->set('is_ajax', $is_ajax);
        if ($is_ajax) {
            echo $this->render('/Livestreams/create');
            die;
        }
    }

    public function create_order_auto($user_id = null, $pakage_id = 0) {
        $this->loadModel('Livestream.LivestreamOrder');
        $this->loadModel('Pakage.Price');

        $arr_amount = $this->Price->getArrayAmountByUser($user_id, $pakage_id);
        $arr_time = $this->Price->getArrayTimeneedviewerByUser($user_id, $pakage_id);
        $arr_reaction = array(0 => __d('livestream', 'I dont want reaction'), 1 => __d('livestream', 'Yes, I want reaction in my video'));
        $arr_slow = array(0 => __d('livestream', 'I dont want slowlly'), 1 => __d('livestream', 'Yes, I want slowlly'));
        $arr_time_per = $this->LivestreamOrder->getArrayTimePer();
        $arr_view_per = $this->LivestreamOrder->getArrayViewPer();
        $arr_auto_create = array(0 => __d('livestream', 'No, I dont want order auto create next time livestream'), ORDER_TYPE_LATER => __d('livestream', 'Yes, I want to create order in the next time livestream'));
        $type = array(ORDER_TYPE_NOW => __d('livestream', 'Now'), ORDER_TYPE_LATER => __d('livestream', 'Later'));
        $is_ajax = $this->request->is('ajax');

        $this->set('user_id', $user_id);
        $this->set('pakage_id', $pakage_id);
        $this->set('type', $type);
        $this->set('arr_amount', $arr_amount);
        $this->set('arr_time', $arr_time);
        $this->set('arr_reaction', $arr_reaction);
        $this->set('arr_slow', $arr_slow);
        $this->set('arr_time_per', $arr_time_per);
        $this->set('arr_view_per', $arr_view_per);
        $this->set('arr_auto_create', $arr_auto_create);
        $this->set('is_ajax', $is_ajax);
        if ($is_ajax) {
            echo $this->render('/Livestreams/create_order_auto');
            die;
        }
    }

    public function create_enter_discountcode() {
        
    }

    public function create_demo($discount_code) {
        $this->loadModel('Livestream.LivestreamDiscountcode');
        $this->loadModel('Livestream.LivestreamOrder');
        $cond = array("LivestreamDiscountcode.code" => $discount_code, "LivestreamDiscountcode.active" => 1);
        $code = $this->LivestreamDiscountcode->getLivestreamDiscountcode('first', $cond);
        $arr_amount = array();
        $arr_time = array();
        $message = '';
        if ($code) {
            if (intval($code['LivestreamDiscountcode']['count_used']) >= intval($code['LivestreamDiscountcode']['num_used'])) {
                $success = false;
                $message = __d('livestream', 'Discount code was used');
            } else {
                $success = true;
                $arr_amount = $this->LivestreamOrder->getArrayViewer($code['LivestreamDiscountcode']['max_viewer']);
                $arr_time = $this->LivestreamOrder->getArrTime($code['LivestreamDiscountcode']['max_time']);
            }
        } else {
            $success = false;
            $message = __d('livestream', 'Discount code invalid');
        }
        $is_ajax = $this->request->is('ajax');
        $this->set('is_ajax', $is_ajax);
        $this->set('success', $success);
        $this->set('message', $message);
        $this->set('arr_amount', $arr_amount);
        $this->set('arr_time', $arr_time);
        $this->set('distcount_code', $discount_code);
        if ($is_ajax) {
            echo $this->render('/Livestreams/create_order_demo');
            die;
        }
    }

    public function create_select_user() {
        $this->loadModel('User');
        $cond[] = array("User.role_id" => ROLE_MEMBER);
        $user_role = $this->_getUserRoleId();
        if ($user_role == ROLE_SALE) {
            $cond[] = array('User.user_create' => $this->Auth->user('id'));
        }
        $members = $this->User->getUserIdName($cond);
        $this->set('members', $members);
    }

    public function create_select_user_auto() {
        $this->loadModel('User');
        $cond[] = array("User.role_id" => ROLE_MEMBER);
        $user_role = $this->_getUserRoleId();
        if ($user_role == ROLE_SALE) {
            $cond[] = array('User.user_create' => $this->Auth->user('id'));
        }
        $members = $this->User->getUserIdName($cond);
        $this->set('members', $members);
    }

    public function create_select_pakage($user_id) {
        $this->loadModel('User');
        $this->loadModel('Pakage.PakageDetail');
        $pakages = $this->PakageDetail->getPakageDetailIdNameByUser($user_id, true);
        $is_ajax = $this->request->is('ajax');

        $this->set('pakages', $pakages);
        $this->set('user_id', $user_id);
        $this->set('is_ajax', $is_ajax);
        if ($is_ajax) {
            echo $this->render('/Livestreams/create_select_pakage');
            die;
        }
    }

    public function create_select_pakage_auto($user_id) {
        $this->loadModel('User');
        $this->loadModel('Pakage.PakageDetail');
        $pakages = $this->PakageDetail->getPakageDetailIdNameByUser($user_id, true);
        $is_ajax = $this->request->is('ajax');

        $this->set('pakages', $pakages);
        $this->set('user_id', $user_id);
        $this->set('is_ajax', $is_ajax);
        if ($is_ajax) {
            echo $this->render('/Livestreams/create_select_pakage_auto');
            die;
        }
    }

    public function save() {
        $this->loadModel('Pakage.Pakage');
        $this->loadModel('Livestream.LivestreamOrder');
        $this->loadModel('Livestream.LivestreamDiscountcode');
        $this->autoRender = false;

        $data = $this->request->data;
            //create livestream order
            if (!isset($data['discount_code'])) {
                $data['discount_code'] = '';
            } else {
                $code = $this->LivestreamDiscountcode->find('first', array('conditions' => array('code' => $data['discount_code'])));
                if (!$code['LivestreamDiscountcode']['active']) {
                    $response['result'] = 0;
                    $response['message'] = __d('livestream', 'Discount code is up to date');
                    echo json_encode($response);
                    die;
                }
            }
            $date = new DateTime();
            $data['add_time'] = $date->getTimestamp();
            $data['status'] = ORDER_STATUS_ADD;
            if (!isset($data['amount_view']))
                $data['amount_view'] = 0;
            $data['price'] = $this->LivestreamOrder->countPrice($data['amount_live'], $data['time_need_viewer'], $data['amount_view'], $data['user_id'], $data['pakage_id'], $data['discount_code']);
            $data['user_create'] = $this->Auth->user('id');
            if ($data['type'] == ORDER_TYPE_LATER) {
                $data['time_start_schedule'] = strtotime($data['time_start_schedule']);
                $data['time_start_schedule'] = $this->LivestreamOrder->convertTime($data['time_start_schedule'], $data['timezone']);
            } else {
                $data['time_start_schedule'] = time();
                $data['timezone'] = '+7';
            }

            $flag = $this->LivestreamOrder->validateLivestreamOrder($data);
            if ($flag['pass'] != true) {
                $response['result'] = 0;
                $response['message'] = $flag['message'];
                echo json_encode($response);
                die;
            }
            
            $flag = $this->LivestreamOrder->createLivestreamOrder($data);
            if ($flag) {
                 if (isset($data['id'])) {
                     $message = __d('livestream', 'Order has been successfully updated');
                 }else{
                     $message = __d('livestream', 'Order has been successfully created');
                 }
                $this->Session->setFlash($message, 'default', array('class' => 'Metronic-alerts alert alert-success fade in'));
                $response['result'] = 1;
                echo json_encode($response);
                die;
            } else {
                $response['result'] = 0;
                $response['message'] = __d('livestream', 'Save order error');
                echo json_encode($response);
                die;
            }
    }

    public function edit($id) {
        $user_id = MooCore::getInstance()->getViewer(true);
        $this->loadModel('Livestream.LivestreamOrder');
        $this->loadModel('Pakage.Price');

        $is_ajax = $this->request->is('ajax');

        $is_owner = $this->LivestreamOrder->checkOwner($user_id, $id);
        $order = array();
        $arr_amount = array();
        $arr_time = array();
        $type = array();
        $arr_reaction = array();
        $arr_slow = array();
        $arr_time_per = array();
        $arr_view_per = array();
        if ($is_owner) {
            $order = $this->LivestreamOrder->findById($id);
            $arr_amount = $this->Price->getArrayAmountByUser($order['LivestreamOrder']['user_id'], $order['LivestreamOrder']['pakage_id'], $order['LivestreamOrder']['amount_live']);
            $arr_time = $this->Price->getArrayTimeneedviewerByUser($order['LivestreamOrder']['user_id'], $order['LivestreamOrder']['pakage_id']);
            $type = array(ORDER_TYPE_NOW => __d('livestream', 'Now'), ORDER_TYPE_LATER => __d('livestream', 'Later'));
            $arr_reaction = array(0 => __d('livestream', 'I dont want reaction'), 1 => __d('livestream', 'Yes, I want reaction in my video'));
            $arr_slow = array(0 => __d('livestream', 'I dont want slowlly'), 1 => __d('livestream', 'Yes, I want slowlly'));
            $arr_time_per = $this->LivestreamOrder->getArrayTimePer();
            $arr_view_per = $this->LivestreamOrder->getArrayViewPer();
        }
        $can_edit = $this->LivestreamOrder->checkEdit($user_id, $id);
        $this->set('is_ajax', $is_ajax);
        $this->set('arr_amount', $arr_amount);
        $this->set('is_owner', $is_owner);
        $this->set('order', $order);
        $this->set('arr_time', $arr_time);
        $this->set('type', $type);
        $this->set('arr_reaction', $arr_reaction);
        $this->set('arr_time_per', $arr_time_per);
        $this->set('arr_view_per', $arr_view_per);
        $this->set('arr_slow', $arr_slow);
        $this->set('can_edit', $can_edit);
    }

    public function getRequestGet($type, $url) {
        $arr_type = explode("&", $url);
        foreach ($arr_type as $list) {
            $arr_key = explode("=", $list);
            if ($arr_key[0] == $type) {
                return $arr_key[1];
            }
        }
        return false;
    }

}
