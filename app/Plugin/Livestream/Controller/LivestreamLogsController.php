<?php

class LivestreamLogsController extends LivestreamAppController {

    public function admin_index() {
        
    }

    public function index() {
        
    }

    public function view($order_id) {
        $this->set('title_for_layout', __d('livestream', 'Chart Orders'));
        $this->loadModel('Livestream.LivestreamLog');
        $cond['LivestreamLog.order_id'] = $order_id;
        $log = $this->LivestreamLog->getLivetreamLog('first', $cond);
        $param = array();
        if (!empty($log)) {
            $param = json_decode($log['LivestreamLog']['param']);
        }
        $json_time = array();
        $json_viewer = array();
        $i = 0;
        foreach ($param as $tmp) {
            if ($i >= count($param) -50) {
                $json_time[] = (string) date("H:i:s", $tmp->time_write_log);
                $json_viewer[] = (int)$tmp->view_count;
            }
            $i++;
        }
        $this->set('json_time', json_encode($json_time));
        $this->set('json_viewer', json_encode($json_viewer));
    }

}
