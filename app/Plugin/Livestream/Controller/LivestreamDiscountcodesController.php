<?php

class LivestreamDiscountcodesController extends LivestreamAppController {

    public $components = array('Paginator');

    public function admin_index() {
        $this->set('title_for_layout', __d('livestream', 'Livestream Discount Codes'));
        $this->loadModel('Livestream.LivestreamDiscountcode');
        $this->Paginator->settings = array(
            'limit' => Configure::read('Livestream.livestream_item_per_pages'),
            'order' => array(
                'LivestreamDiscountcode.id' => 'DESC'
            )
        );

        $cond = array();
        $passedArgs = array();


        $codes = $this->Paginator->paginate('LivestreamDiscountcode', $cond);
        $this->set('codes', $codes);
        $this->set('passedArgs', $passedArgs);
    }

    public function admin_create() {
        $this->_checkPermission(array('super_admin' => 1));
        $uid = $this->Auth->user('id');
        $this->loadModel('Pakage.Price');
        $this->loadModel('Livestream.LivestreamOrder');
        $sys_price= $this->Price->getSystemPrice();
        $arr_viewer = $this->LivestreamOrder->getArrayViewer($sys_price['Price']['max_viewer']);
        $arr_timer = $this->LivestreamOrder->getArrTime($sys_price['Price']['max_time']);
        $this->set('arr_viewer', $arr_viewer);
        $this->set('arr_timer', $arr_timer);
        $this->set('uid', $uid);
    }

    public function admin_save_create() {
        $this->_checkPermission(array('super_admin' => 1));
        $this->autoRender = false;
        $this->loadModel('Livestream.LivestreamDiscountcode');
        $uid = $this->Auth->user('id');
        $data = $this->request->data;
        if (is_numeric($data['loop'])) {
            for ($i = 0; $i < intval($data['loop']); $i++) {
                $data_save = $this->LivestreamDiscountcode->prepairData($data);
                $this->LivestreamDiscountcode->clear();
                $this->LivestreamDiscountcode->set($data_save);
                $this->_validateData($this->LivestreamDiscountcode);
                $this->LivestreamDiscountcode->save();
            }
        }
        $this->Session->setFlash(__d('livestream','Discount code has been successfully created'), 'default', array('class' => 'Metronic-alerts alert alert-success fade in'));
        
        $response['result'] = 1;
        echo json_encode($response);
    }

    public function index() {
        
    }

}
