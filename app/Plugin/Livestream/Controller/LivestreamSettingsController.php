<?php 
class LivestreamSettingsController extends LivestreamAppController{
      public $components = array('QuickSettings');

    public function admin_index($id = null) {
        $this->set('title_for_layout', __d('pakage', 'Settings'));
        $cuser = $this->_getUser();
        if ($cuser['Role']['is_admin']) {
            $this->QuickSettings->run($this, array("Livestream"), $id);
            if (CakeSession::check('Message.flash')) {
                $menuModel = MooCore::getInstance()->getModel('Menu.CoreMenuItem');
                $menu = $menuModel->findByUrl('/livestreams');
                if ($menu) {
                    $menuModel->id = $menu['CoreMenuItem']['id'];
                    $menuModel->save(array('is_active' => Configure::read('Livestream.livestream_enabled')));
                }
                Cache::clearGroup('menu', 'menu');
            }
        }
    }
}