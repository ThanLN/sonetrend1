
CREATE TABLE IF NOT EXISTS `livestream_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT '1' COMMENT '1 now 2 later',
  `video_id` varchar(255) DEFAULT NULL,
  `other_param` mediumtext,
  `amount_live` mediumint(9) DEFAULT '0',
  `amount_view` mediumint(9) DEFAULT '0',
  `amount_like` mediumint(9) DEFAULT '0',
  `amount_share` mediumint(9) DEFAULT '0',
  `amount_comment` mediumint(9) DEFAULT '0',
  `viewer_location` varchar(255) DEFAULT NULL,
  `time_need_viewer` int(11) DEFAULT NULL,
  `time_start_schedule` int(11) DEFAULT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `start_time_real` int(11) DEFAULT '0',
  `add_time` int(11) DEFAULT NULL,
  `start_time` int(11) DEFAULT NULL,
  `complete_time` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1: added, 2: active, 3: running, 4 : complete; 5: cancaled',
  `price` mediumtext,
  `note` mediumtext,
  `run_number` int(11) DEFAULT NULL,
  `run_total` mediumint(9) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  `reaction` varchar(50) DEFAULT '0' COMMENT '1,2,3,4',
  `time_per` int(11) DEFAULT '0',
  `view_per` int(11) DEFAULT '0',
  `run_amount_live` mediumint(9) DEFAULT '0',
  `start_last_time` int(11) DEFAULT '0',
  `started_viewer_live` int(11) DEFAULT '0',
  `is_auto_create` tinyint(1) DEFAULT '0' COMMENT '0: khong auto, 1 auto',
  `pakage_id` tinyint(1) DEFAULT '0' COMMENT '0: order le, 1: order theo goi',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `livestream_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT '0',
  `param` mediumtext COMMENT '{''time_write_log'' =1,''view_count'' = 12}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;