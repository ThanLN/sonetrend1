<?php
require_once(APP . DS . 'Plugin' . DS . 'Livestream' . DS .'Config' . DS . 'constants.php');
if(Configure::read('Livestream.livestream_enabled')){
    App::uses('LivestreamListener', 'Livestream.Lib');
    CakeEventManager::instance()->attach(new LivestreamListener());
    MooSeo::getInstance()->addSitemapEntity("LivestreamListener", array(
        'livestream'
    ));
}