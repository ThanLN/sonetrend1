<?php
Router::connect('/livestreams/:action/*', array(
    'plugin' => 'Livestream',
    'controller' => 'livestreams'
));

Router::connect('/livestreams/*', array(
    'plugin' => 'Livestream',
    'controller' => 'livestreams',
    'action' => 'index'
));
