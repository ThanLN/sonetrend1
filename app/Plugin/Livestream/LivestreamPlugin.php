<?php 
App::uses('MooPlugin','Lib');
class LivestreamPlugin implements MooPlugin{
    public function install(){}
    public function uninstall(){}
    public function settingGuide(){}
    public function menu()
    {
        return array(
            __d('livestream','Orders') => array('plugin' => 'livestream', 'controller' => 'livestreams', 'action' => 'admin_index'),
            __d('livestream','Settings') => array('plugin' => 'livestream', 'controller' => 'livestream_settings', 'action' => 'admin_index'),
            __d('livestream','Demo Codes') => array('plugin' => 'livestream', 'controller' => 'livestream_discountcodes', 'action' => 'admin_index'),
        );
    }
    /*
    Example for version 1.0: This function will be executed when plugin is upgraded (Optional)
    public function callback_1_0(){}
    */
}