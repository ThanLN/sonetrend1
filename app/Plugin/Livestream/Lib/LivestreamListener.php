<?php

App::uses('CakeEventListener', 'Event');

class LivestreamListener implements CakeEventListener {

    public function implementedEvents() {
        return array(
            'MooView.beforeRender' => 'beforeRender',
        );
    }

    public function beforeRender($event) {
        if (Configure::read('Livestream.livestream_enabled')) {
            $e = $event->subject();
            $e->Helpers->Html->css(array(
                'Livestream.main'
                    ), array('block' => 'css')
            );

            if (Configure::read('debug') == 0) {
                $min = "min.";
            } else {
                $min = "";
            }
            $e->Helpers->MooRequirejs->addPath(array(
                "mooOrder" => $e->Helpers->MooRequirejs->assetUrlJS("livestreamorder/js/main.{$min}js")
            ));

          

            $e->addPhraseJs(array(
                'delete_poll_confirm' => __d('livestream', 'Are you sure you want to delete this order?')
            ));

            $e->Helpers->MooPopup->register('createOrderStep1Modal');
            $e->Helpers->MooPopup->register('createOrderStep2Modal');
            $e->Helpers->MooPopup->register('createOrderStep3Modal');
            $e->Helpers->MooPopup->register('createOrderStep4Modal');
            $e->Helpers->MooPopup->register('createOrderStep5Modal');
        }
    }

}
