<?php
/*
 * Copyright (c) SocialLOFT LLC
 * mooSocial - The Web 2.0 Social Network Software
 * @website: http://www.moosocial.com
 * @author: mooSocial
 * @license: https://moosocial.com/license/
 */
App::uses('BillingAppModel','Billing.Model');
class Currency extends BillingAppModel 
{
    public $validate = array(   
        'name' =>   array(   
            'notEmpty' => array(
                'rule'     => 'notBlank',
                'message'  => 'Name is required'
            ),
        ),     
        'currency_code' => array(
            'rule' => array('notBlank'),
            'message'  => 'Code is not valid'
        ),
        'symbol' => array(
            'rule' => array('notBlank'),
            'message'  => 'Symbol is not valid'
        ),
    );
    public function getActiveCurrency(){
       $data = $this->find('all', array('conditions' => array('is_active'=> 1)));
       $result = array();
       foreach ($data as $currency){
           $result[$currency['Currency']['currency_code']] = $currency['Currency']['currency_code'];
       }
       return $result;
    }
}
