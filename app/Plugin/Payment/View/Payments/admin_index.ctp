<?php

$helper = MooCore::getInstance()->getHelper('Payment_Payment');
?>
<?php

echo $this->Html->script(array('admin/layout/scripts/compare.js?'.Configure::read('core.version')), array('inline' => false));
echo $this->Html->css(array('jquery-ui', 'footable.core.min'), null, array('inline' => false));
echo $this->Html->script(array('jquery-ui', 'footable'), array('inline' => false));
$this->Html->addCrumb(__d('livestream','Plugins Manager'), '/admin/plugins');
$this->Html->addCrumb(__d('livestream', 'Payments'), '/admin/payment/payments');
$this->Html->addCrumb(__d('livestream', 'Payments'), '/admin/payment/payments');

$this->startIfEmpty('sidebar-menu');
echo $this->element('admin/adminnav', array("cmenu" => "Payment"));
$this->end();

$this->Paginator->options(array('url' => $passedArgs));
?>
<style>
    <!--
    .dataTables_filter span
    {
        padding: 5px;
    }
    .dataTables_filter .form-control
    {
        margin-top: 7px !important;
    }
    -->
</style>
<?php echo$this->Moo->renderMenu('Payment', 'Payments');?>
<div class="portlet-body">
    <div class="table-toolbar">

        <form style="padding: 14px;"  method="post" action="<?php echo $this->base.'/admin/payment/payments';?>" class="form-inline">
            <div class="form-group">
                <label><?php echo __d('payment','Status');?></label>
                <?php echo $this->Form->select('status', $arr_status, array('class'=>'form-control','value'=> $status_selected,'empty'=>true)); ?>
            </div>
            <button class="btn btn-gray" id="sample_editable_1_new" type="submit">
		<?php echo __d('payment','Search');?>
            </button>
        </form>
    </div>
    <table class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo __d('payment','ID'); ?></th>
                <th><?php echo __d('payment','Price'); ?></th>
                <th><?php echo __d('payment','Currency'); ?></th>
                <th><?php echo __d('payment','Method'); ?></th>
                <th><?php echo __d('payment','User payment'); ?></th>
                <th><?php echo __d('payment','User create'); ?></th>
                <th><?php echo __d('payment','Transaction'); ?></th>
                <th><?php echo __d('payment','Status'); ?></th>
                <th><?php echo __d('payment','Created'); ?></th>
                <th><?php echo __d('payment','Updated'); ?></th>
                <th><?php echo __d('payment','Action'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($payments)):?>
            <?php foreach ($payments as $payment): ?>
            <tr>
                <td><?php echo $payment['PaymentInfo']['id'] ?></td>
                <td><?php echo $payment['PaymentInfo']['price'] ?></td>
                <td><?php echo $payment['PaymentInfo']['currency']?></td>
                <td><?php echo $payment['PaymentInfo']['method'] == PAYMENT_METHOD_PAYPAL? __d('payment','Paypal') : __d('payment','ATM') ?></td>
                <td><a href="<?php echo $this->request->base ?>/users/view/<?php echo $payment['PaymentInfo']['user_id'] ?>"><?php echo $payment['User']['name'] ?></a></td>
                <td><a href="<?php echo $this->request->base ?>/users/view/<?php echo $payment['PaymentInfo']['user_create'] ?>"><?php echo $helper->getUserName($payment['PaymentInfo']['user_create']); ?></a></td>
                <td><?php echo $payment['PaymentInfo']['transaction_id'] ?></td>
                <td>
                    <?php
                        if($payment['PaymentInfo']['status'] == PAYMENT_STATUS_PENDING) { $class_btn = 'btn-success' ;$text =  __d('payment','Pending');}
                        if($payment['PaymentInfo']['status'] == PAYMENT_STATUS_APPROVE) {$class_btn = 'btn-danger' ;$text =  __d('payment','Approved');}
                        if($payment['PaymentInfo']['status'] == PAYMENT_STATUS_CANCEL) {$class_btn = 'btn-warning' ;$text = __d('payment','Cancelled');}
                    ?>
                    <button type="button" class="btn <?php echo $class_btn ?>"><?php echo $text ?></button>
                </td>
                <td><?php echo $this->Moo->getTime($payment['PaymentInfo']['created']);?></td>
                <td><?php echo $this->Moo->getTime($payment['PaymentInfo']['modified']);?></td>
                <td>
                    <?php if($payment['PaymentInfo']['status'] == PAYMENT_STATUS_PENDING): ?>
                    <a class="btn btn-gray" href="<?php echo $this->request->base?>/admin/payment/payments/approve/<?php echo $payment['PaymentInfo']['id']  ?>" title="<?php echo __d('payment','Approve') ?>"><span class="glyphicon glyphicon-check"></span></a><br>
                    <a class="btn btn-gray" href="<?php echo $this->request->base?>/admin/payment/payments/cancel/<?php echo $payment['PaymentInfo']['id']  ?>" title="<?php echo __d('payment','Cancel') ?>"><span class="glyphicon glyphicon-share"></span></a>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach ?>
            <?php else:?>
            <tr>
                <td colspan="11">
                    <?php echo __d('payment','No payments found');?>
                </td>
            </tr>
            <?php endif;?>
        </tbody>
    </table>

    <div class="pagination">
        <?php echo $this->Paginator->first(__d('payment','First'));?>&nbsp;
        <?php echo $this->Paginator->hasPage(2) ? $this->Paginator->prev(__d('payment','Prev')) : '';?>&nbsp;
		<?php echo $this->Paginator->numbers();?>&nbsp;
		<?php echo $this->Paginator->hasPage(2) ?  $this->Paginator->next(__d('payment','Next')) : '';?>&nbsp;
		<?php echo $this->Paginator->last(__d('payment','Last'));?>
    </div>

</div>
<script>
<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    function changeVisiable(id, e)
    {
        var value = 0;
        if ($(e).hasClass('poll_no'))
        {
            value = 1;
        }
        $(e).attr('class', '');
        if (value)
        {
            $(e).addClass('poll_yes');
            $(e).attr('title', '<?php echo __d('poll','yes');?>');
        } else
        {
            $(e).addClass('poll_no');
            $(e).attr('title', '<?php echo __d('poll','no');?>');
        }

        $.post("<?php echo $this->request->base?>/admin/poll/polls/visiable", {'id': id, 'value': value}, function (data) {

        });
    }
    $(document).on('hidden.bs.modal', function (e) {
        $(e.target).removeData('bs.modal');
    });
<?php echo $this->Html->scriptEnd(); ?>
</script>