<?php 
$helper = MooCore::getInstance()->getHelper('Payment_Payment');
?>
<div class="bar-content">
    <div class="content_center">
        <div class="mo_breadcrumb">
            <h1><?php echo __d('payment','Payment Info');?></h1>
            <a class="button button-action topButton button-mobi-top" href="<?php echo $this->request->base?>/payments/create" data-target="#createOrderStep1Modal" data-toggle="modal" data-backdrop="static" title="<?php echo __d('payment','Create New Payment') ?>"><?php echo __d('payment','Create New Payment') ?></a>
        </div>
         <div class="form-actions form-group">
            <a href="<?php echo $this->request->base ?>/payment/payments/index/status:1" class="btn <?php echo $status == '1' ? 'btn-danger' : 'btn-success' ?> btn-filter"><?php echo __d('payment','Approved') ?></a>
            <a href="<?php echo $this->request->base ?>/payment/payments/index/status:0" class="btn <?php echo $status == '0' ? 'btn-danger' : 'btn-success' ?> btn-filter"><?php echo __d('payment','Not Approved') ?></a>
        </div>
        <div class="table-responsive">          
            <table class="table">
                <thead>
                    <tr>
                        <th>Price</th>
                        <th>Currency</th>
                        <th>Method</th>
                        <th>User</th>
                        <th>User Create</th>
                        <th>Transaction</th>
                        <th>Status</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($payments)): ?>
                    <?php foreach ($payments as $payment): ?>
                    <tr>
                        <td><?php echo $payment['PaymentInfo']['price'] ?></td>
                        <td><?php echo $payment['PaymentInfo']['currency']?></td>
                        <td><?php echo $payment['PaymentInfo']['method'] == PAYMENT_METHOD_PAYPAL? __d('payment','Paypal') : __d('payment','ATM') ?></td>
                        <td><a href="<?php echo $this->request->base ?>/users/view/<?php echo $payment['PaymentInfo']['user_id'] ?>"><?php echo $payment['User']['name'] ?></a></td>
                        <td><a href="<?php echo $this->request->base ?>/users/view/<?php echo $payment['PaymentInfo']['user_create'] ?>"><?php echo $helper->getUserName($payment['PaymentInfo']['user_create']); ?></a></td>
                        <td><?php echo $payment['PaymentInfo']['transaction_id'] ?></td>
                        <td>
                            <?php
                                if($payment['PaymentInfo']['status'] == PAYMENT_STATUS_PENDING) { $class_btn = 'btn-success' ;$text =  __d('payment','Pending');}
                                if($payment['PaymentInfo']['status'] == PAYMENT_STATUS_APPROVE) {$class_btn = 'btn-danger' ;$text =  __d('payment','Approved');}
                                if($payment['PaymentInfo']['status'] == PAYMENT_STATUS_CANCEL) {$class_btn = 'btn-warning' ;$text = __d('payment','Cancelled');}
                            ?>
                            <button type="button" class="btn <?php echo $class_btn ?>"><?php echo $text ?></button>
                        </td>
                        <td><?php echo $this->Moo->getTime($payment['PaymentInfo']['created']);?></td>
                        <td><?php echo $this->Moo->getTime($payment['PaymentInfo']['modified']);?></td>
                        <td>
                            <a href="<?php echo $this->request->base?>/payments/create/<?php echo $payment['PaymentInfo']['id']  ?>" data-target="#createOrderStep1Modal" data-toggle="modal" data-backdrop="static" title="<?php echo __d('payment','Edit') ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            
                        </td>
                    </tr>
                <?php endforeach; ?>
                    <?php else: ?>
                    <tr>
                        <td colspan="14"><?php echo __d('payment','No payment found') ?></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <div class="pagination">
                <?php echo $this->Paginator->first(__d('livestream','First'));?>&nbsp;
                <?php echo $this->Paginator->hasPage(2) ? $this->Paginator->prev(__d('livestream','Prev')) : '';?>&nbsp;
		<?php echo $this->Paginator->numbers();?>&nbsp;
		<?php echo $this->Paginator->hasPage(2) ?  $this->Paginator->next(__d('livestream','Next')) : '';?>&nbsp;
		<?php echo $this->Paginator->last(__d('livestream','Last'));?>
            </div>
        </div>

    </div>
</div>
<?php $this->Html->scriptStart(array('inline' => false, 'domReady' => true, 'requires'=>array('jquery','mooOrder'), 'object' => array('$', 'mooOrder'))); ?>
mooOrder.initViewOrder();
<?php $this->Html->scriptEnd(); ?> 
<style>
    .table-responsive table thead tr th{
        background-color: #43abde !important;
    }
</style>