<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <?php if (!$bIsEdit) : ?>
        <h4 class="modal-title"><?php echo __d('payment','Add New Payment');?></h4>
    <?php else: ?>
        <h4 class="modal-title"><?php echo __d('payment','Edit Payment');?></h4>
    <?php endif;?>
</div>
<div class="modal-body">
    <?php if($permisson): ?>
    <form id="createFormPayment" class="form-horizontal" role="form">
        <?php echo $this->Form->hidden('id', array('value' => $payment['PaymentInfo']['id'])); ?>
        <div class="form-body">
             <div class="form-group">
                <label class="control-label"><?php echo __d('payment','Price');?></label>
                    <?php echo $this->Form->text('price', array('placeholder' => __d('pakage','Enter numberic'), 'class' => 'form-control', 'value' => $payment['PaymentInfo']['price'])); ?>
            </div>
            <div class="form-group">
                <label class="control-label"><?php echo __d('payment','Currency');?></label>
                    <?php echo $this->Form->select('currency', $arr_currencies, array('class'=>'form-control','value'=> $payment['PaymentInfo']['currency'],'empty'=>false)); ?>
            </div>
            <div class="form-group">
                <label class="control-label"><?php echo __d('payment','Method');?></label>
                    <?php echo $this->Form->select('method', $arr_methods, array('class'=>'form-control','value'=> $payment['PaymentInfo']['method'],'empty'=>false)); ?>
            </div>
            <div id="set_transaction_id" style="display: none">
                <div class="form-group">
                    <label class="control-label"><?php echo __d('payment','Transaction ID');?></label>
                            <?php echo $this->Form->text('transaction_id', array('placeholder' => __d('payment', 'Transaction ID'), 'class' => 'form-control','value'=> $payment['PaymentInfo']['transaction_id'])); ?>
                </div>
            </div>
            
        </div>
    </form>
    <div class="alert alert-danger error-message" style="display:none;margin-top:10px;">
    </div>
    <?php else: ?>
    <div class="alert alert-danger error-message" style="margin-top:10px;">
        <?php echo $message; ?>
    </div>
    <?php endif; ?>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal"><?php echo  __d('paymnet','Close') ?></button>
    <?php if($permisson): ?>
    <a href="#" id="createButtonPayment" class="btn btn-action"><?php echo  __d('paymnet','Save') ?></a>
    <?php endif; ?>
</div>

<script>
    $(document).ready(function () {
        $('#createButtonPayment').unbind('click').click(function () {
            $.ajax({
                type: "POST",
                cache: false,
                url: '<?php echo  $this->request->base ?>/payments/save',
                dataType: "json",
                data: $("#createFormPayment").serialize(),
                success: function (data)
                {
                    if (data.result) {
                        window.location.reload(true);
                    } else {
                        $('.error-message').show();
                        $('.error-message').html(data.message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    console.log(thrownError);
                }
            });
        });

        $("#method").change(function () {
            var value = $(this).val();
            if (value == '<?php echo PAYMENT_METHOD_PAYPAL ?>') {
                $('#set_transaction_id').show();
            } else
            {
                $('#set_transaction_id').hide();
            }
        });
    });
    function toggleField()
    {
        $('.opt_field').toggle();
    }
</script>