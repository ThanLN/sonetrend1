<?php

App::uses('AppHelper', 'View/Helper');

class PaymentHelper extends AppHelper {

    public function getUserName($user_id) {
        $userModel = MooCore::getInstance()->getModel('User');
        $user = $userModel->findById($user_id);
        
        if ($user > 0)
            return $user['User']['name'];
        return FALSE;
    }

}
