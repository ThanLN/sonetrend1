<?php
__d('payment','Payment Enabled');
__d('payment','Item per pages');
__d('payment','Allow sales to create pakage');

echo $this->Html->css(array('jquery-ui', 'footable.core.min'), null, array('inline' => false));
echo $this->Html->script(array('jquery-ui', 'footable'), array('inline' => false));

$this->Html->addCrumb(__d('payment','Plugins Manager'), '/admin/plugins');
$this->Html->addCrumb(__d('payment', 'Payments'), '/admin/payment/payments');
$this->Html->addCrumb(__d('payment','Settings'), array('controller' => 'payment_settings', 'action' => 'admin_index'));
    $this->startIfEmpty('sidebar-menu');
    echo $this->element('admin/adminnav', array('cmenu' => 'Payment'));
    $this->end();
?>
<div class="portlet-body form">
    <div class=" portlet-tabs">
        <div class="tabbable tabbable-custom boxless tabbable-reversed">
            <?php echo $this->Moo->renderMenu('Payment', __d('payment','Settings'));?>
            <div class="row" style="padding-top: 10px;">
                <div class="col-md-12">
                    <div class="tab-content">
                        <?php if($cuser['Role']['is_admin']): ?>
                        <div class="tab-pane active" id="portlet_tab1">
                            <?php echo $this->element('admin/setting');?>
                        </div>
                        <?php else: ?>
                        <div class="tab-pane active" id="portlet_tab1">
                            <?php echo __d('payment','You cant not access setting') ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>