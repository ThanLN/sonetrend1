<?php

class PaymentsController extends PaymentAppController {

    public $components = array('Paginator');

    public function admin_index() {
        $this->set('title_for_layout', __d('payment', 'Payment Info'));
        $this->loadModel('Payment.PaymentInfo');
        $this->loadModel('Billing.Currency');
        $this->Paginator->settings = array(
            'limit' => Configure::read('Payment.payment_item_per_pages'),
            'order' => array(
                'PaymentInfo.id' => 'DESC'
            )
        );

        $cond = array();
        $passedArgs = array();
        //
        $status_selected = 0;
        if (!empty($this->request->data['status'])) {
            $cond['PaymentInfo.status'] = $this->request->data['status'];
            $passedArgs['status'] = $this->request->data['status'];
            $status_selected = $this->request->data['status'];
        }
        //
        $payments = $this->Paginator->paginate('PaymentInfo', $cond);
        $arr_status = array();
        $arr_status[PAYMENT_STATUS_PENDING] = __d('payment', 'Pending');
        $arr_status[PAYMENT_STATUS_APPROVE] = __d('payment', 'Approved');
        $arr_status[PAYMENT_STATUS_CANCEL] = __d('payment', 'Cancelled');
        $arr_currencies = $this->Currency->getActiveCurrency();
        $arr_methods = array(PAYMENT_METHOD_ATM => __d('payment', 'ATM'), PAYMENT_METHOD_PAYPAL => __d('payment', 'PayPal'));

        $this->set('payments', $payments);
        $this->set('passedArgs', $passedArgs);
        $this->set('arr_status', $arr_status);
        $this->set('arr_currencies', $arr_currencies);
        $this->set('arr_methods', $arr_methods);
        $this->set('status_selected', $status_selected);
    }

    public function index() {
        $this->set('title_for_layout', __d('payment', 'Payment Infos'));
        $this->loadModel('Payment.PaymentInfo');
        $this->loadModel('User');

        $this->Paginator->settings = array(
            'limit' => Configure::read('Payment.payment_item_per_pages'),
            'order' => array(
                'PaymentInfo.id' => 'DESC'
            )
        );

        $viewer = MooCore::getInstance()->getViewer();
        $cond = array();
        $passedArgs = array();
        if ($viewer['Role']['is_admin']) {
            
        } else if ($viewer['Role']['is_mod']) {
            $list_ids = array();
            $ids = $this->User->getUsersByCondition(array('User.user_create' => $viewer['User']['id']));
            foreach ($ids as $item) {
                array_push($list_ids, $item['User']['id']);
            }
            $cond['PaymentInfo.user_id IN'] = $list_ids;
            $passedArgs['user_id'] = $list_ids;
        } else {
            $cond['PaymentInfo.user_id'] = $viewer['User']['id'];
            $passedArgs['user_id'] = $viewer['User']['id'];
        }
        $status = -1;
        if (@$this->request->named['status'] == '0') {
            $status = 0;
            $cond['PaymentInfo.status'] = $status;
            $passedArgs['PaymentInfo.status'] = $status;
        }
        if (@$this->request->named['status'] == '1') {
            $status = 1;
            $cond['PaymentInfo.status'] = $status;
            $passedArgs['PaymentInfo.status'] = $status;
        }
        $payments = $this->Paginator->paginate('PaymentInfo', $cond);
        $this->set('payments', $payments);
        $this->set('status', $status);
        $this->set('passedArgs', $passedArgs);
    }

    public function create($id = NULL) {
        $this->loadModel('Payment.PaymentInfo');
        $this->loadModel('Billing.Currency');
        $bIsEdit = false;
        $permisson = true;
        $message = '';
        if (!empty($id)) {
            $payment = $this->PaymentInfo->findById($id);
            if ($payment['PaymentInfo']['status'] != PAYMENT_STATUS_PENDING) {
                $permisson = false;
                $message = __d('payment', 'You cant not edit this payment');
            }
            $bIsEdit = true;
        } else {
            $payment = $this->PaymentInfo->initFields();
        }

        $arr_currencies = $this->Currency->getActiveCurrency();
        $arr_methods = array(PAYMENT_METHOD_ATM => __d('payment', 'ATM'), PAYMENT_METHOD_PAYPAL => __d('payment', 'PayPal'));
        
        $this->set('arr_currencies', $arr_currencies);
        $this->set('arr_methods', $arr_methods);
        $this->set('permisson', $permisson);
        $this->set('message', $message);
        $this->set('payment', $payment);
        $this->set('bIsEdit', $bIsEdit);
    }

    public function save() {
        $this->loadModel('Payment.PaymentInfo');
        $this->loadModel('Notification');
        $this->loadModel('User');
        $this->autoRender = false;
        $bIsEdit = false;
        $data = $this->request->data;

        if (!empty($data['id'])) {
            $bIsEdit = true;
            $this->PaymentInfo->id = $data['id'];
        }
        $data['user_id'] = $this->Auth->user('id');
        $data['user_create'] = $this->Auth->user('id');
        $data['status'] = PAYMENT_STATUS_PENDING;

        $this->PaymentInfo->set($data);
        
        $this->_validateData($this->PaymentInfo);

        $this->PaymentInfo->save();

        $this->Session->setFlash(__d('payment', 'Payment has been successfully saved'), 'default', array('class' => 'Metronic-alerts alert alert-success fade in'));
        //send notification to admin 
        $user_send = MooCore::getInstance()->getViewer(true);
        $user_recived = $this->User->getAdminAndUserCreate($user_send);
        
        foreach ($user_recived as $user){
            $arr_text = array('sender_id' => $user_send,
            'user_id' => $user['User']['id'],
            'text' => __d('payment','create a payment order'),
            'url' => '/admin/payment/payments',
            'plugin' => 'Payment',
            'action' => 'create_payment',
            );
            $this->Notification->set($arr_text);
            $this->Notification->save();
            $this->Notification->clear();
        }
        
        $response['result'] = 1;
        echo json_encode($response);
    }

    public function admin_approve($id) {
        $this->loadModel('Payment.PaymentInfo');
        $this->autoRender = false;
        $this->_checkPermission(array('super_admin' => 1));
        $this->_checkPermission(array('confirm' => true));
        $payment = $this->PaymentInfo->findById($id);
        if ($payment) {
            $data['id'] = $payment['PaymentInfo']['id'];
            $data['id'] = $payment['PaymentInfo']['id'];
            $data['status'] = PAYMENT_STATUS_APPROVE;
            $this->PaymentInfo->save($data);
            $this->Session->setFlash(__d('payment', 'Payment have been approved'),'default',array('class' => 'Metronic-alerts alert alert-success fade in'));
        }
        
        $this->redirect($this->referer());
    }
     public function admin_cancel($id) {
        $this->loadModel('Payment.PaymentInfo');
        $this->autoRender = false;
        $this->_checkPermission(array('super_admin' => 1));
        $this->_checkPermission(array('confirm' => true));
        $payment = $this->PaymentInfo->findById($id);
        if ($payment) {
            $data['id'] = $payment['PaymentInfo']['id'];
            $data['id'] = $payment['PaymentInfo']['id'];
            $data['status'] = PAYMENT_STATUS_CANCEL;
            $this->PaymentInfo->save($data);
            $this->Session->setFlash(__d('payment', 'Payment have been cancelled'),'default',array('class' => 'Metronic-alerts alert alert-success fade in'));
        }
        
        $this->redirect($this->referer());
    }
}
