<?php 
class PaymentSettingsController extends PaymentAppController{
    public $components = array('QuickSettings');

    public function admin_index($id = null) {
        $this->set('title_for_layout', __d('payment', 'Settings'));
        $cuser = $this->_getUser();
        if ($cuser['Role']['is_admin']) {
            $this->QuickSettings->run($this, array("Payment"), $id);
            if (CakeSession::check('Message.flash')) {
                $menuModel = MooCore::getInstance()->getModel('Menu.CoreMenuItem');
                $menu = $menuModel->findByUrl('/payments');
                if ($menu) {
                    $menuModel->id = $menu['CoreMenuItem']['id'];
                    $menuModel->save(array('is_active' => Configure::read('Payment.payment_enabled')));
                }
                Cache::clearGroup('menu', 'menu');
            }
        }
    }
}