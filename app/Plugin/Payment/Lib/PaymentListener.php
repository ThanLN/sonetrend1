<?php

/**
 * mooSocial - The Web 2.0 Social Network Software
 * @website: http://www.moosocial.com
 */
App::uses('CakeEventListener', 'Event');

//class PakageListener implements CakeEventListener {
//}

class PaymentListener implements CakeEventListener
{
    public function implementedEvents() {
        return array(
            'Controller.Search.search' => 'search',
            'Controller.Search.suggestion' => 'suggestion',
            'MooView.beforeRender' => 'beforeRender',
        );
    }
    public function beforeRender($event)
    {
        if(Configure::read('Payment.payment_enabled')){
            $e = $event->subject();
            
            $e->Helpers->Html->css(array('Payment.main'), array('block' => 'css'));
        }
    }
}