<?php 
App::uses('MooPlugin','Lib');
class PaymentPlugin implements MooPlugin{
    public function install(){}
    public function uninstall(){}
    public function settingGuide(){}
    public function menu()
    {
        return array(
            __d('payment','Payments') => array('plugin' => 'payment', 'controller' => 'payments', 'action' => 'admin_index'),
            __d('payment','Settings') => array('plugin' => 'payment', 'controller' => 'payment_settings', 'action' => 'admin_index'),
        );
    }
    /*
    Example for version 1.0: This function will be executed when plugin is upgraded (Optional)
    public function callback_1_0(){}
    */
}