<?php

App::uses('PaymentAppModel', 'Payment.Model');

class PaymentInfo extends PaymentAppModel {

    public $actsAs = array();
    public $belongsTo = array('User' => array('className' => 'User', 'foreignKey' => 'user_id'));
//    public $mooFields = array('title','href','plugin','type','url','thumb','privacy');

    public $hasMany = array();
    public $order = 'PaymentInfo.id desc';
    public $validate = array(
        'price' => array(
            'rule' => 'notBlank',
            'message' => 'price is required',
        ),
        'currency' => array(
            'rule' => 'notBlank',
            'message' => 'currency is required',
        ),
        'method' => array(
            'rule' => 'notBlank',
            'message' => 'method is required',
        ),
        'user_id' => array(
            'rule' => 'notBlank',
            'message' => 'user_id is required',
        ),
        'user_create' => array(
            'rule' => 'notBlank',
            'message' => 'user_create is required',
        ),
        
        'status' => array(
            'rule' => 'notBlank',
            'message' => 'status is required',
        ),
    );

    public function getActive($row) {
        if (isset($row['active'])) {
            return $row['active'];
        }
        return false;
    }

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    public function getPaymentInfo($status, $cond) {
        return $this->find($status, array('conditions' => $cond));
    }
    public function countPricePayment($user_id, $time_start = false, $time_end = false){
        $cond = array();
        $cond['PaymentInfo.user_id'] = $user_id;
        $cond['PaymentInfo.status'] = PAYMENT_STATUS_APPROVE;
        if($time_start){
            $cond['PaymentInfo.created >'] = $time_start;
        }
        if($time_end){
            $cond['PaymentInfo.created <'] = $time_end;
        }
        $payments = $this->getPaymentInfo('all', $cond);
        $price = 0;
        foreach ($payments as $payment){
            if($payment['PaymentInfo']['currency'] == 'USD'){
                $price += floatval($payment['PaymentInfo']['price']);
            }else{
                $price += floatval($payment['PaymentInfo']['price'] / 20000);
            }
        }
        return $price;
    }
}
