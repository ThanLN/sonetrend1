<?php
Router::connect('/payments/:action/*', array(
    'plugin' => 'Payment',
    'controller' => 'payments'
));

Router::connect('/payments/*', array(
    'plugin' => 'Payment',
    'controller' => 'payments',
    'action' => 'index'
));
