<?php
require_once(APP . DS . 'Plugin' . DS . 'Payment' . DS .'Config' . DS . 'constants.php');
if(Configure::read('Payment.payment_enabled')){
    App::uses('PaymentListener', 'Payment.Lib');
    CakeEventManager::instance()->attach(new PaymentListener());
    MooSeo::getInstance()->addSitemapEntity("Payment", array(
        'payment'
    ));
}