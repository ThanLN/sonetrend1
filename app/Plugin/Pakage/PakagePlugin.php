<?php

App::uses('MooPlugin', 'Lib');

class PakagePlugin implements MooPlugin {

    public function install() {
        
    }

    public function uninstall() {
        
    }

    public function settingGuide() {
        
    }

    public function menu() {
        return array(
            'Settings' => array('plugin' => 'pakage', 'controller' => 'pakage_settings', 'action' => 'admin_index'),
            'Prices' => array('plugin' => 'pakage', 'controller' => 'prices', 'action' => 'admin_index'),
            'Price Details' => array('plugin' => 'pakage', 'controller' => 'price_details', 'action' => 'admin_index'),
            'Pakages' => array('plugin' => 'pakage', 'controller' => 'pakages', 'action' => 'admin_index'),
            'Pakage Details' => array('plugin' => 'pakage', 'controller' => 'pakage_details', 'action' => 'admin_index'),
        );
    }

    /*
      Example for version 1.0: This function will be executed when plugin is upgraded (Optional)
      public function callback_1_0(){}
     */
}
