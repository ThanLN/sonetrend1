<?php

class PricesController extends PakageAppController {

    public $components = array('Paginator');

    public function admin_index() {
        $this->set('title_for_layout', __d('pakage', 'Price'));
        $this->loadModel('Pakage.Price');
        $this->Paginator->settings = array(
            'limit' => Configure::read('Pakage.pakage_item_per_pages'),
            'order' => array(
                'Price.id' => 'DESC'
            )
        );

        $cond = array();
        $passedArgs = array();

        if (!empty($this->request->data['name'])) {
            $cond['Price.name LIKE'] = '%' . $this->request->data['title'] . '%';
            $this->set('name', $this->request->data['name']);
            $passedArgs['name'] = $this->request->data['name'];
        }

        $prices = $this->Paginator->paginate('Price', $cond);
        $this->set('prices', $prices);
        $this->set('passedArgs', $passedArgs);
    }

    public function admin_create($id = NULL) {
        $cuser = $this->_getUser();
        if ($cuser['Role']['is_super']) {
            $this->_checkPermission(array('super_admin' => 1));
            $bIsEdit = false;
            if (!empty($id)) {
                $price = $this->Price->findById($id);
                $bIsEdit = true;
            } else {
                $price = $this->Price->initFields();
            }
            $this->set('price', $price);
            $this->set('bIsEdit', $bIsEdit);
        }
    }

    public function admin_save() {
        $cuser = $this->_getUser();
        if ($cuser['Role']['is_super']) {
            $this->_checkPermission(array('super_admin' => 1));
            $this->autoRender = false;
            $bIsEdit = false;
            if (!empty($this->data['id'])) {
                $bIsEdit = true;
                $this->Price->id = $this->request->data['id'];
            }

            $this->Price->set($this->request->data);

            $this->_validateData($this->Price);

            $this->Price->save();

            $this->Session->setFlash(__d('pakage', 'Price has been successfully saved'), 'default', array('class' => 'Metronic-alerts alert alert-success fade in'));

            $response['result'] = 1;
            echo json_encode($response);
        } else {
            $response['result'] = 0;
            echo json_encode($response);
        }
    }

    public function index() {
        
    }

}
