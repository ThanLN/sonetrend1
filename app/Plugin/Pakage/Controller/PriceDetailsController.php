<?php 
class PriceDetailsController extends PakageAppController{
    public $components = array('Paginator');

    public function admin_index() {
        $this->set('title_for_layout', __d('pakage', 'Price Details'));
        $this->loadModel('Pakage.PriceDetail');
        $this->loadModel('User');
        $this->Paginator->settings = array(
            'limit' => Configure::read('Pakage.pakage_item_per_pages'),
            'order' => array(
                'PriceDetail.id' => 'DESC'
            )
        );
        $cond = array();
        $passedArgs = array();
        
        $user_role = $this->_getUserRoleId();
        if($user_role == ROLE_SALE){
            $list_uid = $this->User->getListIdUserCreate($this->Auth->user('id'));
            $cond['PriceDetail.user_id'] = $list_uid;
        }

        if (!empty($this->request->data['name'])) {
            $cond['PriceDetail.name LIKE'] = '%' . $this->request->data['title'] . '%';
            $this->set('name', $this->request->data['name']);
            $passedArgs['name'] = $this->request->data['name'];
        }

        $pricedetails = $this->Paginator->paginate('PriceDetail', $cond);
        
        $this->set('pricedetails', $pricedetails);
        $this->set('passedArgs', $passedArgs);
    }
    public function admin_create($id = NULL){
        $this->loadModel('Pakage.Price');
        $sys_price = $this->Price->getSystemPrice();
        $is_error = false;
        $message = NULL;
        if(empty($sys_price)){
            $message = __d('pakage','System dont have default price. Please contact supper admin to create');
            $is_error = true;
        }
        $this->_checkPermission(array('super_admin' => 1));
        $bIsEdit = false;
        if (!empty($id)) {
            $pricedetail = $this->PriceDetail->findById($id);
            $bIsEdit = true;
        } else {
            $pricedetail = $this->PriceDetail->initFields();
        }
        $user_role = $this->_getUserRoleId();
        $cond = array();
        if($user_role == ROLE_SALE){
            $list_uid = $this->User->getListIdUserCreate($this->Auth->user('id'));
            $cond[] = array('User.id'=> $list_uid);
        }
        $cond[] = array("User.role_id" => ROLE_MEMBER);
        $members = $this->User->getUserIdName($cond);
        
        $this->set('sys_price', $sys_price);
        $this->set('is_error', $is_error);
        $this->set('message', $message);
        $this->set('members', $members);
        $this->set('pricedetail', $pricedetail);
        $this->set('bIsEdit', $bIsEdit);
    }
     public function admin_save() {
        $this->_checkPermission(array('super_admin' => 1));
        $this->autoRender = false;
        $bIsEdit = false;
        if (!empty($this->data['id'])) {
            $bIsEdit = true;
            $this->PriceDetail->id = $this->request->data['id'];
        }

        $this->PriceDetail->set($this->request->data);

        $this->_validateData($this->PriceDetail);
        
        $result = $this->PriceDetail->validateBeforCreate($this->request->data);
        if(!$result['pass']){
            $response['result'] = 0;
            $response['message'] = $result['message'];
            echo json_encode($response);
            die();
        }
        
        $this->PriceDetail->save();
               
        $this->Session->setFlash(__d('pakage','Price has been successfully saved'), 'default', array('class' => 'Metronic-alerts alert alert-success fade in'));
        
        $response['result'] = 1;
        echo json_encode($response);
        die();
    }
    public function index() {
        
    }

}