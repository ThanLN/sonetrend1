<?php

App::uses('Widget', 'Controller/Widgets');

class listdetailPakageWidget extends Widget {
    
    public function beforeRender(Controller $controller) {
        $num_item_show = $this->params['num_item_show'];
        $controller->loadModel('Pakage.PakageDetail');
        $controller->loadModel('User');
        $viewer = MooCore::getInstance()->getViewer();
        $cond = array();
        if($viewer['Role']['is_admin']){
            $cond = array();
        }else if ($viewer['Role']['is_mod']){
            $list_ids = array();
            $ids = $controller->User->getUsersByCondition(array('User.user_create' => $viewer['User']['id']));
            foreach ($ids as $item){
                array_push($list_ids, $item['User']['id']);
            }
            $cond['user_id IN'] = $list_ids;
        }else {
            $cond['user_id'] = $viewer['User']['id'];
        }
        $limit = Configure::read('Pakage.pakage_item_per_pages');
        $page = 1;
        if (isset($controller->request->params['named']['page']))
            $page = $controller->request->params['named']['page'];
//        var_dump($controller->request->params);die;
        $total = $controller->PakageDetail->find('count',array('conditions' => $cond));
        $pakagedetails = $controller->PakageDetail->find('all',array('conditions' => $cond,'limit' => $limit,'page' => $page));
//        var_dump($pakagedetails);die;
        if ($controller->request->is('androidApp') || $controller->request->is('iosApp')) {
            $controller->set('pakagedetails', $pakagedetails);
        } else {
            $this->setData('pakagedetails', $pakagedetails);
        }
    }

}
