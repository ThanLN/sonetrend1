<?php 
class PakagesController extends PakageAppController{
    
    public $components = array('Paginator');

    public function admin_index() {
        $this->set('title_for_layout', __d('pakage', 'Pakages'));
        $this->loadModel('Pakage.Pakage');
        $this->Paginator->settings = array(
            'limit' => Configure::read('Pakage.pakage_item_per_pages'),
            'order' => array(
                'Pakage.id' => 'DESC'
            )
        );

        $cond = array();
        $passedArgs = array();

        if (!empty($this->request->data['name'])) {
            $cond['Pakage.name LIKE'] = '%' . $this->request->data['title'] . '%';
            $this->set('name', $this->request->data['name']);
            $passedArgs['name'] = $this->request->data['name'];
        }

        $pakages = $this->Paginator->paginate('Pakage', $cond);
        $this->set('pakages', $pakages);
        $this->set('passedArgs', $passedArgs);
    }
    public function admin_create($id = NULL){
        $this->_checkPermission(array('super_admin' => 1));
        $uid = $this->Auth->user('id');
        $bIsEdit = false;
        if (!empty($id)) {
            $pakage = $this->Pakage->findById($id);
            $bIsEdit = true;
        } else {
            $pakage = $this->Pakage->initFields();
        }
        $this->set('uid',$uid);
        $this->set('pakage', $pakage);
        $this->set('bIsEdit', $bIsEdit);
    }
     public function admin_save() {
        $this->loadModel('Pakage.Pakage');
        $this->_checkPermission(array('super_admin' => 1));
        $this->autoRender = false;
        $bIsEdit = false;
        if (!empty($this->data['id'])) {
            $bIsEdit = true;
            $this->Pakage->id = $this->request->data['id'];
        }

        $this->Pakage->set($this->request->data);

        $this->_validateData($this->Pakage);

        $this->Pakage->save();
               
        $this->Session->setFlash(__d('pakage','Price has been successfully saved'), 'default', array('class' => 'Metronic-alerts alert alert-success fade in'));
        
        $response['result'] = 1;
        echo json_encode($response);
    }
    public function index() {
        
    }
}