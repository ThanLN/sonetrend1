<?php

class PakageDetailsController extends PakageAppController {

    public $components = array('Paginator');

    public function admin_index() {
        $this->set('title_for_layout', __d('pakage', 'Pakage Details'));
        $this->loadModel('Pakage.PakageDetail');
        $this->Paginator->settings = array(
            'limit' => Configure::read('Pakage.pakage_item_per_pages'),
            'order' => array(
                'PakageDetail.id' => 'DESC'
            )
        );

        $cond = array();
        $passedArgs = array();

        if (!empty($this->request->data['name'])) {
            $cond['PakageDetail.name LIKE'] = '%' . $this->request->data['title'] . '%';
            $this->set('name', $this->request->data['name']);
            $passedArgs['name'] = $this->request->data['name'];
        }

        $pakagedetails = $this->Paginator->paginate('PakageDetail', $cond);

        $this->set('pakagedetails', $pakagedetails);
        $this->set('passedArgs', $passedArgs);
    }
    public function admin_create_detail($pakage_id) {
        $this->loadModel('Pakage.Pakage');
        $this->loadModel('User');
        $pakage = $this->Pakage->findById($pakage_id);
        $is_error = false;
        $message = NULL;
        if (empty($pakage)) {
            $message = __d('pakage', 'Pakage is not found');
            $is_error = true;
        }
        $this->_checkPermission(array('super_admin' => 1));
        $bIsEdit = false;
        
        $pakagedetail = $this->PakageDetail->initFields();
        $user_role = $this->_getUserRoleId();
        $cond = array();
        if($user_role == ROLE_SALE){
            $list_uid = $this->User->getListIdUserCreate($this->Auth->user('id'));
            $cond[] = array('User.id'=> $list_uid);
        }
        
        $cond[] = array("User.role_id" => ROLE_MEMBER);
        $members = $this->User->getUserIdName($cond);
        
        $this->set('pakage', $pakage);
        $this->set('is_error', $is_error);
        $this->set('message', $message);
        $this->set('members', $members);
        $this->set('pakagedetail', $pakagedetail);
        $this->set('bIsEdit', $bIsEdit);
    }
    public function admin_edit($id = NULL) {
        $this->loadModel('Pakage.Pakage');
        $this->loadModel('User');
        $pakagedetail = $this->PakageDetail->findById($id);
        $pakage = $this->Pakage->findById($pakagedetail['PakageDetail']['pakage_id']);
        
        $is_error = false;
        $message = NULL;
       
        $this->_checkPermission(array('super_admin' => 1));
        $bIsEdit = true;
        
        $cond = array();
        $user_role = $this->_getUserRoleId();
        if($user_role == ROLE_SALE){
            $list_uid = $this->User->getListIdUserCreate($this->Auth->user('id'));
            $cond[] = array('User.id'=> $list_uid);
        }
        
        $cond[] = array("User.role_id" => ROLE_MEMBER);
        $members = $this->User->getUserIdName($cond);

        $this->set('pakage', $pakage);
        $this->set('is_error', $is_error);
        $this->set('message', $message);
        $this->set('members', $members);
        $this->set('pakagedetail', $pakagedetail);
        $this->set('bIsEdit', $bIsEdit);
    }

    public function admin_save() {
        $this->_checkPermission(array('super_admin' => 1));
        $this->autoRender = false;
        $bIsEdit = false;
        if (!empty($this->data['id'])) {
            $bIsEdit = true;
            $this->PakageDetail->id = $this->request->data['id'];
        }
//        $this->PakageDetail->set($this->request->data);
//        $this->_validateData($this->PakageDetail);
       
        $result = $this->PakageDetail->validateBeforCreate($this->request->data);
       
        if (!$result['pass']) {
            $response['result'] = 0;
            $response['message'] = $result['message'];
            echo json_encode($response);
            die();
        }
        $data = $this->PakageDetail->prepareData($this->request->data);
        $this->PakageDetail->set($data);
        $this->PakageDetail->save();

        $this->Session->setFlash(__d('pakage', 'Price has been successfully saved'), 'default', array('class' => 'Metronic-alerts alert alert-success fade in'));

        $response['result'] = 1;
        echo json_encode($response);
        die();
    }

    public function index() {
        $this->set('title_for_layout', __d('pakage', 'Pakage Details'));
        $this->loadModel('Pakage.PakageDetail');
        $this->loadModel('User');
        $this->Paginator->settings = array(
            'limit' => Configure::read('Pakage.pakage_item_per_pages'),
            'order' => array(
                'PakageDetail.id' => 'DESC'
            )
        );
        $viewer = MooCore::getInstance()->getViewer();
        $cond = array();
        $passedArgs = array();
        if($viewer['Role']['is_admin']){
            $cond = array();
        }else if ($viewer['Role']['is_mod']){
            $list_ids = array();
            $ids = $this->User->getUsersByCondition(array('User.user_create' => $viewer['User']['id']));
            foreach ($ids as $item){
                array_push($list_ids, $item['User']['id']);
            }
            $cond['PakageDetail.user_id IN'] = $list_ids;
            $passedArgs['user_id'] = $list_ids;
        }else {
            $cond['PakageDetail.user_id'] = $viewer['User']['id'];
        }
        $active = -1;
        if(@$this->request->named['active'] == '0'){
            $active = 0;
            $cond['PakageDetail.active'] = $active;
            $passedArgs['PakageDetail.active'] = $active;
        }
        if(@$this->request->named['active'] == '1'){
            $active = 1;
            $cond['PakageDetail.active'] = $active;
            $passedArgs['active'] = $active;
        }
        $pakagedetails = $this->Paginator->paginate('PakageDetail', $cond);
        $this->set('pakagedetails', $pakagedetails);
        $this->set('passedArgs', $passedArgs);
        $this->set('active', $active);
    }

}
