<script>
    $(document).ready(function () {
        $('#createButton').click(function () {
            disableButton('createButton');
            $.post("<?php echo  $this->request->base ?>/admin/pakage/price_details/save", $("#createForm").serialize(), function (data) {
                enableButton('createButton');
                var json = $.parseJSON(data);

                if (json.result == 1)
                    location.reload();
                else
                {
                    $(".error-message").show();
                    $(".error-message").html('<strong>Error!</strong>' + json.message);
                }
            });

            return false;
        });

//        $('#type').change(function () {
//            MooAjax.post({
//                url: '<?php echo  $this->request->base ?>/admin/categories/load_parent_categories/' + $('#type').val(),
//            }, function (data) {
//                console.log(data);
//                $('#parent_id').html(data);
//            });
//        });

    });

    function toggleField()
    {
        $('.opt_field').toggle();
    }
</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <?php if (!$bIsEdit) : ?>
        <h4 class="modal-title"><?php echo __d('pakage','Add New Pakage');?></h4>
    <?php else: ?>
        <h4 class="modal-title"><?php echo __d('pakage','Edit Pakage');?></h4>
    <?php endif;?>
</div>
<div class="modal-body">
    <?php if(!$is_error): ?>
    <form id="createForm" class="form-horizontal" role="form">
        <?php echo $this->Form->hidden('id', array('value' => $pricedetail['PriceDetail']['id'])); ?>
        <?php echo $this->Form->hidden('price_id', array('value' => $sys_price['Price']['id'])); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Select User');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->select('user_id', $members, array('class'=>'form-control','value'=> $pricedetail['PriceDetail']['user_id'],'empty'=>false)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Max view');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('max_viewer', array('placeholder' => __d('pakage','Enter number'), 'class' => 'form-control', 'value' => $pricedetail['PriceDetail']['max_viewer'])); ?>
                    <p style="color: red"><?php echo __d('pakage','Please input number less than %s',$sys_price['Price']['max_viewer']) ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Max time (hours)');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('max_time', array('placeholder' => __d('pakage','Enter number of hours'), 'class' => 'form-control', 'value' => $pricedetail['PriceDetail']['max_time'])); ?>
                    <p style="color: red"><?php echo __d('pakage','Please input number less than %s',$sys_price['Price']['max_time']) ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Default view (VND)');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('price_view', array('placeholder' => __d('pakage','Enter number'), 'class' => 'form-control', 'value' => $pricedetail['PriceDetail']['price_view'])); ?>
                    <p style="color: red"><?php echo __d('pakage','Please input number bigger than %s',$sys_price['Price']['default_view']) ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Default live');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->textarea('price_live', array('placeholder' => __d('pakage','Enter json'), 'class' => 'form-control', 'value' => $pricedetail['PriceDetail']['price_live'])); ?>
                    <p style="color: red"><?php echo __d('pakage','Please input string like and bigger than this %s',$sys_price['Price']['default_live']) ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Default like');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('price_like', array('placeholder' => __d('pakage','Enter number'), 'class' => 'form-control', 'value' => $pricedetail['PriceDetail']['price_like'])); ?>
                    <p style="color: red"><?php echo __d('pakage','Please input number bigger than %s',$sys_price['Price']['default_like']) ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Default share');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('price_share', array('placeholder' => __d('pakage','Enter number'), 'class' => 'form-control', 'value' => $pricedetail['PriceDetail']['price_share'])); ?>
                    <p style="color: red"><?php echo __d('pakage','Please input number bigger than %s',$sys_price['Price']['default_share']) ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Default comment');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('price_comment', array('placeholder' => __d('pakage','Enter number'), 'class' => 'form-control', 'value' => $pricedetail['PriceDetail']['price_comment'])); ?>
                    <p style="color: red"><?php echo __d('pakage','Please input number bigger than %s',$sys_price['Price']['default_comment']) ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Active');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->select('active', array( 0 => 'No', 1 => 'Yes'),array('value' => $pricedetail['PriceDetail']['active'],'empty' => false,'class' => 'form-control')); ?>
                </div>
            </div>
        </div>
    </form>
    <div class="alert alert-danger error-message" style="display:none;margin-top:10px;">
    </div>
    <?php else: ?>
    <div class="alert alert-danger error-message" style="display:block;margin-top:10px;">
        <?php echo $message ?>
    </div>
    <?php endif;?>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal"><?php echo  __d('pakage','Close') ?></button>
    <a href="#" id="createButton" class="btn btn-action"><?php echo  __d('pakage','Save') ?></a>

</div>