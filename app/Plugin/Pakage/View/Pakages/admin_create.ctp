<script>
    $(document).ready(function () {
        $('#createButton').click(function () {
            disableButton('createButton');
            $.post("<?php echo  $this->request->base ?>/admin/pakage/pakages/save", $("#createForm").serialize(), function (data) {
                enableButton('createButton');
                var json = $.parseJSON(data);

                if (json.result == 1)
                    location.reload();
                else
                {
                    $(".error-message").show();
                    $(".error-message").html('<strong>Error!</strong>' + json.message);
                }
            });

            return false;
        });

//        $('#type').change(function () {
//            MooAjax.post({
//                url: '<?php echo  $this->request->base ?>/admin/categories/load_parent_categories/' + $('#type').val(),
//            }, function (data) {
//                console.log(data);
//                $('#parent_id').html(data);
//            });
//        });

    });

    function toggleField()
    {
        $('.opt_field').toggle();
    }
</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <?php if (!$bIsEdit) : ?>
        <h4 class="modal-title"><?php echo __d('pakage','Add New Pakage');?></h4>
    <?php else: ?>
        <h4 class="modal-title"><?php echo __d('pakage','Edit Pakage');?></h4>
    <?php endif;?>
</div>
<div class="modal-body">
    <form id="createForm" class="form-horizontal" role="form">
        <?php echo $this->Form->hidden('id', array('value' => $pakage['Pakage']['id'])); ?>
        <?php echo $this->Form->hidden('user_create', array('value' => empty($pakage['Pakage']['user_create'])? $uid :$pakage['Pakage']['user_create'] )); ?>
        <div class="form-body">
             <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Name');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('name', array('placeholder' => __d('pakage','Enter text'), 'class' => 'form-control', 'value' => $pakage['Pakage']['name'])); ?>
                </div>            
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Max view');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('max_viewer', array('placeholder' => __d('pakage','Enter number'), 'class' => 'form-control', 'value' => $pakage['Pakage']['max_viewer'])); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Number of hours');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('num_hours', array('placeholder' => __d('pakage','Enter number of hours'), 'class' => 'form-control', 'value' => $pakage['Pakage']['num_hours'])); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Unit');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->select('active', array( 1 => __d('pakage','Week'), 2 => __d('pakage','Month')),array('value' => $pakage['Pakage']['unit'],'empty' => false,'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Default price');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('default_price', array('placeholder' => __d('pakage','Enter default price ($)'), 'class' => 'form-control', 'value' => $pakage['Pakage']['default_price'])); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Active');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->select('active', array( 0 => 'No', 1 => 'Yes'),array('value' => $pakage['Pakage']['active'],'empty' => false,'class' => 'form-control')); ?>
                </div>
            </div>
        </div>
    </form>
    <div class="alert alert-danger error-message" style="display:none;margin-top:10px;">
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal"><?php echo  __d('pakage','Close') ?></button>
    <a href="#" id="createButton" class="btn btn-action"><?php echo  __d('pakage','Save') ?></a>

</div>