<?php
echo $this->Html->css(array('jquery-ui','pickadate', 'footable.core.min'), null, array('inline' => false));
echo $this->Html->script(array('jquery-ui','pickadate/picker', 'pickadate/picker.date','footable'), array('inline' => false));
?>
<?php
echo $this->Html->script(array('admin/layout/scripts/compare.js?'.Configure::read('core.version')), array('inline' => false));
echo $this->Html->css(array('jquery-ui','Pakage.admin', 'footable.core.min'), null, array('inline' => false));
echo $this->Html->script(array('jquery-ui', 'footable'), array('inline' => false));

$this->Html->addCrumb(__d('pakage','Plugins Manager'), '/admin/plugins');
$this->Html->addCrumb(__d('pakage', 'Pakage'), '/admin/pakage/ppakages');
$this->Html->addCrumb(__d('pakage', 'Pakages'), '/admin/pakage/pakages');

$this->startIfEmpty('sidebar-menu');
echo $this->element('admin/adminnav', array("cmenu" => "Pakage"));
$this->end();

$this->Paginator->options(array('url' => $passedArgs));
?>
<style>
    <!--
    .dataTables_filter span
    {
        padding: 5px;
    }
    .dataTables_filter .form-control
    {
        margin-top: 7px !important;
    }
    -->
</style>
<?php echo$this->Moo->renderMenu('Pakage', 'Pakages');?>
<div class="portlet-body">
    <div class="table-toolbar">
        <?php if($cuser['Role']['is_admin']): ?>
        <div class="row">
            <div class="col-md-6">
                <div class="btn-group">
                    <button class="btn btn-gray" data-toggle="modal" data-target="#ajax" href="<?php echo $this->request->base ?>/admin/pakage/pakages/create">
                        <?php echo __d('pakage','Add New Pakage'); ?>
                    </button>
                </div>
            </div>
        </div>
        <?php endif; ?>
    <!--	<form style="padding: 14px;"  method="post" action="<?php echo $this->base.'/admin/pakage/prices';?>" class="form-inline">
              <div class="form-group">
                <label><?php echo __d('pakage','name');?></label>
                <input class="form-control input-medium input-inline" value="<?php if (isset($name)) echo $name;?>" type="text" name="title">
              </div>
              <button class="btn btn-gray" id="sample_editable_1_new" type="submit">
			<?php echo __d('pakage','Search');?>
          </button>
            </form>-->
    </div>
    <table class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo __d('pakage','ID'); ?></th>
                <th><?php echo __d('pakage','Name'); ?></th>				
                <th><?php echo __d('pakage','Max viewer'); ?></th>
                <th><?php echo __d('pakage','Number of hours'); ?></th>
                <th><?php echo __d('pakage','Unit'); ?></th>
                <th><?php echo __d('pakage','Default Price'); ?></th>
                <th><?php echo __d('pakage','Active'); ?></th>
                <th><?php echo __d('pakage','Created'); ?></th>
                <th><?php echo __d('pakage','Updated'); ?></th>
                <th><?php echo __d('pakage','Action'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($pakages)):?>
            <?php foreach ($pakages as $pakage): ?>
            <tr>
                <td>
                    <?php echo $pakage['Pakage']['id'];?>
                </td>	
                <td>
                    <?php echo $pakage['Pakage']['name'];?>
                </td>					
                <td>
                    <?php echo $pakage['Pakage']['max_viewer'];?>
                </td>
                <td>
                    <?php echo $pakage['Pakage']['num_hours'];?>
                </td>
                <td>
                    <?php echo $pakage['Pakage']['unit'] == 1? 'Week':'Month';?>
                </td>
                <td>
                    <?php echo $pakage['Pakage']['default_price'];?>
                </td>
                <td>
                    <?php echo $pakage['Pakage']['active'] == 1? 'Yes':'No';?>
                </td>
                <td><?php echo $this->Moo->getTime($pakage['Pakage']['created']);?></td>
                <td><?php echo $this->Moo->getTime($pakage['Pakage']['modified']);?></td>
                <td>
                    <?php if($cuser['Role']['is_admin']): ?>
                    <button class="btn btn-gray" data-toggle="modal" data-target="#ajax" href="<?php echo $this->request->base ?>/admin/pakage/pakages/create/<?php echo $pakage['Pakage']['id'] ?>">
                        <?php echo __d('pakage','Edit'); ?>
                    </button>
                    <?php endif; ?>
                    <button class="btn btn-gray" data-toggle="modal" data-target="#ajax" href="<?php echo $this->request->base ?>/admin/pakage/pakage_details/create_detail/<?php echo $pakage['Pakage']['id'] ?>">
                        <?php echo __d('pakage','Add User'); ?>
                    </button>
                </td>
            </tr>
            <?php endforeach ?>
            <?php else:?>
            <tr>
                <td colspan="10">
                    <?php echo __d('pakage','No pakage found');?>
                </td>
            </tr>
            <?php endif;?>
        </tbody>
    </table>

    <div class="pagination">
        <?php echo $this->Paginator->first(__d('pakage','First'));?>&nbsp;
        <?php echo $this->Paginator->hasPage(2) ? $this->Paginator->prev(__d('pakage','Prev')) : '';?>&nbsp;
		<?php echo $this->Paginator->numbers();?>&nbsp;
		<?php echo $this->Paginator->hasPage(2) ?  $this->Paginator->next(__d('pakage','Next')) : '';?>&nbsp;
		<?php echo $this->Paginator->last(__d('pakage','Last'));?>
    </div>

</div>
<script>
<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    function changeVisiable(id, e)
    {
        var value = 0;
        if ($(e).hasClass('poll_no'))
        {
            value = 1;
        }
        $(e).attr('class', '');
        if (value)
        {
            $(e).addClass('poll_yes');
            $(e).attr('title', '<?php echo __d('poll','yes');?>');
        } else
        {
            $(e).addClass('poll_no');
            $(e).attr('title', '<?php echo __d('poll','no');?>');
        }

        $.post("<?php echo $this->request->base?>/admin/poll/polls/visiable", {'id': id, 'value': value}, function (data) {

        });
    }
    $(document).on('hidden.bs.modal', function (e) {
        $(e.target).removeData('bs.modal');
    });
<?php echo $this->Html->scriptEnd(); ?>
</script>