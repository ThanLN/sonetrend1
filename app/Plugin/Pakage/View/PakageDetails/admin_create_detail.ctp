
<script>
    $(document).ready(function () {
        $('#createButton').click(function () {
            disableButton('createButton');
            $.post("<?php echo  $this->request->base ?>/admin/pakage/pakage_details/save", $("#createForm").serialize(), function (data) {
                enableButton('createButton');
                var json = $.parseJSON(data);

                if (json.result == 1)
                    location.reload();
                else
                {
                    $(".error-message").show();
                    $(".error-message").html('<strong>Error!</strong>' + json.message);
                }
            });

            return false;
        });
        $('#time_start').pickadate({
            monthsFull: ['<?php echo addslashes(__( 'January'))?>', '<?php echo addslashes(__( 'February'))?>', '<?php echo addslashes(__( 'March'))?>', '<?php echo addslashes(__( 'April'))?>', '<?php echo addslashes(__( 'May'))?>', '<?php echo addslashes(__( 'June'))?>', '<?php echo addslashes(__( 'July'))?>', '<?php echo addslashes(__( 'August'))?>', '<?php echo addslashes(__( 'September'))?>', '<?php echo addslashes(__( 'October'))?>', '<?php echo addslashes(__( 'November'))?>', '<?php echo addslashes(__( 'December'))?>'],
            monthsShort: ['<?php echo addslashes(__( 'Jan'))?>', '<?php echo addslashes(__( 'Feb'))?>', '<?php echo addslashes(__( 'Mar'))?>', '<?php echo addslashes(__( 'Apr'))?>', '<?php echo addslashes(__( 'May'))?>', '<?php echo addslashes(__( 'Jun'))?>', '<?php echo addslashes(__( 'Jul'))?>', '<?php echo addslashes(__( 'Aug'))?>', '<?php echo addslashes(__( 'Sep'))?>', '<?php echo addslashes(__( 'Oct'))?>', '<?php echo addslashes(__( 'Nov'))?>', '<?php echo addslashes(__( 'Dec'))?>'],
            weekdaysFull: ['<?php echo addslashes(__( 'Sunday'))?>', '<?php echo addslashes(__( 'Monday'))?>', '<?php echo addslashes(__( 'Tuesday'))?>', '<?php echo addslashes(__( 'Wednesday'))?>', '<?php echo addslashes(__( 'Thursday'))?>', '<?php echo addslashes(__( 'Friday'))?>', '<?php echo addslashes(__( 'Saturday'))?>'],
            weekdaysShort: ['<?php echo addslashes(__( 'Sun'))?>', '<?php echo addslashes(__( 'Mon'))?>', '<?php echo addslashes(__( 'Tue'))?>', '<?php echo addslashes(__( 'Wed'))?>', '<?php echo addslashes(__( 'Thu'))?>', '<?php echo addslashes(__( 'Fri'))?>', '<?php echo addslashes(__( 'Sat'))?>'],
            today: "<?php echo addslashes(__( 'Today'))?>",
            clear: "<?php echo addslashes(__( 'Clear'))?>",
            close: "<?php echo addslashes(__( 'Close'))?>",
            format: 'yyyy-mm-dd'
        });
//        $('#type').change(function () {
//            MooAjax.post({
//                url: '<?php echo  $this->request->base ?>/admin/categories/load_parent_categories/' + $('#type').val(),
//            }, function (data) {
//                console.log(data);
//                $('#parent_id').html(data);
//            });
//        });

    });

    function toggleField()
    {
        $('.opt_field').toggle();
    }
</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <?php if (!$bIsEdit) : ?>
    <h4 class="modal-title"><?php echo __d('pakage','Add New Pakage For User');?></h4>
    <?php else: ?>
    <h4 class="modal-title"><?php echo __d('pakage','Edit Pakage For User');?></h4>
    <?php endif;?>
</div>
<div class="modal-body">
    <?php if(!$is_error): ?>
    <form id="createForm" class="form-horizontal" role="form">
        <?php echo $this->Form->hidden('id', array('value' => $pakagedetail['PakageDetail']['id'])); ?>
        <?php echo $this->Form->hidden('pakage_id', array('value' => $pakage['Pakage']['id'])); ?>
        <?php echo $this->Form->hidden('pakage_type', array('value' => $pakage['Pakage']['unit'])); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Select User');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->select('user_id', $members, array('class'=>'form-control','value'=> $pakagedetail['PakageDetail']['user_id'],'empty'=>false)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Name');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('name', array('placeholder' => __d('pakage','Enter name'), 'class' => 'form-control', 'value' => $pakagedetail['PakageDetail']['name'])); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Price');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('price', array('placeholder' => __d('pakage','Enter number'), 'class' => 'form-control', 'value' => $pakagedetail['PakageDetail']['price'])); ?>
                    <p style="color: red"><?php echo __d('pakage','Please input number bigger than %s',$pakage['Pakage']['default_price']) ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Date Start');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->text('time_start', array('placeholder' => __d('pakage','Enter time start'), 'class' => 'form-control', 'value' => $pakagedetail['PakageDetail']['time_start'])); ?>
                </div>
            </div>
            <!--            <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo __d('pakage','Time End');?></label>
                            <div class="col-md-9">
                    <?php echo $this->Form->text('time_end', array('placeholder' => __d('pakage','Enter number'), 'class' => 'form-control', 'value' => $pakagedetail['PakageDetail']['time_end'])); ?>
                            </div>
                        </div>-->
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Paymnent');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->select('is_payment', array( 0 => 'No', 1 => 'Yes'),array('value' => $pakagedetail['PakageDetail']['is_payment'],'empty' => false,'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __d('pakage','Active');?></label>
                <div class="col-md-9">
                    <?php echo $this->Form->select('active', array( 0 => 'No', 1 => 'Yes'),array('value' => $pakagedetail['PakageDetail']['active'],'empty' => false,'class' => 'form-control')); ?>
                </div>
            </div>
        </div>
    </form>
    <div class="alert alert-danger error-message" style="display:none;margin-top:10px;">
    </div>
    <?php else: ?>
    <div class="alert alert-danger error-message" style="display:block;margin-top:10px;">
        <?php echo $message ?>
    </div>
    <?php endif;?>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal"><?php echo  __d('pakage','Close') ?></button>
    <a href="#" id="createButton" class="btn btn-action"><?php echo  __d('pakage','Save') ?></a>

</div>
