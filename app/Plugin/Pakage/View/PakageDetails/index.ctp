<?php

$this->Paginator->options(array('url' => $passedArgs));
?>
<div class="box2">
    <h3><?php echo __d('pakage', 'Pakage Details'); ?></h3>
    <div class="panel panel-default">
        <div class="panel-body">
            <div id="seach_content">
                <div class="pull">
                    <div class="btn-group">
                        <a href="<?php echo $this->request->base ?>/pakage/pakage_details/index/active:1" class="btn <?php echo $active == '1' ? 'btn-danger' : 'btn-success' ?> btn-filter">Active</a>
                        <a href="<?php echo $this->request->base ?>/pakage/pakage_details/index/active:0" class="btn <?php echo $active == '0' ? 'btn-danger' : 'btn-success' ?> btn-filter">Unactive</a>
                    </div>
                </div>
            </div>
            <div class="table-container">
                <?php if(count($pakagedetails)>0): ?>
                <table class="table table-filter">
                    <tbody>
                        <?php foreach ($pakagedetails as $item): ?>
                        <tr data-status="pagado">
                            <td>
                                <div class="ckbox">
                                    <!--<input type="checkbox" id="checkbox1">-->
                                    <label for="checkbox1"></label>
                                </div>
                            </td>
                            <td>
                                <a href="javascript:;" class="star">
                                    <i class="glyphicon glyphicon-star"></i>
                                </a>
                            </td>
                            <td>
                                <div class="media">
                                    <div class="media-body">
                                        <span class="media-meta pull-right"> <?php echo __d('pakage','From') ?> <?php echo date('Y-m-d',$item['PakageDetail']['time_start']) ?> <?php echo __d('pakage','To') ?> <?php echo date('Y-m-d',$item['PakageDetail']['time_end']) ?></span>
                                        <h4 class="title">
                                            <?php echo $item['User']['name'] ?>
                                            <span class="pull-right pagado">(<?php echo $item['PakageDetail']['is_payment'] ? __d('pakage','Payed') : __d('pakage','Not pay') ?>)</span>
                                            <span class="pull-right pagado">(<?php echo $item['PakageDetail']['active'] ? __d('pakage','Active') : __d('pakage','Unactive') ?>)</span>
                                        </h4>
                                        <p class="summary"><?php echo __d('pakage','Time exits %s minutes', intval($item['PakageDetail']['time_exist']/60)); ?></p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="pagination">
                <?php echo $this->Paginator->first(__d('pakage','First'));?>&nbsp;
                <?php echo $this->Paginator->hasPage(2) ? $this->Paginator->prev(__d('pakage','Prev')) : '';?>&nbsp;
		<?php echo $this->Paginator->numbers();?>&nbsp;
		<?php echo $this->Paginator->hasPage(2) ?  $this->Paginator->next(__d('pakage','Next')) : '';?>&nbsp;
		<?php echo $this->Paginator->last(__d('pakage','Last'));?>
                </div>
                <?php else: ?>
                    <?php echo __d('pakage','No item found') ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
