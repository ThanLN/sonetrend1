<div class="box2">
    <h3><?php echo __d('pakage', 'Filter'); ?></h3>
    <div class="box_content">
        <div id="seach_content">
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-filter" data-target="pagado">Pagado</button>
                    <button type="button" class="btn btn-warning btn-filter" data-target="pendiente">Pendiente</button>
                    <button type="button" class="btn btn-danger btn-filter" data-target="cancelado">Cancelado</button>
                    <button type="button" class="btn btn-default btn-filter" data-target="all">Todos</button>
                </div>
            </div>
        </div>
    </div>
</div>

