<?php if(Configure::read('Pakage.pakage_enabled') == 1): ?>
<?php 
echo $this->Html->css(array('Job.flexslider'), null, array('inline' => false));
$jobHelper = MooCore::getInstance()->getHelper('Job_Job'); 
if(empty($title)) $title = __d('job', 'Featured Jobs') ;
    if(empty($num_item_show)) $num_item_show = 10;
    if(isset($title_enable)&&($title_enable)=== "") $title_enable = false; else $title_enable = true;
?>
<?php $this->Html->scriptStart(array('inline' => false, 'domReady' => false, 'requires' => array('jquery', 'mooJob'), 'object' => array('$', 'mooJob'))); ?>	
        $(window).load(function() {
            var flex_width = $('.featured_job').width();
            flex_width = Math.floor(flex_width / (Math.floor(flex_width / 175)));
            $('.featured_job').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: flex_width,
                controlNav: false
            });
        });
<?php $this->Html->scriptEnd(); ?>

<?php if(!empty($featured_jobs)): ?>
    <div class="box2" id="featured_jobs">
        <?php if($title_enable): ?>
            <h3><?php echo h($title) ?></h3>
        <?php endif; ?>
        <div class="flexslider featured_job">
            <ul class="slides">
                <?php foreach($featured_jobs as $job): ?>                
                    <li class="featured_job_item">
                        <div class="job_company_photo">
                        <a href="<?php echo $job['JobJob']['moo_href']; ?>" title="<?php echo h($job['JobJob']['name']) ?>">
                            <span class="featured_job_item_photo" style="background-image:url(<?php echo $jobHelper->getCompanyImage($job, array('prefix' => '150_square')) ?>);"></span>
                        </a> 
                        </div>
                        <div class="job_name">
                            <a href="<?php echo $job['JobJob']['moo_href']; ?>" title="<?php echo h($job['JobJob']['name']) ?>">
                                <?php echo h($job['JobJob']['name']) ?>
                            </a>
                        </div>
                        <div class="company_name">
                            <a href="<?php echo $job['JobCompany']['moo_href'] ?>" title="<?php echo h($job['JobCompany']['name']) ?>">
                                <?php echo h($job['JobCompany']['name']); ?>
                            </a>
                        </div>
                        <div class="job_location">
                            <i class="material-icons">location_on</i>
                            <?php echo $job['JobJob']['location'] ?>
                        </div>
                        <div class="job_level">
                            <i class="material-icons">work</i>
                            <?php echo $jobHelper->getJobLevelName($job['JobLevel']['id']); ?>
                        </div>
                        <div class="featured_job_item_footer">
                            <div class="job_salary">
                                <?php 
                                    if($job['JobJob']['salary_per_hour'] > 0) {
                                        echo $jobHelper->getSymbolCurrencyCode($currency['Currency']['currency_code']).floatval($job['JobJob']['salary_per_hour']) .' '. __d('job', 'per'). ' '. __d('job', 'hour'); 
                                    }else{
                                        echo  __d('job', 'Negotiable');
                                    }
                                ?>
                            </div>
                            <div class="job_type">
                                <?php echo $jobHelper->getJobTypeName($job['JobType']['id']); ?>
                            </div>
                        </div>
                        <div class="featured_job_item_hover">
                            <h5><?php echo __d('job', 'Job Description') ?></h5>
                            <div class="job_description">
                                <?php echo $job['JobJob']['description']; ?>
                            </div>
                            <div class="featured_job_item_footer">
                                <div class="job_view">
                                    <a href="<?php echo $job['JobJob']['moo_href'];  ?>">
                                        <i class="material-icons">search</i> <?php echo __d('job', 'View'); ?>
                                    </a> 
                                </div>
                            </div>
                        </div>
                        </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <?php endif; ?>
<?php endif;