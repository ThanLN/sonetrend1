<?php
require_once(APP . DS . 'Plugin' . DS . 'Pakage' . DS .'Config' . DS . 'constants.php');
if(Configure::read('Pakage.pakage_enabled')){
    App::uses('PakageListener', 'Pakage.Lib');
    CakeEventManager::instance()->attach(new PakageListener());
    MooSeo::getInstance()->addSitemapEntity("Pakage", array(
        'pakage'
    ));
}