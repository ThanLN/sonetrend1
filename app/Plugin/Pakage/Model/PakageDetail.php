<?php

App::uses('PakageAppModel', 'Pakage.Model');

class PakageDetail extends PakageAppModel {

    public $actsAs = array();
    public $belongsTo = array('User' => array('className' => 'User', 'foreignKey' => 'user_id'), 'DefaultPakage' => array('className' => 'Pakage', 'foreignKey' => 'pakage_id'));
//    public $mooFields = array('title','href','plugin','type','url','thumb','privacy');

    public $hasMany = array();
    public $order = 'PakageDetail.id desc';
    public $validate = array(
        'pakage_id' => array(
            'rule' => 'notBlank',
            'message' => 'price is required',
        ),
        'user_id' => array(
            'rule' => 'notBlank',
            'message' => 'User is required',
        ),
        'price' => array(
            'rule' => 'notBlank',
            'message' => 'price is required',
        ),
        'name' => array(
            'rule' => 'notBlank',
            'message' => 'Name is required',
        ),
        'max_viewer' => array(
            'rule' => 'notBlank',
            'message' => 'Max_viewer is required',
        ),
        'max_time' => array(
            'rule' => 'notBlank',
            'message' => 'max_time is required',
        ),
        'default_view' => array(
            'rule' => 'notBlank',
            'message' => 'default_view is required',
        ),
        'default_live' => array(
            'rule' => 'notBlank',
            'message' => 'default_live is required',
        ),
        'default_like' => array(
            'rule' => 'notBlank',
            'message' => 'default_like is required',
        ),
        'default_share' => array(
            'rule' => 'notBlank',
            'message' => 'default_share is required',
        ),
        'default_comment' => array(
            'rule' => 'notBlank',
            'message' => 'default_comment is required',
        ),
        'active' => array(
            'rule' => 'notBlank',
            'message' => 'active is required',
        ),
    );

    public function getActive($row) {
        if (isset($row['active'])) {
            return $row['active'];
        }
        return false;
    }

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    public function validateBeforCreate($data) {
        $pakageModel = MooCore::getInstance()->getModel("Pakage.Pakage");
        $pass = true;
        $message = NULL;
        if (empty($data['pakage_id'] || !is_numeric($data['price_id']))) {
            $pass = false;
            $message = __d('pakage', 'Pakage is not empty or is not numberic');
        }
        $pakage = $pakageModel->getPakage('first', array('id' => $data['pakage_id'], 'active' => 1));
        if (!$pakage) {
            $pass = false;
            $message = __d('pakage', 'Pakage is not active or');
        }
        if (empty($data['user_id']) || !is_numeric($data['user_id'])) {
            $pass = false;
            $message = __d('pakage', 'User is not empty or is not numberic');
        }
        if (empty($data['price']) || !is_numeric($data['price'])) {
            $pass = false;
            $message = __d('pakage', 'price is not empty or is not numberic');
        }
        if ($data['price'] < $pakage['Pakage']['default_price']) {
            $pass = false;
            $message = __d('pakage', 'price is bigger than %s', $pakage['Pakage']['default_price']);
        }
        if ($data['active'] != '0' && $data['active'] != '1') {
            $pass = false;
            $message = __d('pakage', 'active is not empty or is not 0/1');
        }
        $result['pass'] = $pass;
        $result['message'] = $message;
        return $result;
    }

    public function prepareData($data) {
        $pakageModel = MooCore::getInstance()->getModel("Pakage.Pakage");
        $pakage = $pakageModel->getPakage('first', array('id' => $data['pakage_id'], 'active' => 1));
        $result = array();
        $result['user_id'] = $data['user_id'];
        $result['pakage_id'] = $data['pakage_id'];
        $result['name'] = $data['name'];
        $time_start = strtotime($data['time_start']);
        $result['time_start'] = $time_start;
        $sum = ($data['pakage_type'] == PAKAGE_TYPE_WEEK) ? PAKAGE_WEEK_DAY * 24 * 60 * 60 : PAKAGE_MONTH_DAY * 24 * 60 * 60;
        $result['time_end'] = $time_start + $sum;
        $result['active'] = $data['active'];
        $result['price'] = $data['price'];
        $result['num_time'] = intval($pakage['Pakage']['num_hours']);
        $result['hour_exist'] = intval($pakage['Pakage']['num_hours']);
        $result['minute_exist'] = intval($pakage['Pakage']['num_hours']) * 60;
        $result['second_exist'] = intval($pakage['Pakage']['num_hours']) * 60 * 60;
        $result['is_payment'] = $data['is_payment'];
        $return['PakageDetail'] = $result;
        return $return;
    }

    public function getPakageDetail($status, $cond) {
        return $this->find($status, array('conditions' => $cond));
    }

    public function getPakageDetailIdNameByUser($user_id, $is_not_use_pakage = false) {
        $cond['PakageDetail.user_id'] = $user_id;
        $cond['PakageDetail.active'] = 1;
        $data = $this->getPakageDetail('all', $cond);
        $result = array();
        foreach ($data as $pakage) {
            $result[$pakage['PakageDetail']['id']] = $pakage['PakageDetail']['name'];
        }
        if ($is_not_use_pakage) {
            $result[0] = __d('livestream', 'I dont use pakage');
        }
        return $result;
    }

    public function writeLogPakageDetail($data) {
        $pakage = $this->findById($data['pakage_id']);
        if ($pakage) {
            $data_save['id'] = $pakage['PakageDetail']['id'];
            $parrams = array();
            if (empty($pakage['PakageDetail']['parrams'])) {
                $data_save['hour_exist'] = floatval($pakage['PakageDetail']['hour_exist']) - (floatval($data['time_need_viewer']) / 60);
                $data_save['minute_exist'] = floatval($pakage['PakageDetail']['minute_exist']) - floatval($data['time_need_viewer']);
                $data_save['second_exist'] = floatval($pakage['PakageDetail']['second_exist']) - (floatval($data['time_need_viewer']) * 60);
            } else {
                $tmp_json = json_decode($pakage['PakageDetail']['parrams']);
                $num_refund = 0;
                foreach ($tmp_json as $key => $value) {
                    if ($key == $data['id']) {
                        $num_refund = floatval($value);
                    }
                    $parrams[$key] = $value;
                }
                $data_save['hour_exist'] = floatval($pakage['PakageDetail']['hour_exist']) + ($num_refund / 60) - (floatval($data['time_need_viewer']) / 60);
                $data_save['minute_exist'] = floatval($pakage['PakageDetail']['minute_exist']) + $num_refund - floatval($data['time_need_viewer']);
                $data_save['second_exist'] = floatval($pakage['PakageDetail']['second_exist']) + ($num_refund * 60) - (floatval($data['time_need_viewer']) * 60);
            }
            $parrams[$data['id']] = $data['time_need_viewer'];
            $data_save['parrams'] = json_encode($parrams);
            $this->save($data_save);
        }
    }
    public function countPricePayment($user_id, $time_start = false, $time_end = false){
        $cond = array();
        $cond['PakageDetail.user_id'] = $user_id;
        if($time_start){
            $cond['PakageDetail.created >'] = $time_start;
        }
        if($time_end){
            $cond['PakageDetail.created <'] = $time_end;
        }
        $pakages = $this->getPakageDetail('all', $cond);
        $price = 0;
        foreach ($pakages as $pakage){
            $price += floatval($pakage['PakageDetail']['price']);
        }
        return $price;
    }
}
