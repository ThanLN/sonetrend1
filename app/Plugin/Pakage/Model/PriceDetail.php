<?php

App::uses('PakageAppModel', 'Pakage.Model');

class PriceDetail extends PakageAppModel {

    public $actsAs = array();
    public $belongsTo = array('User' => array('className' => 'User','foreignKey' => 'user_id'),'DefaultPrice' => array('className' => 'Price','foreignKey' => 'price_id'));
//    public $mooFields = array('title','href','plugin','type','url','thumb','privacy');

    public $hasMany = array();
    public $order = 'PriceDetail.id desc';
    public $validate = array(
        'price_id' => array(
            'rule' => 'notBlank',
            'message' => 'price is required',
        ),
        'user_id' => array(
            'rule' => 'notBlank',
            'message' => 'User is required',
        ),
        'name' => array(
            'rule' => 'notBlank',
            'message' => 'Name is required',
        ),
        'max_viewer' => array(
            'rule' => 'notBlank',
            'message' => 'Max_viewer is required',
        ),
        'max_time' => array(
            'rule' => 'notBlank',
            'message' => 'max_time is required',
        ),
        'default_view' => array(
            'rule' => 'notBlank',
            'message' => 'default_view is required',
        ),
        'default_live' => array(
            'rule' => 'notBlank',
            'message' => 'default_live is required',
        ),
        'default_like' => array(
            'rule' => 'notBlank',
            'message' => 'default_like is required',
        ),
        'default_share' => array(
            'rule' => 'notBlank',
            'message' => 'default_share is required',
        ),
        'default_comment' => array(
            'rule' => 'notBlank',
            'message' => 'default_comment is required',
        ),
        'active' => array(
            'rule' => 'notBlank',
            'message' => 'active is required',
        ),
    );

    public function getActive($row) {
        if (isset($row['active'])) {
            return $row['active'];
        }
        return false;
    }
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }
    public function validateBeforCreate($data){
        $priceModel = MooCore::getInstance()->getModel("Pakage.Price");
        $sysPrice = $priceModel->getSystemPrice();
        $pass = true;
        $message = NULL;
        if(empty($data['price_id'] || !is_numeric($data['price_id']))){
            $pass = false;
            $message = __d('pakage','Pakage is not empty or is not numberic');
        }
        if(empty($data['user_id']) || !is_numeric($data['user_id'])){
            $pass = false;
            $message = __d('pakage','User is not empty or is not numberic');
        }
        if(empty($data['max_viewer']) || !is_numeric($data['max_viewer'])){
            $pass = false;
            $message = __d('pakage','max_viewer is not empty or is not numberic');
        }
        if($data['max_viewer'] > $sysPrice['Price']['max_viewer']){
            $pass = false;
            $message = __d('pakage','max_viewer is bigger than %s',$sysPrice['Price']['max_viewer']);
        }
        if(empty($data['max_time']) || !is_numeric($data['max_time'])){
            $pass = false;
            $message = __d('pakage','max_time is not empty or is not numberic');
        }
        if($data['max_time'] > $sysPrice['Price']['max_time']){
            $pass = false;
            $message = __d('pakage','max_time is bigger than %s',$sysPrice['Price']['max_time']);
        }
        if(empty($data['price_view'])|| !is_numeric($data['price_view'])){
            $pass = false;
            $message = __d('pakage','price_view is not empty or is not numberic');
        }
        if($data['price_view'] < $sysPrice['Price']['default_view']){
            $pass = false;
            $message = __d('pakage','price_view is less than %s',$sysPrice['Price']['default_view']);
        }
        if(empty($data['price_live'])){
            $pass = false;
            $message = __d('pakage','price_live is not empty');
        }
        if(!is_array(json_decode($data['price_live']))){
             $pass = false;
            $message = __d('pakage','price_live is not json');
        }
         if(empty($data['price_like'])|| !is_numeric($data['price_like'])){
            $pass = false;
            $message = __d('pakage','price_like is not empty or is not numberic');
        }
        if($data['price_like'] < $sysPrice['Price']['default_like']){
            $pass = false;
            $message = __d('pakage','price_like is less than %s',$sysPrice['Price']['default_like']);
        }
        //share
         if(empty($data['price_share'])|| !is_numeric($data['price_share'])){
            $pass = false;
            $message = __d('pakage','price_share is not empty or is not numberic');
        }
        if($data['price_share'] < $sysPrice['Price']['default_share']){
            $pass = false;
            $message = __d('pakage','price_share is less than %s',$sysPrice['Price']['default_share']);
        }
        //comment
         if(empty($data['price_comment'])|| !is_numeric($data['price_comment'])){
            $pass = false;
            $message = __d('pakage','price_comment is not empty or is not numberic');
        }
        if($data['price_comment'] < $sysPrice['Price']['default_comment']){
            $pass = false;
            $message = __d('pakage','price_comment is less than %s',$sysPrice['Price']['default_comment']);
        }
        //
         if(empty($data['active'])|| !in_array($data['active'],array('0','1'))){
            $pass = false;
            $message = __d('pakage','price_view is not empty or is not 0/1');
        }
        $result['pass'] = $pass;
        $result['message'] = $message;
        return $result;
    }
     public function getPriceDetail($status,$cond){
        return $this->find($status, array('conditions' => $cond));
    }
    public function getPriceDetailByUserId($user_id){
        $cond['PriceDetail.user_id'] = $user_id;
        $cond['PriceDetail.active'] = 1;
        return $this->getPriceDetail('first', $cond);
    }
    
}
