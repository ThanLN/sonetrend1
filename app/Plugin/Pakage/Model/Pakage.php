<?php

App::uses('PakageAppModel', 'Pakage.Model');

class Pakage extends PakageAppModel {

    public $actsAs = array();
//    public $belongsTo = array( 'User','Category');
//    public $mooFields = array('title','href','plugin','type','url','thumb','privacy');

    public $hasMany = array();
    public $order = 'Pakage.id desc';
    public $validate = array(
        'name' => array(
            'rule' => 'notBlank',
            'message' => 'Name is required',
        ),
        'max_viewer' => array(
            'rule' => 'notBlank',
            'message' => 'Max_viewer is required',
        ),
        'max_time' => array(
            'rule' => 'notBlank',
            'message' => 'max_time is required',
        ),
        'default_view' => array(
            'rule' => 'notBlank',
            'message' => 'default_view is required',
        ),
        'default_live' => array(
            'rule' => 'notBlank',
            'message' => 'default_live is required',
        ),
        'default_like' => array(
            'rule' => 'notBlank',
            'message' => 'default_like is required',
        ),
        'default_share' => array(
            'rule' => 'notBlank',
            'message' => 'default_share is required',
        ),
        'default_comment' => array(
            'rule' => 'notBlank',
            'message' => 'default_comment is required',
        ),
        'active' => array(
            'rule' => 'notBlank',
            'message' => 'active is required',
        ),
    );

    public function getActive($row) {
        if (isset($row['active'])) {
            return $row['active'];
        }
        return false;
    }

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    public function getPakage($status, $cond) {
        return $this->find($status, array('conditions' => $cond));
    }

    public function getSystemPrice() {
        return $this->find('first', array('conditions' => array('active' => 1)));
    }

    public function getIdNamePakages() {
        $data = $this->find('all', array('conditions' => array('active' => 1)));
        $result = array();
        foreach ($data as $pakage) {
            $result[$pakage['Pakage']['id']] = $pakage['Pakage']['name'];
        }
        return $result;
    }


}
