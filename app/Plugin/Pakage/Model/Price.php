<?php

App::uses('PakageAppModel', 'Pakage.Model');

class Price extends PakageAppModel {

    public $actsAs = array();
//    public $belongsTo = array( 'User','Category');
//    public $mooFields = array('title','href','plugin','type','url','thumb','privacy');

    public $hasMany = array();
    public $order = 'Price.id desc';
    public $validate = array(
        'name' => array(
            'rule' => 'notBlank',
            'message' => 'Name is required',
        ),
        'max_viewer' => array(
            'rule' => 'notBlank',
            'message' => 'Max_viewer is required',
        ),
        'max_time' => array(
            'rule' => 'notBlank',
            'message' => 'max_time is required',
        ),
        'default_view' => array(
            'rule' => 'notBlank',
            'message' => 'default_view is required',
        ),
        'default_live' => array(
            'rule' => 'notBlank',
            'message' => 'default_live is required',
        ),
        'default_like' => array(
            'rule' => 'notBlank',
            'message' => 'default_like is required',
        ),
        'default_share' => array(
            'rule' => 'notBlank',
            'message' => 'default_share is required',
        ),
        'default_comment' => array(
            'rule' => 'notBlank',
            'message' => 'default_comment is required',
        ),
        'active' => array(
            'rule' => 'notBlank',
            'message' => 'active is required',
        ),
    );

    public function getActive($row) {
        if (isset($row['active'])) {
            return $row['active'];
        }
        return false;
    }

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    public function getSystemPrice() {
        return $this->find('first', array('conditions' => array('active' => 1)));
    }

    public function getArrayAmountByUser($user_id, $pakage_id, $limit =0) {
        $pakageDetailModel = MooCore::getInstance()->getModel("Pakage.PakageDetail");
        $priceDetailModel = MooCore::getInstance()->getModel("Pakage.PriceDetail");
        $result = array();
        //get pakage detail
        if ($pakage_id) {
            $cond['PakageDetail.id'] = $pakage_id;
            $cond['PakageDetail.active'] = 1;
            $cond['PakageDetail.user_id'] = $user_id;
            $pakageDetail = $pakageDetailModel->getPakageDetail('first', $cond);
            if (!$pakageDetail)
                return false;
            $amount = $pakageDetail['DefaultPakage']['max_viewer'];
        }else {
            $cond['PriceDetail.user_id'] = $user_id;
            $cond['PriceDetail.active'] = 1;
            $priceDetail = $priceDetailModel->getPriceDetail('first', $cond);
            if ($priceDetail) {
                $amount = $priceDetail['PriceDetail']['max_viewer'];
            } else {
                $priceDefault = $this->getSystemPrice();
                $amount = $priceDefault['Price']['max_viewer'];
            }
        }
        $num = intval($amount) / 100;
        for ($i = 1; $i <= $num; $i++) {
            if($limit <= $i* 100){
            $result[$i * 100] = $i * 100;
            }
        }
        return $result;
    }

    public function getArrayTimeneedviewerByUser($user_id, $pakage_id, $limit = 0) {
        $pakageDetailModel = MooCore::getInstance()->getModel("Pakage.PakageDetail");
        $priceDetailModel = MooCore::getInstance()->getModel("Pakage.PriceDetail");
        $result = array();
        $priceDefault = $this->getSystemPrice();
        $amount = intval($priceDefault['Price']['max_time']);
        //get pakage detail
        if ($pakage_id) {
            $cond['PakageDetail.id'] = $pakage_id;
            $cond['PakageDetail.active'] = 1;
            $cond['PakageDetail.user_id'] = $user_id;
            $pakageDetail = $pakageDetailModel->getPakageDetail('first', $cond);
            if (!$pakageDetail)
                return false;
            $amount = intval($pakageDetail['PakageDetail']['hour_exist']) < intval($priceDefault['Price']['max_time']) ? intval($pakageDetail['PakageDetail']['hour_exist']) : intval($priceDefault['Price']['max_time']);
        }else {
            $cond['PriceDetail.user_id'] = $user_id;
            $cond['PriceDetail.active'] = 1;
            $priceDetail = $priceDetailModel->getPriceDetail('first', $cond);
            if ($priceDetail) {
                $amount = intval($priceDetail['PriceDetail']['max_time']);
            }
        }
        $num = $amount * 2;
        for ($i = 1; $i <= $num; $i++) {
            if($i * 30 >= $limit)
            $result[$i * 30] = $this->convertToHoursMins($i * 30);
        }
        return $result;
    }

    public function convertToHoursMins($time, $format = '%02d hours %02d minutes') {
        if ($time <= 60) {
            return __d('pakage','%s minutes', $time);
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }
    
}
