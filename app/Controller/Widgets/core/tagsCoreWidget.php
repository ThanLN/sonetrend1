<?php
/*
 * Copyright (c) SocialLOFT LLC
 * mooSocial - The Web 2.0 Social Network Software
 * @website: http://www.moosocial.com
 * @author: mooSocial
 * @license: https://moosocial.com/license/
 */
App::uses('Widget','Controller/Widgets');

class tagsCoreWidget extends Widget {
    public function beforeRender(Controller $controller) {
        // APP_BLOG APP_ALBUM APP_TOPIC APP_VIDEO
      
        $this->setData('hashtags',array());

        $this->setData('tagsWidget',array());

        $this->setData('order',array());

    }
	public function getFunctionUnions($type)
    {
   		list($plugin, $modelClass) = mooPluginSplit($type);
		$function_name = 'getTagUnions'.str_replace('_','',$modelClass);
		return $function_name;
    }

    private function _getAllowTags($tags)
    {
        $aTags = Hash::combine($tags,'{n}.Tag.target_id','{n}.Tag.tag','{n}.Tag.type');
        $intersectTag = array();
        foreach($aTags as $type => &$tag)
        {
            if($type != 'Topic_Topic'){
                list($plugin, $modelClass) = mooPluginSplit($type);
                // Get all the items this user can view
                $allowTags = $this->_getAllowItems($plugin,$modelClass);
                $allTags = array_keys($tag);
                $intersectTag[$type] = array_intersect($allTags,$allowTags);
            }
        }
        foreach($tags as $key => &$tag){
            if(!empty($intersectTag) && $tag['Tag']['type'] != 'Topic_Topic' && !in_array($tag['Tag']['target_id'],$intersectTag[$tag['Tag']['type']]))
                unset($tags[$key]);
        }
        return $tags;
    }

    private function _getAllowItems($plugin, $modelClass)
    {
        $uid = $this->Auth->user('id');
        $sFriendsList = $this->_getFriendList($uid);
        if($plugin)
            $this->loadModel($plugin.'.'.$modelClass);
        else
            $this->loadModel($modelClass);
        $aData = $this->$modelClass->find('all',array(
            'conditions' => array(
                'OR' => array(
                    array(
                        $modelClass.'.privacy' => PRIVACY_EVERYONE,
                    ),
                    array(
                        $modelClass.'.user_id' => $uid
                    ),
                    array(
                        'Find_In_Set('.$modelClass.'.user_id,"'.$sFriendsList.'")',
                        $modelClass.'.privacy' => PRIVACY_FRIENDS
                    )
                ),
            ),
            'fields' => array('id')
        ));
        return Hash::extract($aData,'{n}.'.$modelClass.'.id');
    }

    private function _getFriendList($uid)
    {
        $sFriendsList = '';
        $aFriendListId =  0;
        $sFriendsList = implode(',',$aFriendListId);
        return $sFriendsList;
    }
}