<?php
/*
 * Copyright (c) SocialLOFT LLC
 * mooSocial - The Web 2.0 Social Network Software
 * @website: http://www.moosocial.com
 * @author: mooSocial
 * @license: https://moosocial.com/license/
 */
App::uses('Widget','Controller/Widgets');

class managePaymentsCoreWidget extends Widget {
    public function beforeRender(Controller $controller) {
        $controller->loadModel('Livestream.LivestreamOrder');
        $controller->loadModel('Pakage.PakageDetail');
        $controller->loadModel('Payment.PaymentInfo');
        
        $viewer = MooCore::getInstance()->getViewer();
        
        $price_order = $controller->LivestreamOrder->countPricePayment($viewer['User']['id']);
        $price_pakage = $controller->PakageDetail->countPricePayment($viewer['User']['id']);
        $payed = $controller->PaymentInfo->countPricePayment($viewer['User']['id']);
        $show = false;
        if(intval($price_order +  $price_pakage - $payed) >= intval(Configure::read('Payment.payment_price_owe_show'))){
            $show = true;
        }
        $this->setData('show',$show);
        $this->setData('viewer',$viewer);
        $this->setData('price_order',$price_order);
        $this->setData('price_pakage',$price_pakage);
        $this->setData('payed',$payed);
    }
}
